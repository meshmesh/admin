import { Component, OnInit, Inject, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatChipInputEvent } from '@angular/material';
import { SpecialityService } from '../speciality.service';
import { Specialiy } from '../spec';
import { Observable, of, ReplaySubject } from 'rxjs';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

import * as _ from 'lodash';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-addSpec',
  templateUrl: './addSpec.component.html',
  styleUrls: ['./addSpec.component.scss']
})
export class AddSpecComponent implements OnInit, OnDestroy {


  medicallineslist: any[] = [];
  localImages: File[] = [];
  imagePreview: any[] = [];
  spForm: Specialiy;
  imgurl: any = 'https://s3-us-west-2.amazonaws.com/aumet-data/icons/specialities/';
  testurl: any = 'https://aumet-data.s3.us-west-2.amazonaws.com/specialities/';
  selectedMedicalName: any[] = []
  selectedMedicalId: any[] = []
  Medicals: any[] = [];
  removable = true;
  keywordss: any[] = [];
  wordKey: any[] = [];
  speciality: number;
  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  apiData: any;
  onDestroy$ = new ReplaySubject<void>();

  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  constructor(
    public dialogRef: MatDialogRef<AddSpecComponent>,
    private http: HttpClient,
    private alertfy: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any, private serv: SpecialityService) { }







  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  ngOnInit() {
    this.spForm = new Specialiy();
    this.renderMedicalLine('a');
    

    this.serv.getSpecalities().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.specialitieslist = respone;
      this.specialitiesFilter = of(this.specialitieslist);
    });

    this.serv.getSpecality(this.data.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.apiData = res;
      this.spForm.ParentId = this.apiData.parentId;
      console.log(this.apiData);
    });



    if (this.data) {

      if (this.data.iconImg != null && (this.data.iconImg.includes(this.testurl) || this.data.iconImg.includes(this.imgurl))) {
        this.imagePreview[0] = this.data.iconImg;
        /* this.localImages = (this.data.iconImg); */

      }
      else {
        this.imagePreview[0] = this.imgurl + this.data.iconImg;
      }
      this.selectedMedicalName = _.cloneDeep(this.data.medicalLine) ? this.data.medicalLine : [];
      this.selectedMedicalName.forEach(element => {
        this.selectedMedicalId.push(element.id);
      });
      this.spForm.speciality = this.data.speciality;
      
      this.keywordss = this.data.keywords ? this.data.keywords : [];
    }

  }


  removeTag(i) {
    let temp = this.keywordss[i];
    this.wordKey.push(temp);
    this.keywordss.splice(i, 1);
  }

  addKeyWord(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;


    if ((value || '').trim()) {
      this.keywordss.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }


  async submit(obj) {
    if (obj.invalid)
      return;

    if (this.data.id) {
      this.spForm.id = this.data.id;
      this.spForm.createdAt = this.data.createdAt;
      this.spForm.updatedAt = new Date();

      this.spForm.ParentId = obj.ParentId;
      this.spForm.MedicalLineIds = this.selectedMedicalId;
      this.keywordss = this.data.keywords;
      if(this.keywordss !== null){
      this.keywordss.forEach(element => {
        if (element.value === 0) {
          this.spForm.KeywordsID.push(element.id);

        }
        else {
          this.spForm.Keywords.push(element);
        }
      });
    }
      if (this.localImages.length > 0) {
        const _thisProductImage = this;
        await this.uploadProductImage(this.localImages[0], function (data) {
          _thisProductImage.spForm.iconImg = data;

          _thisProductImage.addUpdateFunction(_thisProductImage.spForm);
          _thisProductImage.alertfy.success("Updated Successfully");
        }
        );
      } else {
        this.spForm.iconImg = this.imagePreview[0];
        this.addUpdateFunction(this.spForm);
        this.alertfy.success("Updated Successfully")
      }








    }
    else {

      this.spForm.MedicalLineIds = this.selectedMedicalId;
      this.spForm.createdAt = new Date();
      this.spForm.updatedAt = new Date();

      this.spForm.ParentId = obj.ParentId;

      this.keywordss.forEach(element => {
        if (element.value === 0) {
          this.spForm.KeywordsID.push(element.id);

        }
        else {
          this.spForm.Keywords.push(element);
        }
      });

      if (this.localImages.length > 0) {
        const _thisProductImage = this;
        await this.uploadProductImage(this.localImages[0], function (data) {


          _thisProductImage.spForm.iconImg = data;

          _thisProductImage.addUpdateFunction(_thisProductImage.spForm);
          _thisProductImage.alertfy.success("inserted Successfully ");
        }
        );
      }
      else {
        this.addUpdateFunction(this.spForm);
        this.alertfy.success("inserted Successfully ");
      }


    }
  }


  addUpdateFunction(form) {
    const _thisTimer = this;
    this.http.post(environment.ApiUrl + '/api/AdminSpeciality/AddUpdate', form).pipe(takeUntil(this.onDestroy$)).subscribe(res => {

      this.close("success");
      setTimeout(function () {

      }, 1500);

    });
  }

  close(data) {
    this.dialogRef.close(data);
  }

  handleBaseImageChange(e) {
    this.hideBaseImage();
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview = [];
        this.imagePreview.push(reader.result);
      };
    }
  }


  handleImageError(i) {
    this.imagePreview = [];
  }

  hideBaseImage() {
    this.imagePreview.splice(1);
    this.imagePreview = [];

    this.localImages = [];
  }


  handleBaseImgError() {
    this.imagePreview.pop();
  }


  removeMedicalsPoints(i) {
    let temp = this.selectedMedicalName[i];
    this.Medicals.push(temp);
    this.selectedMedicalName.splice(i, 1);
    this.selectedMedicalId.splice(i, 1);
  }

  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.speciality;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialitieslist.filter(item => item.speciality.toLowerCase().indexOf(filterValue) === 0));
  }
  AddSelectedMedicals(event) {
    const value = event.value;
    let index = this.Medicals.map(function (x) { return x.id }).indexOf(event.option.value.id);
    if (index === -1) {
      this.selectedMedicalName.push(event.option.value)
      this.selectedMedicalId.push(event.option.value.id);
      this.searchInput.nativeElement.value = '';

    }
    /*  else{
       this.selectedMedical.push(event.option.value);
       this.selectedMedicalId.push(event.option.value.id);
 
       this.searchInput.nativeElement.value = "";
     } */
  }


  handleTextChanging(event) {
    this.renderMedicalLine(event);
  }



  displayMedicalLine(medical) {
    return medical ? medical.name : undefined;
  }


  renderMedicalLine(searchvalue: string) {

    this.serv.getMedicalLines(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      
      this.medicallineslist = res;
      this.displayMedicalLine(res);
    });

  }

  uploadProductImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'specialities/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }



}
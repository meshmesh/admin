import { Guid } from "guid-typescript"

export class User {
    id: number
    firstName: String;
    lastName: String;
    email: string;
    phone: String;
    stage: String;
    isAdmin: boolean;
    companyId: number;
}


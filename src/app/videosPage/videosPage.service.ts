import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideosPageService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) {}

  getVideos(pageNumber) {
    return this.http.get(
      this.APIEndpoint +
        '/api/AumetVideos/Search?pagesize=10&pagenumber=' +
        pageNumber
    );
  }

  addVideo(form) {
    return this.http.post(this.APIEndpoint + '/api/AumetVideos/addvideo', form);
  }
  DeleteVideo(id) {
    return this.http.post(this.APIEndpoint + '/api/AumetVideos/Delete?id=' + id, {});
  }


  PublishVideo(id) {
    return this.http.post(this.APIEndpoint + '/api/AumetVideos/Publish?Id=' + id, {});
  }
  UnPublishVideo (id) {
    return this.http.post(this.APIEndpoint + '/api/AumetVideos/UnPublish?Id=' + id, {});
  }
}

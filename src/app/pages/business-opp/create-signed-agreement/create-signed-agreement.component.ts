import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  OnDestroy,
  AfterViewInit,
} from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from "@angular/material";
import { ReplaySubject, Observable, of } from "rxjs";
import { takeUntil, startWith, debounceTime, switchMap, catchError, map, tap, finalize } from "rxjs/operators";
import { BusinessService } from "../business.service";
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SACreate, ManuForSearch } from "../business.models";
import { SignedAgreementsService } from "../signed-agreements.service";
import { OptionView } from "src/app/shared/optionView";
import { ToastService } from "src/app/_services/toast.service";

@Component({
  selector: "app-create-signed-agreement",
  templateUrl: "./create-signed-agreement.component.html",
  styleUrls: ["./create-signed-agreement.component.scss"],
})
export class CreateSignedAgreementComponent implements OnInit, OnDestroy, AfterViewInit {


  agreementForm: FormGroup;

  manufacturerCtrl: FormControl = new FormControl();
  distributorCtrl: FormControl = new FormControl();
  countryCtrl: FormControl = new FormControl();
  // date: FormControl = new FormControl(null);
  // contractValue: FormControl = new FormControl(null);

  manufacturers$: Observable<any[]>;
  distributors$: Observable<any[]>;
  countries$: Observable<any[]>;
  countriesList: Observable<any[]>;

  Manufacturer: any;
  Distributor: any;

  isLoading: boolean = false;
  onDestroy$ = new ReplaySubject<void>();

  constructor(
    private fb: FormBuilder, 
    private toast: ToastService, 
    public dialogRef: MatDialogRef<CreateSignedAgreementComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: BusinessService,
    private sAservice: SignedAgreementsService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // this.dataSource.sort = this.sort;
  }



  ngOnInit() {
    this.createForm();
    this.doOnChanges();
  }


  get dateCtrl() {
    return this.agreementForm.get('date');
  }

  get contractValueCtrl() {
    return this.agreementForm.get('contractValue');
  }

  createForm() {
    this.agreementForm = this.fb.group({
      manufacturerID: new FormControl('', Validators.required),
      distributorID: new FormControl('', Validators.required),
      countryID: new FormControl('', Validators.required),
      date: new FormControl('' ,Validators.required),
      contractValue: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
    })
  }

  doOnChanges() {

    this.service.getCountries().subscribe((res: any) => {
      console.log(res, " countries")
      // this.countries$ = res;
      this.countriesList = res;
    })

    this.manufacturers$ = this.manufacturerCtrl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once

      // switchMap(value => this.service.getAllManu(value))
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return this._filterManufacturer(value);
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );

    this.distributors$ = this.distributorCtrl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once

      // switchMap(value => this.service.getAllManu(value))
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return this._filterDistributor(value);
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );



    this.countries$ = this.countryCtrl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once

      // switchMap(value => this.service.getAllManu(value))
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return of(this.countriesList.filter((item: any) => item.name.toLowerCase().indexOf(value) === 0));
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );
  }


  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }
  displayFnDistributor(distributor?: any): string | undefined {
    return distributor ? distributor.name : undefined;
  }
  displayFnCountry(country?: any): string | undefined {
    return country ? country.name : undefined;
  }

  _filterManufacturer(value: any): Observable<any> {
    if (typeof value === 'string') {
      return this.service.getAllManu(value.toLowerCase()).pipe(
        // map the item property of the github results as our return object
        // catch errors
        map(item => item.filter(manu => manu.manufacturerId !== 0)),
        catchError(_ => {
          return of(null);
        })
      );
    } else {
      return of(null);
    }

  }
  _filterDistributor(value: any): Observable<any[]> {
    if (typeof value === 'string') {
      return this.service.getAllDistributors(value.toLowerCase()).pipe(
        // map the item property of the github results as our return object
        // catch errors
        map(item => item.filter(dist => dist.id !== 0)),
        catchError(_ => {
          return of(null);
        })
      );
    } else {
      return of(null);
    }
  }


  onSubmit() {

    this.agreementForm.controls.manufacturerID.patchValue(this.manufacturerCtrl.value ? this.manufacturerCtrl.value.manufacturerId : null);
    this.agreementForm.controls.distributorID.patchValue(this.distributorCtrl.value ? this.distributorCtrl.value.id : null);
    this.agreementForm.controls.countryID.patchValue(this.countryCtrl.value ? this.countryCtrl.value.id : null);


    if(!this.agreementForm.valid) {
      this.toast.error("Please check all required fields");
      return;
    }


    let data: SACreate = {
      "manufactureId": this.agreementForm.value.manufacturerID,
      "distributorId": this.agreementForm.value.distributorID,
      "countryId": this.agreementForm.value.countryID,
      "signedDate": this.agreementForm.value.date,
      "status": 1,
      "contractValue": this.agreementForm.value.contractValue,
    }
    console.log('${data}', data)
    this.sAservice.add(data).subscribe((res: any) => {
      console.log(res)
      if(res.success) {
        this.dialogRef.close(true);
        this.toast.success("Success!");
      } else {
        this.dialogRef.close(false);
        this.toast.error("Something wrong happened");
      }
    }, err => {
       this.toast.error(err.error.message);
    })
  }

  onCancel() {
    this.dialogRef.close();
  }
  
  close(data) {
    this.dialogRef.close(data);
  }
}

export class SubProduct {
    id: number; 
    productTitle: string;
    productSubtitle: string;
    baseImage: string; 
    productDescription: string; 
    specialityId: number; 
    speciality:any
    slug: string;
    productrangeId: number;
    manufacturerId: number;
    createdAt: Date; 
    updatedAt: Date; 
    madeIn: number;
    aumetComment: string;
    published: boolean;
    seo: string; 
    SoldToIds: number[] = [];
    SellingToIds: number[] = [];
    ScientificIds: number[] = [];
    SEOOptimized: boolean;
    Ecommerce: boolean;
    CatalogURL: string; 
    Images:  string[] = []; 
    Attribute: any;
    Tags: any;
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmailTemplateService } from './emailTemplate.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { EditEmailComponent } from './editEmail/editEmail.component';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-emailTemplate',
  templateUrl: './emailTemplate.component.html',
  styleUrls: ['./emailTemplate.component.scss']
})
export class EmailTemplateComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private serv: EmailTemplateService, public dialog: MatDialog) { }
  length: number;
  pageIndex = 1;
  pageSize = 10
  emailSource: MatTableDataSource<EmailTemplateComponent>;
  displayedColumns: string[] = ['actions', 'title', 'keys', 'ccEmail', 'note']

  ngOnInit() {
  this.serv.getTemplates().pipe(takeUntil(this.onDestroy$)).subscribe(res=> {
    this.renderEmailsData(res);
  })
  }
  
  Edit(value){
    this.serv.getEmailbyID(value.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      
    })
  }

  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.serv.getTemplates().pipe(takeUntil(this.onDestroy$)).subscribe(res =>{ 
        this.renderEmailsData(res);
      });
  }

  renderEmailsData(data) {
    if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.length = data[0].totalRows;
    }
    this.emailSource = new MatTableDataSource<EmailTemplateComponent>(data);

    }

AddEmail(){
  const dialogRef = this.dialog.open(EditEmailComponent, {
    width: 'auto',
    height: 'auto',
  });

  dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
    console.log('The dialog was closed');

  });
}
EditEmail(row){
  const dialogRef = this.dialog.open(EditEmailComponent, {
    width: 'auto',
    height: 'auto',
    data: {row}
  });

  dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
    console.log('The dialog was closed');

  });
}

Del(value){
  this.serv.DeleteEmail(value.Id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
    
  })
  }
  


  }


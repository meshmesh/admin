

import { Component, OnInit, Inject, ViewChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatAutocompleteTrigger,
  MatDialog,
  MatAutocompleteSelectedEvent
} from '@angular/material';
import { trigger, transition, style, animate } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { NgxCarousel } from 'ngx-carousel';
import { ClipboardService } from 'ngx-clipboard';
import { HttpClient } from '@angular/common/http';
import { map, switchMap, debounceTime, take, tap, takeUntil } from 'rxjs/operators';
import { Observable, of, ReplaySubject } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';



import { UsersService } from 'src/app/pages/users/users.service';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';
import { ToastService } from 'src/app/_services/toast.service';
import { environment } from 'src/environments/environment';
import { FilesViewModel } from '../model/files-view.model';
import { ExistingModel } from '../model/existing.model';
import { DistributorService } from '../services/distributor.service';
import { ScientificNameService } from '../../scientific-name/services/scientific-name.service';
import { AuthService } from 'src/app/_services/auth.service';
import { AddUpdateUserComponent } from '../../users/addUpdate-user/addUpdate-user.component';
import { AddUpdateComponent } from '../../interest/addUpdate/addUpdate.component';

@Component({
  selector: 'app-distributor-details',
  templateUrl: './distributor-details.component.html',
  styleUrls: ['./distributor-details.component.scss'],

  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms ease-in-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ transform: 'translate(0)' }), animate('400ms ease-in-out', style({ opacity: 0 }))])
    ])
  ]
})

export class DistributorDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  public show = false;
  public buttonName: any = 'Show';
  openProfileLink : string = environment.OpenProfileLink;
  comment = '';
  Website = '';
  Domain = '';
  contractSource: MatTableDataSource<any>;
  filesDisplayedColumns: string[] = ['Id', 'Name', 'Type', 'Path'];
  displayedInterestColumns: string[] = ['Id', 'ScientificName', 'MedicalLine', 'Speciality', 'Action'];
  displayedExperienceColumns: string[] = ['Id', 'ScientificName', 'MedicalLine', 'Speciality', 'Action'];
  inputTarget: any;
  public show2 = false;
  public buttonName2: any = 'Show';
  desciption = [];
  debounceTime = environment.debounceTime;
  closing = false;
  fav = false;
  editAboutIcon = 'icofont-edit';
  ContactDataSource: any[] = [];
  profilelink = '';
  bolink = '';
  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  selectedSpecialitiesList: any[] = [];
  selectedSpecialitiesListid: any[] = [];
  medicalline: any[] = [];
  MedicallineFilter: Observable<any>;
  selectedMedicallineList: any[] = [];
  selectedMedicallineListid: any[] = [];
  manufacturers: any[] = [];
  filteredmanufacturers: Observable<any>;
  selectedmanufacturers: any[] = [];
  selectedmanufacturersid: any[] = [];
  selectedScientificNames: any[] = [];
  specialityForm: FormGroup;
  shareProfileForm: FormGroup;
  callForm: FormGroup;
  commentForm: FormGroup;
  callTypes: any = [];
  timeNow = new Date().getHours() + ':' + new Date().getMinutes();
  userName = this.auth.currentUserValue.firstName + ' ' + this.auth.currentUserValue.lastName;
  userId = this.auth.currentUserValue.id;
  seoData: any = {};
  distributorSeoData: any = {};
  finalSeoData = [];
  scientificNameSearch: FormControl = new FormControl('');
  scientificNameSuggestion$: Observable<any[]>;

  Durations = [{ name: '15 Days', value: 15 }, { name: '30 Days', value: 30 }, { name: '90 Days', value: 90 }];

  ngAfterViewInit(): void {
    this.dialogData.files.forEach(element => {
      if (!element.exists) {
        this.updatedfileslist.push(element);
      }
    });

    this.renderfilesview(this.updatedfileslist);
    this.renderExistingsview(this.dialogData.exisitingFiles);
    this.renderCallLogTable(this.dialogData.callLog);
  }

  get shareControl() {
    return this.shareProfileForm.controls;
  }
  get specialityControl() {
    return this.specialityForm.controls;
  }
  get commentControl() {
    return this.commentForm.controls;
  }

  displayedusresColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'currentStage', 'phone', 'Action'];

  callLogSource: MatTableDataSource<any>;
  @ViewChild('table', { read: MatSort }) sortTargeting: MatSort;
  @ViewChild('tableVacant', { read: MatSort }) sortVacant: MatSort;
  @ViewChild('tableExclusivity', { read: MatSort }) sortExclusivity: MatSort;
  @ViewChild('tableCallLog', { read: MatSort }) sortCallLog: MatSort;

  public carouselTileOne: NgxCarousel;
  selectionTargeting: SelectionModel<any>;
  selectionVacant: SelectionModel<any>;
  selectionExclusivity: SelectionModel<any>;
  selectionCallLog: SelectionModel<any>;

  @ViewChild('paginatorCallLog') paginatorCallLog: MatPaginator;

  @ViewChild('paginator') paginator: MatPaginator;
  public editor;
  public editorContent: string;
  public editorConfig = {
    placeholder: 'About Your Self'
  };

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  profitChartOption: any;

  filesSource: MatTableDataSource<FilesViewModel>;
  Existing: MatTableDataSource<ExistingModel>;

  @ViewChild('table', { read: MatSort }) sort: MatSort;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  @ViewChild('manufacturerInput') manufacturerInput: ElementRef<HTMLInputElement>;

  selection: SelectionModel<FilesViewModel>;
  Existselection: SelectionModel<ExistingModel>;

  editProfile = true;
  editProfileIcon = 'icofont-edit';

  editAbout = true;
  Pagenumber: number = 1;
  updatedfileslist: any[] = [];
  pageSize = 10;
  Interestlength: number;
  IntrestList: MatTableDataSource<any>;
  displayedInterstColumns: string[] = ['id', 'name', 'speciality', 'source', 'date', 'TargetedPrice', 'AnnualTarget', 'TargetedRegion',]
  constructor(
    public dialogRef: MatDialogRef<DistributorDetailsComponent>,
    private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private _clipboardService: ClipboardService,
    private auth: AuthService,
    private _distributor: DistributorService,
    private toast: ToastService,
    public dialog: MatDialog,
    private scientificNameService: ScientificNameService,
    private _userService: UsersService
  ) { }

  // OnTabForm(event) {
  //   setTimeout(() => {
  //     this.callLogSource.paginator = this.callLogSource.paginator
  //       ? this.callLogSource.paginator
  //       : this.paginatorCallLog;
  //     this.filesSource.paginator = this.filesSource.paginator ? this.filesSource.paginator : this.paginator;
  //   });
  //   switch (event.nextId) {
  //     case '1':
  //       this.loadInterest();
  //       break;
  //   }
  // }

  // loadInterest() {
  //   this._distributor.GetIntrest(this.dialogData.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
  //     this.renderDataSourceD(res.data[0].interests);
  //   });
  // }

  renderDataSourceD(data) {
    if (data.length < this.pageSize) {
      this.Interestlength = data.length;
    } else {
      this.Interestlength = data[0].totalNumber;
    }
    this.IntrestList = new MatTableDataSource<any>(data);
  }

  // InterestpageChange(event) {
  //   this.Pagenumber = event.pageIndex;
  //   this._distributor
  //     .GetIntrest(this.dialogData.id)
  //     .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
  //       this.renderDataSourceD(res.data[0].interests);
  //     });
  // }

  ShareProfile(data) {
    this.shareProfileForm.reset({
      search: null,
      manufacturers: '',
      contactName: this.userName,
      duration: '',
      messages: '',
      UserId: this.userId,
      CompanyId: ''
    });
  }

  onShareProfile() {
    this.shareProfileForm.patchValue({
      CompanyId: this.dialogData.id,
      manufacturers: this.selectedmanufacturersid
    });

    this._distributor.addShareProfile(this.shareProfileForm.value).pipe(takeUntil(this.onDestroy$)).subscribe((response: any) => {
      this.toast.success('Has been added successfully');
    });
    this.ShareProfile(this.shareProfileForm.value);
  }

  renderCallLogTable(data) {
    this.callLogSource = new MatTableDataSource<any>(data);
    this.callLogSource.paginator = this.paginatorCallLog;

    this.callLogSource.sort = this.sortCallLog;
    this.selectionCallLog = new SelectionModel<any>(true, []);
    this.callLogSource.sortingDataAccessor = (sortData, sortHeaderId) => {
      if (!sortData[sortHeaderId]) {
        return this.sortVacant.direction === 'asc' ? '3' : '1';
      }

      // tslint:disable-next-line:max-line-length
      return /^\d+$/.test(sortData[sortHeaderId])
        ? Number('2' + sortData[sortHeaderId])
        : '2' + sortData[sortHeaderId].toString().toLocaleLowerCase();
    };
  }

  ngOnInit() {

    debugger
    this.comment = this.dialogData.comment;
    this.Website = this.dialogData.website;
    this.Domain = this.dialogData.domain;
    this.onViewRequirement(this.dialogData);
    // this.createspecialityForm();
    // this.createCommentForm();
    this.scientificNameSuggestion$ = this._filterScientificNames();
    this.selectedScientificNames = this.loadScientificNames();
    this.carouselTileOne = {
      grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
      speed: 600,
      interval: 3000,
      point: {
        visible: true,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 45%;
            background: #6b6b6b;
            padding: 5px;
            margin: 0 3px;
            transition: .4s;
          }
          .ngxcarouselPoint li.active {
              border: 2px solid rgba(0, 0, 0, 0.55);
              transform: scale(1.2);
              background: transparent;
            }
        `
      },
      loop: true,
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };
  }

  // savedescription() {
  //   const obj = {
  //     Id: this.dialogData.id,
  //     Items: [
  //       {
  //         Key: 'description',
  //         Value: this.dialogData.desciption
  //       }
  //     ]
  //   };
  //   this._distributor.Update(obj).pipe(takeUntil(this.onDestroy$)).subscribe((response: any) => { });
  // }

  loadmanufacturer() {
    this.shareControl.manufacturers.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(data => {
      if (data.name !== undefined) {
        this.filteredmanufacturers = this._distributor.getListOfManufacturer(data.name);
      } else if (data !== '') {
        this.filteredmanufacturers = this._distributor.getListOfManufacturer(data);
      }
    });
  }

  renderfilesview(data) {
    this.filesSource = new MatTableDataSource<FilesViewModel>(data);
    this.filesSource.paginator = this.paginator;
    this.filesSource.sort = this.sort;
    this.selection = new SelectionModel<FilesViewModel>(true, []);
    this.filesSource.sortingDataAccessor = (sortData, sortHeaderId) => {
      if (!sortData[sortHeaderId]) {
        return this.sort.direction === 'asc' ? '3' : '1';
      }

      // tslint:disable-next-line:max-line-length
      return /^\d+$/.test(sortData[sortHeaderId])
        ? Number('2' + sortData[sortHeaderId])
        : '2' + sortData[sortHeaderId].toString().toLocaleLowerCase();
    };
  }

  renderExistingsview(data) {
    this.Existing = new MatTableDataSource<FilesViewModel>(data);
    this.Existing.paginator = this.paginator;
    this.Existing.sort = this.sort;
    this.selection = new SelectionModel<FilesViewModel>(true, []);
    this.Existing.sortingDataAccessor = (sortData, sortHeaderId) => {
      if (!sortData[sortHeaderId]) {
        return this.sort.direction === 'asc' ? '3' : '1';
      }

      // tslint:disable-next-line:max-line-length
      return /^\d+$/.test(sortData[sortHeaderId])
        ? Number('2' + sortData[sortHeaderId])
        : '2' + sortData[sortHeaderId].toString().toLocaleLowerCase();
    };
  }

  @ViewChild(MatAutocompleteTrigger) inputAutoComplit: MatAutocompleteTrigger;

  AddseletedManufacturer(event) {
    const exist = this.selectedmanufacturers
      .map(function (x) {
        return x.id;
      })
      .indexOf(event.option.value.id);
    if (exist === -1) {
      this.selectedmanufacturers.push(event.option.value);
      this.selectedmanufacturersid.push(event.option.value.id);
    }

    const index = this.selectedmanufacturers
      .map(function (x) {
        return x.id;
      })
      .indexOf(event.option.value.id);
    if (index > -1) {
      this.manufacturers.splice(index, 1);
      this.manufacturerInput.nativeElement.value = '';
      this.shareControl.manufacturers.patchValue('');
    }
  }

  removemanufacturer(i) {
    const temp = this.selectedmanufacturers[i];
    this.manufacturers.push(temp);
    this.selectedmanufacturers.splice(i, 1);
    this.selectedmanufacturersid.splice(i, 1);
  }

  removeScientificName(scientificName): void {
    this.selectedScientificNames = this.selectedScientificNames.filter(name => name.id !== scientificName.id);
    this.scientificNameSearch.reset();
  }

  AddselectedSpeciality(event) {
    this.selectedSpecialitiesList.push(event.option.value);
    this.selectedSpecialitiesListid.push(event.option.value.id);
    const index = this.specialitieslist.indexOf(event.option.value);
    if (index > -1) {
      this.specialitieslist.splice(index, 1);
      this.searchInput.nativeElement.value = '';
      this.specialityControl.speciality.patchValue('');
    }
  }

  addSelectedScientificName(event: MatAutocompleteSelectedEvent): void {
    this.selectedScientificNames.push(event.option.value);
    this.scientificNameSearch.reset();
  }

  AddselectedMedicalline(event) {
    this.selectedMedicallineList.push(event.option.value);
    this.selectedMedicallineListid.push(event.option.value.id);
    const index = this.medicalline.indexOf(event.option.value);
    if (index > -1) {
      this.medicalline.splice(index, 1);
      this.specialityControl.speciality.patchValue('');
    }
  }

  removeSpeciality(i) {
    const temp = this.selectedSpecialitiesList[i];
    this.specialitieslist.push(temp);
    this.selectedSpecialitiesList.splice(i, 1);
    this.selectedSpecialitiesListid.splice(i, 1);
  }

  removeMedicalline(i) {
    const temp = this.selectedMedicallineList[i];
    this.medicalline.push(temp);
    this.selectedMedicallineList.splice(i, 1);
    this.selectedMedicallineListid.splice(i, 1);
  }

  _filterSpeciality(value: any): any[] {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.speciality;
    } else {
      filterValue = value.toLowerCase();
    }
    return this.specialitieslist.filter(data => data.speciality.toLowerCase().indexOf(filterValue) === 0);
  }

  _filterMedicalline(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.medicalline.filter(data => data.name.toLowerCase().indexOf(filterValue) === 0));
  }

  _filterScientificNames(): Observable<any> {
    return this.scientificNameSearch.valueChanges.pipe(
      debounceTime(this.debounceTime),
      switchMap((value: string) => {
        if (value && value.length > 0) {
          return this.scientificNameService.getScientificNameSuggestion(value);
        } else {
          return of([]);
        }
      }),
      map((names: any[]) => {
        const selectedIds = this.selectedScientificNames.map(e => e.id);

        return names.filter(e => !selectedIds.includes(e.id));
      })
    );
  }

  loadScientificNames(): any[] {
    return this.dialogData.scientificName;
  }

  loadMedicallineData() {
    this._distributor.getmedicalline().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.medicalline = respone;
      this.MedicallineFilter = of(this.medicalline);
    });
  }

  onDeleteManuf(Id) {
    this._distributor.deleteDistributor(Id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.close(5);
    });
  }

  // openpublicview(slug) {
  //   window.open("https://aumet.me/profile/" + slug, "_blank");
  // }

  // onViewProfile(token, x) {
  //   const obj = { CompanyToken: token, CompanyId: x, CompanyType: 'distributor', RedirectType: 1 };
  //   this._distributor.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // onViewProfileQalifications(token, x) {
  //   const obj = { CompanyToken: token, CompanyId: x, CompanyType: 'distributor', RedirectType: 2 };
  //   this._distributor.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // OpenDemo(token, x) {
  //   const obj = { CompanyToken: token, CompanyId: x, CompanyType: 'distributor', RedirectType: 2 };
  //   this._distributor.DemoQualifications(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // copyLink(token, manuId) {
  //   const obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'distributor', RedirectType: 1 };
  //   this._distributor.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     this._clipboardService.copyFromContent(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // updateScientificNames(): void {
  //   const ids = this.selectedScientificNames.map(e => e.id);
  //   this._distributor
  //     .updateScientificName(ids, this.dialogData.token)
  //     .pipe(
  //       take(1),
  //       tap(() => {
  //         this.toast.success('Successfully Updated');
  //         this.renderInfo();
  //       })
  //     )
  //     .pipe(takeUntil(this.onDestroy$)).subscribe();
  // }

  toggleEditProfile() {
    this.editProfileIcon = this.editProfileIcon === 'icofont-close' ? 'icofont-edit' : 'icofont-close';
    this.editProfile = !this.editProfile;
  }

  toggleEditAbout() {
    this.editAboutIcon = this.editAboutIcon === 'icofont-close' ? 'icofont-edit' : 'icofont-close';
    this.editAbout = !this.editAbout;
  }

  onEditorBlured(quill) { }

  onEditorFocused(quill) { }

  onEditorCreated(quill) {
    this.editor = quill;
  }

  onContentChanged({ quill, html, text }) { }

  close(data): void {
    this.dialogRef.close(data);
  }

  getLogo(logo) {
    if (logo) {
      if (logo.includes('https')) {
        return logo;
      } else {
        return 'assets/images/distributor-logo-icn.svg';
      }
    } else {
      return 'assets/images/distributor-logo-icn.svg';
    }
  }

  getSpecialityLogo(logo) {
    if (logo) {
      if (logo.includes('https://s3-us-west-2.amazonaws.com/aumet-data/icons/specialities')) {
        return logo;
      } else {
        return 'https://s3-us-west-2.amazonaws.com/aumet-data/icons/specialities/' + logo;
      }
    }
  }

  toggle() {
    this.show = !this.show;
    // CHANGE THE NAME OF THE BUTTON.
    if (this.show) {
      this.buttonName = 'Hide';
    } else {
      this.buttonName = 'Show';
    }
  }

  toggle2() {
    this.show2 = !this.show2;
    // CHANGE THE NAME OF THE BUTTON.
    if (this.show2) {
      this.buttonName2 = 'Hide';
    } else {
      this.buttonName2 = 'Show';
    }
  }

  getScientificName(icon) {
    if (icon) {
      if (icon.includes('https://aumet-data.s3.us-west-2.amazonaws.com/icons/scientificname')) {
        return icon;
      } else {
        return 'https://aumet-data.s3.us-west-2.amazonaws.com/icons/scientificname/' + icon;
      }
    } else {
      return '/assets/images/scientific.png';
    }
  }

  renderInfo() {
    this._distributor.get(this.dialogData.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.dialogData = res.data[0];
    });
  }

  onAddComment(formData) {
    if (!formData.invalid) {
      let data = {
        Text: formData.value.comment,
        DistributorId: this.dialogData.id
      }
      this._distributor
        .addCommentdata(data)
        .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
          this.toast.success('Has been added successfully');
        });
    } else {
      // formData.fileds.tou
      Object.keys(formData.controls).forEach(key => {
        formData.getControl(key).markAsTouched();
      });
    }
  }

  openAddUser(details): void {
    const dialogRef = this.dialog.open(AddUpdateUserComponent, {
      height: 'auto',
      maxHeight: '600px',
      width: '45%',
      data: {
        items: {},
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  EditUser(values, details) {
    const dialogRef = this.dialog.open(AddUpdateUserComponent, {
      height: 'auto',
      maxHeight: '600px',
      width: '45%',
      data: {
        items: values,
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  openAddInterests(details): void {
    const dialogRef = this.dialog.open(AddUpdateComponent, {
      // height: 'auto',
      // maxHeight: '600px',
      width: '45%',
      data: {
        type: 'Interest',
        items: {},
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  EditInterest(values, details) {
    const dialogRef = this.dialog.open(AddUpdateComponent, {
      height: 'auto',
      maxHeight: '600px',
      width: '45%',
      data: {
        items: values,
        type: 'Interest',
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    }

    );
  }

  onDeactivate(id: number): void {
    let Activate = {
      IsActive: false,
      id: id
    }

    this.activateDeactivate(Activate);
  }

  onActivate(id: number): void {
    let Activate = {
      IsActive: true,
      id: id
    }

    this.activateDeactivate(Activate);
  }

  activateDeactivate(Activate) {
    this._userService
      .ActivateDeactivateUser(Activate)
      .pipe(
        take(1),
        tap((res) => {
          this.renderInfo();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  openAddExperience(details): void {
    const dialogRef = this.dialog.open(AddUpdateComponent, {
      height: 'auto',
      maxHeight: '600px',
      width: '45%',
      data: {
        type: 'Experience',
        items: {},
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  EditExperiences(values, details) {
    const dialogRef = this.dialog.open(AddUpdateComponent, {
      height: 'auto',
      maxHeight: '600px',
      width: '45%',
      data: {
        items: values,
        type: 'Experience',
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  DeleteInterest(id): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Distributor',
        id: id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this._distributor.deleteDistributorInt(id) : of(undefined);
        }), tap((res: number) => {
          this.renderInfo();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  DeleteExperiences(id): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Distributor',
        id: id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this._distributor.deleteDistributorExp(id) : of(undefined);
        }), tap((res: number) => {
          this.renderInfo();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  actionTabChangeContact(event) {
    switch (event.nextId) {
      case '100':
        break;
      case '101':
        break;
    }
  }

  onAddCrawler(formData) {
    if (!formData.invalid) {
      let data = {
        CompanyID: this.dialogData.companyID,
        Domain: formData.value.Domain,
        Website: formData.value.Website
      }
      this._distributor
        .addAdminCrawllerdata(data)
        .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
          this.toast.success('Has been added successfully');
        });
    } else {
      // formData.fileds.tou
      Object.keys(formData.controls).forEach(key => {
        formData.getControl(key).markAsTouched();
      });
    }
  }

  onViewRequirement(data) {
    let token = data.token,
      manuId = data.companyID;
    const obj = '?CompanyToken=' + token + '&CompanyId=' + manuId + '&CompanyType=distributor';
    this._distributor.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.profilelink = res.data.find(x => x.key.toLowerCase() == 'profile').link;
      this.bolink = res.data.find(x => x.key.toLowerCase() == "businessopportunity").link;;
      // window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token + "&redirectUrl=" + response.nextStage);
    });
  }
}

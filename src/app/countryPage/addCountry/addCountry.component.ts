import { Component, OnInit, Inject } from '@angular/core';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';
import { Countrys } from './Countrys';
import { S3Service } from 'src/app/_services/s3.service';

import { CountryPageService } from '../countryPage.service';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator
} from '@angular/material';

import { ToastService } from 'src/app/_services/toast.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-addCountry',
  templateUrl: './addCountry.component.html',
  styleUrls: ['./addCountry.component.scss']
})
export class AddCountryComponent implements OnInit {
  imgurl: any = 'https://s3-us-west-2.amazonaws.com/product/catelogues';
  testurl: any =
    'https://aumet-data.s3.us-west-2.amazonaws.com/product/catelogues/';

  localImages: File[] = [];
  imagePreview: any = [];

  localImagesMap: File[] = [];
  imagePreviewMap: any = [];

  statisticPanel: any[] = [];
  Add = true;
  disbalestories = true;
  imagePreviewLogo: any = [];
  localImagesLogo: File[] = [];

  imagePreviewPerson: any = [];
  localImagesPerson: File[] = [];

  storiesPanel: any;
  countryForm: Countrys;

  MapImage: any;
  FlagImage: any;

  cntries: any = [];
  GDP: any;
  Population: any;
  NumberOfhos: any;
  numberofhospitalbeds: any;
  numberofdistributors: any;
  Specialities: any;
  items: any;
  PageCountryId: any;

  Quotation: any;
  Description: any;
  SubTitle: any;
  CompanyName: any;
  PersonName: any;

  dataSource: MatTableDataSource<any>;
  paginator: MatPaginator;
  displayedColumns: any[] = [
    'Actions',
    'personName',
    'companyName',
    'quotation'
  ];

  exist = false; // show stories table
  StoryId: number = 0;
  Title: any;
  allowedExtensions = allExtensions;
  extensionsWithDots = this.allowedExtensions.map(e => '.' + e);
  showimagevalidator: boolean;
  constructor(
    private ImageService: S3Service,
    private service: CountryPageService,
    public dialogRef: MatDialogRef<AddCountryComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.countryForm = new Countrys();

    this.service.getCountries().subscribe(x => {
      this.cntries = x;
    });

    if (this.dialogData.id) {
      this.disbalestories = false;

      this.service.GetStoryByCountryId(this.dialogData.id).subscribe((Response: any) => {
        this.renderTableData(Response);
        this.exist = true;
      });
      this.PageCountryId = this.dialogData.id;

      /* this.items = this.cntries.filter(item => item.Name.toLowerCase().indexOf(this.dialogData.name) === 0);
      this.cntries.map(e=> {
        console.log(e.name.toLowerCase())
      }) */
      this.countryChangeAction(this.dialogData.countryId);
      this.countryForm.Flag = this.dialogData.flag;
      this.countryForm.Map = this.dialogData.map;

      this.countryForm.Title = this.dialogData.title;
      this.countryForm.Description = this.dialogData.description;
      this.imagePreview[0] = this.dialogData.flag;
      this.imagePreviewMap[0] = this.dialogData.map;

      // tslint:disable-next-line: max-line-length
      if (
        this.dialogData.gdp ||
        this.dialogData.specialties ||
        this.dialogData.population ||
        this.dialogData.noofhospitals ||
        this.dialogData.numberofhospitalbeds ||
        this.dialogData.numberofdistributors
      ) {
        /*   this.statisticPanel.push({
            GDP: parseInt(this.dialogData.gdp),
            Specialties: this.dialogData.specialties,
            Population: this.dialogData.population,
            Noofhospitals: this.dialogData.noofhospitals,
            Numberofhospitalbeds: this.dialogData.numberofhospitalbeds,
            Numberofdistributors: this.dialogData.numberofdistributors
          });
   */
        this.countryForm.Noofhospitals = this.dialogData.noofhospitals;
        this.countryForm.Population = this.dialogData.population;
        this.countryForm.GDP = this.dialogData.gdp;
        this.countryForm.Title = this.dialogData.title;

        this.countryForm.Numberofhospitalbeds = this.dialogData.numberofhospitalbeds;
        this.Population = this.dialogData.population;
        this.GDP = this.dialogData.gdp;
        this.NumberOfhos = this.dialogData.noofhospitals;
        this.Title = this.dialogData.Title;

        this.numberofhospitalbeds = this.dialogData.numberofhospitalbeds;
        this.numberofdistributors = this.dialogData.numberofdistributors;
        this.Specialities = this.dialogData.specialties;
        this.countryForm.Statistic = this.statisticPanel;
        this.Add = false;
      }
    }
  }

  renderTableData(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  uploadFlagImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key:
        'company/avatar/' +
        Guid.create() +
        '.' +
        this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  AddStatistic(form) {
    this.statisticPanel.splice(0);
    // this block is for countryForm
    this.countryForm.Specialties = form.Specialties;
    this.countryForm.Noofhospitals = form.Noofhospitals;
    this.countryForm.Numberofhospitalbeds = form.numberofhospitalbeds;
    this.countryForm.Numberofdistributors = form.numberofdistributors;
    this.countryForm.Population = form.Population;

    this.countryForm.GDP = form.GDP;
    // end of countryForm

    // this block is for expansion panel
    this.statisticPanel.push({
      GDP: this.countryForm.GDP,
      Specialties: this.countryForm.Specialties,
      Population: this.countryForm.Population,
      Noofhospitals: this.countryForm.Noofhospitals,
      Numberofhospitalbeds: this.countryForm.Numberofhospitalbeds,
      Numberofdistributors: this.countryForm.Numberofdistributors
    });
    // end of expansion panel

    // bind the new data in expansion
    this.Specialities = this.statisticPanel[0].Specialties;
    this.Population = this.statisticPanel[0].Population;
    this.GDP = this.statisticPanel[0].GDP;
    this.numberofdistributors = this.statisticPanel[0].Numberofdistributors;
    this.numberofhospitalbeds = this.statisticPanel[0].Numberofhospitalbeds;
    this.NumberOfhos = this.statisticPanel[0].Noofhospitals;

    this.Add = false;
  }

  editStatistic(form) {
    this.statisticPanel.pop();
    this.statisticPanel.push(form);
    this.countryForm.Specialties = this.Specialities;
    this.countryForm.Noofhospitals = this.NumberOfhos;
    this.countryForm.Numberofhospitalbeds = this.numberofhospitalbeds;
    this.countryForm.Numberofdistributors = this.numberofdistributors;
    this.countryForm.Population = this.Population;

    this.countryForm.GDP = this.GDP;
    this.Add = false;
  }

  /* ==================================>Country Map<================================= */
  /* ==================================>Country Map<================================= */
  /* ==================================>Country Map<================================= */
  /* ==================================>Country Map<================================= */
  handleBaseImageChangeMap(e) {
    this.localImagesMap[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreviewMap.pop();
        this.imagePreviewMap.push(reader.result);
      };
    }
  }

  hideBaseImageMap() {
    this.imagePreviewMap = [];
    this.localImagesMap = [];
  }

  async handleUploadMap(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  uploadMapImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key:
        'company/avatar/' +
        Guid.create() +
        '.' +
        this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUploadMap(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }
  /* ==================================> End of Country Map<================================= */
  /* ==================================> End of Country Map<================================= */
  /* ==================================> End of Country Map<================================= */
  /* ==================================> End of Country Map<================================= */

  /* ==================================> Person Logo<================================= */
  /* ==================================> Person Logo<================================= */
  /* ==================================> Person Logo<================================= */
  /* ==================================> Person Logo<================================= */

  hideBaseImageLogo() {
    this.imagePreviewLogo = [];
    this.localImagesLogo = [];
  }

  async handleUploadLogo(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  uploadLogoImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key:
        'company/avatar/' +
        Guid.create() +
        '.' +
        this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUploadLogo(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  handleBaseImageChangeLogo(e) {
    this.localImagesLogo[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreviewLogo.pop();
        this.imagePreviewLogo.push(reader.result);
      };
    }
  }
  /* ==================================>End Of Person Logo<================================= */
  /* ==================================>End Of Person Logo<================================= */
  /* ==================================>End Of Person Logo<================================= */
  /* ==================================>End Of Person Logo<================================= */

  /* ==================================>Person Image<================================= */
  /* ==================================>Person Image<================================= */
  /* ==================================>Person Image<================================= */
  /* ==================================>Person Image<================================= */
  hideBaseImagePerson() {
    this.imagePreviewPerson = [];
    this.localImagesPerson = [];
  }

  async handleUploadPerson(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  uploadPersonImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key:
        'company/avatar/' +
        Guid.create() +
        '.' +
        this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUploadMap(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  handleBaseImageChangePerson(e) {
    this.localImagesPerson[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreviewPerson.pop();
        this.imagePreviewPerson.push(reader.result);
      };
    }
  }
  /* ==================================>End Of Person Image<================================= */
  /* ==================================>End Of Person Image<================================= */
  /* ==================================>End Of Person Image<================================= */
  /* ==================================>End Of Person Image<================================= */

  DeleteStory(i) {
    this.service.DeleteCountryStory(i).subscribe(res => {
      this.toast.success("Deleted");
      this.service.GetStoryByCountryId(this.dialogData.id).subscribe((Response: any) => {
        this.renderTableData(Response);
        this.exist = true;
      });
    });
  }
  DeleteStatistic(i) {
    this.statisticPanel.splice(i);
    this.countryForm.Specialties = '';
    this.countryForm.Noofhospitals = '';
    this.countryForm.Numberofhospitalbeds = '';
    this.countryForm.Numberofdistributors = '';
    this.countryForm.Population = '';
    this.countryForm.GDP = '';
    this.Add = true;
  }

  async submit(form) {
    if (form.invalid) {
  
      return;
    }
    if (this.dialogData.id) {
      if (this.localImages.length > 0 && this.localImagesMap.length > 0) {
        const _thisCOuntryImage = this;
        this.ImageService.uploadFile(
          'product/catelogues/',
          this.localImages[0]
        ).subscribe(async x => {
          if ((await x.percentage) === 0 && x.data !== null && x.error === '') {
            _thisCOuntryImage.countryForm.Flag = x.data.Location;
            // tslint:disable-next-line: no-shadowed-variable
            this.ImageService.uploadFile(
              'product/catelogues/',
              this.localImagesMap[0]
              // tslint:disable-next-line: no-shadowed-variable
            ).subscribe(async x => {
              if (
                (await x.percentage) === 0 &&
                x.data !== null &&
                x.error === ''
              ) {
                _thisCOuntryImage.countryForm.Map = x.data.Location;

                _thisCOuntryImage.countryForm.CountryId =
                  _thisCOuntryImage.items.id;
                _thisCOuntryImage.countryForm.Id =
                  _thisCOuntryImage.dialogData.id;
                // tslint:disable-next-line: no-shadowed-variable
                this.service
                  .addCOuntryPage(_thisCOuntryImage.countryForm)
                  // tslint:disable-next-line: no-shadowed-variable
                  .subscribe(x => {

                    _thisCOuntryImage.disbalestories = false;
                    _thisCOuntryImage.toast.success(
                      'success'
                    );
                  });
              }
            });
          }
        });
      } else {
        this.countryForm.CountryId = this.items.id;
        this.countryForm.Id = this.dialogData.id;
        this.service.addCOuntryPage(this.countryForm).subscribe(x => {



          this.disbalestories = false;
          this.toast.success('success');
        });
      }
    } else {
      this.ImageService.uploadFile(
        'product/catelogues/',
        this.localImages[0]
      ).subscribe(async x => {
        if ((await x.percentage) === 0 && x.data !== null && x.error === '') {
          this.countryForm.Flag = x.data.Location;
          this.ImageService.uploadFile(
            'product/catelogues/',
            this.localImagesMap[0]
            // tslint:disable-next-line: no-shadowed-variable
          ).subscribe(async x => {
            if (
              (await x.percentage) === 0 &&
              x.data !== null &&
              x.error === ''
            ) {
              this.countryForm.Map = x.data.Location;
              this.countryForm.CountryId = this.items.id;
              // tslint:disable-next-line: no-shadowed-variable
              this.service.addCOuntryPage(this.countryForm).subscribe(x => {
                this.PageCountryId = x;

                this.disbalestories = false;

                this.toast.success('Form has been Added successfuly');
              });
            }
          });
        }
      });
    }
  }

  async AddStories(form) {
    
    if (form.invalid) {
      if (this.localImagesPerson[0] == null || this.localImagesPerson[0] == undefined || this.localImagesPerson.length == 0) {
        this.showimagevalidator = true;
        return;

      }
      return;
    }
    if (this.localImagesPerson[0] == null || this.localImagesPerson[0] == undefined || this.localImagesPerson.length == 0) {
      this.showimagevalidator = true;
      return;

    }
    form = form.value;
    if (this.StoryId > 0) {
      
      if (this.localImagesPerson.length > 0) {
        const _thisStroyImage = this;
        await this.uploadPersonImage(this.localImagesPerson[0], function (data) {
          form.PersonImage = data;
          form.Id = _thisStroyImage.StoryId;
          form.PageCountryId = _thisStroyImage.PageCountryId;
          _thisStroyImage.storiesPanel = form;
          _thisStroyImage.service
            .addStory(_thisStroyImage.storiesPanel)
            .subscribe(x => {

              _thisStroyImage.service.GetStoryByCountryId(_thisStroyImage.dialogData.id).subscribe((Response: any) => {
                _thisStroyImage.renderTableData(Response);
                _thisStroyImage.exist = true;
              });
              _thisStroyImage.PersonName = '';
              _thisStroyImage.CompanyName = '';
              _thisStroyImage.SubTitle = '';
              this.StoryId = 0;

              _thisStroyImage.Description = '';
              _thisStroyImage.Quotation = '';
              _thisStroyImage.imagePreviewPerson[0] = '';
              _thisStroyImage.service.GetStoryByCountryId(_thisStroyImage.dialogData.id).subscribe((Response: any) => {
                _thisStroyImage.renderTableData(Response);
                _thisStroyImage.exist = true;
              });
            });
        });
      } else {
        form.PersonImage = this.imagePreviewPerson[0];
        if (this.StoryId > 0) {

          form.Id = this.StoryId;

        }
        form.PageCountryId = this.PageCountryId;
        form.description = this.Description;
        form.SubTitle = this.SubTitle;
        this.storiesPanel = form;
        this.service.addStory(this.storiesPanel).subscribe(x => {
     
          this.PersonName = '';
          this.CompanyName = '';
          this.Description = '';
          this.Quotation = '';
          this.SubTitle = '';
          this.StoryId = 0;
          this.imagePreviewPerson[0] = '';

          this.service.GetStoryByCountryId(this.dialogData.id).subscribe((Response: any) => {
            this.renderTableData(Response);
            this.exist = true;
          });
        });
      }
    } else {
      
      this.ImageService.uploadFile(
        'product/catelogues/',
        this.localImagesPerson[0]
      ).subscribe(async x => {
        if ((await x.percentage) === 0 && x.data !== null && x.error === '') {
          form.PersonImage = x.data.Location;
          form.PageCountryId = this.PageCountryId;
          this.storiesPanel = form;
          // tslint:disable-next-line: no-shadowed-variable
          
          this.service.addStory(this.storiesPanel).subscribe(x => {
            this.service.GetStoryByCountryId(this.dialogData.id).subscribe((Response: any) => {
              this.renderTableData(Response);
              this.exist = true;
              this.PersonName = '';
              this.CompanyName = '';
              this.Description = null;
              this.SubTitle = '';

              this.Quotation = null;
              this.imagePreviewPerson[0] = null;
              this.service.GetStoryByCountryId(this.dialogData.id).subscribe((Response: any) => {
                this.renderTableData(Response);
                this.exist = true;
              });
            });
          });
        }
      });
    }
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  onEditStory(values) {
    this.PageCountryId = this.dialogData.id;
    this.PersonName = values.personName;
    this.CompanyName = values.companyName;
    this.Description = values.description;
    this.Quotation = values.quotation;
    this.SubTitle = values.subTitle;
    this.imagePreviewPerson[0] = values.personImage;
    this.StoryId = values.id;

    /* if (values.personImage != null && values.personImage.includes(this.testurl)) {
      this.imagePreviewPerson[0] = values.personImage;
    } else {
      this.imagePreviewPerson[0] = this.imgurl + values.profile_image;
    } */
  }

  countryChangeAction(id) {
    this.service.getCountries().subscribe(x => {
      this.cntries = x;
      const dropDownData = this.cntries.find((data: any) => data.id === id);
      this.items = dropDownData;
    });
  }






  validateFileExtensions(files: File[]): File[] {
    // Missing showing error messages
    const filesList = Object.values(files);
    const validFiles = filesList.filter((f: File) => this.extensionsWithDots.includes(this.getNewFileExtention(f)));
    if (validFiles.length < filesList.length) {
      // this.onFileInvalid();
    } else {
      //this.invalid$.next(undefined);
    }
    return validFiles;
  }
  getNewFileExtention(file: File): string {
    return file.name.match(/\.[0-9a-z]+$/i)[0];
  }



  secondfileEvent(e): void {
    const filesList = e.target.files;
    const validatedFiles = this.validateFileExtensions(filesList);
    if (validatedFiles.length > 0) {
      this.localImagesMap[0] = validatedFiles[0];
      const reader = new FileReader();
      reader.readAsDataURL(validatedFiles[0]);
      reader.onload = () => {
        this.imagePreviewMap.pop();
        this.imagePreviewMap.push(reader.result);
      };
    }
    //this.addFiles(validatedFiles);
  }

  fileEvent(e): void {
    const filesList = e.target.files;
    const validatedFiles = this.validateFileExtensions(filesList);
    if (validatedFiles.length > 0) {
      this.localImages[0] = validatedFiles[0];
      const reader = new FileReader();
      reader.readAsDataURL(validatedFiles[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
    //this.addFiles(validatedFiles);
  }

  handleBaseImageChange(e) {

    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }
}

export const allExtensions = [
  'png',
  'jpg',
  'svg',
  'jpeg'

];
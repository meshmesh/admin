import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ReviewsService } from './reviews.service';
import {
  MatTableDataSource,
  MatPaginator,
  MatDialog,
  MatDialogRef
} from '@angular/material';
import { AddReviewsComponent } from './addReviews/addReviews.component';
import { ReviewForm } from './addReviews/ReviewForm';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit, OnDestroy {
  displayedColumns: any[] = [
    'Actions',
    'Name',
    'Title',
    'Position',
    'Picture',
    'Rate'
  ];
  constructor(private serv: ReviewsService, private dialog: MatDialog) {}
  dataSource: MatTableDataSource<any>;
  length: number;
  pagenumber = 1;
  pageSize = 10;
  onDestroy$ = new ReplaySubject<void>();
  

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.renderTable(this.pagenumber, this.pageSize);
  }
  ngOnDestroy() {
    this.onDestroy$.next();
  }
  renderTable(pagenumber, pagesize) {
    this.serv
      .getReviews(pagenumber, pagesize)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(res => {
        this.dataSource = res;
        this.length = res[0].totalNumber;
      });
  }
  pageChange(event) {
    this.pagenumber = event.pageIndex + 1;
    this.renderTable(this.pagenumber, this.pageSize);
  }
  addReview() {
    const dialogRef = this.openReviewsDialog(undefined, 'create');
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(e => this.serv.getReviews(this.pagenumber, this.pageSize))
      )
      .subscribe(e => this.renderTable(this.pagenumber, this.pageSize));
  }

  Edit(item) {
    const dialogRef = this.openReviewsDialog(item, 'edit');
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        filter(res => !!res),
        switchMap(e => this.serv.getReviews(this.pagenumber, this.pageSize))
      )
      .subscribe(e => this.renderTable(this.pagenumber, this.pageSize));
  }

  Delete(item) {
    this.serv
      .DeleteReview(item)
      .pipe(
        takeUntil(this.onDestroy$),
        filter(e => !!e)
      )
      .subscribe(e => this.renderTable(this.pagenumber, this.pageSize));
  }

  Publish(item) {
    this.serv
      .PublishReiew(item)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(e => this.renderTable(this.pagenumber, this.pageSize));
/*       this.unpublish = true;
 */  }
  openReviewsDialog(
    dialogData: ReviewForm,
    dialogType: 'create' | 'edit'
  ): MatDialogRef<AddReviewsComponent> {
    return this.dialog.open(AddReviewsComponent, {
      width: '80%',
      height: '65%',
      data: { dialogData, dialogType }
    });
  }

  unPublishReview(id){
/*     this.publish = true;
 */    this.serv
      .unPublishReiew(id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(e => this.renderTable(this.pagenumber, this.pageSize));
  }


}

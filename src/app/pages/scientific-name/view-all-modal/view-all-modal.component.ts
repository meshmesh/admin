import { Component, OnInit, Inject, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ReplaySubject } from 'rxjs';

@Component({
   selector: 'app-view-all-modal',
  templateUrl: './view-all-modal.component.html',
  styleUrls: ['./view-all-modal.component.scss']
})
export class ViewAllModalComponent implements OnInit , OnDestroy, AfterViewInit {

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  onDestroy$ = new ReplaySubject<void>();
  dialogTitle: string;
  displayedColumns: Array<string> = ['id', 'name'];

  constructor(
    public dialogRef: MatDialogRef<ViewAllModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) { }
 

  ngOnDestroy(): void {
    this.onDestroy$.next(); 
  }
  
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
      this.displayedColumns = this.data.displayedColumns;
      this.dialogTitle = this.data.title;
        this.renderData(this.data.displayData);
  }

  renderData(data) {
    this.dataSource = new MatTableDataSource<any>(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  close(data) {
    this.dialogRef.close(data);
  }
}
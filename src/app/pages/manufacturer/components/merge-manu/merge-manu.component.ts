

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { ManufacturerService } from '../../services/manufacturer.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-merge-manu',
  templateUrl: './merge-manu.component.html',
  styleUrls: ['./merge-manu.component.scss']
})
export class MergeManuComponent implements OnInit, OnDestroy {
 
  onDestroy$ = new ReplaySubject<void>();
  ofCompany: any;
  company1: string = this.data.comp.name;
  company2: any;
  submittedObj: any = {
    CompanyID: this.data.comp.id,
    OldCompanyId: null
  }

  constructor(
    private toast: ToastService,
    private serv: ManufacturerService, 
    private dialogRef: MatDialogRef<MergeManuComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }


  ngOnInit() {
  }
 
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  handleTextChanging(event) {
    this.renderCompany(event);
  }

  renderCompany(searchvalue: string) {
    this.serv.getCOmpanies(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.ofCompany = res.data.items;
      this.displayCompany(res.data.items);
    });
  }

  displayCompany(company) {
    return company ? company.name : undefined;
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  submit(obj) {
    if(!obj.value.company2 || !obj.value.company2.id) {
      this.toast.error("Please select Manufacturer.");
      return;
    }
    
    this.submittedObj.OldCompanyId = obj.value.company2.id;
    let data = {
      SourceManuId: this.submittedObj.CompanyID,
      DestManuId: obj.value.company2.id
    }

    this.serv.mergCompanies(data).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      if(res.success) {
        this.dialogRef.close(true);
        this.toast.success("Successfully Merged!");
      } else {
        this.toast.error("Oops! Something wrong happened");
      }
    }, err => {
      this.toast.error(err);
    })
  }
}

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Company, FollowUpLog, Action } from '../../_model/followup';
import { ReplaySubject, Observable } from 'rxjs';
import { FollowUpService } from 'src/app/_services/followup.service';
import { tap, map, take, takeUntil } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-actions-log-modal',
  templateUrl: './actions-log-modal.component.html',
  styleUrls: ['./actions-log-modal.component.scss']
})
export class ActionsLogModalComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  constructor(
    public dialogRef: MatDialogRef<ActionsLogModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Company,
    private followUpService: FollowUpService
  ) {}

  nextActionForm: { name: FormControl; date: FormControl };
  state: 'view' | 'edit';
  nextAction: Action;
  actionName$: Observable<string[]>;

  displayedColumns: string[] = ['id', 'process', 'date', 'name', 'actions'];
  dataSource: MatTableDataSource<Action>;
  ngOnInit() {
    this.followUpService
      .getLog(this.data.id)
      .pipe(
        take(1),
        map((data: FollowUpLog) => data.actions),
        tap((actions: Action[]) => (this.dataSource = new MatTableDataSource(actions))),
        map((actions: Action[]) => actions.find(e => e.completed === false)),
        tap((nextAction: Action) => {
          this.nextAction = nextAction;
          this.nextActionForm = this.createFormGroup();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();

    this.state = 'view';
    this.actionName$ = this.followUpService.getActionNames();
  }

  toggleState(): void {
    this.state = this.state === 'view' ? 'edit' : 'view';
  }

  createFormGroup(): { name: FormControl; date: FormControl } {
    const parsedDate = new Date(this.nextAction.date);
    const name = new FormControl(this.nextAction.name, [Validators.required]);
    const date = new FormControl(parsedDate, [Validators.required]);
    return { name, date };
  }

  onSave(): void {
    console.log(this.nextActionForm.name.value);
  }
}

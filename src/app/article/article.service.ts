import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../_model/article';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class ArticleService {
  constructor(private http: HttpClient) {}

  search(args, pageNumber) {
    let url = APIEndpoint + '/api/AdminArticles/SearchArticals?PageSize=10&PageNumber=' + pageNumber;
    for (let i = 0; i < args.length; i = i + 2) {
      url = url + '&' + args[i] + '=' + args[i + 1];
    }
    return this.http.get<any>(url);
  }

  getFilteredArticles(args: {}): Observable<Article[]> {
    let url: string = APIEndpoint + '/api/AdminArticles/SearchArticals';
    Object.keys(args).forEach((key, i) => {
      if (i === 0) {
        url = url + '?' + key.toString() + '=' + args[key];
      } else {
        url = url + '&' + key.toString() + '=' + args[key];
      }
    });
    return this.http.get<Article[]>(url);
  }

  puplishArticle(id) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/Publish?Id=' + id, {});
  }

  deleteArticle(id) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/Delete?Id=' + id, {});
  }

  getArticlesById(id): Observable<Article> {
    const url = APIEndpoint + '/api/AdminArticles/' + id;
    return this.http.get<Article>(url);
  }

  getMainCategory() {
    return this.http.get<any>(APIEndpoint + '/api/AdminArticles/GetListOfCategories?MainOnly=true');
  }

  getSubCategory(id) {
    const url = APIEndpoint + '/api/AdminArticles/GetListOfCategories?MainCategory=' + id;
    return this.http.get<any>(url);
  }

  getPortal() {
    return this.http.get<any>(APIEndpoint + '/api/portals/getportals');
  }

  createUpdateArticle(obj) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/AddUpdate', obj);
  }

  getPortals() {
    return this.http.get(APIEndpoint + '/api/portals/getportals');
  }
}

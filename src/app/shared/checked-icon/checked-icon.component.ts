import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-checked-icon',
  template: `
    <mat-icon *ngIf="boolean" color="primary">check_circle</mat-icon>
    <mat-icon *ngIf="!boolean" color="accent">cancel</mat-icon>
  `
})
export class CheckedIconComponent implements OnInit {
  constructor() {}

  @Input() boolean: boolean;
  ngOnInit() {}
}

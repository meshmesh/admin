import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { SciNamesSearchParams } from '../model/scientific-name.model';
import { pluck } from 'rxjs/operators';

const APIEndpoint = environment.ApiUrlV3;
@Injectable({
  providedIn: 'root'
})
export class ScientificNameService {
  constructor(private http: HttpClient) {}

  getall(data: SciNamesSearchParams, queryParams: string) {

    let URL = APIEndpoint + `AdminScientificNames/Search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;
    URL += queryParams;

    // append data search params 

    URL += data.Name ? `&Name=${data.Name}` : '';
    URL += data.Id ? `&Id=${data.Id}` : '';
    return this.http.get(URL);
  }
  
  getManuInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=1';
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/Manufacturers");
  }

  getDistInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=6';
    
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/Distributors");
  }

  getProdInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminProducts/GetProductByScientificName?ScientificId=' + ObjectId;
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/products");
  }

  deleteScientificName(id) {
    console.log(id);
    return this.http.post(APIEndpoint + 'AdminScientificNames/Delete?Id=' + id, {});
  }

  approveScientificName(id: number): Observable<any> {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Approve?Id=' + id + '&Approved='+ true, {});
  }

  UnapproveScientificName(id: number): Observable<any> {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Approve?Id=' + id + '&Approved='+ false, {});
    // return this.http.post(APIEndpoint + 'AdminScientificNames/UnApprove?Id=' + id, {});
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url =
      APIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url);
  }
  
  getScientificNameSug(str): Observable<any> {
    const url =
      APIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1${str}`;
    return this.http.get<any>(url);
  }

  getAllScientificName(name = ''): Observable<any> {
    let URL =  environment.ApiUrlV3 + 'AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=' + name ;
    return this.http.get<any>(URL).pipe(pluck('data', 'items'));
  }

   mergScientific(originalId, alternateId) {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Merge', {
      'new': alternateId,
      'old': originalId
    });
  }

  
}

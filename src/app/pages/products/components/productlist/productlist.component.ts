import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {

  displayedColumns = ['id', 'title'];
  
  list:any;

  constructor(
    public dialogRef: MatDialogRef<ProductlistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }


  ngOnInit() {
    debugger
    this.list = this.data.data.products.products;
    console.log(this.list);

  }

}

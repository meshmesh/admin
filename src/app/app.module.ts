import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BlockUIModule } from 'ng-block-ui';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MenuItems } from './shared/menu-items/menu-items';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthComponent } from './layout/auth/auth.component';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { ErrorInterceptor, JwtInterceptor } from './_helpers';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { AuthService } from './_services/auth.service';
import { AuthGuard } from './_guards';
import { ToastrModule } from 'ngx-toastr';
import { WinToastrComponent } from './shared/win-toastr/win-toastr.component';
import { ToastService } from './_services/toast.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';

const TOASTR_CONFIGS = {
	closeButton: true,
	tapToDismiss: false,
	positionClass: 'toast-bottom-right',
  toastComponent: WinToastrComponent,
  timeOut: 7000
}


// TODO: move to factory functions
export function requestFilter(request: HttpRequest<any>): boolean {
  return request.url.includes('api/Message/');
}



@NgModule({
  declarations: [
    AuthComponent,
    AppComponent,
    AdminComponent,
    BreadcrumbsComponent,
    LoginComponent,
    WinToastrComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    ToastrModule.forRoot(TOASTR_CONFIGS),
    BlockUIModule.forRoot({
      template: SpinnerComponent
    }),
    BlockUIHttpModule.forRoot({
      blockAllRequestsInProgress: true,
      requestFilters: [requestFilter]
    }),
  ],
  schemas: [],
  providers: [
    MenuItems,
    AuthService,
    ToastService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  entryComponents: [WinToastrComponent, SpinnerComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }



export interface Notification {
  id: number;
  message: string;
  status: 0 | 1 | 2;
}

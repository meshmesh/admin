import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class AutoCompleteService {

    stringList: string[] = [];
    idList: number[] = [];

    constructor(private http: HttpClient) { }
    getCompanys(str) {
        const url = APIEndpoint + '/api/Companies/GetCompanies?CompanyName=' + str + '&PageSize=10&PageNumber=1';
        return this.http.get(url);
    }

    getAnnualSales() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=5';
        return this.http.get(url);
    }

    getManufactureStage() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=7';
        return this.http.get(url);
    }

    getDistributerStage() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=8';
        return this.http.get(url);
    }

    getSatisfaction() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=13';
        return this.http.get(url);
    }

    getDistributerSupervisor() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=11';
        return this.http.get(url);
    }

    getManufacturerSupervisor() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=10';
        return this.http.get(url);
    }

}

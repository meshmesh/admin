import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataListComponent } from './dataList.component';


const router: Routes = [
    {
        path: '',
        component: DataListComponent,
        data: {
            title: 'LookUps',
            icon: 'icon-layout-cta-right',
            caption: 'my landing',
            status: true
        },
      
    }
];
@NgModule({
    imports: [RouterModule.forChild(router)],
    exports: [RouterModule]
})

export class DataListRoutingModule { }

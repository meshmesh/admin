import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { ManufacturerModel, ManuListSearchParams } from '../model/manufacturer.model';
import { environment } from '../../../../environments/environment';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ManufacturerService implements OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  APIEndpoint = environment.ApiUrlV3;

  scientificNames: any[] = [];

  constructor(private http: HttpClient,
  ) { }


  deletecontact(id){
    return this.http.get<any[]>( this.APIEndpoint + 'AdminCompany/deletecontact?id='+id );
  }

  getTargetCountry(id): Observable<any[]> {
    return this.http.get<any[]>( this.APIEndpoint + 'Manufacturer/GetTargeting?CompanyID='+id ); 
  
  }
  getContactId(id) {
    const url = this.APIEndpoint + 'AdminCompanyContact/' + id;
    return this.http.get(url);
  }
  getProductById(id) {
    const url = this.APIEndpoint + 'AdminProducts/' + id;
    return this.http.get(url);
  }
  getIgnoreComment(id,compid) {
    return this.http.get(this.APIEndpoint + 'AdminInterest/GetIgnoreComment?scientificnameid='+ id+'&ManufactureId='+compid);
  }

  ActivateDeactivateManufacturer(data: any): Observable<any> {
    return this.http.put(this.APIEndpoint + "AdminManufacturer/Activate", data);
  }

  get(id): Observable<any[]> {
    return this.http.get<ManufacturerModel[]>(
      this.APIEndpoint + 'AdminManufacturer/' + id
    );
  }
  // loadCallTypes() {
  //   const url = this.APIEndpoint + 'Lookups/Get?MajorCode=21';
  //   return this.http.get(url);
  // }
  getCountrySuggestion(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + 'Countries/GetCountries'
    );
  }
  GetScientificNameIds(str) {
    const url =
      this.APIEndpoint +
      `AdminScientificNames/GetScientificNameIds?input=${str}`;
    return this.http.get<any>(url);
  }
  GetScientificNames(str) {
    const url =
      this.APIEndpoint +
      `AdminScientificNames/ScientificnameFilter?PageSize=10&PageNumber=1&nameinput=${str}`;
    return this.http.get<any>(url);
  }
  getCountry(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + 'Countries'
    );

  }

  getProspectedSciName(proscinameid,pageNumber, pageSize): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + 'AdminProspectedCompanyScientificName?PageSize='+pageSize+'&PageNumber='+pageNumber+'&ProspectedCompanyId='+proscinameid
    );
  }

  addCompany(obj){
   return this.http.post(this.APIEndpoint + 'AdminCompany/AddCompany/', obj);
  }

  getAnnual(int){
    return this.http.get<any>(this.APIEndpoint + 'Lookups/Get?MajorCode=' + int);
  }
  getEmployees(){
    return this.http.get<any>(this.APIEndpoint + 'Lookups/Get?MajorCode=6');
  }

  addContract(data) {
    return this.http.post(this.APIEndpoint + 'AdminManufacturer/AddContract', data);
  }

  addcalllog(data) {

    data.CallDate = new Date(data.CallDate, data.time);
    data.CallDate = data.date;
    return this.http.post(this.APIEndpoint + 'AdminCalls/Add', data);
  }

  getCOmpanies(name): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminManufacturer/Search?PageNumber=1&PageSize=10&CompanyName=' +
      name);
  }
  
  viewProfile(obj) {
    return this.http.get(this.APIEndpoint + 'Auth/RedirectLinks'+ obj)
  }
  DemoQualifications(obj) {
    return this.http.post(this.APIEndpoint + 'Users/AddRequestLoginDemo', obj)

  }

  getAll(data: ManuListSearchParams, queryParams: string) {
    let URL = this.APIEndpoint + `AdminManufacturer/search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;
    URL += queryParams;

    // append data search params 
    URL += data.CompanyName ? `&CompanyName=${encodeURIComponent(data.CompanyName)}` : '';
    URL += data.CompanyId ? `&CompanyId=${data.CompanyId}` : '';
    URL += data.CompanyRegistered ? `&CompanyRegistered=${data.CompanyRegistered}` : '';
    URL += data.IsCrawled ? `&IsCrawled=${data.IsCrawled}` : '';
    return this.http.get(URL);
  }

  deleteManufacturers(id): Observable<any> {
    return this.http.delete(this.APIEndpoint + 'AdminManufacturer?ManufacturerId=' + id, {});
  }

  deleteManufacturersProspecSciName(id): Observable<any> {
    return this.http.delete(this.APIEndpoint + 'AdminProspectedCompanyScientificName?Id=' + id, {});
  }

  updateSeo(obj) {
    const url = this.APIEndpoint + 'AdminManufacturer/Update';
    return this.http.post(url, obj);
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url = this.APIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1${str}`;
    return this.http.get<any>(url);
  }
  getScientificNameList(str): Observable<any[]> {
    const url = this.APIEndpoint + 'AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=' + str;
    return this.http.get<any>(url);
  }
  getScientificNameID(str): Observable<any[]> {
    const url = this.APIEndpoint + 'AdminScientificNames/SearchScientificNamesAdmin?Id=' + str;
    return this.http.get<any>(url);
  }
  getBuyers() {
    const url = this.APIEndpoint + 'Lookups/Get?MajorCode=3';
    return this.http.get(url);
  }
  getScientificNameListId(pagenumber, pagesize, id): Observable<any> {
    const url = this.APIEndpoint + 'AdminProspectedCompanyScientificName?PageSize='+pagesize+'&PageNumber='+pagenumber+'&ProspectedCompanyId=' + id;
    return this.http.get<any>(url);
  }
  getProdcut(pagenumber, pagesize, id): Observable<any> {
    const url = this.APIEndpoint + 'AdminProduct?PageSize='+pagesize+'&PageNumber='+pagenumber+'&CompanyID=' + id;
    return this.http.get<any>(url);
  }
  addscientificform(data) {
    return this.http.post(this.APIEndpoint + 'AdminManufacturer/AddManufacturerScientific', data);
  }
  DeleteScientificname(data) {
    return this.http.post(this.APIEndpoint + 'AdminManufacturer/DeleteAdminScientificname', data);
  }
  addAdminCrawllerdata(data) {
    return this.http.put(this.APIEndpoint + 'AdminCrawller', data);
  }
  addCommentdata(data) {
    return this.http.put(this.APIEndpoint + 'AdminManufacturer/Comment', data);
  }

  addProduct(data) {
    return this.http.post(this.APIEndpoint + 'AdminProduct', data);
  }

  getManufacturerById(id) {
    return this.http.get(this.APIEndpoint + 'AdminManufacturer?Id=' + id);
  }
  // getManufacturerSciById(id) {
  //   return this.http.get(this.APIEndpoint + 'AdminManufacturer/GetManuProspectedCompanyScientificNames?Id=' + id);
  // }
  // getManufacturerProductById(id) {
  //   return this.http.get(this.APIEndpoint + 'AdminManufacturer/GetManuProducts?Id=' + id);
  // }
  getSeo() {
    const url = this.APIEndpoint + 'portals/getportals';
    return this.http.get(url);
  }


  addShareProfile(data) {
    return this.http.post(this.APIEndpoint + 'AdminShardProfiles/Add', data);
  }

  getListOfDistributors(name): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint +
      'Companies/GetListOfCompaniesLimited?Name=' + name + '&Type=distributor'
    );
  }
  getSpecalities(medId): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + 'Specialties/Search?ForAdmin=true&MedicalLineId=' + medId
    );
    // return this.http.get<any>(this.APIEndpoint + 'Specialities/GetSpecialities');
  }

  setScientificNames(value) {
    const url = this.APIEndpoint + 'AdminScientificNames/AdminSearchScientificNames?PageSize=10&PageNumber=1&NameInput=' + value;
    this.http.get<any>(url).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.scientificNames = res;
    });
  }
  getScientificNames(): any[] {
    return this.scientificNames;
  }
  getMedicalLines(): Observable<any[]> {
    return this.http.get<any>(
      this.APIEndpoint + 'MedicalLines?PageNumber=1&PageSize=100'
    );
    // return this.http.get<any[]>(this.APIEndpoint + 'MedicalLines/GetMedicalLines?Name=' + name);
  }

  getmedicalline(): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + 'MedicalLines?PageNumber=1&PageSize=100'
    );
  }

  getCompanyScientificName(CompanyId, ScientificID) {
    return this.http.get<any>(this.APIEndpoint + 'AdminManufacturer/GetScientificNameWithDetails?CompanyID=' + CompanyId + '&ScientificNameId=' + ScientificID);
  }

  getSCiwithNarrow(param){
   return this.http.get<any>(this.APIEndpoint + 'AdminScientificNames/GetScientificWithNarrow?id=' + param)
  }

  AddProspectedSciName(param){
    return this.http.post(this.APIEndpoint + 'AdminProspectedCompanyScientificName', param)
  }

  UpdateProspectedSciName(param){
    return this.http.put(this.APIEndpoint + 'AdminProspectedCompanyScientificName', param)
  }

  mergCompanies(data) {
    return this.http.put(this.APIEndpoint + 'AdminManufacturer/Merge', data);
  }

  getManuNotRegistered(pageNumber,pageSize,search){
    return this.http.get(this.APIEndpoint + 'AdminManufacturer/Search?PageSize='+pageSize+'&PageNumber='+pageNumber + search)
  }

  getManuNotRequirments(id) {
    return this.http.get(this.APIEndpoint + 'Manufacturers/'+id+'/Requirement')
  }

  getManuNotRegisteredWithoutContact(){
    return this.http.get(this.APIEndpoint + 'AdminManufacturer/SearchManuWithNoContact?PageSize=10&PageNumber=1')
  }

  editCompanyWithoutContact(param){
    return this.http.post(this.APIEndpoint + 'AdminCompany/EditCompanyWithoutContact', param)
  }

  getContracts(id){
    return this.http.get(this.APIEndpoint + 'AdminManufacturer/GetManufactureContracts?CompanyID=' + id)
  }

  addClient(form){
    return this.http.put(this.APIEndpoint + 'AdminManufacturer/UpdateClient', form);
}

getClient(id) {
  return this.http.get(this.APIEndpoint + 'AdminManufacturer/GetClient?Id=' + id);
}


getCompanyContact(CompanyId){
  return this.http.get(this.APIEndpoint + 'AdminCompanyContact/Get/' + CompanyId);
}

getJobTitle(param, type): Observable<any[]> {
  return this.http.get<any[]>(this.APIEndpoint + 'JobTitles?searchParams=' +
    param + '&type=' + type + '&limit=10');
}

addCompanyContact(form){
  return this.http.post(this.APIEndpoint + 'AdminCompanyContact/AddContact', form);
}


}
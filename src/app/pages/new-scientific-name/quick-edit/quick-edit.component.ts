import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Guid } from "guid-typescript";
import * as S3 from "aws-sdk/clients/s3";
import { Observable, of, ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ToastService } from "src/app/_services/toast.service";
import {
  ISpecialityRequest,
  SciNameUpdateRequest,
} from "../model/add-scientific-name.model";
import {
  IMedicalLine,
  ISpeciality,
  IScientificName,
  IParentSpeciality,
} from "../model/scientific-name.model";
import { ScientificNameService } from "../services/scientific-name.service";

@Component({
  selector: "app-quick-edit",
  templateUrl: "./quick-edit.component.html",
  styleUrls: ["./quick-edit.component.scss"],
})
export class QuickEditComponent implements OnInit, OnDestroy, OnChanges {
  public Scientificform: FormGroup;
  public contactList: FormArray;

  medicalLineList: IMedicalLine[];
  specialitiesParentList: IParentSpeciality[];
  subSpecialitiesList: ISpeciality[];
  sciNamesListSuggetions: IScientificName[];

  submitLoader: boolean = false;
  file: File;

  onDestroy$ = new ReplaySubject<void>();
  @Input("sciData") sciData: IScientificName;
  @Output("onSubmitEvent") onSubmitEvent = new EventEmitter();

  constructor(
    public fb: FormBuilder,
    private toast: ToastService,
    private SCIservice: ScientificNameService
  ) {}

  ngOnInit(): void {
    this.buildUserForm();
    this.getParentSpeciality();
    this.getSubSpecialityList();
    this.getMedicalLineList();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnChanges(changes): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }



  // convenience getter for easy access to form fields
  get f() {
    return this.Scientificform.controls;
  }

  get subSpecialityFormGroup() {
    return this.Scientificform.get("subSpeciality") as FormArray;
  }

  get medicalLineFormControl() {
    return this.Scientificform.get("medicalLine");
  }

  get mainParentSpecialityFormControl() {
    return this.Scientificform.get("mainParentSpeciality");
  }

  get mainChildSpecialityFormControl() {
    return this.Scientificform.get("mainChildSpeciality");
  }

  private getSubSpecialitiesFormGroup(index): FormGroup {
    return this.contactList.controls[index] as FormGroup;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.Scientificform.controls[controlName].hasError(errorName);
  };
  
  //  [autocompleteObservable]="requestSubSpecialityAutocomplete"
  public requestSubSpecialityAutocomplete = (text: string): Observable<any> => {
    return this.SCIservice.getSubSpecialitySugg(
      this.Scientificform.value.mainParentSpeciality[0].id,
      text
    );
  };

  // [autocompleteObservable]='requestparentSubSpecialityistAutocomplete'
  public requestparentSpecialityistAutocomplete = (
    text: string
  ): Observable<any> => {
    return this.SCIservice.searchSpeciality(
      this.Scientificform.value.medicalLine[0].id,
      "true",
      text
    );
  };

  public requestmedicalLineListAutocomplete = (
    text: string
  ): Observable<Response> => {
    return this.SCIservice.getMedicalSugg(text);
  };

  public requestAutocompleteItems = (text: string): Observable<Response> => {
    return this.SCIservice.getScientificNameSuggestion(text);
  };



  public requestSubSpecialitySuggAutocomplete = (
    text: string
  ): Observable<any> => {
    return this.SCIservice.getSubSpecialitySugg(null, text);
  };

  private buildUserForm(): void {
    this.Scientificform = this.fb.group({
      name: [
        this.sciData ? this.sciData.name : null,
        [Validators.required, Validators.maxLength(100)],
      ],
      description: [
        this.sciData ? this.sciData.description : null,
        Validators.maxLength(1000),
      ],
      imageUrl: [this.sciData ? this.sciData.imagePath : ""],

      medicalLine: [
        this.sciData ? [this.sciData.medicalLine] : null,
        [Validators.required],
      ],

      mainParentSpeciality: [
        this.sciData ? [this.sciData.mainSpeciality.parent] : null,
        Validators.required,
      ],
      mainChildSpeciality: [
        this.sciData ? this.sciData.mainSpeciality.child : null,
        Validators.required,
      ],

      subSpeciality: this.sciData
        ? this.fb.array(this.bindSubSpecialities())
        : this.fb.array([this.createSubSpecialities()]),

      bt: [this.sciData ? this.sciData.bt : null],
      nt: [this.sciData ? this.sciData.nt : null],
      rt: [this.sciData ? this.sciData.rt : null],
      alternatives: [this.sciData ? this.sciData.alterNative : null],
      references: [this.sciData ? this.sciData.references : null],
    });

    this.medicalLineFormControl.valueChanges
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((value) => {
        if (value && value.length) this.getParentSpeciality(value[0].id);
      });

    this.mainParentSpecialityFormControl.valueChanges
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((value) => {
        if (value && value.length) this.getSubSpecialityList(value[0].id);
      });

    // set contactlist to this field
    this.contactList = this.Scientificform.get("subSpeciality") as FormArray;









    // this.contactList.controls.forEach((element: FormGroup, i: number) => {

    //   debugger
    //   this.getSubSpecialitiesFormGroup(i).controls['parent'].valueChanges.pipe(takeUntil(this.onDestroy$))
    //   .subscribe((value) => {
    //     debugger
    //     console.log("||>>", value)
    //     // this.getSubSpecialityList()
    //   });

    //   this.getSubSpecialitiesFormGroup(i).controls['child'].valueChanges.pipe(takeUntil(this.onDestroy$))
    //   .subscribe((value) => {
    //     debugger
    //     console.log(">>", value)
    //   });

    //   element.controls.child.valueChanges.pipe(takeUntil(this.onDestroy$))
    //   .subscribe((value) => {
    //     debugger
    //     console.log(">>>>", value)
    //     // if(value && value.length) this.getSubSpecialityList(value[0].id);
    //   });
    // });

  }



  onB(e, i)  {
    this.getSubSpecialitiesFormGroup(i).controls['parent'].valueChanges.pipe(takeUntil(this.onDestroy$))
    .subscribe((value) => {
      console.log("||>>", value)
      this.getSubSpecialityList(value[0].id)
    });
  }



  onF(e, i) {
    const pp = this.getSubSpecialitiesFormGroup(i).controls['parent'];
    const parentId = pp.value && pp.value.length ? pp.value[0].id : null;
    if(parentId) this.getSubSpecialityList(parentId);
  }

  createSubSpecialities(): FormGroup {
    return this.fb.group({
      parent: [[]],
      child: [[]],
    });
  }

  bindSubSpecialities(): Array<FormGroup> {
    return this.sciData.specialities.map((x, i) =>
      this.fb.group({
        parent: [[x.parent]],
        child: [x.child],
      })
    );
  }

  // add a contact form group
  addSubSpecialities() {
    this.contactList.push(this.createSubSpecialities());
  }

  // remove contact from group
  removeSubSpecialities(index) {
    this.contactList.removeAt(index);
  }

  getMedicalLineList() {
    this.SCIservice.getMedicleLineSuggestion()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.medicalLineList = res.data;
      });
  }

  getParentSpeciality(medicalLineId?: number) {
    let id = medicalLineId
      ? medicalLineId
      : this.medicalLineFormControl.value &&
        this.medicalLineFormControl.value.length
      ? this.medicalLineFormControl.value[0].id
      : this.sciData
      ? this.sciData.medicalLine.id
      : null;
    this.SCIservice.searchSpeciality(id, "true")
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.specialitiesParentList = res;
      });
  }

  getSubSpecialityList(parentId?: number) {
    const parentSpecialityId = parentId
      ? parentId
      : this.sciData
      ? this.sciData.mainSpeciality.parent.id
      : null;
    this.SCIservice.getSubSpecialitySuggestion(parentSpecialityId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.subSpecialitiesList = res.data;
      });
  }

  getScientificNameSuggestion() {
    this.SCIservice.getScientificNameSuggestion()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        this.sciNamesListSuggetions = res;
      });
  }

  onFileChanged(e) {
    this.file = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.Scientificform.patchValue({ imageUrl: reader.result });
        this.Scientificform.get("imageUrl").updateValueAndValidity();
      };
    }
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: "aumet-data",
      Key:
        "icons/scientificname/" +
        Guid.create() +
        "." +
        this.getFileExtension(image.name),
      Body: image,
    };
    const that = this;
    that.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: "AKIAQQRZF2VNB3KYOT4O",
      secretAccessKey: "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",
      region: "us-west-2",
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  onCancel() {
    this.onSubmitEvent.emit(false);
  }

  submit() {
    console.log(this.Scientificform);
    if (this.Scientificform.invalid) {
      this.toast.error("Please check all required fields!");
      return;
    }

    this.submitLoader = true;
    // TODO: should check if form value label is true
    // which indicates whether the form control has been changed since first binding =>> for each suspected controller
    // 1- If value didn't change at all then append values of parentID instead of Id
    // 2- Else If value has been changed at least once, then
    let data = new SciNameUpdateRequest();
    let speciality = new ISpecialityRequest();
    let subspeciality: ISpecialityRequest[] = [];

    if (this.Scientificform.value.mainChildSpeciality) {
      this.Scientificform.value.mainChildSpeciality
        .filter((a) => typeof a.id == "string")
        .map((a) => {
          console.log(a);
          a.id = 0;
        });
    }

    if (this.Scientificform.value.subSpeciality) {
      for (let i = 0; i < this.Scientificform.value.subSpeciality.length; i++) {
        let element = this.Scientificform.value.subSpeciality[i];
        element.child.forEach((e) => {
          subspeciality.push({
            id: e.id,
            name: e.name,
            parentId: element.parent[0].id,
          });
        });
      }
      subspeciality
        .filter((a) => typeof a.id == "string")
        .map((a) => {
          console.log(a);
          a.id = 0;
        });
      subspeciality
        .filter((a) => typeof a.parentId == "string")
        .map((a) => {
          console.log(a);
          a.parentId = 0;
        });
    }

    speciality.id = this.Scientificform.value.mainChildSpeciality[0].id;
    speciality.name = this.Scientificform.value.mainChildSpeciality[0].name;
    speciality.parentId = this.Scientificform.value.mainParentSpeciality[0].id;

    data.id = this.sciData ? this.sciData.id : 0;
    data.name = this.Scientificform.value.name;
    data.medicalLineId = this.Scientificform.value.medicalLine[0].id;
    data.speciality = speciality;
    data.subSpeciality = [...subspeciality];
    data.ntIds = this.Scientificform.value.nt
      ? this.Scientificform.value.nt.map((a) => a.id)
      : [];
    data.rtIds = this.Scientificform.value.rt
      ? this.Scientificform.value.rt.map((a) => a.id)
      : [];
    data.btIds = this.Scientificform.value.bt
      ? this.Scientificform.value.bt.map((a) => a.id)
      : [];
    data.references = this.Scientificform.value.references
      ? this.Scientificform.value.references
      : [];
    data.alternatives = this.Scientificform.value.alternatives
      ? this.Scientificform.value.alternatives
      : [];
    data.description = this.Scientificform.value.description
      ? this.Scientificform.value.description
      : "";

    if (this.file) {
      this.uploadImage(this.file, (result: string) => {
        data.imagePath = result ? result : this.Scientificform.value.imageUrl;
        this.sciData ? this.updateSubmit(data) : this.CreateSubmit(data);
      });
    } else {
      data.imagePath = this.Scientificform.value.imageUrl;
      this.sciData ? this.updateSubmit(data) : this.CreateSubmit(data);
    }
  }

  CreateSubmit(data: SciNameUpdateRequest) {
    this.SCIservice.add(data)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          this.submitLoader = false;

          if (res.success) {
            this.toast.success("Success");
            this.onSubmitEvent.emit(true);
            return;
          }

          this.toast.error("Something wrong happend.");
          this.onSubmitEvent.emit(null);
        },
        (err) => {
          this.submitLoader = false;
          this.toast.error(err);
          this.onSubmitEvent.emit(null);
        }
      );
  }

  updateSubmit(data: SciNameUpdateRequest) {
    this.SCIservice.update(data)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          this.submitLoader = false;

          if (res.success) {
            this.toast.success("Success");
            this.onSubmitEvent.emit(true);
            return;
          }

          this.toast.error("Something wrong happend.");
          this.onSubmitEvent.emit(null);
        },
        (err) => {
          this.submitLoader = false;
          this.toast.error(err);
          this.onSubmitEvent.emit(null);
        }
      );
  }

}

import { _crawlerService } from "./_crawler.service";
import { OptionView } from "./../../shared/optionView";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { ReplaySubject, Observable, of } from "rxjs";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { CompanyScientificName, CompanyScientificNameRequest } from "./companyScientificName";
import {
  catchError,
  startWith,
  debounceTime,
  switchMap,
  takeUntil,
  map,
} from "rxjs/operators";
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialog, MatCheckboxChange, MatSelectChange
} from "@angular/material";
import { NgxDrpOptions, PresetItem, Range } from "ngx-mat-daterange-picker";
import { SelectionModel } from "@angular/cdk/collections";
import { ToastService } from "src/app/_services/toast.service";
import { IMedicalLine, IParentSpeciality, ISpeciality } from "src/app/pages/new-scientific-name/model/scientific-name.model";
@Component({
  selector: "app-crawler",
  templateUrl: "./crawler.component.html",
  styleUrls: ["./crawler.component.scss"],
})
export class CrawlerComponent implements OnInit, OnDestroy {

  requestData: CompanyScientificNameRequest = new CompanyScientificNameRequest();
  selectedCountries: OptionView[] = [];
  onDestroy$ = new ReplaySubject<void>();
  crawlerForm: FormGroup;
  submitted: boolean = false;

  @ViewChild("dateRangePicker") dateRangePicker;
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];
  range: Range = { fromDate: null, toDate: null };


  manufacturerCtrl = new FormControl(null);
  manufacturers$: Observable<OptionView[]>;
  medicalLineList: IMedicalLine[];
  specialitiesList: IParentSpeciality[];
  subSpecialitiesList: ISpeciality[];
  countries: OptionView[];



  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild("table", { read: MatSort }) sort: MatSort;
  dataSource: MatTableDataSource<CompanyScientificName>;
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = [
    // "select",
    "scientificNameId",
    "scientificName",
    "checked",
    "isApproved",
    "createdAt",
    "companyName",
    "medicalLineName",
    "mainSpeciality",
    "childSpeciality",
    // "specialities",
    "taggedTexts",


    "countryName",
    "language",
    "note",

    "link",
  ];
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];
  isLoading: boolean = false;

  optionList = [      {
    name: 'Yes',
    id: 'true'
  },
  {
    name: 'No',
    id: 'false'
  },]

  constructor(
    private fb: FormBuilder,
    private crawlerService: _crawlerService,
    private toast: ToastService,
    public dialog: MatDialog
  ) {}

  get medicalLineFormControl() {
    return this.crawlerForm.get("medicalLineIds");
  }

  get parentSpecialityFormControl() {
    return this.crawlerForm.get("mainSpecialityCtrl");
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngOnInit() {
    this.createCrawlerForm();
    this.getMedicalLineList();
    this.getCountryList();
    this.getProspectedSciNames();
    this.crawlerService.searchSpeciality(null, 'true')
    .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.specialitiesList = res;
      });
    this.manufacturers$ = this.manufacturerCtrl.valueChanges.pipe(
      startWith(""),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once
      switchMap((value) => {
        if (value !== "") {
          // lookup from github
          return this._filtermanufacturer(value);
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );
    this.setupPresets();
  }

  createCrawlerForm() {
    this.crawlerForm = this.fb.group({
      manufacturerID: new FormControl(
        this.manufacturerCtrl.value,
        Validators.required
      ),
      companyWebSite: new FormControl(""),
      companySceitficName: new FormControl(""),
      isApproved: new FormControl(null),
      checked: new FormControl(null),
      scientficNameIDs: new FormControl(''),
      countryIDs: new FormControl(''),
      medicalLineIds: new FormControl(''),
      specialityIds: new FormControl(''),
      mainSpecialityCtrl: new FormControl(''),
    });

  }

  getCountryList() {
    this.crawlerService
      .getCountries()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.countries = res;
      });
  }

  getMedicalLineList() {
    this.crawlerService.getMedicleLineSuggestion()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.medicalLineList = res.data;
      });
  }

  getProspectedSciNames() {
    this.isLoading = true;
    this.crawlerService
    .getCompanyScientificName(this.requestData)
    .pipe(takeUntil(this.onDestroy$))
    .subscribe((res: any) => {

      this.renderData(res);


      // this.isLoading = false;
      // this.dataSource = new MatTableDataSource(res.items);
      // this.length = res.count;
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;
      // this.selection = new SelectionModel<CompanyScientificName>(true, []);
      // this.dataSource.sortingDataAccessor = (sortData, sortHeaderId) => {
      //   if (!sortData[sortHeaderId]) {
      //     return this.sort.direction === "asc" ? "3" : "1";
      //   }

      //   // tslint:disable-next-line:max-line-length
      //   return /^\d+$/.test(sortData[sortHeaderId])
      //     ? Number("2" + sortData[sortHeaderId])
      //     : "2" + sortData[sortHeaderId].toString().toLocaleLowerCase();
      // };
    }, err => {
      this.isLoading = false;
    });
  }

  renderData(res: any) {
    if(res && res.items) {
      this.length = res.count;
      this.dataSource = new MatTableDataSource<CompanyScientificName>(res.items);
    } else {
      this.length = 0;
      this.dataSource = new MatTableDataSource<CompanyScientificName>([]);
    }
    this.isLoading = false;
  }


  pageChange(event) {
    this.requestData.pageSize = event.pageSize;
    this.requestData.pageNumber = event.pageIndex + 1;
    this.getProspectedSciNames();
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }


  OnCheckedClick(e: MatCheckboxChange, row: CompanyScientificName) {
    this.crawlerService
      .checkUncheck(row, e.checked)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        if (res.success) {
          this.toast.success("Success!");
          this.getProspectedSciNames();
        } else {
          this.toast.error("Oops! Something wrong happened!")
        }
      }, err => this.toast.error("Oops! Something wrong happened!"));
  }

  OnApprovedClick(e: MatCheckboxChange, row: CompanyScientificName) {
    let array: number[] = [];
    array.push(row.prospectedCompanyScientificNameId);
    this.crawlerService
      .approveDisApprove(array, e.checked)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        if (res.success) {
          this.toast.success("Success!");
          this.getProspectedSciNames();
        } else {
          this.toast.error("Oops! Something wrong happened!")
        }
      }, err => this.toast.error("Oops! Something wrong happened!"));
  }

  public requestAutocompleteItems = (text: string): Observable<any> => {
    return this.crawlerService.getScientificNamesSuggestions(text);
  };


  public requestSpecialityAutocompleteItems = (text: string): Observable<any> => {
    return this.crawlerService.getParentSpecialtySuggestion(text);
  };

  public requestSubSpecialityAutocompleteItems = (text: string): Observable<any> => {
    const parentId = this.parentSpecialityFormControl.value ? this.parentSpecialityFormControl.value[0].id : null;
    return this.crawlerService.getSubSpecialitySuggestion(parentId, text);
  };


  private _filtermanufacturer(value: any): Observable<OptionView[]> {
    if (value) {
      return this.crawlerService
        .getCompanyManfacturerByNameAndType(
          typeof value === "string" ? value.toLowerCase() : value.name,
          "manufacturer"
        )
        .pipe(
          // map the item property of the github results as our return object
          // catch errors
          map((item) => item.filter((manu) => manu.id !== 0)),
          catchError((_) => {
            return of(null);
          })
        );
    } else {
      return of(null);
    }
  }



  updateRange(range: Range) {
    this.range = range;
  }



  setupPresets() {

    this.options = {
      presets: this.presets,
      format: "mediumDate",
      range: { fromDate: null, toDate: null },
      placeholder: "Creation Date",
      applyLabel: "Submit",
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: true,
      },
    };

    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    };

    const today = new Date();
    const yesterday = backDate(1);
    const minus7 = backDate(7);
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(
      today.getFullYear(),
      today.getMonth() - 1,
      1
    );
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      {
        presetLabel: "Yesterday",
        range: { fromDate: yesterday, toDate: today },
      },
      {
        presetLabel: "Last 7 Days",
        range: { fromDate: minus7, toDate: today },
      },
      {
        presetLabel: "Last 30 Days",
        range: { fromDate: minus30, toDate: today },
      },
      {
        presetLabel: "This Month",
        range: { fromDate: currMonthStart, toDate: currMonthEnd },
      },
      {
        presetLabel: "Last Month",
        range: { fromDate: lastMonthStart, toDate: lastMonthEnd },
      },
    ];
  }


  displayFnManufacturer(manufacturer: OptionView): string {
    return manufacturer && manufacturer.name ? manufacturer.name : "";
  }




  onSelectApprovedChange(e: MatSelectChange) {
    console.log(e)
  }

  onSelectCheckedChange(e: MatSelectChange) {
    console.log(e)
  }

  onSubmit() {
    this.crawlerForm.controls.manufacturerID.patchValue(
      this.manufacturerCtrl.value ? this.manufacturerCtrl.value.id : null
    );
    this.requestData = {
      companyId: this.crawlerForm.value.manufacturerID,
      mainSpecialityIds: this.crawlerForm.value.mainSpecialityCtrl && this.crawlerForm.value.mainSpecialityCtrl.length ? this.crawlerForm.value.mainSpecialityCtrl.map((item) => item.id) : null,
      medicalLineIds: this.crawlerForm.value.medicalLineIds && this.crawlerForm.value.medicalLineIds.length ? this.crawlerForm.value.medicalLineIds.map((item) => item.id) : null,
      scientificNameIds: this.crawlerForm.value.scientficNameIDs && this.crawlerForm.value.scientficNameIDs.length ? this.crawlerForm.value.scientficNameIDs.map((item) => item.id) : null,
      countryIds: this.crawlerForm.value.countryIDs && this.crawlerForm.value.countryIDs.length > 0 ? this.crawlerForm.value.countryIDs.map((item) => item.id) : null,
      specilaityIds: this.crawlerForm.value.specialityIds && this.crawlerForm.value.specialityIds.length > 0 ? this.crawlerForm.value.specialityIds.map((item) => item.id) : null,
      fromCreatedAt: null,
      toCreatedAt: null,
      webSite: this.crawlerForm.value.companyWebSite !== "" ? this.crawlerForm.value.companyWebSite : null,
      isApproved: this.crawlerForm.value.isApproved === 'null' ? null : this.crawlerForm.value.isApproved,
      checked: this.crawlerForm.value.checked  === 'null' ? null : this.crawlerForm.value.checked,
      pageNumber: 1,
      pageSize: this.pageSize,
    };

    debugger
    if (this.range.fromDate && this.range.toDate) {
      this.requestData.fromCreatedAt = this.range.fromDate.toLocaleString();
      this.requestData.toCreatedAt = this.range.toDate.toLocaleString();
    }

    this.getProspectedSciNames();
  }

  disApprove(element: CompanyScientificName) {
    let array: number[] = [];
    array.push(element.prospectedCompanyScientificNameId);
    this.crawlerService
      .approveDisApprove(array, true)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        if (res.success) {
          this.toast.success("The opperation successfully done");
          //   this.onSubmit();
        }
      });
  }


  approve(element: CompanyScientificName) {
    let array: number[] = [];
    array.push(element.prospectedCompanyScientificNameId);
    this.crawlerService
      .approveDisApprove(array, false)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        if (res.success) {
          this.toast.success("The opperation successfully done");
          // this.onSubmit();
        }
      });
  }


  openDialog(data: string[]): void {
    const dialogRef = this.dialog.open(LinkDialog, {
      width: "450px",
      data: data,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}



@Component({
  selector: "link-dialog",
  templateUrl: "./link-dialog.html",
})
export class LinkDialog {
  constructor(
    public dialogRef: MatDialogRef<LinkDialog>,
    @Inject(MAT_DIALOG_DATA) public data: string[]
  ) {}

  close(): void {
    this.dialogRef.close();
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent implements OnInit {
  @Input() mainData;
  @Output() dataFromMulti = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }
  handleAdd(event) {
  }

  handleRemove(event) {
  }

  handleAdding(event) { }

  handleTextChange(event) {
  }

}

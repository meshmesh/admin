export interface Country {
  id: number;
  code: string;
  logo: string;
  country: string;
  display: string;
}

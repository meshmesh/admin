import { IFilterOption } from "src/app/filter/filters.model";


export const BusinessOppFilterOptions: Array<IFilterOption> = [

  {
    index: 0,
    order: 1,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminCompany/CompanyList?Type=manufacturer&NameOrSlug=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Manfacturer Name',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'ManfacturerIds'
  },
  {
    index: 1,
    order: 2,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminCompany/CompanyList?Type=distributor&NameOrSlug=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Distributor Name',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'DistributorIds'
  },
  {
    index: 2,
    order: 3,
    type: 'rangeDate',
    value: [],
    placeholder: 'Sent on date from',
    // default: '1/1/2000',
    searchQuery: '',
    querystring: 'SendDateFrom',
  },
  {
    index: 3,
    order: 4,
    type: 'rangeDate',
    value: [],
    placeholder: 'Sent on date to',
    // default: '1/1/2090',
    searchQuery: '',
    querystring: 'SendDateTo',
  },
  {
    index: 4,
    order: 5,
    type: 'rangeDate',
    value: [],
    placeholder: 'Finishes on date from',
    // default: '1/1/2000',
    searchQuery: '',
    querystring: 'FinishDateFrom',
  },
  {
    index: 5,
    order: 6,
    type: 'rangeDate',
    value: [],
    placeholder: 'Finishes on date to',
    // default: '1/1/2090',
    searchQuery: '',
    querystring: 'FinishDateTo',
  },
  {
    index: 6,
    order: 7,
    type: 'toggel',
    value: false,
    selected: [],
    placeholder: 'Active',
    searchQuery: '',
    querystring: 'IsActive'
  },
  {
    index: 7,
    order: 8,
    type: 'toggel',
    value: false,
    selected: [],
    placeholder: 'Scheduled Call',
    searchQuery: '',
    querystring: 'ScheduledCall'
  },
  {
    index: 8,
    order: 9,
    type: 'toggel',
    value: false,
    selected: [],
    placeholder: 'Uploaded Registration certificate',
    searchQuery: '',
    querystring: 'HaveCertificate'
  }
]

export class DUser {
  company: string;
  companyBussinessUser: Array<any>;
  companyId: number;
  companySalesUser: Array<any>;
  contractComment: Array<any>;
  country: string;
  countryId: number;
  createdAt: string;
  createdBy: string;
  deletedAt: string;
  deletedBy: string;
  email: string;
  firstName: string;
  id: number;
  isAdmin: string;
  isAuthorized: string;
  isCorrectEmail: string;
  isOtpConfirmed: string;
  jobTitle: string;
  lastName: string;
  manufacturerContract: Array<any>;
  manufacturerContractComment: Array<any>;
  password: string;
  phoneNumber: string;
  pipeDriveId: string;
  profileImage: string;
  requestApprovalCompanyAdmin: Array<any>;
  requestApprovalUser: Array<any>;
  stage: string;
  suggestion: Array<any>;
  token: string;
  tracking: Array<any>;
  unsbscribedDate: string;
  updatedAt: string;
  updatedBy: string;
  verificationCode: string;
  xrefId: string;
}

export class SignedAgreementResponse {
  data: {
    count: number;
    items: Array<SignedAgreement>;
  };
  errorCode: number;
  status: boolean;
}

export class SAManuCompany {
  logo: string;
  token: string;
  name: string;
}
export class SAManu {
  id: number;
  company: SAManuCompany;
}

export class SignedAgreement {
  signedDate: string;
  manufacture: SAManu;
  distributor: SAManu;
}

export class Cert {
  id: number;
  link: string;
}

export class BusinessOpp {
  count: number;
  distributorID: number;
  distributorName: string;
  distributorToken: string;
  manufacturerID: number;
  manufacturerName: string;
  manufacturerToken: string;
  opportunityComment: string;
  opportunityFinshedDate: string;
  opportunityID: number;
  opportunityScheduledCall: number;
  opportunitySendDate: string;
  userEmail: string;
  userID: number;
  userName: string;
  distributorCertificates: Array<Cert>;
  users: Array<DUser>;
  active: boolean;
  distributorComment: string;

}

export class BusinessOppSearchParams {
  // required and default
  PageNumber: number = 1;
  PageSize: number = 10;

  ManfId: string;
  DistId: string;
  IsActive: boolean;
  ScheduledCall: boolean;
  SendDateFrom: string;
  SendDateTo: string;
  FinishDateFrom: string;
  FinishDateTo: string;
}

export class SASearchParams {
    // required and default
    PageNumber: number = 1;
    PageSize: number = 10;
}

export class SACreate {
  manufactureId: number;
  distributorId: number;
  signedDate: string;
  status: number;
  countryId: number;
  contractValue: number;
  
}

export class BusinessOppResponse {
  data: {
    count: number;
    items: Array<BusinessOpp>;
  };
  errorCode: number;
  status: boolean;
}


export class ManuForSearch {
  desc: string;
  image: string;
  key: string;
  location: string;
  manufacturerId: number;
  name: string;
  slug: string;
  text: string;
  token: string;
}

export class IManuListResponse {
  data: Array<ManuForSearch>;
  errorCode: number;
  success: boolean;
}
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ViewAllTableChipsModalComponent } from './view-all-table-chips-modal/view-all-table-chips-modal.component';

@Component({
  selector: 'app-table-chips',
  templateUrl: './table-chips.component.html',
  styleUrls: ['./table-chips.component.scss']
})
export class TableChipsComponent implements OnInit {
  constructor(private dialog: MatDialog) {}

  @Input() items: string[];

  ngOnInit() {}

  onViewAll(): void {
    const dialogRef = this.dialog.open(ViewAllTableChipsModalComponent, {
      data: this.items
    });
  }
}

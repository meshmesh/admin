import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import * as S3 from 'aws-sdk/clients/s3';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ReplaySubject, Observable, of } from 'rxjs';
import { takeUntil, map, catchError } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';
import { NgForm } from '@angular/forms';
import { ProductCatalogService } from '../../services/products-catalog.service';

@Component({
  selector: 'app-add-edit-catalog',
  templateUrl: './add-edit-catalog.component.html',
  styleUrls: ['./add-edit-catalog.component.scss']
})
export class AddEditCatalogComponent implements OnInit , OnDestroy {


  basic_title: string = 'Add';
  onDestroy$ = new ReplaySubject<void>();


  catalogLink: string;
  catalogTitle = '';
  catalogUrl = '';
  showCatType: boolean = false;
  catalogChossen: number;
  finalData: any = {};
  finalImage: any = {};
  allcatalogs = [];
  seoData = [];
  company : any;
  localCatalogImage: File[] = [];
  catalogImagePreview: any[] = [];
  catalogId: number;
  allCompany = [];
  manufacturerFilter: Observable<any>;
  isImageToDisplay: boolean = false;
  uploadedFileName: string = 'Click to Choose file from your PC'
  submitLoader: boolean;


  constructor(
    private productCatalogService: ProductCatalogService,
    private route: ActivatedRoute,
    private router: Router,
    public dialogRef: MatDialogRef<AddEditCatalogComponent>,
    private toast: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnInit() {
    this.getManuListSugg();
    this.getSeoDataPortals();


    if (this.data && this.data.id) {
      this.catalogId = this.data.id;
      this.company = this.data.company;
      this.catalogTitle = this.data.name;
      this.catalogUrl = this.data.id;
      this.catalogChossen = this.data.id;
      this.catalogLink = this.data.link;
      this.allcatalogs.push({
        value: this.data.id,
        display: this.data.name
      });
      this.showCatType = true;
    }
  }


  _filterManufacturer(value: any): void {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    this.productCatalogService.getManufacturersuggestions(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.manufacturerFilter = response.data;
      },
      error => console.log(error)
    );
  }


  doFilterManufacturer(value: any): Observable<any> {
    if (typeof value === 'string') {
      return this.productCatalogService.getManufacturersuggestions(value.toLowerCase()).pipe(
        // map the item property of the github results as our return object
        // catch errors
        map(item => item.filter(manu => manu.manufacturerId !== 0)),
        catchError(_ => {
          return of(null);
        })
      );
    } else {
      return of(null);
    }

  }


  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  getManuListSugg(paramKey: string = 'a') {
    this.productCatalogService.getManufacturersuggestions(paramKey).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        if(response && response.data) {
          this.manufacturerFilter = response.data;
        }

      },
      error => console.log(error)
    );
  }

  onCompanySelected(e) {
    console.log(e)
  }

  getSeoDataPortals() {
    this.productCatalogService.getPortals().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        for (let i = 0; i < response.length; i++) {
          const name = response[i].name;
          const id = response[i].id;
          this.seoData.push({
            name: name,
            id: id,
            metaTitle: '',
            metaDescription: '',
            metaKeyWord: ''
          });
        }
      },
      error => console.log(error)
    );
  }


  showCat() {
    this.showCatType = !this.showCatType;

    // if(this.showCat) this.catalogImagePreview.pop();
  }

  hideCatalogImage() {
    this.catalogImagePreview.pop();
    this.localCatalogImage.pop();
  }

  handleCatalogChange(e) {
    this.localCatalogImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = e => {
        this.catalogImagePreview.pop();
        this.catalogImagePreview.push(reader.result);
        this.uploadedFileName = this.localCatalogImage[0].name;
        if(typeof reader.result === 'string' && reader.result.startsWith('data:image'))  {
          this.isImageToDisplay = true;
        } else {
          this.isImageToDisplay = false;
        }
      };
    }
  }





  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }


  uploadCatalogToAWS(file, callbackFn) {
    
    const params = {
      Bucket: 'aumet-data',
      Key: 'product/catelogues/' + Guid.create() + '.' + this.getFileExtension(file.name),
      Body: file
    };
    const _this3 = this;
    _this3.handleUpload(params, function (resultc) {
      callbackFn(resultc);
    });
  }


  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function(err, data) {

      console.log("err >> s3", err)
      console.log("data >> s3", data)
      if (err) {
        images(false);
      }
      images(data.Location);
    });
  }



  submit(form: NgForm) {

    console.log(form)

    if(!form.valid) {
      this.toast.error("Please fill all reuquired fields!")
      return;
    }

    this.submitLoader = true;
    if (this.localCatalogImage.length > 0) {

      this.uploadCatalogToAWS(this.localCatalogImage[0], (res: any) => {
        debugger
        this.finalImage = res;
        const data = this.fillData();
        this.addOrUpdate(data);
      });

    } else {
      const data = this.fillData();
      this.addOrUpdate(data);
    }
  }


   csvInputChange(fileInputEvent: any) {

    console.log(fileInputEvent)
    console.log(fileInputEvent.target.files[0])
  }



  fillData() {
    if (this.catalogId) {
      // const x = parseInt(this.catalogId);
      this.finalData.id = this.catalogId;
    }

    if (this.showCatType) {
      this.finalData.productrangeId = this.catalogChossen;
    } else {
      this.finalData.CatalogURL = this.finalImage;
    }

    debugger
    this.finalData.name = this.catalogTitle;
    this.finalData.companyId = this.company.id;
    this.finalData.seo = JSON.stringify(this.seoData);
    return this.finalData;
  }



  addOrUpdate(data) {
    this.productCatalogService.addUpdateCatalog(data).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.submitLoader = false;
        this.toast.success('Successfully Added!')
        this.dialogRef.close(true);
      },
      error => {
        console.log(error);
        this.submitLoader = false;
        this.toast.success('Oops! Something wrong happened');
      }
    );
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}

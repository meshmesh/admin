export class Bulk {
    Id: number;
    baseImageFileName: string;  // Product Image 
    catalogueId: string;// Catalog Id  to be mapped with Catalog 
    description: string;// Product Description ,and we need to remove the description keyword 
    imageUrls: string;//JSON Contains array of Images 
    images: string;//JSON Contains array of Images 
    logoFileName: string;// Logo 
    manufacturerName: any; //Manufacture Name to be displayed if the company Id not matched or deleted
    medicalexpoLink: string; // Link for medical Expo
    productCatalogueLink: string;// Link for medical Expo
    productId: string;//Serial #

    productLink: string[]= []; // as note for the source of the Data.
    productName: string = ''; // Product Sub Title 
    productSubtitle: string = '';   // Product Subtitle
    refererLink: string[]= []; // as note for the source of the Data.

    scientificName: any;// to be displayed if the Scintific Id is not there or deleted 
    specialty: any;// Speciaity Name -- to be displayed if the Scintific Id is not there or deleted 
    subSpecifications: string;// To be Displayed as Note 
    type: string; // its alwys as Product (No Need)
    status: string; //"imported" 35 ,"saved" 4, "default" 53361 

    madeIn: any;//Country
    companyId: number;// Manufacture Id
    specialtyId: number;// Speciality 
    scientificNameId: number;// Scientific Id
    createdAt: string;
    updatedAt: string;
    sellingPoints:any;
    buyers:any[] = [];
    speciality : any;
    scientificname: any;
}

import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, OnChanges } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
  animations: []
})
export class ImageUploadComponent implements OnInit, OnChanges {
  constructor() {}

  @Input() title: string;
  @Input() imageList: any[] = [];
  @Output() imageListChange = new EventEmitter();
  @Input() multiple: boolean;
  @Input() max: number;
  @Input() required: boolean;
  @Input() valid: boolean;
  @Output() validChange = new EventEmitter<boolean>();
  imagesPreview: any[] = [];
  valid$: Subject<'valid' | 'invalid'> = new Subject();
  @ViewChild('uploadContainer') uploadContainer: ElementRef;

  ngOnChanges(change) {
    // Parent component takes time to get the url of the image to edit,
    // Watch for value changes, and add unique and only http url to the images preview,
    // otherwise it will keep adding new selected files to the list
    if(this.imageList){
    this.imageList.forEach((image: any) => {
      const exists = this.imagesPreview.includes(image);
      const isUrl = typeof image === 'string';
      if (!exists && isUrl) {
        this.imagesPreview.push(image);
      }
    });
  }
  }

  ngOnInit() {
    this.valid = true;
  }

  onFileSelected(e) {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageList.push(file);
      this.imagesPreview.push(reader.result);
      this.imageListChange.emit(this.imageList);
      this.switchValidation(true);
    };
  }

  onImageRemove(image: File | string): void {
    // const listIndex = this.imageList.findIndex(e => e === image);
    // const previewIndex = this.imagesPreview.findIndex(e => e === image);
    // this.imageList.splice(listIndex, 1);
    // this.imagesPreview.splice(previewIndex, 1);
    // this.imageListChange.emit(this.imageList);
    this.imageList = this.imageList.filter(e => e !== image);
    this.imagesPreview = this.imagesPreview.filter(e => e !== image);
    this.imageListChange.emit(this.imageList);
    this.switchValidation(false);
  }

  switchValidation(status: boolean): void {
    if (this.required) {
      this.valid = status;
    }
  }
  prevent(e: Event) {
    e.preventDefault();
  }
}

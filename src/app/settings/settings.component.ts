import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SettingsService } from '../settings/settings.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private http: HttpClient, private serv: SettingsService) { }

  values: any[] = [];
  dataSource: MatTableDataSource<any>;
  SearchKey: string;
  displayedColumns = ['key', 'name', 'value', 'description'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selected: any;
  adding: any = {};

  
  obj: any = {
    Id: 0,
    Name: null,
    Value: null,
    Description: '',
    Active: true,
    Admin: false
  }



  ngOnInit() {
    this.loadmajor();


  }
  AddUpdate(form) {
    this.obj.Id = this.adding.id;
    this.obj.Key = this.adding.key;
    this.obj.Name = this.adding.name;
    this.obj.Value = this.adding.value;
    this.obj.Description = this.adding.description;
    this.obj.Active = true;
    console.log(this.obj);
    this.serv.saveEdit(this.obj).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.loadmajor();
    })
  }

  del(form) {
    this.obj.Id = this.adding.id;
    this.obj.Key = this.adding.key;
    this.obj.Name = this.adding.name;
    this.obj.Value = this.adding.value;
    this.obj.Description = this.adding.description;
    this.obj.Active = false;
    console.log(this.obj);
    this.serv.saveEdit(this.obj).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.loadmajor();
    })
  }


  loadmajor() {
    this.serv.getAdmin().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        this.renderDataSource(response);
        this.paginDataSource(response);
      },
      error => {
        console.log(error);
      }
    );

  }


  renderDataSource(data) {
    this.dataSource = new MatTableDataSource<any>(data);
  }

  paginDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  Filter(SearchKey) {
    this.dataSource.filter = this.SearchKey.trim().toLowerCase();
  }

  onOptionsSelected() {
    console.log(this.selected);

    if (this.selected == '0') {
      let filterData = this.values
      this.renderDataSource(filterData);
      this.paginDataSource(filterData)
    }
    else {
      let filterData = this.values.filter(t => t.majorId == this.selected);
      this.renderDataSource(filterData);
      this.paginDataSource(filterData)
    }
  }

  displaymajors(row) {

    this.adding = row;

  }


}

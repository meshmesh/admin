import { ISpeciality } from "src/app/pages/new-scientific-name/model/scientific-name.model";

export interface CompanyScientificName {
    companyName: string;
    countryName: string;
    prospectedCompanyScientificNameId: number;
    taggedTexts?: any;
    language?: any;
    scientificName: string;
    links?: any;
    isApproved?: any;
    note?: any;
    createdAt: Date;
    count: number;
    isChecked: boolean;
    scientificNameID:  number;
    specialities: Array<ISpeciality>;
    medicalLineName: any

}


export class CompanyScientificNameRequest {
    mainSpecialityIds: Array<number>;
    specilaityIds: Array<number>;
    medicalLineIds: Array<number>;
    checked: boolean;
    companyId: number;
    scientificNameIds: Array<number>;
    countryIds: Array<number>;
    fromCreatedAt: string;
    toCreatedAt: string;
    webSite: string;
    isApproved: boolean
    pageNumber: number = 1;
    pageSize: number = 10;
  }




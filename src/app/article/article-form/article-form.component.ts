import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { ArticleService } from '../article.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Article, ArticleRequest } from 'src/app/_model/article';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, takeUntil, tap, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { NewSeoModalComponent } from 'src/app/shared/seo-modal/seo-modal.component';
import { SEO } from 'src/app/_model/SEO';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';

import { ToastService } from 'src/app/_services/toast.service';
export interface ArticleForm {
  title: string;
  subtitle: string;
  description: string;
  details: string;
  maincategory: number;
  published: boolean;
  tags: string[];
}

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent implements OnInit, OnDestroy {
  /**
   *  Categories' dropdown options
   */
  categorie$: Observable<any[]>;
  /**
   * Article's FormGroup
   */
  articleForm: FormGroup;
  /**
   * Destruction Subject
   */
  onDestroy$: Subject<void> = new Subject();
  /**
   * Primary Image List, passed to image upload component
   */
  priamryImage = [];
  /**
   * Secondary Images List, passed to image upload component

   */
  secondryImages = [];
  /**
   * Empty SEO Array
   */
  SEO: SEO[] = [
    { name: 'International', metaDescription: '', metaKeyWord: '', metaTitle: '' },
    { name: 'Jordan', metaDescription: '', metaKeyWord: '', metaTitle: '' },
    { name: 'KSA', metaDescription: '', metaKeyWord: '', metaTitle: '' },
    { name: 'Iraq', metaDescription: '', metaKeyWord: '', metaTitle: '' },
    { name: 'UAE', metaDescription: '', metaKeyWord: '', metaTitle: '' }
  ];
  /**
   * Primary Image Validation Status
   */
  primaryImageValid = true;
  /**
   * List of current tags
   */
  tags: string[] = [];
  /**
   * Article's ID (If Edit)
   */
  articleID: number;


  type:any;


  constructor(
    private articleService: ArticleService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private toast: ToastService,
    private router: Router
  ) {}

  ngOnInit() {
    this.categorie$ = this.getCategories();
    this.articleForm = this.createEmptyForm();
    this.getArticleById()
      .pipe(tap((article: Article) => this.fillEditForm(article)))
      .subscribe();
  }

  /**
   * Get Categories for articles
   */
  getCategories(): Observable<Article[]> {
    return this.articleService.getMainCategory();
  }

  /**
   * Create an initial empty form object
   */
  createEmptyForm(): FormGroup {
    const form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      subtitle: new FormControl('', [Validators.required]),
      maincategory: new FormControl(null, [Validators.required]),
      tags: new FormControl([]),
      description: new FormControl(''),
      details: new FormControl(''),
      published: new FormControl(false)
    });
    return form;
  }

  /**
   * If url has an id, a get request is dispatched to get the article's data
   */
  getArticleById(): Observable<Article> {
    return this.activatedRoute.paramMap.pipe(
      takeUntil(this.onDestroy$),
      switchMap(params => {
        const id: number = parseInt(params.get('id'));
        if (id) {
          this.articleID = id;
          return this.articleService.getArticlesById(id);
        } else {
          return of(null);
        }
      })
    );
  }

  /**
   * Fills the form with a specific article data to edit it
   * @param article Article data to fill the form with
   */
  fillEditForm(article?: Article): void {
    if (article) {
      this.articleForm.setValue({
        title: article.title,
        subtitle: article.subtitle,
        maincategory: article.maincategory,
        tags: article.tags,
        description: article.description,
        details: article.details,
        published: article.published
      });
      this.priamryImage = [article.articleimage];
      this.secondryImages = article.subimages;
      this.tags = article.tags;
    }
  }

  /**
   * Open SEO form dialog, updates the current SEO object if a value is returned
   */
  onSEO(): void {
    const dialogRef = this.dialog.open(NewSeoModalComponent, {
      data: this.SEO
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(d => {
        this.SEO = d ? d : this.SEO;
      });
  }

  addTag(name: string): { name: string; tag: boolean } {
    return { name, tag: true };
  }

  /**
   * Iterate and check, then upload to Amzon S3 only if it's Base6
   * @param images Array of Images
   *
   */
  async handleImages(images: any[]) {
    return new Promise(async (resolve, reject) => {
      const newImages = [];
      for (const image of images) {
        const uploadedURl = await this.handleImage(image);
        newImages.push(uploadedURl);
      }
      resolve(newImages);
    });
  }

  /**
   * Input an image string, if url returns the same value, if base64 uploads and resolves uploaded url
   * @param image Image string to handle, might be base64 or HttpUrl
   */
  async handleImage(image: string | File) {
    return new Promise(async (resolve, reject) => {
      if (typeof image !== 'string') {
        const url = await this.uploadImageToS3(image);
        resolve(url);
      } else {
        resolve(image);
      }
    });
  }

  /**
   * Returns file extension from filename
   * @param filename Name of the file to be uploaded
   */
  getFileExtension(filename: string): string {
    const startIndex = filename.indexOf('.');
    const extenstion = filename.slice(startIndex + 1);
    return extenstion;
  }

  /**
   * Upload file(image) to amazon S3
   * @param file File to be uploaded
   */
  uploadImageToS3(file) {
    if(file){

  
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'product/Article/' + Guid.create() + '.' + this.getFileExtension(file.name),
      Body: file
    };
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    return new Promise((resolve, reject) => {
      bucket.upload(paramsProduct, {}, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data.Location);
        }
      });
    });
  }
  }

  /**
   * Return the article request body's object
   */
  async createRequestObject() {
    const formData: ArticleForm = this.articleForm.getRawValue();
    const primaryImage = await this.handleImage(this.priamryImage[0]);
    const secondryImages = await this.handleImages(this.secondryImages);
    // Case Sensitive
    const object: ArticleRequest = {
      Title: formData.title,
      Subtitle: formData.subtitle,
      Categories: [],
      CategoryId: formData.maincategory,
      subCategory: {},
      Tags: formData.tags,
      Published: formData.published,
      Details: formData.details,
      Description: formData.description,
      Seo: JSON.stringify(this.SEO),
      Portals: [],
      Articleimage: primaryImage,
      Subimages: secondryImages
    };

    if (this.articleID) {
      object.id = this.articleID;
    }
    return object;
  }

  /**
   * Sequence of Actinos on save button click
   */
  async onSave() {
    if (!this.primaryImageValid || this.articleForm.invalid) {
      this.toast.error('Invalid Form');
    } else {
      const requestObject: ArticleRequest = await this.createRequestObject();
      this.articleService
        .createUpdateArticle(requestObject)
        .pipe(
          take(1),
          tap(() => {
            this.router.navigate(['Articles']);
          })
        )
        .subscribe();
    }
  }

  /**
   * Route back to article's page
   */
  onCancel(): void {
    this.router.navigate(['Articles']);
  }

  /**
   * Watch for component destruction to unsubscribe from any observables to prevent memory leaks
   */
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}

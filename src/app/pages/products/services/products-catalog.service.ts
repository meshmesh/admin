import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/_services/auth.service';

const APIEndpoint = environment.ApiUrl;
const newAPIEndpoint = environment.ApiUrlV3;

@Injectable()
export class ProductCatalogService {
  allSellingPoints: any = [];
  allSpeciality: any[] = [];
  allBuyers: any[] = [];
  AllmidicalLine: any[] = [];
  allMadeIn: any[] = [];


  constructor(
    private http: HttpClient,       
    private authenticationService: AuthService) {}



  search(args, pageNumber) {
    let url = APIEndpoint + '/api/AdminProducts/SearchProducts?PageSize=10&PageNumber=' + pageNumber;
    // const a = url;
    for (let i = 0; i < args.length; i = i + 2) {
      url = url + '&' + args[i] + '=' + args[i + 1];
    }
    return this.http.get(url);
  }


  getBuyers() {
    const url = APIEndpoint + '/api/Lookups/Get?MajorCode=3';
    return this.http.get(url);
  }

  getSpeciality() {
    const url = newAPIEndpoint + 'Specialties/Search?ForAdmin=true';
    return this.http.get(url);
  }

  getSellingPoints() {
    const url = APIEndpoint + '/api/Lookups/Get?MajorCode=2';
    return this.http.get(url);
  }

  getMadeIn() {
    const url = newAPIEndpoint + 'Countries';
    return this.http.get(url);
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url = newAPIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1${str}`;
    return this.http.get<any>(url);
  }

  getCountrySuggestion() {
    const url = newAPIEndpoint + 'Countries';
    return this.http.get(url);
  }

  getPortals() {
    return this.http.get(APIEndpoint + '/api/portals/getportals');
  }


  getCatalogsByCompanyId(id) {
    const url = newAPIEndpoint + 'AdminProduct/GetCatalog?PageSize=100&PageNumber=1&CompanyId=' + id;
    return this.http.get(url);
  }

  getManufacturersuggestions(str): Observable<any> {
    const url = newAPIEndpoint + 'AdminCompany/CompanyList?NameOrSlug=' + str + '&Type=manufacturer';
    return this.http.get<any>(url);
  }

  addUpdateProduct(obj) { 
    const url = APIEndpoint + '/api/AdminProducts/AddUpdate';
    return this.http.post(url, obj, {
      headers: new HttpHeaders({
      'Authorization': 'Bearer '+ this.authenticationService.currentUserValue.jwt ,
      'Accept': 'application/json'
    })
   });
  }


  addUpdateCatalog(obj) {
    const url = APIEndpoint + '/api/AdminCatalog/AddUpdate';
    return this.http.post(url, obj);
  }


}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { CountryPageService } from './countryPage.service';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { AddCountryComponent } from './addCountry/addCountry.component';
import { ReplaySubject, of } from 'rxjs';
import { takeUntil, switchMap, take, tap } from 'rxjs/operators';
import { NewSeoModalComponent } from '../shared/seo-modal/seo-modal.component';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-countryPage',
  templateUrl: './countryPage.component.html',
  styleUrls: ['./countryPage.component.scss']
})
export class CountryPageComponent implements OnInit, OnDestroy {
  onDestroy$ = new ReplaySubject<void>();
  dataSource: MatTableDataSource<any>;
  paginator: MatPaginator;
  displayedColumns: any[] = ['Actions', 'Country', 'Flag', 'NumberofStories'];
  length = 1;
  pagenumber = 1;
  searchKey = '';
  filteredString = '';
  pageSize = 10;
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  constructor(private serv: CountryPageService,     private _clipboardService: ClipboardService,
    private dialog: MatDialog) { }
  ngOnInit() {
    this.serv
      .getCOuntryPage(this.pagenumber, this.pageSize)
      .subscribe((response: any) => {
        this.renderTable(response);

        this.length = response[0].totalNumber;
      });
  }

  renderTable(data) {
    this.dataSource = new MatTableDataSource(data);
    this.length = data[0].totalNumber;
    this.dataSource.paginator = this.paginator;
  }

  pageChange(event) {
    this.pagenumber = event.pageIndex + 1;

    this.serv
      .getCOuntryPage(this.pagenumber, this.pageSize)
      .subscribe((res: any) => {
        this.renderTable(res);
      });
  }

  onCreateCountry() {
    const dialogRef = this.dialog.open(AddCountryComponent, {
      width: '80%',
      height: '80%'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(result => {
        console.log('The dialog was closed');
        this.serv
          .getCOuntryPage(this.pagenumber, this.pageSize)
          .subscribe((response: any) => {
            this.renderTable(response);
          });
      });
  }

  copylink(row){
    window.open("https://www.aumet.me/country/" +row.country.name, "_blank");   


  }
  onDelete(id) {
    this.serv
      .DeleteCountry(id)
      .subscribe(e => {
        this.serv
          .getCOuntryPage(this.pagenumber, this.pageSize)
          .subscribe((response: any) => {
            
            this.renderTable(response);
          });
        console.log(e);
      });
  }
  Unpublished(id) {

    this.serv.unPublish(id).subscribe(res => {
      this.serv
        .getCOuntryPage(this.pagenumber, this.pageSize)
        .subscribe((response: any) => {
          
          this.renderTable(response);
        });
    })

  }
  published(id) {
    
    this.serv.Publish(id).subscribe(res => {
      this.serv
        .getCOuntryPage(this.pagenumber, this.pageSize)
        .subscribe((response: any) => {
          
          this.renderTable(response);
        });
    })

  }
  onEditCountry(id) {
    this.serv.getCountryById(id).subscribe(response => {
      const dialogRef = this.dialog.open(AddCountryComponent, {
        width: '80%',
        height: '65%',
        data: response
      });
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(result => {
          console.log('The dialog was closed');
          this.serv
            .getCOuntryPage(this.pagenumber, this.pageSize)
            .pipe(takeUntil(this.onDestroy$))
            // tslint:disable-next-line: no-shadowed-variable
            .subscribe((response: any) => {
              this.renderTable(response);
            });
        });
    });
  }
  onOpenSEO(row: any): void {
    
    const dialogRef = this.dialog.open(NewSeoModalComponent, {
      data: [row.seoData],
      minWidth: '650px',
      minHeight: '500px'
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap(seo => {
          if (seo) {
            const s = JSON.stringify(seo);
            const obj = {
              Id: row.id,
              Items: [
                {
                  Key: 'SEO',
                  Value: s
                }
              ]
            };
            
            return this.serv.UpdateSEO(obj);
          } else {
            return of(null);
          }
        }),
        tap(res => {
          if (res) {
            this.serv
          .getCOuntryPage(this.pagenumber, this.pageSize)
          .subscribe((response: any) => {
            this.renderTable(response);
          });
          }
        })
      )
      .subscribe();
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggel',
  templateUrl: './toggel.component.html',
  styleUrls: ['./toggel.component.scss']
})
export class ToggelComponent implements OnInit {
  @Input() mainData;
  @Output() dataFromToggel = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  handleToggel(event) {
    this.mainData.value = event;
    this.dataFromToggel.emit(this.mainData);
  }
}

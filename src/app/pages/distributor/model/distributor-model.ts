export class DistributorModel {

    DistributorId: number;
    IsUplodedLogo: boolean;
    IsUploadBanner: boolean;
    IsPersonalProfile: boolean;
    IsVerified: boolean;
    HaveOpportunities: boolean;
    HaveOpportunitiesDetails: boolean;
    HaveBrands: boolean;
    HaveAdmin: boolean;
    CompanySupervises: number[] = [];
    Deals: boolean;
    Client: boolean;
    Registed: boolean;
    Closing: boolean;
    Patent: boolean;
    Fav: boolean;
    BusinessTeam_Call: boolean;
    QA_Call: boolean;
    HaveMatching: boolean;
    CompanyName: string;
    UTM_Source: string;
    UTM_Campaign: string;
    UTM_Content: string;
    AnnualSales: string[] = [];
    CompanyNumberOfEmployee: string[] = [];
    CountryIds: number[] = [];
    Satisfactions: number[] = [];
    Stages: number[] = [];
    Files: number[] = [];
    MedicalLineIds: number[] = [];
    ScintficNameIds: number[] = [];
    SpecialityIds: number[] = [];
    RegistrationFromDate: string;
    RegistrationToDate: string;
    ManufactureCountryids: number[] = [];
    TotalOrderFrom: number;
    TotalOrderTo: number;
    PageNumber: number;
    PageSize: number;
    SortBy: string;
    OrderBy: string;




    annualSales: string;
    authorized: boolean;
    comment: string;
    companyId: number;
    country: any
    id: number;
    isActive: boolean;
    isRegistered: boolean;
    name: string;
    numExperiences: number;
    numInterests:  number;
    numOpportunities:  number;
    numberOfEmployees:  number;
    registrationDate:  number;
    slug: string;
    token: string;
    utmname: string;
    utmsource: string;
}



export class DistributorSearchParams {
    PageSize: number = 10;
    PageNumber: number = 1;
  
    CompanyName: string;
    CompanyId: string;
  
    CompanyRegistered: string;
    IsCrawled: string;
  }
  
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../../_services/auth.service';

@Component({
  selector: 'app-single-date',
  templateUrl: './single-date.component.html',
  styleUrls: ['./single-date.component.scss']
})
export class SingleDateComponent implements OnInit {
  @Input() mainData;
  @Output() fromDateOut = new EventEmitter();
  @Output() toDateOut = new EventEmitter();
  FromDate;
  ToDate;

  constructor(private date: AuthService) {}

  ngOnInit() {}

  onchangeFrom() {
    if (this.FromDate) {
      this.fromDateOut.emit(this.date.convertDate(this.FromDate));
    } else {
      this.fromDateOut.emit('');
    }
  }

  onchangeTo() {
    if (this.ToDate) {
      this.toDateOut.emit(this.date.convertDate(this.ToDate));
    } else {
      this.toDateOut.emit('');
    }
  }
}
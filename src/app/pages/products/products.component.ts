import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ElementRef,
} from "@angular/core";

import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
} from "@angular/material";

import { SelectionModel } from "@angular/cdk/collections";




import { tap, takeUntil } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { environment } from "src/environments/environment";


import { NgForm } from "@angular/forms";
import { AddEditCatalogComponent } from "./components/add-edit-catalog/add-edit-catalog.component";
import { Catalogview } from "./models/Catalogview";
import { Productview } from "./models/productview";
import { ProductService } from "./services/product.service";
import { ProductlistComponent } from "./components/productlist/productlist.component";
import { AddUpdateProductComponent } from "./components/add-update-product/add-update-product.component";
import { AddSubProductComponent } from "./components/addSubProduct/addSubProduct.component";
import { ToastService } from "src/app/_services/toast.service";

@Component({
  selector: "app-products-page",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"],
})
export class ProductsComponent implements OnInit, OnDestroy {
  openProfileLink: string = environment.OpenProfileLink;
  productsource;
  catalogsoruce: MatTableDataSource<Catalogview>;
  catalogsData: Catalogview[] = [];
  length: number;
  cataloglength: number;
  pageIndex = 1;
  catalogpageIndex = 1;
  pageSize = 10;
  productArray: any = [];
  selectedProductId = 0;
  showFilters = false;
  ProductProfile: string;
  inputTarget: any;
  seletedscientificNames: any[] = [];

  scientificNames: any = [];
  filteredscientificNames: Observable<any>;
  selectedScientificId: any[] = [];
  ScientificNames: any;
  selection: SelectionModel<Productview>;
  Catalogselection: SelectionModel<Catalogview>;
  public configOpenRightBar: string;
  public config: any;
  @ViewChild("scientificinput") scientificinput: ElementRef<HTMLInputElement>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) catpaginator: MatPaginator;
  @ViewChild("table", { read: MatSort }) sort: MatSort;
  filtersRes = "";
  searchPageNumber = 1;
  dataFromFilters: any[] = [];
  mainSearch: any = {};
  searchResult: any = [];
  tags: string[];
  showpopup = false;
  popupData = {
    type: "",
    action: "",
    text: "",
  };
  filteredString = "";
  filterOptions: any[];
  seoData = [];
  productSeoData = [];
  curentCardId: number;
  seoDataToSend: any;
  pageType = "product";
  searchKey = "";
  SearchKeyy: string;
  SearchKeyyCatalog: string;
  removable = true;

  searchWord = "";
  typingTimer: any; //timer identifier
  doneTypingInterval = 500;
  SelectedProductDeleteId = 0;
  SelectedProductPublishId = 0;
  SelectedDeleteCatalogId = 0;
  onDestroy$: Subject<void> = new Subject();
  isLoading: boolean;


  displayedColumns = [
    "actions",
    "id",
    "title",
    "Country",
    "company",
    "speciality",
    "productScientificNames",
    "CreatedBy"
  ];
  displayedCatalogColumns = [
    "id",
    "name",
    "company",
    "Date",
    "Count",
    "link",
    "actions",
  ];
  constructor(
    private service: ProductService,
    public dialog: MatDialog,
    private toast: ToastService,
  ) { }

  ngOnInit() {
    this.setoptions(this.pageType);
    this.getproduct();
    this.ProductProfile = environment.ProductProfile;
    this._filterscientificNames("");
    // this.service
    //   .getPortals()
    //   .pipe(takeUntil(this.onDestroy$))
    //   .subscribe(
    //     (response: any) => {
    //       for (let i = 0; i < response.length; i++) {
    //         const name = response[i].name;
    //         const id = response[i].id;
    //         this.seoData.push({
    //           name: name,
    //           id: id,
    //           metaTitle: "",
    //           metaDescription: "",
    //           metaKeyWord: "",
    //         });
    //       }
    //     },
    //     (error) => console.log(error)
    //   );
  }

  SearchScientificNames(form: NgForm) {
    let querystring = "";
    this.pageIndex = 1;

    if(this.paginator) this.paginator.firstPage(); 
    
    if (this.selectedScientificId != null) {
      this.selectedScientificId.forEach((element) => {
        querystring += "&ScentificNameIds=" + element;
      });
    }

    if (form.value.SearchKeyy != "" && form.value.SearchKeyy != null) {
      querystring += "&ProductTitle=" + form.value.SearchKeyy;
    }

    this.searchproduct(this.pageIndex, querystring);
  }

  getproduct() {
    this.isLoading = true;
    this.service
      .getall(1)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {

          this.pageType = "product";
          if(response && response.data) {
            this.length = response.data.count;
            this.renderData(response.data.items);
          } else {
            this.length = 0;
            this.renderData([]);
          }

        },
        (error) => {
          console.log(error);
        }
      );
  }


  searchproduct(pagenumber, data) {
    this.isLoading = true;
    let querystring = "";

    if (this.filteredString != "" && this.filteredString != null) {
      querystring += "&" + this.filteredString;
    }
    if (data != "" && data != null) {
      querystring += "&" + data;
    }


    this.service
      .searchfilter(pagenumber, querystring)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {

        this.pageType = "product";
        if(res && res.data) {
          this.length = res.data.count;
          this.renderData(res.data.items);
        } else {
          this.length = 0;
          this.renderData([]);
        }

      });


  }

  AddSelectedscientificNames(event) {
    this.seletedscientificNames.push(event.option.value);
    this.selectedScientificId.push(event.option.value.id);
    const index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);
      this.scientificinput.nativeElement.value = "";
    }
    this.scientificinput.nativeElement.value = "";
  }

  _filterscientificNames(value: any): void {
    let filterValue = "";
    if (value.id !== undefined) {
      //On  selection of sci name, no need to call API
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    
    this.service.GetScientificNames(filterValue)
      .pipe(takeUntil(this.onDestroy$)).subscribe(
        (response: any) => {
          this.filteredscientificNames = response.data.items;
        },
        (error) => {
          console.log(error);
        }
      );
    }
    // filteredscientificNames
  }

  removescientificNames(i) {
    const temp = this.seletedscientificNames[i];
    const tempIds = this.selectedScientificId[i];
    this.scientificNames.push(temp);
    this.seletedscientificNames.splice(i, 1);
    this.selectedScientificId.splice(i, 1);
  }

  setoptions(pagetype) {
    if (pagetype === "product") {
      this.filterOptions = [
        {
          index: 0,
          order: 1,
          type: 'multi-select',
          output: 'id',
          apiurl: '/api/AdminCompany/CompanyList?Type=manufacturer&NameOrSlug=',
          hasPagination: true,
          value: {},
          selected: [],
          placeholder: 'Company Name',
          chipsList: [],
          list: [],
          searchQuery: '',
          querystring: 'CompanyIds'
        },
        {
          index: 6,
          type: "rangeDate",
          order: 7,
          value: "",
          dateValue: "",
          placeholder: "Created From Date",
          default: "1/1/2000",

          searchQuery: "",
          querystring: "CreatedDateFrom",
        },
        {
          index: 6,
          type: "rangeDate",
          order: 7,
          value: "",
          placeholder: "Created To Date",
          default: "1/1/2090",

          searchQuery: "",
          querystring: "CreatedDateTo",
          dateValue: "",
        },
        {
          index: 0,
          type: "multi-select",
          output: "id",
          apiurl: '/api/Countries',
          order: 1,
          list: [],
          chipsList: [],
          value: {},
          placeholder: "Countries",
          selected: [],
          searchQuery: "",
          querystring: "CountryIds",
        },
        {
          index: 1,
          type: "multi-select",
          output: "id",
          apiurl: "/api/Specialties/GetByName?Name=",
          order: 2,
          hasPagination: true,
          value: {},
          selected: [],
          placeholder: "Specialty",
          chipsList: [],
          list: [],
          searchQuery: "",
          querystring: "SpecialtyIds",
        },
        {
          index: 2,
          type: "multi-select",
          output: "id",
          apiurl: '/api/MedicalLines',
          order: 3,
          value: {},
          selected: [],
          placeholder: "Medical Line",
          chipsList: [],
          list: [],
          querystring: "MedicalLineIds",
          searchQuery: "",
        },
        {
          index: 3,
          type: "multi-select",
          output: "id",
          // apiurl: "/api/AdminScientificNames/List?PageSize=10&PageNumber=1&Name=",
          apiurl: "/api/ScientificNames/SearchElsatic?PageSize=10&PageNumber=1&Key=",
          order: 4,
          hasPagination: true,
          value: {},
          selected: [],
          placeholder: "Scientific Name",
          chipsList: [],
          list: [],
          searchQuery: "",
          querystring: "ScentificNameIds",
        },
        {
          index: 11,
          type: "toggel",
          order: 12,
          value: false,
          selected: [],
          placeholder: "CompanyClient",
          searchQuery: "",
          querystring: "CompanyClient",
        },
      ];
    } else {
      this.filterOptions = [
        {
          index: 0,
          type: "multi-select",
          output: "id",
          apiurl: "/api/Countries",
          order: 1,
          list: [],
          chipsList: [],
          value: {},
          placeholder: "Countries",
          selected: [],
          searchQuery: "",
          querystring: "CountryIds",
        },
        {
          index: 6,
          type: "rangeDate",
          order: 7,
          value: "",
          dateValue: "",
          placeholder: "Created From Date",
          default: "1/1/2010",
          searchQuery: "",
          querystring: "CreatedAtFrom",
        },
        {
          index: 6,
          type: "rangeDate",
          order: 7,
          value: "",
          placeholder: "Created To Date",
          default: "1/1/2090",
          searchQuery: "",
          querystring: "CreatedAtTo",
          dateValue: "",
        },
        {
          index: 4,
          type: "multi-select",
          output: "id",
          apiurl: '/api/AdminCompany/CompanyList?Type=manufacturer&NameOrSlug=',
          order: 5,
          hasPagination: true,
          value: {},
          selected: [],
          placeholder: "Company Name",
          chipsList: [],
          list: [],
          searchQuery: "",
          querystring: "CompanyIds",
        },
      ];
    }
  }


  getCatalog() {
    this.service
      .searchCatalog(this.pageIndex, this.filteredString)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        this.catalogsoruce = res;
        this.renderCatalogData(res);
      });
  }

  renderData(data) {
    this.productsource = new MatTableDataSource<Productview>(data);
    this.selection = new SelectionModel<Productview>(true, []);
    this.isLoading = false;
  }

  renderCatalogData(data) {
    if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.cataloglength = data[0].totalNumber;
    }

    this.catalogsoruce = new MatTableDataSource<Catalogview>(data);
    this.Catalogselection = new SelectionModel<Catalogview>(true, []);
  }

  assignId(id) {
    this.curentCardId = id;
    this.service
      .getProductById(id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((response: any) => {
        this.productSeoData = response.seoData;
      });
  }

  openProductDialog(row) {
    const dialogRef = this.dialog.open(ProductlistComponent, {
      width: "65%",
      data: { data: row },
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((result) => {
        console.log("The dialog was closed");
      });
  }

  handleDataFromSeo(event) {
    this.seoDataToSend = event;
  }

  sendSeo() {
    const s = JSON.stringify(this.seoDataToSend);
    const obj = {
      Id: this.curentCardId,
      Items: [
        {
          Key: "seo",
          Value: s,
        },
      ],
    };
    this.service
      .updateSeo(obj)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          this.popupData.type = "SEO";
          this.popupData.text = "";
          this.popupData.action = "Updated";
          this.showpopup = true;
          const x = this;
          setTimeout(function () {
            x.showpopup = false;
          }, 1500);
        },
        (error) => console.log(error)
      );
  }

  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.searchproduct(this.pageIndex, "");
  }

  CatalogpageChange(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.service
      .searchCatalog(this.pageIndex, this.filteredString)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        this.renderCatalogData(res);
      });
  }

  publishProduct() {
    this.service
      .publishProduct(this.SelectedProductPublishId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {

          if(response.success) {
            this.popupData.type = "Product";
            this.popupData.text = "";
            this.popupData.action = "Published";
            this.showpopup = false;
            this.toast.success("Successfully published!")
            this.searchproduct(1, "");
          } else {
            this.popupData.type = "Product";
            this.popupData.text = "";
            this.popupData.action = "Published";
            this.showpopup = false;
            this.toast.error("Something wrong happened!")
          }

        },
        (error) => {
          this.toast.error("Something wrong happened!")
        }
      );

    // const length = this.productArray.length;
    // for (let i = 0; i < length - 1; i++) {
    //   if (this.productArray[i].id === this.SelectedProductPublishId) {
    //     this.productArray[i].isPublished = true;
    //     break;
    //   }
    // }

  }

  deleteProduct() {
    this.service
      .deleteProduct(this.SelectedProductDeleteId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          this.popupData.type = "Product";
          this.popupData.text = "";
          this.popupData.action = "Deleted";
          this.showpopup = true;
          const x = this;
          setTimeout(function () {
            x.showpopup = false;
          }, 1500);
          this.getproduct();
        },
        (error) => console.log(error)
      );
    const length = this.productArray.length;
    for (let i = 0; i < length - 1; i++) {
      if (this.productArray[i].id === this.SelectedProductDeleteId) {
        this.productArray.splice(i, 1);
        break;
      }
    }
  }


  deleteCatalog() {
    this.service
      .deleteCatalog(this.SelectedDeleteCatalogId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          this.getproduct();
          this.popupData.type = "Catalog";
          this.popupData.text = "";
          this.popupData.action = "Deleted";
          this.showpopup = true;
          const x = this;
          setTimeout(function () {
            x.showpopup = false;
          }, 1500);
          this.getCatalog();
        },
        (error) => console.log(error)
      );
  }

  actionTabChange(event) {
    switch (event.nextId) {
      case "1":
        this.getproduct();
        this.pageType = "product";
        this.setoptions(this.pageType);
        break;
      case "2":
        this.getCatalog();
        this.pageType = "catalog";
        this.setoptions(this.pageType);
        break;
    }
  }

  handleDataFromFilters(event) {

    if(this.paginator) this.paginator.firstPage(); 
    this.pageIndex = 1;


    this.filterOptions.forEach((element) => {
      if (element.type === "multi-select") {
        element.searchQuery = element.selected.join("");
      }
    });
    this.filteredString = "";
    this.filterOptions.forEach((element) => {
      this.filteredString += element.searchQuery;
    });
    if (this.pageType === "product") {
      this.searchproduct(1, "");
    } else {
      this.service
        .searchCatalog(this.pageIndex, this.filteredString)
        .pipe(takeUntil(this.onDestroy$))
        .subscribe((res: any) => {
          this.renderCatalogData(res);
        });
    }
  }

  Add() {
    const dialogRef = this.dialog.open(AddUpdateProductComponent, {
      width: "65%",
    });
    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => { 
          if(result) {
            this.searchproduct(this.pageIndex, "");
          }
        }),
        takeUntil(this.onDestroy$)
      )
      .subscribe();
  }

  AddCatalog() {
    const dialogRef = this.dialog.open(AddEditCatalogComponent, {
      width: "65%",
      // height: "80%",
      data: {},
    });
    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => {
          if(result) this.getCatalog();
        }),
        takeUntil(this.onDestroy$)
      )
      .subscribe();
  }

  Update(row) {
    this.service
      .getProductById(row.id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        const dialogRef = this.dialog.open(AddUpdateProductComponent, {
          data: res.data,
          width: "65%",
        });
        dialogRef.afterClosed().pipe(
            takeUntil(this.onDestroy$),
            tap((result) => {
              if(result) {
                this.getproduct();
              }
            }),
          )
          .subscribe();
      });
  }

  UpdateCatalog(id) {
    this.service
      .getcatalogbyid(id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        const dialogRef = this.dialog.open(AddEditCatalogComponent, {
          data: res,
          // height: "75%",
          width: "65%",
        });
        dialogRef
          .afterClosed()
          .pipe(
            tap((result) => {
              if(result) this.getCatalog();
            }),
            takeUntil(this.onDestroy$)
          )
          .subscribe();
      });
  }

  filterCatalogData() {
    window.this.service
      .SearchCatalog(
        window.this.pageIndex,
        window.this.filteredString,
        window.this.searchWord
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        window.this.renderCatalogData(res);
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  // Filter(SearchKeyy) {
  //   this.service.search;
  // }

  FilterCatalog() {
    this.catalogsoruce.filter = this.SearchKeyyCatalog.trim().toLowerCase();
  }

  AddSub(element) {
    this.dialog.open(AddSubProductComponent, {
      width: "80%",
      height: "75%",
      data: element,
    });
  }

  copyLink(link, title, companyname, medicalline, speciality, id) {
    let url: any =
      link +
      title +
      "/" +
      companyname +
      "/" +
      medicalline +
      "/" +
      speciality +
      "/" +
      id;
    let selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = url;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
  }
}
declare global {
  interface Window {
    this: any;
  }
}

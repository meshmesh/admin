import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DynamicPageTableService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) {}

  getScientificNames(str) {
    const url =
      this.APIEndpoint +
      `/api/AdminScientificNames/ScientificnameFilter?PageSize=10&PageNumber=1&nameinput=${str}`;
    return this.http.get<any>(url);
  }

  getCountrySuggestion(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/Countries/GetCountries'
    );
  }
  UpdateSEO(seo): Observable<any[]>{
    return this.http.post<any[]>(this.APIEndpoint + '/api/AdminDynamicPage/UpdateSeo',seo);
  }
  getmedicalline(): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + '/api/MedicalLines/GetMedicalLines'
    );
  }

  addDynamicPage(form) {
    return this.http.post(
      this.APIEndpoint + '/api/AdminDynamicPage/Addupdate',
      form
    );
  }

  getDynamicPage(pageNumber, text) {
    return this.http.get(
      this.APIEndpoint +
        '/api/AdminDynamicPage/searchdynamicpage?PageSize=10&PageNumber=' +
        pageNumber +
        '&' +
        'FullInput=' +
        text
    );
  }
  getDynamicPageById(id, pageNumber) {
    return this.http.get(
      this.APIEndpoint +
        '/api/AdminDynamicPage/searchdynamicpage?DynamicId=' +
        id +
        '&PageSize=1&PageNumber=' +
        pageNumber
    );
  }
  Publish(id) {
    return this.http.post(this.APIEndpoint + '/api/AdminDynamicPage/Publish?Id='+ id,{});
  }
  unPublish(id) {
    return this.http.post(this.APIEndpoint + '/api/AdminDynamicPage/UnPublish?Id=' + id, {});
  }
  Delete(id) {
    return this.http.post(
      this.APIEndpoint + '/api/AdminDynamicPage/Delete?Id=' + id,   {}
    );
  }
}

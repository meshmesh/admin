import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatInputModule,
  MatDialogModule, 
  MatSortModule
} from "@angular/material";

import { ScientificNameListComponent } from './scientific-name-list/scientific-name-list.component';
import { ViewAllModalComponent } from './view-all-modal/view-all-modal.component';
import { MergeScientificNameComponent } from './merge-scientific-name/merge-scientific-name.component';
import { AddScientificNameComponent } from './add-scientific-name/add-scientific-name.component';
import { TranslateComponent } from './translate/translate.component';
import { FilterModule } from 'src/app/filter/filter.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddScientificNameService } from './services/add-scientific-name.service';
import { SuggestedScientificNameComponent } from './suggested-scientific-name/suggested-scientific-name.component';
import { ReplaceSuggestedNameComponent } from './suggested-scientific-name/replace-suggested-name/replace-suggested-name.component';
import { ApproveScientificNameComponent } from './suggested-scientific-name/approve-scientific-name/approve-scientific-name.component';
import { NewScientificNameRoutingModule } from './new-scientific-name-routing.module';
import { NewScientificNameComponent } from './new-scientific-name.component';
import { QuickEditComponent } from './quick-edit/quick-edit.component';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  declarations: [QuickEditComponent, NewScientificNameComponent, ScientificNameListComponent, ViewAllModalComponent, 
    MergeScientificNameComponent, AddScientificNameComponent, TranslateComponent, 
    SuggestedScientificNameComponent, ReplaceSuggestedNameComponent, ApproveScientificNameComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    FilterModule,
    TagInputModule,
    NewScientificNameRoutingModule
  ],
  providers: [AddScientificNameService],
  entryComponents: [ScientificNameListComponent, AddScientificNameComponent, ViewAllModalComponent,
     MergeScientificNameComponent,TranslateComponent, ReplaceSuggestedNameComponent,
     ApproveScientificNameComponent
    ]
})
export class NewScientificNameModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegoinComponent } from './regoin.component';
import { SharedModule } from '../shared/shared.module';
import { RegoinRoutingModule } from './regoin-routing.module';
import { MatDialogModule } from '@angular/material';
import { DestributorinregoinComponent } from './_modal/destributorinregoin/destributorinregoin.component';
import { FilterModule } from '../filter/filter.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RegoinRoutingModule,
    MatDialogModule,
    FilterModule
  ],
  declarations: [RegoinComponent,DestributorinregoinComponent],
  entryComponents: [DestributorinregoinComponent]
})
export class RegoinModule { }

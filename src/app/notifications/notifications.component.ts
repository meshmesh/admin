import { Component, OnInit } from '@angular/core';
import { MessagingService } from '../_services/messaging.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  constructor(private msgService: MessagingService) {}

  ngOnInit() {}
}

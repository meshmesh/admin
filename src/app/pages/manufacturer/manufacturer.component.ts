import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { take, tap, switchMap, takeUntil } from 'rxjs/operators';
import { of, ReplaySubject } from 'rxjs';
import { MatPaginator, MatTableDataSource, MatDialog, Sort, TooltipPosition } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ManufacturerModel, ManuListSearchParams } from './model/manufacturer.model';
import { ManufacturerDetailComponent } from './manufacturer-detail/manufacturer-detail.component';
import { ManufacturerService } from './services/manufacturer.service';
import { ManufacturerActionsComponent } from './components/manufacturer-actions/manufacturer-actions.component';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';

import { NgForm } from '@angular/forms';
import { IFilterOption } from 'src/app/filter/filters.model';
import { ManufacturerFilterOptions, ManufacturerNotRegisteredFilterOptions, ManuColumnTable, ManuNotRegColumnTable } from './model/manufacturer-filter-options.model';
import { AddEditCompanyComponent } from './components/add-edit-company/add-edit-company.component';
import { MergeManuComponent } from './components/merge-manu/merge-manu.component';
import { AddManuClientComponent } from './components/add-manu-client/add-manu-client.component';

@Component({
  selector: 'app-manufacturer',
  templateUrl: './manufacturer.component.html',
  styleUrls: ['./manufacturer.component.scss']
})
export class ManufacturerComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: Array<string> = ManuColumnTable;

  displayedColumns2: Array<string> = ManuNotRegColumnTable;

  filterOptions: Array<IFilterOption> = ManufacturerFilterOptions;

  manufacturerSource: MatTableDataSource<ManufacturerModel>;

  searchParams: ManuListSearchParams = new ManuListSearchParams();

  onDestroy$: ReplaySubject<void> = new ReplaySubject();

  filteredString: string = '';

  position: TooltipPosition = 'above';

  pageType: string = 'Manufaturers';

  isLoading: boolean;

  length: number;

  pageSize: number = 10;



  constructor(
    private router: ActivatedRoute,
    private dialog: MatDialog,
    private manufacturerService: ManufacturerService
  ) { }


  ngOnInit() {
    this.getManufacturers();
    this.setoptions(this.pageType);
  }

  ngAfterViewInit() {
    console.log(this.paginator);
  }


  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.dialog.closeAll();
  }

  getManufacturers() {
    this.isLoading = true;
    this.manufacturerService.getAll(this.searchParams, this.filteredString)
    .pipe(
      take(1),
      tap((res: any) => {
        this.renderData(res);
      })
    )
    .pipe(takeUntil(this.onDestroy$))
    .subscribe();
  }

  renderData(res: any) {
    if(res && res.data) {
      this.length = res.data.count;
      this.manufacturerSource = new MatTableDataSource<ManufacturerModel>(res.data.items);
    } else {
      this.length = 0;
      this.manufacturerSource = new MatTableDataSource<ManufacturerModel>([]);
    }
    this.isLoading = false;
  }


  searchSubmit(form: NgForm) {

    // if(this.paginator) this.paginator.firstPage(); 
    if(this.paginator) this.paginator.pageIndex = 0; 
    this.searchParams.PageNumber = 1;

    if(!form.value.id && !form.value.name) {
      this.searchParams.CompanyId = '';
      this.searchParams.CompanyName = '';
      this.getManufacturers();
      return;
    } 

    this.searchParams.CompanyId = form.value.id ? String(form.value.id).trim() : '';
    this.searchParams.CompanyName = form.value.name ? String(form.value.name).trim() : '';
    this.getManufacturers();
  }


  pageChange(event) {
    this.pageSize = event.pageSize;
    this.searchParams.PageSize = event.pageSize;
    this.searchParams.PageNumber = event.pageIndex + 1;
    this.getManufacturers();
  }

  actionTabChange(event) {
    debugger
    switch (event.nextId) {
      case '1':
        this.searchParams.CompanyRegistered = null;
        this.searchParams.IsCrawled = null;
        this.pageType = 'Manufaturers';
        this.setoptions(this.pageType);
        this.getManufacturers();
        break;

      case '2':
        this.searchParams.CompanyRegistered = 'false';
        this.searchParams.IsCrawled = null;
        this.pageType = 'ManufaturersWithoutContact';
        this.setoptions(this.pageType);
        this.getManufacturers();
        break;

      case '3':
        this.searchParams.IsCrawled = 'true';
        this.searchParams.CompanyRegistered = null;
        this.pageType = 'ManufaturersWithoutContact';
        this.setoptions(this.pageType);
        this.getManufacturers();
        break;
    }
  }

  setoptions(pagetype) {
    if (pagetype === 'Manufaturers') {
      this.filterOptions = ManufacturerFilterOptions;
    } else {
      this.filterOptions = ManufacturerNotRegisteredFilterOptions;
    }
  }


  handleDataFromFilters(event) {

    // if(this.paginator) this.paginator.firstPage(); 
    if(this.paginator) this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;


    if (event === 0) {
      this.filteredString = "";
      this.getManufacturers();
      return;
    }

    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });

    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });

    this.getManufacturers();
  }


  openDialog(row, type: number = 1) {
    this.dialog.open(ManufacturerActionsComponent, {
      width: '65%',
      data: { row, type }
    });
  }

  displayManufacturer(row) {
    this.manufacturerService.getManufacturerById(row.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const dialogRef = this.dialog.open(ManufacturerDetailComponent, {
        data: res.data,
        width: '85%',
        height: '80%'
      });
      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(result => {
          if(result) {
            this.getManufacturers();
          }
        });
    });
  }


  Add() {
    const dialogRef = this.dialog.open(AddEditCompanyComponent, {
      width: '65%',
    });
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        tap((result) => {
          if(result) {
            this.getManufacturers();
          }
        })
      )
      .subscribe();
  }

  EditManufacturer(element) {
    this.manufacturerService.getManufacturerById(element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const dialogRef = this.dialog.open(AddEditCompanyComponent, {
        width: '65%',
        data: res.data[0]
      });
      dialogRef
        .afterClosed()
        .pipe(
          takeUntil(this.onDestroy$),
          tap((result) => {
            if(result) {
              this.getManufacturers();
            }
          })
        )
        .pipe(takeUntil(this.onDestroy$)).subscribe();
    })
  }

  addClient(element) {
    const dialogRef = this.dialog.open(AddManuClientComponent, {
      width: "65%",
      data: { element: element, Update: false }
    })
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(result => {
        if (result) {
          this.getManufacturers();
        }
      });
  }

  mergeComp(comp) {
    const dialogRef = this.dialog.open(MergeManuComponent, {
      width: '65%',
      data: { comp }
    }); 
    
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$)).subscribe(result => {
        if(result) {
          this.getManufacturers();
        }
      });
  }

  // SHOULD BE DONE SPECIAL 
  deleteRecord(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Manufacturer',
        id: id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this.manufacturerService.deleteManufacturers(id) : of(undefined);
        }),
        tap((res: number) => {
          if (res) {
            this.getManufacturers();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  onDeactivate(id: number): void {
    let Activate = {
      IsActive: false,
      ManufacturerId: id
    }
    this.activateDeactivate(Activate);
  }

  onActivate(id: number): void {
    let Activate = {
      IsActive: true,
      ManufacturerId: id
    }
    this.activateDeactivate(Activate);
  }

  activateDeactivate(Activate) {
    this.manufacturerService
      .ActivateDeactivateManufacturer(Activate)
      .pipe(
        take(1),
        tap((res) => {
          this.getManufacturers();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }






}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DynamicService {
  APIEndpoint = environment.ApiUrl;

constructor(private http: HttpClient) { }
getSpecalities() : Observable<any>{ 
  return  this.http.get<any>(this.APIEndpoint + '/api/Specialities/GetSpecialities');
  }
  getCountrySuggestion(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/Countries/GetCountries'
    );
  }
  getMedicalLines2() : Observable<any> {
    return this.http.get<any>(this.APIEndpoint + '/api/MedicalLines/GetMedicalLines')
       
   }
   getListOfManufacturer(name): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint +
        '/api/Companies/GetListOfCompaniesLimited?Name=' + name + '&Type=manufacturer'
    );
  }
  getListOfDistributors(name): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint +
        '/api/Companies/GetListOfCompaniesLimited?Name=' + name + '&Type=distributor'
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatButtonModule, MatTableModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';



@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MatCardModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule
  ],
  declarations: [SettingsComponent]
})
export class SettingsModule { }

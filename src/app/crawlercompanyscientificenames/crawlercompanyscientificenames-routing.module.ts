import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrawlercompanyscientificenamesComponent } from './crawlercompanyscientificenames.component';

const router: Routes = [
    {
        path: '',
        component: CrawlercompanyscientificenamesComponent ,
        data: {
            title: 'Crawler Scientific Names',
            icon: 'icon-layout-cta-right',
            caption: 'my landing',
            status: true
        }
    }
];
@NgModule({
    imports: [RouterModule.forChild(router)],
    exports: [RouterModule]
})

export class crawlercompanyscientificenamesRoutingModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CrawlerService } from '../Crawler.service';
import { Route } from '@angular/router';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-crawleractions',
  templateUrl: './crawleractions.component.html',
  styleUrls: ['./crawleractions.component.scss']
})
export class CrawleractionsComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrawleractionsComponent>,
    private serv: CrawlerService,
    private toast: ToastService, @Inject(MAT_DIALOG_DATA) public data: any) { }
  Company = [];
  displayedColumns = ['id', 'name', 'Link','Actions'];
  serializer: any;
  length: number;
  ngOnInit() {
    
  this.Company = [this.data];
  }
 /*  getdata() {
    
    let Ids = this.data.companyIds;
    let querystring ="";
Ids.forEach(element => {
   querystring += "&Ids=" +element;
});
    this.serv.getcompanysbyids(querystring,this.data.scientificNameId).subscribe(res => {
      this.Company.push(res);
      this.Company = this.Company[0];
      this.length = this.Company.length;

    });
    console.log(this.Company);
  } */


/*   addscientificname(object) {

    this.serv.AddCompnayScientificNameFromCrawler(this.data.scientificNameId,object.companyId).subscribe(res => {
      this.toast.success("Imported Successfully ");
    }) 
  } */

/*   IgnoreResult(object) {
    
this.serv.AddCompnayScientificNameFromCrawler(this.data.scientificNameId,object.companyId).subscribe(res => {
    this.toast.success("Ignored Successfully ");
}) 
  } */
}

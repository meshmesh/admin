import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { MatChipInputEvent, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as S3 from 'aws-sdk/clients/s3';
import { Observable, of, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Guid } from 'guid-typescript';

import { AddScientificNameService } from '../../services/add-scientific-name.service';
import { ScientificNameModel, AddScientificNameModel, UpdateScientificNameModel } from '../../model/add-scientific-name.model';
import { ScientificNameService } from '../../services/scientific-name.service';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'approve-scientific-name',
  templateUrl: './approve-scientific-name.component.html',
  styleUrls: ['./approve-scientific-name.component.scss']
})
export class ApproveScientificNameComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  updateScientificNameform: UpdateScientificNameModel;
  AddScientificNameform: AddScientificNameModel;
  scientificnameform: ScientificNameModel;
  showmore = false;
  smbutton: any = 'Show';
  categoryList: any[] = [];
  FilterdcategoryList: Observable<any>;
  FilterScientificClass: Observable<any>;
  scientificclassList: any[] = [];
  attribute: string[] = [];
  scientificNameList: any[] = [];
  medicalLines: any[];
  medicalLine = 0;
  speciality: number;

  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  manufacturerFilter: Observable<any>;
  ntSelected = [];
  scientificNames: any[] = [];
  filteredscientificNames: Observable<any>;
  filteredBroaderTerms: Observable<any>;
  medicallineslist: any[] = [];
  medicallinefilter: Observable<any>;
  SelectedNarrowTerms: any[] = [];
  SelectedManufacturer: any[] = [];
  SelectedSpeciality: any[] = [];
  SelectedRelatedTerms: any[] = [];
  SelectedBroaderTerms: any[] = [];
  removable = true;
  NoneDescriptor: any[] = [];
  NoneDescript: any[] = [];
  file: File;
  imagePreview: any;
  selectedCategory: any[] = [];
  ScientificClassList: any[] = [];
  SelectedScientificClass: any[] = [];
  totalCount = 1;
  totalTask = 0;
  attributeList: any = [];
  Manufacturer: any = [];
  selectedattribute: any[] = [];
  Filterattribute: Observable<any>;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  @ViewChild('ManuInput') ManuInput: ElementRef<HTMLInputElement>;
  @ViewChild('broadInput') broadInput: ElementRef<HTMLInputElement>;
  @ViewChild('relatedInput') relatedInput: ElementRef<HTMLInputElement>;
  constructor(
    private scientificNameService: AddScientificNameService,
    public service: ScientificNameService,
    private toast: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ApproveScientificNameComponent>
  ) { }

  removeManufacturer(i) {
    const temp = this.SelectedManufacturer[i];
    if (temp.id !== undefined) {
      this.Manufacturer.push(temp);
    }
    this.SelectedManufacturer.splice(i, 1);
  }
  removeSpeciality(i) {
    const temp = this.SelectedSpeciality[i];
    if (temp.id !== undefined) {
      this.specialitieslist.push(temp);
    }
    this.SelectedSpeciality.splice(i, 1);
  }

  displayFnSpeciality(Speciality?: any): string | undefined {
    return Speciality ? Speciality.name : undefined;
  }

  AddSelectedSpeciality(event) {
    const value = event.value;
    const index = this.SelectedSpeciality.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      this.SelectedSpeciality.push(event.option.value);
      this.ManuInput.nativeElement.value = '';
    }
  }
  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialitieslist.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
  _filterMedicalLine(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.Name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.medicallineslist.filter(item => item.Name.toLowerCase().indexOf(filterValue) === 0));
  }
  _filterborderscientificNames(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = '&key=' + value.name;
    } else {
      filterValue = '&key=' + value.toLowerCase();
    }

    this.scientificNameService.getScientificNameSuggestion(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      // this.specialitieslist = respone.data;
      this.filteredBroaderTerms = of(respone.data);
    });
    // return data;
  }

  _filterscientificNames(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = '&key=' + value.name;
    } else {
      filterValue = '&key=' + value.toLowerCase();
    }

    this.scientificNameService.getScientificNameSuggestion(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.filteredscientificNames = of(respone.data);
    });
  }

  _filterManufacturer(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    this.scientificNameService.getManufacturersuggestions(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.manufacturerFilter = of(respone.data);
    });
  }

  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  close(data) {
    this.dialogRef.close(data);
  }

  bindspecialities(medicallineId: any) {
    this.SelectedSpeciality = [];
    this.scientificNameService.getSpecalities(medicallineId).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.specialitieslist = respone.data;
      this.specialitiesFilter = of(this.specialitieslist);
    });
  }

  ngOnInit() {
    this.scientificnameform = new ScientificNameModel();
    this.scientificnameform.Id = this.data.id;
    this.scientificnameform.Name = this.data.valueItem

    this.scientificNameService.getMedicalLines2().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.medicallineslist = respone.data;
      this.medicallinefilter = of(this.medicallineslist);
    });

    this.scientificNameService.getManufacturersuggestions('a').pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      let manufacturerlist = respone.data;
      this.manufacturerFilter = of(manufacturerlist);
    });
    this.scientificNameService
      .getScientificNameSuggestion('&key=a')
      .pipe(takeUntil(this.onDestroy$)).subscribe(
        (response: any) => {
          this.scientificNames = response.data;
          this.filteredscientificNames = of(this.scientificNames);
          this.filteredBroaderTerms = of(this.scientificNames);
        },
        error => console.log(error)
      );
  }

  AddSelectedNarrowTerms(event) {
    const value = event.value;
    const index = this.SelectedNarrowTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    const index2 = this.SelectedBroaderTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);

    if (this.scientificnameform.Id && this.scientificnameform.Id != event.option.value.id) {
      if (index === -1 && index2 === -1) {
        this.SelectedNarrowTerms.push(event.option.value);
        this.searchInput.nativeElement.value = '';
      }

    } else if (index === -1 && index2 === -1) {
      this.SelectedNarrowTerms.push(event.option.value);
      this.searchInput.nativeElement.value = '';
    }
  }
  AddSelectedManufacturer(event) {
    const value = event.value;
    const index = this.SelectedManufacturer.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      this.SelectedManufacturer.push(event.option.value);
      this.ManuInput.nativeElement.value = '';
    }
  }

  AddSelectedRelatedNames(event) {
    const value = event.value;
    const index = this.SelectedRelatedTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      this.SelectedRelatedTerms.push(event.option.value);
      this.relatedInput.nativeElement.value = '';
    }
  }

  AddSelectedBroaderNames(event) {
    const value = event.value;
    const index = this.SelectedBroaderTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    const index2 = this.SelectedNarrowTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);

    if (this.scientificnameform.Id && this.scientificnameform.Id != event.option.value.id) {
      if (index === -1 && index2 === -1) {
        this.SelectedBroaderTerms.push(event.option.value);
        this.broadInput.nativeElement.value = '';
      }
    }
    else if (index === -1 && index2 === -1) {
      this.SelectedBroaderTerms.push(event.option.value);
      this.broadInput.nativeElement.value = '';
    }
  }
  removeNarrowTerms(i) {
    this.SelectedNarrowTerms.splice(i, 1);
  }
  removeRelatedTerms(i) {
    this.SelectedRelatedTerms.splice(i, 1);
  }
  removeBroaderTerms(i) {
    const temp = this.SelectedBroaderTerms[i];
    if (temp.id !== undefined) {
      this.attributeList.push(temp);
    }
    this.SelectedBroaderTerms.splice(i, 1);
    this.SelectedBroaderTerms.splice(i, 1);
  }
  moreoptions() {
    this.showmore = !this.showmore;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.showmore) {
      this.smbutton = 'Hide';
    } else {
      this.smbutton = 'Show';
    }
  }
  addNoneDescriptor(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.NoneDescriptor.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  hideImage() {
    this.imagePreview = '';
    this.file = null;
  }
  iconHandler(event) {
    this.file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
    }
  }

  async submit(form) {
    if (form.invalid) {
      return;
    }
    let checker = this.scientificnameform.Name.trim()
    if (checker == "") {
      this.scientificnameform.Name = "";
      form.controls['Name'].setValue('');
      form.controls['Name'].setErrors({ 'required': true });
      form.controls['Name'].markAsTouched();

      if (form.invalid) {
        return;
      }

    }
    // this.scientificnameform.IsApproved = true;

      this.AddScientificNameform = new AddScientificNameModel();
      this.AddScientificNameform.id = this.scientificnameform.Id;
      this.AddScientificNameform.name = this.scientificnameform.Name;
      this.AddScientificNameform.IsApproved = true;
      this.AddScientificNameform.medicalLineId = this.scientificnameform.MedicalLineId;

      this.SelectedNarrowTerms.forEach(element => {
        this.AddScientificNameform.ntIds.push(element.id);
      });
      this.SelectedBroaderTerms.forEach(element => {
        this.AddScientificNameform.btIds.push(element.id);
      });
      this.SelectedRelatedTerms.forEach(element => {
        this.AddScientificNameform.rtIds.push(element.id);
      });
      this.AddScientificNameform.CompanyIds = this.SelectedManufacturer.map(manu => manu.id);

      this.NoneDescriptor.forEach(element => {
        if (element.value === 0) {
          this.AddScientificNameform.alternatives.push(element.id);
        } else {
          this.AddScientificNameform.alternatives.push(element);
        }
      });

      this.SelectedSpeciality.forEach(element => {
        this.AddScientificNameform.specialityIds.push(element.id);
      });

      if (!this.file && !this.imagePreview) {
        this.AddScientificNameform.imagePath = 'https://aumet.me/assets/images/scientific.png';
        this.imagePreview = 'https://aumet.me/assets/images/scientific.png';
      }

      if (this.file) {
        const _thisfile = this;
        this.uploadImage(this.file, function (result) {
          _thisfile.AddScientificNameform.imagePath = result;
          _thisfile.totalTask++;
          if (_thisfile.totalTask === _thisfile.totalCount) {
            _thisfile.addUpdateFunction(_thisfile.AddScientificNameform);
          }
        });
      } else if (this.imagePreview) {
        this.AddScientificNameform.imagePath = this.imagePreview;
        this.addUpdateFunction(this.AddScientificNameform);
      }
  }

  addUpdateFunction(form) {
    const _thisTimer = this;
    this.scientificNameService.approveScieName(form).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.toast.success('Approved Successfully');

      setTimeout(function () {
        _thisTimer.close(null);
      }, 500);
    });
  }

  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'icons/scientificname/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }
  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }
}
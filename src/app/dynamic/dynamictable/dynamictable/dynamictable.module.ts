import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { BulkRoutingModule } from '../../../bulk/bulk-routing.module';

@NgModule({
  imports: [
    CommonModule,
BulkRoutingModule,
    SharedModule
  ],
  declarations: [],
  providers:[]
})
export class DynamictableModule { }

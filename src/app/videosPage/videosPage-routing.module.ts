import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideosPageComponent } from './videosPage.component';
const routes: Routes = [
  {
    path: '',
    component: VideosPageComponent,
    data: {
      title: 'Videos',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideosPageRoutingModule {}

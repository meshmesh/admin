import { ISpeciality } from "./scientific-name.model";

export class ISpecialityRequest {
    id: number;
    name: string;
    parentId?: number;
  }

export class SciNameUpdateRequest {
    id: number;
    name: string;
    description: string;
    imagePath: string;
    medicalLineId: number;
    speciality: ISpecialityRequest;
    subSpeciality: ISpecialityRequest[];
    alternatives: string[];
    references: string[];
    ntIds: number[];
    rtIds: number[];
    btIds: number[];
}


export class AddScientificNameModel {
    id: number;
    name: string;
    specialityIds: number[] = [];
    IsApproved: boolean;
    medicalLineId: any;
    rtIds: number[] = [];
    btIds: number[] = [];
    ntIds: number[] = [];
    CompanyIds: number[] = [];
    imagePath: string;
    alternatives: string[] = [];


}

export class UpdateScientificNameModel {
    Id: number;
    Name: string;
    SpecialityIds: number[] = [];
    IsApproved: boolean;
    MedicalLineId: any;
    RTIds: number[] = [];
    BTIds: number[] = [];
    NTIds: number[] = [];
    CompanyIds: number[] = [];
    ImagePath: string;
    AlterNative: string[] = [];
}


export class ScientificNameModel {
    Id: number;
    Name: string;
    IsApproved: boolean;
    Note: string;
    Rank: number;
    MedicalLineId: any;
    RelatedTerms: number[] = [];
    BroaderTerms: number[] = [];
    NarrowerTerms: number[] = [];
    NonpreferredTerms: any[] = [];
    CategoryId: number;
    AttributeType: number[] = [];
    ScientificClassId: number[] = [];
    CreatedAt: string;
    CompanyIds: number[] = [];
    Nonpreferredtermsid: number[] = [];
    borderTerm: any = {};
    narrowTerm: any = {};
    relatedTerm: any = {};
    manufacturer: number[] = [];
    manufacturerId: number;
    ImagePath: string;
    AlterNative: string;
    Specialities: object[] = [];
    NarawerSIN: object[] = [];
    BroderSIN: object[] = [];
    RelatedSIN: object[] = [];
}

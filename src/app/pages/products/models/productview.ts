export class Productview {
    ProductTitle: string;

    CompanyClient: boolean;

    SEOOptimized: boolean;

    ECommerce: boolean;

    IsPublished: boolean;

    CountryIds: number[];

    MadeInIds: number[];

    SpecialtyIds: number[];

    MedicalLineIds: number[];

    ScentificNameIds: number[];

    CompanyIds: number[];

    SoldToIds: number[];

    SellingPointIds: number[];

    CatalogIds: number[];

    CreatedDateFrom: string;

    CreatedDateTo: string;

    ModificationDateFrom: string;

    ModificationDateTo: string;

    PageNumber: number;

    PageSize: number;

    ProductId: number;

    SortBy: string;

    OrderBy: string;
}

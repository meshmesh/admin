import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';

import { SharedModule } from '../shared/shared.module';
import { ArticleService } from './article.service';
import { CardsComponent } from './article/cards/cards.component';
import { QuillEditorModule } from 'ngx-quill-editor';
import { TagInputModule } from 'ngx-chips';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddEditArticleComponent } from './article/add-edit-article/add-edit-article.component';
import { SeoComponent } from './article/add-edit-article/seo/seo.component';
import { MatNativeDateModule } from '@angular/material';
import { SeomodalComponent } from './article/_modal/seomodal/seomodal.component';
import { SeoArticleComponent } from './seo-Article/seo-article.component';
import { ArticleRoutingModule } from './article-routing.module';
import { ArticleFormComponent } from './article-form/article-form.component';

const components = [
  CardsComponent,
  AddEditArticleComponent,
  SeoComponent,
  SeoArticleComponent,
  ArticleComponent,
  ArticleFormComponent
];
const modals = [SeomodalComponent];

@NgModule({
  imports: [
    CommonModule,
    ArticleRoutingModule,
    SharedModule,
    QuillEditorModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ArticleService],
  declarations: [...components, ...modals],
  entryComponents: [...modals]
})
export class ArticleModule {}

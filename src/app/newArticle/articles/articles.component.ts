import { Component, OnInit, Inject } from '@angular/core';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ArticlesForm } from '../ArticlesForm';
import { ArticleTableService } from '../articleTable/articleTable.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  Tag: any;

  constructor(public dialogRef: MatDialogRef<ArticlesComponent>,
    private serv: ArticleTableService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  title: any
  type: any
  categories: any = []
  relatedArticles: any = []
  details: any
  description: any
  feature = false
  pImage: any[] = [];
  sImage: any[] = [];
  priamryImage: any[] = [];
  secondryImages: any[] = [];
  primaryImageValid = true;
  artForm: ArticlesForm;
  category: any = [];
  related: any = [];
  contentType: any = [];
  URL: any;
  Sort: any
  relaArticles: any;
  disabled = false;
  relatedid : any ;

  types = [{ name: "Manufacturer", id: 2 }, { name: "Distributor", id: 1 }];
  contents = [{ name: "URL", id: 1 }, { name: "Details", id: 2 }];

  ngOnInit() {
    this.artForm = new ArticlesForm();
    if (this.data > 0) {
      this.serv.getArticleById(this.data).subscribe((x: any) => {
        console.log(x);
        this.priamryImage = [x.articleimage]
        this.secondryImages = x.subimages
        this.title = x.title
        this.description = x.description
        this.details = x.details
        this.feature = x.feature
        this.URL = x.url
        this.Tag = x.tag;
        this.Sort = x.sort;
        this.relatedid =x.relatedArtical
     
        
        this.typeChange(null, x.type)
        this.getRelated(null, x.maincategory)
        this.contentChange(null, x.contentType)
      
      })
    }



    this.serv.getMainCategory().subscribe(x => {
      this.category = x
    })
  }


  getRelated(event, id) {

    if (event != null) {
      if (this.categories != null) {
        const ID = parseInt(event.value.id);
        this.serv.SearchCategory(ID).subscribe(x => {
          this.related = x
          if(this.relatedid)
          {
            let dropDownData = this.related.find((data :any) => data.id === this.relatedid[0]);
            this.relatedArticles = dropDownData
          }
        })
      }
      if (this.data > 0) {
        this.serv.getMainCategory().subscribe(x => {
          this.category = x
          let dropDownData = this.category.find((data: any) => data.id === event.value.id);
          this.categories = dropDownData
        })
      }
    } else {
      if (this.categories != null) {
        const ID = parseInt(id);
        this.serv.SearchCategory(ID).subscribe(x => {
          this.related = x
          if(this.relatedid)
          {
            let dropDownData = this.related.find((data :any) => data.id === this.relatedid[0]);
            this.relatedArticles = dropDownData
          }
        
        })
      }
      if (this.data > 0) {
        this.serv.getMainCategory().subscribe(x => {
          this.category = x
          let dropDownData = this.category.find((data: any) => data.id === id);
          this.categories = dropDownData
        })
      }

    }


  }

  contentChange(event, id) {
    if (event != null) {
      let dropDownData = this.contents.find((data: any) => data.id === event.value.id);
      this.contentType = dropDownData
    } else {
      let dropDownData = this.contents.find((data: any) => data.id === id);
      this.contentType = dropDownData
    }

  }


  parseRelated(event) {
    if (this.categories > 0) {
      const ID = parseInt(event.value.id);
      this.relaArticles = ID;
    }
    if (this.data > 0) {
      let dropDownData = this.related.find((data: any) => data.id === event.value.id);
      this.relatedArticles = dropDownData
    }
  }

  uploadImageToS3(file) {
    if (file) {


      const paramsProduct = {
        Bucket: 'aumet-data',
        Key: 'product/Article/' + Guid.create() + '.' + this.getFileExtension(file.name),
        Body: file
      };
      const bucket = new S3({
        accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
   secretAccessKey: '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',
   region: 'us-west-2'

      });

      return new Promise((resolve, reject) => {
        bucket.upload(paramsProduct, {}, (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data.Location);
          }
        });
      });
    }
  }


  getFileExtension(filename: string): string {
    const startIndex = filename.indexOf('.');
    const extenstion = filename.slice(startIndex + 1);
    return extenstion;
  }


  async handleImage(image: string | File) {
    return new Promise(async (resolve, reject) => {
      if (typeof image !== 'string') {
        const url = await this.uploadImageToS3(image);
        resolve(url);
        this.pImage.push(url)
      } else {
        resolve(image);
      }
    });
  }

  async handleImages(images: any[]) {
    return new Promise(async (resolve, reject) => {
      const newImages = [];
      for (const image of images) {
        const uploadedURl = await this.handleImage(image);
        newImages.push(uploadedURl);
      }
      resolve(newImages);
      this.sImage.push(newImages)
    });
  }


  async onSave(form) {
    
    if (this.data > 0) {
      this.artForm.Id = this.data;
      this.artForm.Description = this.description
      this.artForm.Details = this.details
      this.artForm.Tag = this.Tag;
            
      this.artForm.maincategory = this.categories.id;
      this.artForm.Feature = this.feature
      if (this.relatedArticles != null) {
        this.artForm.RelatedArtical.push(this.relatedArticles.id)
      }
      this.artForm.Title = this.title
      const primaryImage = await this.handleImage(this.priamryImage[0]);
      const secondryImages: any = await this.handleImages(this.secondryImages);
      this.artForm.Articleimage = primaryImage
      secondryImages.forEach(element => {
        this.artForm.Subimages.push(element)
      });
      this.artForm.Type = parseInt(this.type.id);
      this.artForm.ContentType = this.contentType.id;
      this.artForm.URL = this.URL
      this.artForm.Sort = this.Sort
      if (this.artForm.Articleimage) {
        this.serv.createUpdateArticle(this.artForm).subscribe(x => {
          this.disabled = true;
          this.dialogRef.close();
        })
      }


    }
    else {
      

      this.artForm.Description = this.description
      this.artForm.Details = this.details
      this.artForm.maincategory = this.categories.id;
      this.artForm.Feature = this.feature
      this.artForm.Tag = this.Tag;

      if (this.relatedArticles != null && this.relatedArticles.length > 0) {
        this.artForm.RelatedArtical.push(this.relatedArticles.id)

      }
      this.artForm.Title = this.title
      const primaryImage = await this.handleImage(this.priamryImage[0]);
      const secondryImages: any = await this.handleImages(this.secondryImages);
      this.artForm.Articleimage = primaryImage
      secondryImages.forEach(element => {
        this.artForm.Subimages.push(element)
      });
      this.artForm.Type = parseInt(this.type.id);
      this.artForm.ContentType = this.contentType.id;
      this.artForm.URL = this.URL
      this.artForm.Sort = this.Sort
      if (this.artForm.Articleimage) {
        this.serv.createUpdateArticle(this.artForm).subscribe(x => {
          this.disabled = true;
          this.dialogRef.close();
        })
      }
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  typeChange(event, id) {
    if (event != null) {
      let dropDownData = this.types.find((data: any) => data.id === event.value.id);
      this.type = dropDownData
    } else {

      let dropDownData = this.types.find((data: any) => data.id === id);
      this.type = dropDownData
    }

  }




}

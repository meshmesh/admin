import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newmanufacturer-view',
  templateUrl: './newmanufacturer-view.component.html',
  styleUrls: ['./newmanufacturer-view.component.scss']
})
export class NewmanufacturerViewComponent implements OnInit {
  displayedColumns: any = ['id','name','annualSales','numberOfDeals',  'numberOfDroped', 'numberOfAssigned','dateOfOpportunity','action']
  List = [];
  constructor() { }

  ngOnInit() {
    this.List = [
      {
        "id": 37255,
        "token": "d3ee7f29-dd43-4d63-aa31-baafaf86eb61",
        "name": "LUKI PRODUCTS",
        "numberOfDeals": 2,
        "numberOfAssigned": 3,
        "annualSales": "$100K - $1M",
        "formula": null,
        "country": {
          "id": 175,
          "name": "Pakistan",
          "country": "Pakistan",
          "value": 175,
          "display": "Pakistan",
          "countryCode": "PK",
          "regionId": null,
          "count": null,
          "code": "PK",
          "logo": "http://beta.aumet.me/flags24/PK.png"
        },
        "scientificName": [
          {
            "id": 3036,
            "iconImg": "Cataract Knives.jpg",
            "scientificName": "Ziegler Cataract Knives",
            "value": 0,
            "display": null,
            "speciality": null,
            "specialityIconImg": null,
            "specialityId": null,
            "medicalLineId": null,
            "medicalLine": null,
            "name": "Ziegler Cataract Knives",
            "nameDisplay": "Ziegler Cataract Knives",
            "doYouMean": null
          }
        ],
        "specialities": [
          
        ],
        "distributors": [
          {
            "id": 24910,
            "token": "15aaebd4-2d21-4ee7-9970-170732430b64",
            "name": "Pinnacle Pharma Ltd",
            "formula": "#ScientificName #Buyers #Country #Specialty #Targeting #Patent #Maknez",
            "annualSales": "$100K - $2M",
            "numberOfDeals": 0,
            "numberOfDroped": 0,
            "numberOfAssigned": 0,
            "country": {
              "id": 2,
              "name": "UAE",
              "country": "UAE",
              "value": 2,
              "display": "UAE",
              "countryCode": "AE",
              "regionId": null,
              "count": null,
              "code": "AE",
              "logo": "http://beta.aumet.me/flags24/AE.png"
            },
            "dateOfOpportunity": "0001-01-01T00:00:00"
          },
          {
            "id": 24910,
            "token": "15aaebd4-2d21-4ee7-9970-170732430b64",
            "name": "Pinnacle Pharma Ltd",
            "formula": "#ScientificName #Buyers #Country #Specialty #Targeting #Patent #Maknez",
            "annualSales": "$100K - $2M",
            "numberOfDeals": 0,
            "numberOfDroped": 0,
            "numberOfAssigned": 0,
            "country": {
              "id": 2,
              "name": "UAE",
              "country": "UAE",
              "value": 2,
              "display": "UAE",
              "countryCode": "AE",
              "regionId": null,
              "count": null,
              "code": "AE",
              "logo": "http://beta.aumet.me/flags24/AE.png"
            },
            "dateOfOpportunity": "0001-01-01T00:00:00"
          },
          {
            "id": 24910,
            "token": "15aaebd4-2d21-4ee7-9970-170732430b64",
            "name": "Pinnacle Pharma Ltd",
            "formula": "#ScientificName #Buyers #Country #Specialty #Targeting #Patent #Maknez",
            "annualSales": "$100K - $2M",
            "numberOfDeals": 0,
            "numberOfDroped": 0,
            "numberOfAssigned": 0,
            "country": {
              "id": 2,
              "name": "UAE",
              "country": "UAE",
              "value": 2,
              "display": "UAE",
              "countryCode": "AE",
              "regionId": null,
              "count": null,
              "code": "AE",
              "logo": "http://beta.aumet.me/flags24/AE.png"
            },
            "dateOfOpportunity": "0001-01-01T00:00:00"
          },{
            "id": 24910,
            "token": "15aaebd4-2d21-4ee7-9970-170732430b64",
            "name": "Pinnacle Pharma Ltd",
            "formula": "#ScientificName #Buyers #Country #Specialty #Targeting #Patent #Maknez",
            "annualSales": "$100K - $2M",
            "numberOfDeals": 0,
            "numberOfDroped": 0,
            "numberOfAssigned": 0,
            "country": {
              "id": 2,
              "name": "UAE",
              "country": "UAE",
              "value": 2,
              "display": "UAE",
              "countryCode": "AE",
              "regionId": null,
              "count": null,
              "code": "AE",
              "logo": "http://beta.aumet.me/flags24/AE.png"
            },
            "dateOfOpportunity": "0001-01-01T00:00:00"
          }
        ]
      }];
  }
  test(){
    console.log("hey");
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessRoutingModule } from './business-routing.module';
import { BusinessComponent } from './business.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BusinessService } from './business.service';

import { MatDialogModule, MatCardModule } from '@angular/material';
import { SignedAgreementsService } from './signed-agreements.service';
import { SignedAgreementsComponent } from './signed-agreements/signed-agreements.component';
import { FilterModule } from 'src/app/filter/filter.module';
import { ActionsPopupComponent } from './actions-popup/actions-popup.component';
import { CreateSignedAgreementComponent } from './create-signed-agreement/create-signed-agreement.component';
import { ManufacturerModule } from '../manufacturer/manufacturer.module';
import { DistributorModule } from '../distributor/distributor.module';
import { CreateBusinessOppComponent } from './create-business-opp/create-business-opp.component';
@NgModule({
  declarations: [
    BusinessComponent,
    SignedAgreementsComponent, 
    CreateSignedAgreementComponent,
    ActionsPopupComponent,
    CreateBusinessOppComponent
  ],
  imports: [
    CommonModule,
    BusinessRoutingModule,
    SharedModule,
    FilterModule,
    MatCardModule,
    MatDialogModule,
    ManufacturerModule,
    DistributorModule
  ],
  providers: [
    BusinessService,
    SignedAgreementsService
  ],
  entryComponents: [
    ActionsPopupComponent,
    CreateSignedAgreementComponent,
    CreateBusinessOppComponent
  ]
}) 
export class BusinessModule { }
 
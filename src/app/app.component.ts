import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MessagingService } from './_services/messaging.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>(); 
  title = 'Aumet';
  message: any;
  constructor(private router: Router/* , private msgService: MessagingService */) {}

  ngOnInit() {
    this.router.events.pipe(takeUntil(this.onDestroy$)).subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });/* 
    this.msgService.init();
    this.message = this.msgService.currentMessage; */
  }
}

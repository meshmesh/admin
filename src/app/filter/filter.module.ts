import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { AutoCompleteMultiComponent } from './auto-complete-multi/auto-complete-multi.component';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';
import { MatAutocompleteModule, MatChipsModule, MatInputModule, MatFormFieldModule, MatSelectModule, MatIconModule, MatDatepickerModule, MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ng2-ui-switch';
import { OrderModule } from 'ngx-order-pipe';
import { Ng5SliderModule } from 'ng5-slider';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarModule,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { TagsComponent } from './tags-input/tags-input.component';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  imports: [
    CommonModule,
    MatAutocompleteModule,
    MatChipsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    UiSwitchModule,
    OrderModule,
    MatIconModule,
    PerfectScrollbarModule,
    MatDatepickerModule,
    MatCheckboxModule,
    Ng5SliderModule

  ],
  exports: [
    FilterComponent, AutoCompleteMultiComponent, AutoCompleteComponent, TagsComponent,
    MatAutocompleteModule,
    MatChipsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    UiSwitchModule,
    OrderModule,
    MatIconModule  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },],
  declarations: [FilterComponent, AutoCompleteMultiComponent, AutoCompleteComponent, TagsComponent]
})
export class FilterModule { }

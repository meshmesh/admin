export interface ScientificName {
  adminComment: any;
  approvedBy: any;
  attributeType: number[];
  attributes: any[];
  broaderScientificName: any[];
  broaderterms: any[];
  categoryid: number;
  classIds: any[];
  classes: any[];
  companies: any[];
  comapny: { id: number; name: string; token: string; slug: any; currentState: any };
  companyIds: any;
  createdAt: string | Date | number;
  createdBy: number | string;
  details: any;
  iconImg: string;
  id: number;
  isApproved: boolean;
  manufacturers: any[];
  marketAnalysis: any;
  medicalLine: { name: string; id: number; value: number };
  medicallineid: number;
  narrowerScientificName: ScientificTerm;
  narrowerterms: number[];
  nonpreferredterms: any[];
  note: any;
  numberOfDistributors: number;
  numberOfManufactures: number;
  numberOfProduct: number;
  rank: any;
  relatedScientificName: any[];
  relatedterms: any[];
  scientificName: string;
  scientificclassid: any;
  speciality: { display: string; icon: string; id: number; name: string; orderId: number; value: number };
  specialityId: number;
  totalNumber: number;
  usedFor: string;
}

export interface ScientificTerm {
  display: string;
  doYouMean: any;
  iconImg: string;
  id: number;
  medicalLine: string;
  medicalLineId: number;
  name: string;
  nameDisplay: string;
  scientificName: string;
  speciality: string;
  specialityIconImg: string;
  specialityId: number;
  value: number;
}

export interface ScientificClass {
  approved: boolean;
  companyId: number;
  deleted: boolean;
  id: number;
  langId: any;
  majorId: number;
  name: string;
  note: string;
  orderId: number;
  value: number;
}

export class ScientificAttribute {
  approved: boolean;
  companyId: number;
  deleted: boolean;
  id: number;
  langId: number;
  majorId: number;
  name: string;
  note: string;
  orderId: number;
  value: number;
}

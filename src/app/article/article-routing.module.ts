import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { AddEditArticleComponent } from './article/add-edit-article/add-edit-article.component';
import { ArticleFormComponent } from './article-form/article-form.component';
const routes: Routes = [
  {
    path: '',
    component: ArticleComponent,
    data: {
      title: 'article',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  {
    path: 'form',
    component: ArticleFormComponent,
    data: {
      title: 'Add-Article',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  {
    path: 'form/:id',
    component: ArticleFormComponent,
    data: {
      title: 'Edit-Article',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  {
    path: 'add/:id',
    component: AddEditArticleComponent,
    data: {
      title: 'Edit-Article',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  {
    path: 'add',
    component: AddEditArticleComponent,
    data: {
      title: 'Edit-Article',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule {}

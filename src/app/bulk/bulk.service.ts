import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BulkService {
  APIEndpoint = environment.ApiUrl;

  constructor(private http: HttpClient) { }

  getdata(pageIndex,filterres) : Observable<any>{ 
    return  this.http.get<any>(this.APIEndpoint + '/api/AdminBulkProduct/Search?PageSize=10&PageNumber='+pageIndex+'&'+filterres);
    }
    getbyid(id): Observable<any>{
      return  this.http.get<any>(this.APIEndpoint + '/api/AdminBulkProduct/Search?BulkProductId='+id);

    }
    getManufacturersuggestions(str): Observable<any> {
      const url =
      this.APIEndpoint +
        '/api/Companies/GetListOfCompaniesLimited?Name=' +
        str +
        '&Type=manufacturer';
      return this.http.get<any>(url);
    }
    getSpeciality() {
      const url =this.APIEndpoint + '/api/Specialities/GetSpecialities';
      return this.http.get(url);
    }
    getScientificNameSuggestion(str): Observable<any> {
      const url =
        this.APIEndpoint +
        `/api/AdminScientificNames/SearchScientificNames?PageSize=10&PageNumber=1&Name=${str}`;
      return this.http.get<any>(url);
    }
    getScientificNameById(id): Observable<any> {
      const url =
        this.APIEndpoint +
        '/api/AdminScientificNames/AdminSearchScientificNames?ScientificNameId='+id;
      return this.http.get<any>(url);
    }
    getMadeIn() {
      const url = this.APIEndpoint + '/api/Countries/GetCountries';
      return this.http.get(url);
    }
    getSellingPoints() {
      const url = this.APIEndpoint + '/api/Lookups/Get?MajorCode=2';
      return this.http.get(url);
    }
    GetSpecialityById(id) {
      const url = this.APIEndpoint + '/api/Specialities/'+id;
      return this.http.get(url);
    }
    
  getBuyers() {
    const url = this.APIEndpoint + '/api/Lookups/Get?MajorCode=3';
    return this.http.get(url);
  }
  

  SearchBulkProd(pageNumber,text, data) {

    return this.http.get(this.APIEndpoint + '/api/AdminBulkProduct/Search?PageSize=10&PageNumber=' + pageNumber + '&ProductTitle=' + text +'&'+ data);

  }
  addUpdateProduct(obj) {
    const url = this.APIEndpoint + '/api/AdminBulkProduct/Update';
    return this.http.post(url, obj);
  }
}

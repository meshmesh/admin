export class Assign {
    EmailTemplateId: number;
    SenderId: number;
    ManfacturerId: number;
    DistributorId: number;
    Subject: string;
    Body: string;
}

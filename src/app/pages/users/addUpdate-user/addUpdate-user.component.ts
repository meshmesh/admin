import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersService } from '../users.service';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';

// interface AddUpdateUserDialogData {
//   company?: { id: number; name: string };
//   type?: string;
// }
@Component({
  selector: 'app-addUpdate-user',
  templateUrl: './addUpdate-user.component.html',
  styleUrls: ['./addUpdate-user.component.scss']
})
export class AddUpdateUserComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  values: any[] = [];
  ofCompany: any;
  countries: any = [];
  userForm: User;
  titles: any = [];
  id: any;
  userImag: any[] = [];
  imagePreview = [];
  modalData: any;
  localImages: File[] = [];
  imgurl: any = 'https://s3-us-west-2.amazonaws.com/company/avatar';
  testurl: any = 'https://aumet-data.s3.us-west-2.amazonaws.com/company/avatar/';

  constructor(
    private serv: UsersService,
    private router: ActivatedRoute,
    public dialog: MatDialogRef<AddUpdateUserComponent>,
    private http: HttpClient,
    public dialogRef: MatDialogRef<AddUpdateUserComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private toast: ToastService,
  ) { }

  ngOnInit() {
    debugger;
    this.userForm = new User();
    this.modalData = this.dialogData.items;
    if (this.modalData.id) {
      this.userForm.id = this.modalData.id;
    }
    if (this.userForm.id) {
      this.userForm.firstName = this.modalData.firstName;
      this.userForm.lastName = this.modalData.lastName;
      this.userForm.email = this.modalData.email;
      this.userForm.phone = this.modalData.phoneNumber;
      this.userForm.stage = this.modalData.stage;
      this.userForm.isAdmin = this.modalData.isAdmin;
    }
    this.userForm.companyId = this.dialogData.details.companyID;
  }

  async submit(obj) {
    if (obj.invalid) {
      return;
    }
    if (this.userForm.id > 0)
      this.UpdateFunction(this.userForm);
    else
      this.addFunction(this.userForm);
  }

  addFunction(form) {
    const _thisTimer = this;
    this.http.post(environment.ApiUrlV3 + 'Users', form).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {

      if (!res.success) {
        _thisTimer.toast.error(res.errorCode);
      } else {
        setTimeout(function () {
          _thisTimer.toast.success('Has been added successfuly');
          _thisTimer.close('success');
        }, 1500);
      }
    });
  }

  UpdateFunction(form) {
    const _thisTimer = this;
    this.http.put(environment.ApiUrlV3 + 'Users/UpdateUserInfo', form).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      if (!res.success) {
        _thisTimer.toast.error(res.errorCode);
      } else {
        setTimeout(function () {
          _thisTimer.toast.success('Has been added successfuly');
          _thisTimer.close('success');
        }, 1500);
      }
    });
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  onCancel(): void {
    this.dialog.close();
  }
}

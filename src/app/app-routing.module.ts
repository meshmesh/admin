import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';

import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './_guards/auth.guard';
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'inbox',
        loadChildren: './inbox/inbox.module#InboxModule'
      },
      {
        path: 'Companies',
        children: [
          {
            path: 'Manufacturer',
            loadChildren: './pages/manufacturer/manufacturer.module#ManufacturerModule'
          },
          {
            path: 'Distributor',
            loadChildren: './pages/distributor/distributor.module#DistributorModule'
          }
        ]
      },
      {
        path: 'Crawling',
        children: [
          {
            path: 'Crawler',
            loadChildren: './Crawler/Crawler.module#CrawlerModule'
          },
          {
            path: 'CompanyScientificNames',
            loadChildren: './crawlercompanyscientificenames/crawlercompanyscientificenames.module#CrawlercompanyscientificenamesModule'

          },
          {
            path: 'new',
            loadChildren: './crawlerpage/crawler/crawler.module#CrawlerModule'
          },
        ]
      },

      {
        path: 'Setup/Scientificname',
        loadChildren: './pages/scientific-name/scientific-name.module#ScientificNameModule'

      },
      {
        path: 'Setup/NewScientificname',
        loadChildren: './pages/new-scientific-name/new-scientific-name.module#NewScientificNameModule'

      },
      {
        path: 'Setup/Product',
        loadChildren: './pages/products/products.module#ProductsModule'
      },
      {
        path: 'Inquiries/Inquiry',
        loadChildren: './pages/inquiry/inquiry.module#InquiryModule'
      },
      {
        path: 'Business',
        children: [
          {
            path: 'Opportunity',
            loadChildren: './pages/business-opp/business.module#BusinessModule'
          }
        ]
      },


      {
        path: 'StaticPages/Aumetknowledge',
        children: [
          {
            path: 'Articles1',
            loadChildren: './newArticle/articleTable/articleTable.module#ArticleTableModule'

          },
          {
            path: 'Videos',
            loadChildren: './videosPage/videosPage.module#VideosPageModule'
          },
          {
            path: 'Reviews',
            loadChildren: './reviews/reviews.module#ReviewsModule'
          },
        ]
      },
      {
        path: 'StaticPages',
        children: [
          {
            path: 'Dynamic1',
            loadChildren: './dynamicPage/dynamicPageTable/dynamicPageTable.module#DynamicPageTableModule'

          },
          {
            path: 'Countries',
            loadChildren: './countryPage/countryPage.module#CountryPageModule'

          }]
      },

      {
        path: 'Setting/Users',
        loadChildren: './pages/users/users.module#UsersModule'

      },
      {
        path: 'Setting/Region',
        loadChildren: './regoin/regoin.module#RegoinModule'

      },

      {
        path: 'Bulk/Product',
        loadChildren: './bulk/bulk.module#BulkModule'

      },

      {
        path: 'Setting/Majors',
        loadChildren: './lookup/lookup.module#LookupModule'

      },
      {
        path: 'Setting/Interest',
        loadChildren: './pages/interest/interest.module#InterestModule'

      },
      {
        path: 'Bulk/Contact',
        loadChildren: './contact/contact.module#ContactModule'

      },

      {
        path: 'Setting/Settings',
        loadChildren: './settings/settings.module#SettingsModule'

      },
      {
        path: 'Setting/EmailTemplate',
        loadChildren: './emailTemplate/emailTemplate.module#EmailTemplateModule'

      },

      {
        path: 'Setup/Speciality',
        loadChildren: './speciality/speciality.module#SpecialityModule'

      },
      {
        path: 'dataList',
        loadChildren: './dataList/dataList.module#DataListModule'

      },
      {
        path: 'notifications',
        loadChildren: './notifications/notifications.module#NotificationsModule'

      },
      {
        path: 'FollowUp',
        loadChildren: './followup/followup.module#FollowupModule', 
        canActivate: [AuthGuard],
      },
      {
        path: 'Setting/Languages',
        loadChildren: './languages/languages.module#LanguagesModule',
        canActivate: [AuthGuard],
      },

    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    redirectTo: '',
    pathMatch: 'full',
  },
  { path: '**', component: AdminComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

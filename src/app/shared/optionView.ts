export interface OptionView {
    id: number;
    name: string;
}

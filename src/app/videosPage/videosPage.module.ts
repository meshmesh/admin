import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillEditorModule } from 'ngx-quill-editor';
import { TagInputModule } from 'ngx-chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatFormFieldModule, MatInputModule, MatDialogModule } from '@angular/material';
import { VideosPageService } from './videosPage.service';
import { AddVideoComponent } from './addVideo/addVideo.component';
import { VideosPageRoutingModule } from './videosPage-routing.module';
import { VideosPageComponent } from './videosPage.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    QuillEditorModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    VideosPageRoutingModule,
    MatInputModule,
    MatDialogModule
  ],
  providers: [VideosPageService],
  exports: [VideosPageComponent],
  declarations: [VideosPageComponent, AddVideoComponent],
  entryComponents: [AddVideoComponent]
})
export class VideosPageModule {}

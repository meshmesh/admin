import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryPageComponent } from './countryPage.component';
const routes: Routes = [
  {
    path: '',
    component: CountryPageComponent,
    data: {
      title: 'Countries',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryPageRoutingModule {}

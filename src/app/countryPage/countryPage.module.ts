import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryPageComponent } from './countryPage.component';
import { AddCountryComponent } from './addCountry/addCountry.component';
import { SharedModule } from '../shared/shared.module';
import { MatDialogModule, MatInputModule, MatExpansionModule, MatFormFieldModule } from '@angular/material';
import { CountryPageRoutingModule } from './countryPage-routing.module';
import { QuillEditorModule } from 'ngx-quill-editor';
import { TagInputModule } from 'ngx-chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountryPageService } from './countryPage.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    QuillEditorModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatInputModule,
    MatDialogModule,
    CountryPageRoutingModule,
    MatExpansionModule,
    MatFormFieldModule,
    NgbModule
  ],
  providers: [CountryPageService],
  exports: [CountryPageComponent],
  declarations: [CountryPageComponent, AddCountryComponent],
  entryComponents: [AddCountryComponent]
})
export class CountryPageModule { }

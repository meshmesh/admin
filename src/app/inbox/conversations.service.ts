import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import Pusher from 'pusher-js';
import { CurrentUser } from '../_model/CurrentUser';
import { AuthService } from '../_services/auth.service';
@Injectable({
  providedIn: 'root'
})
export class ConversationsService {

  pusher: Pusher;
  currentUser: CurrentUser;
  constructor(private http: HttpClient, private authService: AuthService) {
    this.currentUser = this.authService.currentUserValue;
  


  }

  GetConversations() {
    return this.http.get(environment.ApiUrl + '/api/Group/GetAll');
  }

  init() {
    let _this = this;
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      authEndpoint: environment.ApiUrl + '/api/Channel/ChannelAuthAsync',
      auth: {
        headers: {
          'authorization': 'Bearer ' + _this.currentUser.jwt
        }
      }
    });
  }

  createMessage(messageObject, groupId) {
    messageObject.SocketId = this.pusher.connection.socket_id;
    messageObject.GroupId = groupId;
    return this.http.post(environment.ApiUrl + '/api/Message/Create', messageObject);
  }
  GetMessages(conversationId) {
    return this.http.get(environment.ApiUrl + '/api/Message/GetByIdAdmin/' + conversationId);
  }
  GetUserLog() {
    return this.http.get(environment.ApiUrl + '/api/Message/GetUserLog');
  }

  SubscribeToNewGroup() {
    return this.pusher.subscribe('group_chat')

  }
  SubscripeToChannel(groupId) {
    return this.pusher.subscribe('private-' + groupId);
  }
  NewGroupCreated() {
    this.SubscribeToNewGroup().bind('new_group', (data) => {
      this.SubscripeToChannel(data.ID).bind('new_message', (message) => {

      });
    });
  }

  ReceviedNewNotification() {
    return this.pusher.subscribe('new_notification');
  }

  hasReceived() {
    this.ReceviedNewNotification().bind('new_action_log', (not) => {
      // new notification will be received here 
    });
  }



}

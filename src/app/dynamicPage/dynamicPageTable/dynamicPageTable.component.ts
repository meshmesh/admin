import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { AddDynamicComponent } from '../addDynamic/addDynamic.component';
import { DynamicPageTableService } from './dynamicPageTable.service';
import { ReplaySubject, of } from 'rxjs';
import { takeUntil, switchMap, take, tap } from 'rxjs/operators';
import { NewSeoModalComponent } from 'src/app/shared/seo-modal/seo-modal.component';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: "app-dynamicPageTable",
  templateUrl: './dynamicPageTable.component.html',
  styleUrls: ['./dynamicPageTable.component.scss']
})
export class DynamicPageTableComponent implements OnInit, OnDestroy {
  onDestroy$ = new ReplaySubject<void>();

  displayedColumns: any[] = ['Actions', 'Type', 'Title',  'Description'];
  constructor(
    private dialog: MatDialog,
    private serv: DynamicPageTableService,
    private _clipboardService: ClipboardService
  ) { }
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  Index = 1;
  filteredString = '';
  length = 10;
  pageSize = 10;
  ngOnInit() {
    this.loadPages();
  }
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  renderDataSource(data) {
    if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.length = data[0].totalNumber;
    }
    this.dataSource = new MatTableDataSource<any>(data);
  }

  Add() {
    const dialogRef = this.dialog.open(AddDynamicComponent, {
      width: '80%',
      height: '70%'
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(result => {
        console.log('The dialog was closed');
        this.loadPages();
      });
  }

  copylink(row){
    if (row.categoryId == 1) {
      window.open("https://www.aumet.me/d/" +row.slug +"?type=1", "_blank");   

    } else {
      window.open("https://www.aumet.me/d/" +row.slug +"?type=2", "_blank");   

    }

  }
  pageChange(event) {
    this.Index = event.pageIndex + 1;

    this.serv.getDynamicPage(this.Index, this.filteredString).subscribe(res => {
      this.renderDataSource(res);
    });
  }

  loadPages() {
    this.serv.getDynamicPage(this.Index, this.filteredString).subscribe(res => {
      this.renderDataSource(res);
    });
  }
  /*
handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });
    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });
    this.serv.usersFilter(this.pageIndex, this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }
 */
  Publish(id) {

    this.serv
      .Publish(id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(e => {
        this.loadPages(), console.log(e);
      });
  }
  UnPublish(id) {
    this.serv.unPublish(id).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(e => {
      this.loadPages(), console.log(e);
    });
  }
  Edit(values) {
    const dialogRef = this.dialog.open(AddDynamicComponent, {
      width: '80%',
      height: '70%',
      data: values
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(result => {
        console.log('The dialog was closed');
        this.loadPages();
      });
  }
  Delete(id) {
    this.serv.Delete(id).subscribe(x => {
      this.loadPages();
    });
  }

  onOpenSEO(row: any): void {
    
    const dialogRef = this.dialog.open(NewSeoModalComponent, {
      data: row.seoData,
      minWidth: '650px',
      minHeight: '500px'
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap(seo => {
          if (seo) {
            const s = JSON.stringify(seo);
            const obj = {
              Id: row.id,
              Items: [
                {
                  Key: 'SEO',
                  Value: s
                }
              ]
            };
            
            return this.serv.UpdateSEO(obj);
          } else {
            return of(null);
          }
        }),
        tap(res => {
          if (res) {
            this.loadPages();
          }
        })
      )
      .subscribe();
  }
}

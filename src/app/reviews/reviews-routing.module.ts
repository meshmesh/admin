import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewsComponent } from './reviews.component';
const routes: Routes = [
  {
    path: '',
    component: ReviewsComponent,
    data: {
      title: 'Reviews',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewsRoutingModule {}

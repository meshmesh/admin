import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { of, ReplaySubject, throwError, Observable } from "rxjs";
import { takeUntil, catchError } from "rxjs/operators";
import { ToastService } from "src/app/_services/toast.service";
import { ManufacturerService } from "../../services/manufacturer.service";
import { ProsSciName } from "../../model/manufacturer.model";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-add-edit-sciname",
  templateUrl: "./add-edit-sciname.component.html",
  styleUrls: ["./add-edit-sciname.component.scss"],
})
export class AddEditScinameComponent implements OnInit, OnDestroy {
  basic_title: string;
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(
    public dialogRef: MatDialogRef<AddEditScinameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private serv: ManufacturerService,
    private toast: ToastService
  ) {}

  intForm: ProsSciName;

  sciNameOptionsList: any[] = [];
  medline: any[] = [];
  specialities: any[] = [];
  modalData: any;
  imagePreview = [];
  localImages: File[] = [];

  ngOnInit() {
    debugger;
    this.basic_title = "Add";
    this.intForm = new ProsSciName();
    this.renderMedicleLine();
    this.renderScientificName();
    this.modalData = this.data.items;
    this.intForm.Id = 0;


    if (this.modalData.id) {
      this.basic_title = "Update";
      this.intForm.Id = this.modalData.id;

      this.displayScientific(this.modalData.scientificName);
      this.displaySpeciality(this.modalData.specialty);
      this.displayMedicalLine(this.modalData.medicalLine);

      this.getSpeciality(this.modalData.medicalLine.id);

      this.intForm.Scientific = this.modalData.scientificName;
      this.intForm.MedicleLine = this.modalData.medicalLine;
      this.intForm.Specialities = this.modalData.specialty;
    }
  }



  onCancel(): void {
    this.dialogRef.close(null);
  }



  submit(form: NgForm) {
    if (form.invalid) { 
      this.toast.error("Please check all required fields!");
      return;
    }

    debugger
    let formDataToSend = {
      ProspectedCompanyId: this.data.details.prospectedCompanyId,
      Id: 0,
      ScientificNameId: this.intForm.Scientific.id,
      MedicalLineId: this.intForm.MedicleLine.id,
      specialtyId: this.intForm.Specialities.id
    };


    if (this.intForm.Id != null || this.intForm.Id != 0) {
      formDataToSend.Id = this.intForm.Id;
    }


    if (formDataToSend.Id > 0) {
      this.serv
        .UpdateProspectedSciName(formDataToSend)
        .pipe(
          catchError((err) => {
            this.toast.error(err);
            return throwError(err);
          })
        )
        .subscribe(
          (res) => {
            this.dialogRef.close(true);
            this.toast.success("Updated Successfuly");
          },
          (error) => console.log(">>> ", error)
        );
    } else {
      this.serv
        .AddProspectedSciName(formDataToSend)
        .pipe(
          catchError((err) => {
            this.toast.error(err);
            return throwError(err);
          })
        )
        .subscribe(
          (res) => {
            this.dialogRef.close(true);
            this.toast.success("Added Successfuly");
          },
          (error) => console.log(">>> ", error)
        );
    }
  }

  handleScientificChanging(event) {
    let searchText = "";
    if (event.name !== undefined) {
      this.intForm.MedicleLine = event.medicalLine;
      this.intForm.Specialities = event.specialities[0];
      searchText = event.name;
    } else searchText = event;

    this.renderScientificName(searchText);
  }

  renderScientificName(searchvalue: string = '') {
    let searchText = "&Name=" + searchvalue;
    this.serv
      .getScientificNameSuggestion(searchText)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.sciNameOptionsList = res.data.items;
        this.displayScientific(res.data.items);
      });
  }

  displayScientific(Scient) {
    return Scient ? Scient.name : undefined;
  }

  displaySpeciality(spec) {
    return spec ? spec.name : undefined;
  }

  displayMedicalLine(medLine) {
    return medLine ? medLine.name : undefined;
  }

  renderMedicleLine() {
    this.serv
      .getmedicalline()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        this.medline = res.data;
        this.displayMedicalLine(res.data);
      });
  }

  bindspecialities(e: any) {
    this.getSpeciality(e.option.value.id);
  }

  getSpeciality(medicalLineId) {
    this.serv
      .getSpecalities(medicalLineId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.specialities = res.data;
        this.displaySpeciality(res.data);
      });
  }

  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(
      this.specialities.filter(
        (item) => item.name.toLowerCase().indexOf(filterValue) === 0
      )
    );
  }
}

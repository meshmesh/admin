import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrawlercompanyscientificenamesComponent } from './crawlercompanyscientificenames.component';
import { crawlercompanyscientificenamesRoutingModule } from './crawlercompanyscientificenames-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FilterModule } from '../filter/filter.module';

@NgModule({
  imports: [
    CommonModule,
    crawlercompanyscientificenamesRoutingModule,
    SharedModule,
    FilterModule
  ],
  declarations: [CrawlercompanyscientificenamesComponent]
})
export class CrawlercompanyscientificenamesModule { }

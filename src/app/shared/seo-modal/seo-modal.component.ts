import { Component, OnInit, Inject, ViewChildren, QueryList } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SEO } from 'src/app/_model/SEO';
import { SeoFormComponent } from './seo-form/seo-form.component';

@Component({
  selector: 'app-seomodal',
  templateUrl: './seo-modal.component.html',
  styleUrls: ['./seo-modal.component.scss']
})
export class NewSeoModalComponent implements OnInit {
  @ViewChildren('form') forms: QueryList<SeoFormComponent>;

  constructor(public dialogRef: MatDialogRef<NewSeoModalComponent>, @Inject(MAT_DIALOG_DATA) public data: SEO[]) {}

  ngOnInit() {}

  onSave(): void {
    const newSeo = this.forms.map(f => f.createObject());
    this.dialogRef.close(newSeo);
  }

  onCancel(): void {
    this.dialogRef.close(false);
  }
}

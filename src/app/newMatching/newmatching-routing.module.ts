import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewMatchingComponent } from './newMatching.component';
const routes: Routes = [
  {
    path: '',
    component: NewMatchingComponent,
    data: {
      title: 'Matching',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  
  }
 


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewMatchingRoutingModule { }

import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import * as S3 from 'aws-sdk/clients/s3';
import { Observable, of, ReplaySubject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';
import { ProductToCreate } from '../../models/productToCreate';
import { ProductCatalogService } from '../../services/products-catalog.service';
import { Product } from '../../models/product';

interface ProductFormDialogData {
  company: { id: number; name: string };
}

@Component({
  selector: 'app-add-update-product',
  templateUrl: './add-update-product.component.html',
  styleUrls: ['./add-update-product.component.scss']
})
export class AddUpdateProductComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  @ViewChild('sellingpointinput') sellingpointinput: ElementRef<HTMLInputElement>;
  @ViewChild('buyersInput') buyersInput: ElementRef<HTMLInputElement>;
  @ViewChild('scientificnameInput') scientificnameInput: ElementRef<HTMLInputElement>;


  product: NgForm;
  productForm: Product;
  productId: number;
  manuFacturerID: number;
  seoData: any[] = [];
  baseImagePreview = [];
  imagePreview = [];
  localBaseImage: File[] = [];
  localImages: File[] = [];
  localCatalogImage: File[] = [];
  catalogImagePreview: any[] = [];
  finalImagess: any = {};
  finalBaseImage: string[] = [];
  countries: any[] = [];
  specialities: any[] = [];
  catalogs: any[] = [];
  invalid = false;

  countriesFilteration: Observable<any>;
  manufacturerFilter: Observable<any>;
  showCatType = false;
  removable = true;
  taskCount = 0;
  updateTaskCount = 0;
  filteredBuyers: Observable<any>;
  buyers: any[] = [];
  seletedBuyers: any[] = [];
  filteredSellingPoints: Observable<any>;
  sellingPoints: any[] = [];
  seletedSellingPoints: any[] = [];
  productToCreate: ProductToCreate;
  filteredscientificNames: Observable<any>;
  totalCount: number;
  arraycat = [];
  basic_title: string;

  selectedSciName;
  isImageToDisplay: boolean = false;

  uploadedFileName: string = 'Click to Choose file from your PC'
  submitLoader: boolean;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productCatalogService: ProductCatalogService,
    private http: HttpClient,
    private toast: ToastService,
    public dialogRef: MatDialogRef<AddUpdateProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any | ProductFormDialogData
  ) { }


  ngOnInit() {
    debugger
    this.getCountries();
    this.getSellingPoints();
    this.getManuSuggestions();
    this.getBuyers();
    this._filterManufacturer('a');
    this.productForm = new Product();
    this.basic_title = "Add";

    if (this.data && this.data.product && this.data.product.id) {
      this.basic_title = "Update";
      this.seletedSellingPoints = this.data.sellingPoints;
      this.seletedBuyers = this.data.buyers;
      this.productForm.sellingPoints = this.data.sellingPoints;
      this.productForm.buyer = this.data.buyers;
      this.productForm.manufacturer = this.data.product.company;
      this.productForm.madeIn = this.data.product.productMadeIn;
      this.productForm.productRangeId = this.data.product.productrangeId;
      this.productForm.scientificNames = this.data.product.scientificName;
      this.productForm.catalogName = this.data.product.catalogueUrl;
      this.productForm.aumetComment = this.data.product.comments;
      this.productForm.description = this.data.product.productDescription;
      this.productForm.id = this.data.product.id;
      this.productForm.image = this.data.product.baseImage;
      this.productForm.published = this.data.product.isPublished;
      this.productForm.productTitle = this.data.product.productTitle;
      this.productForm.productSubTitle = this.data.product.productSubtitle;

      this.baseImagePreview[0] = this.data.product.baseImage;
      this.imagePreview = this.data.product.subImage;
      this.catalogImagePreview.push(this.data.product.catalogueUrl);

      this.uploadedFileName = this.productForm.catalogName ? this.productForm.catalogName : 'Click to Choose file from your PC';
      this.getCatalog(this.data.product.company.id);
      this.showCatType = true;
    }
  }

  removeBuyers(i) {
    const temp = this.seletedBuyers[i];
    this.buyers.push(temp);
    this.seletedBuyers.splice(i, 1);
  }
  removeSellingPoints(i) {
    const temp = this.seletedSellingPoints[i];
    this.sellingPoints.push(temp);
    this.seletedSellingPoints.splice(i, 1);
  }




  getManuSuggestions() {
    this.productCatalogService.getManufacturersuggestions('').pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.manufacturerFilter = response.data;
      },
      error => console.log(error)
    );
  }

  getBuyers() {
    this.productCatalogService.getBuyers().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.buyers = response;
        this.filteredBuyers = of(this.buyers);
      },
      error => console.log(error)
    );
  }

  getSellingPoints() {
    this.productCatalogService.getSellingPoints().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.sellingPoints = response;
        this.filteredSellingPoints = of(this.sellingPoints);
      },
      error => console.log(error)
    );
  }

  getCountries() {
    this.productCatalogService.getMadeIn().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        
        this.countries = response.data;
        this.countriesFilteration = of(this.countries);
      },
      error => console.log(error)
    );
  }

  AddSelectedBuyers(event) {
    this.seletedBuyers.push(event.option.value);
    const index = this.buyers.indexOf(event.option.value);
    if (index > -1) {
      this.buyers.splice(index, 1);
      this.buyersInput.nativeElement.value = '';
    }
  }


  AddSelectedSellingPoints(event) {
    this.seletedSellingPoints.push(event.option.value);
    const index = this.sellingPoints.indexOf(event.option.value);
    if (index > -1) {
      this.sellingPoints.splice(index, 1);
      this.sellingpointinput.nativeElement.value = '';
    }
  }


  AddSelectedscientificNames(e) {
    debugger
    this.selectedSciName = e.option.value;
  }


  displayFnCounty(country?: any): string | undefined {
    return country ? country.name : undefined;
  }

  displayFnSpicality(speciality?: any): string | undefined {
    return speciality ? speciality.name : undefined;
  }

  displayFnSciName(SciName?: any): string | undefined {
    return SciName ? SciName.name : undefined;
  }

  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  _filterCountry(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.country;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.countries.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
  
  _filterBuyers(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value!.toLowerCase();
    }
    return of(this.buyers.filter(item => (item.name ? item.name.toLowerCase().indexOf(filterValue) === 0 : '')));
  }

  _filterSellingPoints(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.sellingPoints.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }

  filterscientificNames(value: any): void {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
      this.productCatalogService.getScientificNameSuggestion(`&Name=${filterValue}`)
      .pipe(takeUntil(this.onDestroy$)).subscribe(res => {
        this.filteredscientificNames = res.data.items;
      });
    }
  }
  
  _filterscientificNamesOnSpeciality(value: any) {
    this.productCatalogService
      .getScientificNameSuggestion('')
      .pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
        this.filteredscientificNames = res.data.items;
      });
  }

  _filterManufacturer(value: any): void {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    this.productCatalogService.getManufacturersuggestions(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.manufacturerFilter = response.data;
      },
      error => console.log(error)
    );
  }

  handleBaseImgError() {
    this.baseImagePreview.pop();
  }

  handleImageError(i) {
    this.imagePreview.splice(i, 1);
  }

  hideBaseImage() {
    this.baseImagePreview.pop();
    this.localBaseImage.pop();
  }

  hideimage(i) {
    this.imagePreview.splice(i, 1);
    this.localImages.splice(i, 1);
  }


  hidecatalog() {
    this.catalogImagePreview.pop();
    this.productForm.productRangeId = null;
  }

  handleBaseImageChange(e) {
    this.localBaseImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.baseImagePreview.pop();
        this.baseImagePreview.push(reader.result);
      };
    }
  }

  handleImageChange(e) {
    for (let i = 0; i < e.target.files.length; i++) {
      this.localImages.push(e.target.files[i]);
      if (e.target.files && e.target.files[i]) {
        const reader = new FileReader();
        reader.readAsDataURL(e.target.files[i]);
        reader.onload = e => {
          this.imagePreview.push({ url: reader.result });
        };
      }
    }
  }


  handleCatalogChange(e) {
    this.localCatalogImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = e => {
        this.catalogImagePreview = [];
        this.catalogImagePreview.push(reader.result);
        this.uploadedFileName = this.localCatalogImage[0].name;
        if(typeof reader.result === 'string' && reader.result.startsWith('data:image'))  {
          this.isImageToDisplay = true;
        } else {
          this.isImageToDisplay = false;
        }
      };
    }
  }

  getCatalog(id) {
    this.productCatalogService.getCatalogsByCompanyId(id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.catalogs = [];
        this.catalogs = response.data.items;
        debugger
        if (this.catalogs.length > 0) {
          this.showCatType = true;
        } else {
          this.hideCatalogImage();
          this.showCatType = false;
        }
      },
      error => console.log(error)
    );
  }



  showCat() {
    this.showCatType = !this.showCatType;
  }

  hideCatalogImage() {
    this.catalogImagePreview = [];
    this.localCatalogImage.pop();
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  async submit(form) {
    
    if (form.invalid) {
      this.toast.error("Please check all required fields!");
      return;
    }

    if (this.baseImagePreview[0] == null || this.baseImagePreview[0] == undefined || this.baseImagePreview[0].length == 0) {
      this.toast.error("Please select base image!");
      return;
    }
 
    this.taskCount = 0;
    this.updateTaskCount = 0;

    this.totalCount = this.localBaseImage.length + this.localImages.length + this.localCatalogImage.length;
    this.productToCreate = new ProductToCreate();

    this.productToCreate.aumetComment = this.productForm.aumetComment;
    this.productToCreate.baseImage = this.baseImagePreview[0];

    if (typeof this.productForm.madeIn === 'string') {
      this.invalid = true;
      return;
    }

    this.productToCreate.madeIn = this.productForm.madeIn.id;
    if (typeof this.productForm.manufacturer === 'string') {
      this.invalid = true;
      return;
    }

    if(!this.productForm.scientificNames.id) {
      this.toast.error("Please select a valid Sci name");
      return
    }

    if(this.productForm.productRangeId && String(this.productForm.productRangeId) !== "0") {
      this.productToCreate.productrangeId = this.productForm.productRangeId;
    } else {
      this.productToCreate.productrangeId = null;
    }

    this.productToCreate.manufacturerId = this.productForm.manufacturer.id;
    this.productToCreate.productDescription = this.productForm.description;
    this.productToCreate.productSubtitle = this.productForm.productSubTitle;
    this.productToCreate.productTitle = this.productForm.productTitle;
    this.productToCreate.seo = JSON.stringify(this.seoData);
    this.productToCreate.ScientificId = this.productForm.scientificNames.id;

    this.productToCreate.id = this.productForm.id || 0;


    if(this.seletedSellingPoints.length) {
      this.seletedSellingPoints.forEach(element => {
        this.productToCreate.SellingToIds.push(element.value);
      });
    }


    if (this.seletedBuyers.length) {
      this.seletedBuyers.forEach(element => {
        this.productToCreate.SoldToIds.push(element.value);
      });
    }


    this.productToCreate.Images = this.productForm.productImages;
    this.productToCreate.published = this.productForm.published;



    if (this.totalCount === 0) {
      this.productToCreate.Images = [];

      this.imagePreview.forEach(element => {
        this.productToCreate.Images.push(element.url);
      });


      if (this.localBaseImage.length === 0 && this.productForm.id) {
        this.productToCreate.baseImage = this.baseImagePreview[0];
        this.addUpdateProduct(this.productToCreate);
      }
    }



    if (this.localBaseImage.length === 0 && this.baseImagePreview.length === 0) {
      return;
    } else {
      if (this.localBaseImage.length > 0) {
        const _thisProductImage = this;
        await this.uploadProductImage(this.localBaseImage[0], function (data) {
          _thisProductImage.taskCount++;

          _thisProductImage.productToCreate.baseImage = data;
          if (_thisProductImage.totalCount === _thisProductImage.taskCount) {
            _thisProductImage.addUpdateProduct(_thisProductImage.productToCreate);
          }
        });
      }
    }



    if (this.localImages.length > 0) {
      this.localImages.forEach(async element => {
        const _thisImages = this;
        const _thisImages2 = this;
        await _thisImages.uploadProductImage(element, function (data) {
          _thisImages2.taskCount++;
          _thisImages2.productToCreate.Images.push(data);
          if (_thisImages2.totalCount === _thisImages2.taskCount) {
            _thisImages2.addUpdateProduct(_thisImages2.productToCreate);
          }
        });
      });
    }


    if (this.localCatalogImage.length > 0) {
      const _thisCatalog = this;
      await this.uploadCatalog(this.localCatalogImage[0], function (data) {
        _thisCatalog.taskCount++;
        debugger
        _thisCatalog.productToCreate.CatalogURL = data;
        if (_thisCatalog.totalCount === _thisCatalog.taskCount) {
          _thisCatalog.addUpdateProduct(_thisCatalog.productToCreate);
        }
      });
    }


  }

  addUpdateProduct(data) {
    this.submitLoader = true;
    this.productCatalogService.addUpdateProduct(data).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.submitLoader = false;
        this.toast.success("Updated Successfuly");
        this.dialogRef.close(true);
      },
      error => {
        this.submitLoader = false;
        this.toast.error("Something wrong happened!");
      }
    );
  }

  uploadCatalog(catalog, catalogDataLocation) {
    const params = {
      Bucket: 'aumet-data',
      Key: 'product/catelogues/' + Guid.create() + '.' + this.getFileExtension(catalog.name),
      Body: catalog
    };
    const _this3 = this;
    _this3.handleUpload(params, function (resultc) {
      catalogDataLocation(resultc);
    });
  }

  uploadProductImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'product/images/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }


}

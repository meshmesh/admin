import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { trigger, transition, style, animate } from '@angular/animations';
import { TranslateService } from './translate.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms ease-in-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ transform: 'translate(0)' }), animate('400ms ease-in-out', style({ opacity: 0 }))])
    ])
  ]
})
export class TranslateComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private serv: TranslateService, public dialogRef: MatDialogRef<TranslateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, ) { }
  key: any;
  usedfor: any;
  value: any;
  Descriptor: any;

  submittedForm: any = {
    keyName: this.data.key,
    keyID: this.data.keyID,
    usedFor: null,
    keyValue: null,
    noneDescriptor: null,
    language: null,
    languageId: null
  };

  languageList: any = [];


  ngOnInit() {
    
    this.key = this.data.key;
    this.serv.getLangugaesId(50).pipe(takeUntil(this.onDestroy$)).subscribe(res => { //Get Languages` shortcuts
      this.languageList = res;
      
      this.submittedForm.language = this.languageList[0].name;
      this.submittedForm.languageId = this.languageList[0].id;
    });
  }




  OnTabForm(event) {

    if (event.nextId) {
      this.submittedForm.language = this.languageList[event.nextId].name;
      this.submittedForm.languageId = this.languageList[event.nextId].id;
      this.usedfor = '';
      this.value = '';
      this.Descriptor = '';
    }
  }


  close(data) {
    this.dialogRef.close(data);
  }

  async submit(form) {

    this.submittedForm.usedFor = form.value.usedfor;
    this.submittedForm.keyValue = form.value.value;
    this.submittedForm.noneDescriptor = form.value.Descriptor;
    console.log(this.submittedForm);
  }

}

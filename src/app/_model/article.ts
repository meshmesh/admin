import { SEO } from './SEO';

export class Article {
  id: number;
  articleimage: string;
  categories: Category[];
  createdby: number;
  creationDate: string | Date | number;
  deleted: boolean;
  description: string;
  details: string;
  category: Category;
  modificationDate: string | Date | number;
  numberofhits: number;
  orderid: number;
  portals: any[];
  published: boolean;
  publishedby: number;
  seo: SEO[];
  slug: string;
  subimages: string[];
  subtitle: string;
  tags: string[];
  title: string;
  updatedby: number;
  // Not Sure about the following:
  totalNumber?: number;
  categoryId?: number;
  categoryList?: any[];
  portalList?: any[];
  maincategory?: Category;
}

export interface Category {
  id: number;
  image: string;
  label: string;
  name: string;
  seo: any;
  slug: string;
  value: number;
}

export interface ArticleRequest {
  id?: number;
  Articleimage: any;
  Subimages: any;
  CategoryId: number;
  Description: string;
  Details: string;
  Portals: any[];
  Title: string;
  Subtitle: string;
  Seo: any;
  Categories: any[];
  Published: boolean;
  Tags: string[];
  subCategory: any;
}

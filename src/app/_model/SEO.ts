export interface SEO {
  id?: number;
  metaDescription: string;
  metaKeyWord: string;
  metaTitle: string;
  name: 'International' | 'Jordan' | 'KSA' | 'Iraq' | 'UAE';
}


import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatDialog } from '@angular/material';
import { DistributorService } from '../../services/distributor.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-distributor-list',
  templateUrl: './distributor-list.component.html',
  styleUrls: ['./distributor-list.component.scss']
})
export class DistributorListComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private serv: DistributorService, public dialog: MatDialog) { }
  IntrestList: MatTableDataSource<any>;
  ExperianceList: MatTableDataSource<any>;
  BusinessOppList: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'scientificname', 'speciality', 'medicalline']
  displayedExpColumns: string[] = ['id', 'scientificname', 'speciality', 'medicalline']
  displayedBusinessoppColumns: string[] = ['id', 'manufacturername']
  length: number;
  bolength: number;
  explength: number;
  pageSize = 10;
  pagenumber: number = 1;
  bopagenumber: number = 1;
  expagenumber: number = 1;

  title: string;


  ngOnInit() {
    debugger
    this.title = this.data.element.name;
    this.serv.GetIntrest(this.pagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      // this.renderDataSourceD(res.data[0].interests);
      this.length = res.data.count;
      this.IntrestList = new MatTableDataSource<any>(res.data.items);
    });

    this.serv.GetBusinessOpp(this.bopagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.bolength = res.data.count;
      this.BusinessOppList = new MatTableDataSource<any>(res.data.items);
    });

    this.serv.GetExperiance(this.expagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.explength = res.data.count;
      this.ExperianceList = new MatTableDataSource<any>(res.data.items);
    });

  }
  
  pageChange(event) {
    this.pagenumber = event.pageIndex;
    this.serv.GetIntrest(this.pagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      
      this.length = res.data[0].count;
      this.IntrestList = new MatTableDataSource<any>(res.data[0].items);
    });
  }

  bopageChange(event) {
    this.bopagenumber = event.pageIndex;
    this.serv.GetBusinessOpp(this.bopagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.bolength = res.data.count;
      this.BusinessOppList = new MatTableDataSource<any>(res.data.items);
    });

  }

  expageChange(event) {
    this.expagenumber = event.pageIndex;
    this.serv.GetExperiance(this.expagenumber, this.data.element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.explength = res.data.count;
      this.ExperianceList = new MatTableDataSource<any>(res.data.items);
    });
  }

  // ViewComments(row) {

  //   const dialogRef = this.dialog.open(ViewCommentComponent, {
  //     data: { row },
  //     height: '75%',
  //     width: '75%'
  //   });

  // }

}

export class ManufacturerModel {
  id: number;
  name: string;
  token: string;
  address: string;
  slug: string;
  currentState: string;
  type: string;
  logo: string;
  registred: boolean;
  regsitrationDate: Date;
  moificationDate: Date;
  annualSales: string;
  aumetComment: string;
  numberofEmployee: string;
  satisfaction: string;
  utm: string;
  authenticated: boolean;
  closing: string;
  currentStage: string;
  numberOfCatalogs: number;
  numberOfproducts: number;
  numberOfUsers: number;
  numberoFShared: number;
  numberOfPatent: number;
  totalRows: number;
  description: number;
  seoData: string;
  users: any[] = [];
  files: any[] = [];
  exisitingFiles: any[] = [];
  scientificName: any[] = [];
  specialities: any[] = [];
  country: any = {};
  companyClient: any = {};
  deals: any[] = [];
  brands: any[] = [];
  targetingCountries: any[] = [];
  callLog: any[] = [];
  vacantCountries: any[] = [];
  exclusivityCountries: any[] = [];
  patent: any[] = [];
  loginReqest: any[] = [];
  numberofInquiries: number;
  comment: string;
}

export class AvailableModel {
  name: string;
  orderId: number;
  id: number;
  display: string;
  value: number;
}

export class CompanyModel {
  Speciality: any;
  annual: any;
  country: any;
  Employees: any;
  Type: string;
  Logo: string;
  Address: string;
  CompanyID: number;
  CompanyName: string;
  CountryId: number;
  AnnualSales: string;
  NumberOfEmployess: string;
  CompanyAdmin: number;
  IsActive: boolean;
  SoldTo: number[] = [];
  SpecialityIds: number[] = [];
  MedicalLineIds: number[] = [];
  Domain: string;
  Description: string;
  WebsiteUrl: string;
  EstablishmentDate: string;
  AumetComments: string;
}

export class ExistingModel {
  description: string;
  exists: boolean;
  id: number;
  link: string;
  type: number;
}

export class FilesViewModel {
  description: string;
  exists: boolean;
  id: number;
  link: string;
  type: number;
}

export class ManuListSearchParams {
  PageSize: number = 10;
  PageNumber: number = 1;

  CompanyName: string;
  CompanyId: string;

  CompanyRegistered: string;
  IsCrawled: string;
}

export class ScientificFormModel {
  CompanyId: number;
  scientificName: string;
}


export class ProsSciName {
  Id: number;
  ScientificNameId: number;
  Scientific: any;
  MedicleLineId: number;
  MedicleLine: any;
  SpecialitiesId: number;
  Specialities: any;
}

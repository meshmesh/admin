
import { Component, OnInit, ViewChild, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatDialog, TooltipPosition, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { DistributorDetailsComponent } from './distributor-details/distributor-details.component';

import { ReplaySubject, of } from 'rxjs';
import { take, takeUntil, tap, switchMap } from 'rxjs/operators';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';
import { DistributorService } from './services/distributor.service';
import { DistributorModel, DistributorSearchParams } from './model/distributor-model';
import { DistributorFilterOptions, DistributorListColumns, DistributorListQaColumns } from './model/distributor-options';
import { DistributorListComponent } from './components/distributor-list/distributor-list.component';
import { AddUpdateComponent } from '../interest/addUpdate/addUpdate.component';
import { MergeDistributorComponent } from './components/merge-distributor/merge-distributor.component';
import { AddCompanyModalComponent } from './components/add-company-modal/add-company-modal.component';
import { IFilterOption } from 'src/app/filter/filters.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.scss']
})
export class DistributorViewComponent implements OnInit, OnDestroy {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  filterOptions: Array<IFilterOption> = DistributorFilterOptions;

  onDestroy$ = new ReplaySubject<void>();

  displayedColumns: Array<string> = DistributorListColumns;

  displayedQaColumns: Array<string> = DistributorListQaColumns;

  searchParams: DistributorSearchParams = new DistributorSearchParams();

  distributorSource: MatTableDataSource<DistributorModel>;

  distributorQASource: MatTableDataSource<DistributorModel>;

  selection: SelectionModel<DistributorModel>;

  selectionQA: SelectionModel<DistributorModel>;

  position: TooltipPosition = 'above';

  pageSize = 10;

  length: number;

  dataFromFilters: any[] = [];

  manufacturersList: any;

  searchKey = '';

  filteredString: any = '';

  distributers: any[];

  pageType = 'Distributor';

  isLoading: boolean;



  constructor(
    private router: ActivatedRoute,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private distributorService: DistributorService
  ) { }

  ngOnInit() {

    this.getDistributors();

  }


  ngOnDestroy(): void {
    this.onDestroy$.next();
  }


  getDistributors() {
    this.isLoading = true;
    this.distributorService.getAll(this.searchParams, this.filteredString)
    .pipe(
      take(1),
      tap((res: any) => {
        this.renderData(res);
      })
    )
    .pipe(takeUntil(this.onDestroy$))
    .subscribe();
  }

  renderData(res: any) {
    if(res && res.data) {
      this.length = res.data.count;
      this.distributorSource = new MatTableDataSource<DistributorModel>(res.data.items);
    } else {
      this.length = 0;
      this.distributorSource = new MatTableDataSource<DistributorModel>([]);
    }
    this.isLoading = false;
  }

  actionTabChange(event) {
    switch (event.nextId) {

      case '1':
        this.pageType = 'Distributor';
        this.getDistributors();
        break;

      case '2':
        this.searchParams.CompanyRegistered = 'false';
        this.pageType = 'DistributorQA';
        this.getDistributors();
        break;
    }
  }


  handleDataFromFilters(event) {

    if(this.paginator) this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;

    if(event === 0) {
      this.filteredString = "";
      this.getDistributors();
      return;
    }

    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });

    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });


    this.getDistributors();
  }

  pageChange(event) {
    this.pageSize = event.pageSize;
    this.searchParams.PageSize = event.pageSize;
    this.searchParams.PageNumber = event.pageIndex + 1;
    this.getDistributors();
  }



  searchSubmit(form: NgForm) {
    if(this.paginator) this.paginator.pageIndex = 0; 
    this.searchParams.PageNumber = 1;

    if(!form.value.id && !form.value.name) {
      this.searchParams.CompanyId = '';
      this.searchParams.CompanyName = '';
      this.getDistributors();
      return;
    } 

    this.searchParams.CompanyId = form.value.id ? String(form.value.id).trim() : '';
    this.searchParams.CompanyName = form.value.name ? String(form.value.name).trim() : '';
    this.getDistributors();
  }


  openDialog(element: DistributorModel) {
    this.dialog.open(DistributorListComponent, {
      width: '65%',
      // height: '70%',
      data: { element }
    });
  }

  Add(id) {
    this.distributorService.getComp(id).pipe(takeUntil(this.onDestroy$)).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      const dialogRef = this.dialog.open(AddUpdateComponent, {
        width: '65%',
        // height: '80%',
        data: { 
          res,
          m: true, 
        }
      }); 
      
      
      dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
        if(result) {
          this.getDistributors();
        }

      });
    });
  }


  mergeComp(comp) {
    const dialogRef = this.dialog.open(MergeDistributorComponent, {
      width: '50%',
      height: '50%',
      data: { comp }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {

      if(result) {
        this.getDistributors();
      }

    });

  }



  displayDis(id) {
    this.distributorService.get(id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const dialogRef = this.dialog.open(DistributorDetailsComponent, {
        data: res.data[0],
        width: '85%',
        height: '80%',
      });

      dialogRef
        .afterClosed()
        .pipe(takeUntil(this.onDestroy$))
        .pipe(takeUntil(this.onDestroy$))
        .subscribe((result: any) => {
          if(result) {
            this.getDistributors();
          }
        });
    });
  }


  AddCompany() {
    const dialogRef = this.dialog.open(AddCompanyModalComponent, {
      width: '65%',
      // height: '75%'
    });
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        tap((result) => {
          if(result) {
            this.getDistributors();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }




  EditDist(element) {
    this.distributorService.get(element.id).pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
      const dialogRef = this.dialog.open(AddCompanyModalComponent, {
        width: '75%',
        height: '75%',
        data: res.data[0]
      });
      dialogRef
        .afterClosed()
        .pipe(
          takeUntil(this.onDestroy$),
          tap((result) => {
            if(result) {
              this.getDistributors();
            }
          })
        )
        .pipe(takeUntil(this.onDestroy$)).subscribe();
    })
  }

  onDelete(element): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Distributor',
        id: element.id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this.distributorService.deleteDistributor(element.id) : of(undefined);
        }), 
        tap((result: number) => {
          if(result) {
            this.getDistributors();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  onDeactivate(id: number): void {
    let Activate = {
        IsActive: false,
        DistributorId: id
      }
    this.activateDeactivate(Activate);
  }

  onActivate(id: number): void {
    let Activate = {
        IsActive: true,
        DistributorId: id
      }
    this.activateDeactivate(Activate);
  }

  activateDeactivate(Activate) {
    this.distributorService
      .ActivateDeactivateDistributor(Activate)
      .pipe(
        take(1),
        tap((result) => {
          if(result) {
            this.getDistributors();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }


}

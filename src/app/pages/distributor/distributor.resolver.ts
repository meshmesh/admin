import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DistributorService } from './services/distributor.service';
import { DistributorModel, DistributorSearchParams } from './model/distributor-model';

@Injectable()
export class DistributorResolver implements Resolve<Array<DistributorModel>> {

  constructor(private service: DistributorService, private router: Router) {}

  resolve(): Observable<Array<DistributorModel>> {
    let data: DistributorSearchParams = new DistributorSearchParams();
    return this.service.getAll(data, '').pipe(
      catchError(error => {
        this.router.navigate(['']);
        return of(null);
      })
    );
  }
}

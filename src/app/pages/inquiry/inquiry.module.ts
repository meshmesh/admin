import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InquiryComponent } from "./inquiry.component";
import {
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatInputModule,
  MatDialogModule,
} from "@angular/material";
import { SharedModule } from "../../shared/shared.module";
import { InquiryRoutingModule } from "./inquiry-routing.module";
import { QuillEditorModule } from "ngx-quill-editor";
import { FilterModule } from "../../filter/filter.module";
import { NumOfSuggSciNameComponent } from "./components/helper-popup/NumOfSuggSciName.component";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ManufacturerModule } from "../manufacturer/manufacturer.module";
import { DistributorModule } from "../distributor/distributor.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatTooltipModule,
    MatDialogModule,
    InquiryRoutingModule,
    QuillEditorModule,
    FilterModule,
    ManufacturerModule,
    DistributorModule
  ],
  exports: [
    NumOfSuggSciNameComponent,
  ],
  declarations: [
    InquiryComponent,
    NumOfSuggSciNameComponent,
  ],
  entryComponents: [
    NumOfSuggSciNameComponent,
  ],
})
export class InquiryModule {}

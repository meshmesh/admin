import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource, MatPaginator, MatDialog } from "@angular/material";
import { VideosPageService } from "./videosPage.service";
import { AddVideoComponent } from "./addVideo/addVideo.component";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-videosPage",
  templateUrl: "./videosPage.component.html",
  styleUrls: ["./videosPage.component.scss"]
})
export class VideosPageComponent implements OnInit {
  displayedColumns: any[] = ["Actions", "Id", "Type", "Title", "Link"];
  constructor(private serv: VideosPageService, private dialog: MatDialog) {}
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pagenumber = 1;
  filteredString = "";
  length = 10;

  ngOnInit() {
    this.serv.getVideos(this.pagenumber).subscribe((response: any) => {
      this.renderTable(response);
      console.log(response);
    });
  }

  renderTable(data) {
    if (data.length < 10) {
      this.length = data.length;
    } else {
      this.length = data[0].totalNumber;
    }
    this.dataSource = new MatTableDataSource<any>(data);
  }

  onCreateVideo() {
    const dialogRef = this.dialog.open(AddVideoComponent, {
      width: "80%",
      height: "70%"
    });
    dialogRef
      .afterClosed()
      .pipe(switchMap(e => this.serv.getVideos(this.pagenumber)))
      .subscribe(e => {
        this.renderTable(e), console.log(e);
      });

    /*.subscribe(result => {
      if (result == "true") {
        this.serv.getVideos(this.pagenumber).subscribe((response: any) => {
          this.renderTable(response);
          console.log(response);
        });
      }
    }); */
  }

  Publish(id) {
    this.serv
    .PublishVideo(id)
    .pipe()
    .subscribe(e => this.getData(this.pagenumber));

  }

  UnPublish(id) {
    this.serv
    .UnPublishVideo(id)
    .pipe()
    .subscribe(e => this.getData(this.pagenumber));
  }
  Delete(id) {
    this.serv
      .DeleteVideo(id)
      .pipe()
      .subscribe(e => this.getData(this.pagenumber));
  }
  Edit(item) {
    const dialogRef = this.dialog.open(AddVideoComponent, {
      width: "80%",
      height: "70%",
      data: item
    });
    dialogRef
      .afterClosed()
      .pipe(switchMap(e => this.serv.getVideos(this.pagenumber)))
      .subscribe(e => {
        this.renderTable(e), console.log(e);
      });
  }

  pageChange(event) {
    this.pagenumber = event.pageIndex + 1;

    this.serv
      .getVideos(this.pagenumber /* ,this.filteredString */)
      .subscribe(res => {
        this.renderTable(res);
      });

  }
  getData(pagenumber) {
    this.serv.getVideos(pagenumber).subscribe((res: any) => {
      this.dataSource = new MatTableDataSource<any>(res);
      this.length = res[0].totalNumber;
    });
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-single-select',
  templateUrl: './single-select.component.html',
  styleUrls: ['./single-select.component.scss']
})
export class SingleSelectComponent implements OnInit {
  @Input() mainData;
  @Output() dataFromSelect = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {

  }

  selectHandler(event) {
    this.mainData.value = event.target.value;
    this.dataFromSelect.emit(this.mainData);
  }
}

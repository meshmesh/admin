import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InterestService } from './interest.service';
import { Router } from '@angular/router';
import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { AddCommentComponent } from './addComment/addComment.component';
import { AddUpdateComponent } from './addUpdate/addUpdate.component';
import { Interest } from './interest';
import { ViewCommentComponent } from './ViewComment/ViewComment.component';
// import { AdminComponent } from '../layout/admin/admin.component';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';



@Component({
  selector: 'app-interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.scss'],
  animations: [
    trigger('notificationBottom', [
      state('an-off, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('an-animate',
        style({
          overflow: 'visible',
          height: AUTO_STYLE,
        })
      ),
      transition('an-off <=> an-animate', [
        animate('400ms ease-in-out')
      ])
    ]),
    trigger('slideInOut', [
      state('in', style({
        width: '280px',
        // transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        width: '0',
        // transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('mobileHeaderNavRight', [
      state('nav-off, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('nav-on',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('nav-off <=> nav-on', [
        animate('400ms ease-in-out')
      ])
    ]),
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ]),
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ]


})

export class InterestComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  configOpenRightBar: string;



  filterOptions = [
   
    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Specialities/GetSpecialities',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Specialty',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'Specialtiesids'
    },
    {
      index: 2,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/MedicalLines/GetMedicalLines',
      order: 3,
      value: {},
      selected: [],
      placeholder: 'Medical Line',
      chipsList: [],
      list: [],
      querystring: 'Medicallinesids',
      searchQuery: '',
    },
    {
      index: 3,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/AdminScientificNames/ScientificnameFilter?PageSize=10&PageNumber=1&nameinput=',
      order: 4,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Scientific Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'Scientificesids',
    },
    {
      index: 4,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Companies/GetListOfCompaniesLimited?Name=',
      order: 5,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Company Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'CompanyIds',
    },

    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/AdminRegion/Search',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Region',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'RegionIds'
    }
  ];


  toggleRightbar() {
    this.configOpenRightBar = this.configOpenRightBar === 'open' ? '' : 'open';
  }


  constructor(private http: HttpClient, private serv: InterestService,
    // private adminComponent: AdminComponent , 
    private rout: Router, public dialog: MatDialog) { }
  dataSource: MatTableDataSource<any>;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ['Action','Id', 'UserName', 'Date', 'ScientificName', 'Speciality', 'MedicalLine', 'TargetedPrice', 'AnnualTarget', 'TargetedRegion', 'InitialOrder', 'source', 'Comments'];
  values: any[] = [];
  length = 10;
  pageIndex = 1;
  pageSize = 10;
  typingTimer: any;                //timer identifier
  doneTypingInterval = 500;
  filteredString : any;
  searchKey = '';
  SearchKey:any;

  ngOnInit() {

    this.loadInterest();
    // setTimeout(() => {
    //   this.adminComponent.verticalNavType = 'expanded';
    //   this.adminComponent.toggleOpened('');
    // });
  }

  loadInterest() {
    this.serv.getInterest(this.pageIndex, '').pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        console.log(response);
        // this.renderDataSource(response);
        this.renderDataSource(response);
        this.length = response[0].totalNumber;
      },
      error => {
        console.log(error);
      }
    );

  }

  
  handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join("");
      }

    });
    this.filteredString = "";
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });
   
      this.serv.getInterestFilteration(this.pageIndex,  this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe((res :  any) => {
   

        this.renderDataSource(res)
      });
  }


  pageChange(event) {
    this.pageIndex = event.pageIndex;

    this.serv.getInterestFilteration(this.pageIndex + 1,  this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }







  addComment(row) {
    const dialogRef = this.dialog.open(AddCommentComponent, {
      width: '85%',
      height:'80%',
      data: {row}
    });

    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      console.log('The dialog was closed');

    });
  }

  Add() {
    const dialogRef = this.dialog.open(AddUpdateComponent, {
      width: '85%',
      height:'80%',
      data:{}
    }
    );

  }

  viewCompany(token, manuId) {
    let obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'manufacturer' }
    this.serv.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      const response: any = res;
      window.open(response.url);
    });;
  }

  Update(row) {
    /*const dialogRef = this.dialog.open(AddCommentComponent, { 
      width: '850px', });*/

    this.serv.GetInterestById(row.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      const dialogRef = this.dialog.open(AddUpdateComponent, {
        data: res,
        height: '75%',
        width: '75%'
      });

      dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
       
       
       
        this.serv.getInterestFilteration(this.pageIndex,  this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
          this.renderDataSource(res);
        });
        });
    });
    
  }


  renderDataSource(data) {
    if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.length = data[0].totalNumber;
    }
    this.dataSource = new MatTableDataSource<any>(data);
  }




  ViewComments(row) {
  
      const dialogRef = this.dialog.open(ViewCommentComponent, {
        data: {row},
        height: '75%',
        width:'75%'
      });
    
  }


  DeleteInt(row){
    this.serv.delInterest(row.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.serv.getInterestFilteration(this.pageIndex,  this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
        this.renderDataSource(res);
    });
  });
}

  keyUpapplyFilter(event) {

    this.searchKey = event;
    let _this = this;
    clearTimeout(this.typingTimer);
    window.this = _this;
    this.typingTimer = setTimeout(_this.filterData, _this.doneTypingInterval);

  }

  keyDownfilter(event) {
    clearTimeout(this.typingTimer);
  }

  loadInterestdata(){
    
    this.serv.getInterest2(this.pageIndex, this.searchKey , this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
      this.renderDataSource(res);
    })

  }


  filterData() {
    window.this.serv.getInterest(window.this.pageIndex, window.this.filtersRes, window.this.searchKey).pipe(takeUntil(this.onDestroy$)).subscribe(res => {


      window.this.renderDataSource(res)

    });
  }





}
declare global {
  interface Window { this: any; }
}
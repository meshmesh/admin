/* import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FbService } from 'src/app/_services/fb.service';
import { tap, map } from 'rxjs/operators';
import { Observable, concat } from 'rxjs';

@Component({
  selector: 'app-messages-menu',
  templateUrl: './messages-menu.component.html',
  styleUrls: ['./messages-menu.component.scss'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          width: '280px'
          // transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        'out',
        style({
          width: '0'
          // transform: 'translate3d(100%, 0, 0)'
        })
      ),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class MessagesMenuComponent implements OnInit {
  constructor(private fbService: FbService) {}

  @Output() toggleMain = new EventEmitter();
  @Output() toggleInner = new EventEmitter();
  @Input() showMain: 'out' | 'in';
  @Input() showInner: 'out' | 'in';

  conversation$: Observable<any[]>;
  ngOnInit() {
    this.fbService.init();
    // this.fbService.newConversation();
    this.conversation$ = this.getConversations();

    // this.fbService.getAll();
    this.fbService.getById();
  }

  _toggleMain(): void {
    this.toggleMain.emit();
  }
  _toggleInner(): void {
    this.toggleInner.emit();
  }

  getConversations() {
    return this.fbService.conversations.valueChanges().pipe(
      map(conversations => {
        const newConversations = [];
        conversations.forEach(conversation => {
          const otherUser = conversation.users.filter(user => user.id !== this.fbService.currentUser.id);
          newConversations.push(otherUser[0]);
        });
        return newConversations;
      }),
      tap(conversations => {
        // console.log(conversations);
      })
    );
  }

  // createConversation() {
  //   this.fbService.newConversation();
  // }
}
 */
import { Component, OnInit, Input, OnChanges, Output, EventEmitter, AfterViewChecked } from '@angular/core';
import { ArticleService } from '../../article.service';
import { AdminComponent } from '../../../layout/admin/admin.component';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit, OnChanges {
  @Input() articlesArray;
  @Output() pageNumber = new EventEmitter();
  showpopup = false;
  popupData = {
    type: '',
    action: '',
    text: ''
  };
  
  ArticleSeoData  = [];
  seoData: any = [];
  seoDataToSend: any;
  curentCardId : number;


  public bigTotalItems = 0;
  constructor(private articleService: ArticleService,
    private adminComponent: AdminComponent) { }

  ngOnInit() {
    
    this.articleService.getPortals()
      .subscribe(
        (response: any) => {
          for (let i = 0; i < response.length; i++) {
            const name = response[i].name;
            const id = response[i].id;
            this.seoData.push({ name: name, id: id, metaTitle: '', metaDescription: '', metaKeyWord: '' });
          }
        },
        (error) => console.log(error)
      );


  }

  assignId(id) {
    this.curentCardId = id;
    this.articleService.getArticlesById(id)
      .subscribe(
        (response: any) => {
          this.ArticleSeoData = response.seoData;
        }
      );
  }
  handleDataFromSeo(event) {
    this.seoDataToSend = event;
  }
  
  sendSeo() {
    const s = JSON.stringify(this.seoDataToSend);
    const obj = {
      'Id': this.curentCardId,
      'Items': [{
        'Key': 'seo',
        'Value': s
      }]
    };
    this.articleService.createUpdateArticle(obj)
      .subscribe(
        (response: any) => {
          this.popupData.type = 'SEO';
          this.popupData.text = '';
          this.popupData.action = 'Updated';
          this.showpopup = true;
          const x = this;
          setTimeout(function () { x.showpopup = false; }, 1500);
        },
        (error) => console.log(error)
      );
  }

  ngOnChanges() {
    if (this.articlesArray[0]) {
      this.bigTotalItems = this.articlesArray[0].totalNumber;
    } else {
      this.bigTotalItems = 1;
    }
    for (let i = 0; i < this.articlesArray.length; i++) {
      this.articlesArray[i].description = this.articlesArray[i].description.replace(/<[^>]*>/g, '');
      this.articlesArray[i].details = this.articlesArray[i].details.replace(/<[^>]*>/g, '');
    }
    // setTimeout(() => {
    //   this.adminComponent.verticalNavType = 'expanded';
    //   this.adminComponent.toggleOpened('');
    // });
  }
  publish(id) {
    this.articleService.puplishArticle(id)
      .subscribe((response) => {
        this.popupData.type = 'Article';
        this.popupData.text = 'has been';
        this.popupData.action = 'Published';
        this.showpopup = true;
        const x = this;
        setTimeout(function () { x.showpopup = false; }, 1500);
      },
        (error) => {
          this.popupData.type = 'ERROR';
          this.popupData.text = 'some thing went wrong, Maybe your Article is allrady Published!';
          this.popupData.action = '';
          this.showpopup = true;
          const x = this;
          setTimeout(function () { x.showpopup = false; }, 3500);
          console.log(error);
        }
      );
    const length = this.articlesArray.length;
    for (let i = 0; i < length - 1; i++) {
      if (this.articlesArray[i].id === id) {
        this.articlesArray[i].published = true;
      }
    }
  }
  delete(id) {
    this.articleService.deleteArticle(id)
      .subscribe((response) => {
        this.popupData.type = 'Article';
        this.popupData.action = 'Deleted';
        this.showpopup = true;
        const x = this;
        setTimeout(function () { x.showpopup = false; }, 1500);
      },
        (error) => console.log(error)
      );
    const length = this.articlesArray.length;
    for (let i = 0; i < length - 1; i++) {
      if (this.articlesArray[i].id === id) {
        this.articlesArray.splice(i, 1);
      }
    }
  }
  changePage(event) {
    this.pageNumber.emit(event);
  }
}



import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { VideosService } from "./videos.service";
import { S3Service } from "../_services/s3.service";
import { CompanyFile } from "./companyFile";
import { ToastService } from "../_services/toast.service";

@Component({
  selector: "app-videos",
  templateUrl: "./videos.component.html",
  styleUrls: ["./videos.component.scss"]
})
export class VideosComponent implements OnInit, OnChanges {
  public dataToSend: any = [];
  VideoTOSend: VideoTOSend;
  image: any;
  @Input("CompanyId") CompanyId: number;
  @Input("BussniessId") BussniessId: number;
  @Input("VideoLinks") VideoLinks: string[] = [];
  saveInProgress = false;
  constructor(
    private toast: VideosService,
    private s3: S3Service,
    public alertfy: ToastService
  ) {}

  compFile: CompanyFile;
  uploadLink: any;
  thumbnailYoutube: any;
  youtubeUrl: any;
  selectedYoutubeURL: any;
  files: any = [];
  links: any = [];
  imagePreview: any = [];

  Id: any; //if exist
  Link: any; //v3 Link
  Exists: boolean;
  YouTube: any; //just the Id

  Type: any; //sent by yazan on Slack
  Description: any; //Title
  ExpiryDate: any; //No need
  Caption: any; //User Editing

  ngOnChanges() {
    console.log(this.VideoLinks);
    if (this.VideoLinks) {
      this.VideoLinks.forEach(element => {
        this.imagePreview.push(element.link);
        this.files.push(element);
      });
    }
  }

  ngOnInit() {
    console.log(this.VideoLinks);
  }

  parseYouTubeId(i: string): string {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;

    const match = this.youtubeUrl.match(regExp);
    const link = "https://www.youtube.com/watch?v=" + match[7];
    if (this.Id) {
      this.Exists = true;
    }
    this.YouTube = match[7];
    this.thumbnailYoutube =
      "http://img.youtube.com/vi/" + match[7] + "/default.jpg";
    return match && match[7].length === 11 ? match[7] : undefined;
  }

  uploadFile(event) {
    for (let index = 0; index < event.length; index++) {
      const element = event[index];

      this.files.push(element);
      this.imagePreview.push(element.name);
    }
  }

  AddThumbnail() {
    this.parseYouTubeId(this.youtubeUrl);
    if (this.links.length < this.thumbnailYoutube.length) {
      this.links.push(this.thumbnailYoutube);
    }
  }

  deleteAttachment(file, index) {
    this.files.splice(index, 1);
    this.imagePreview.splice(index, 1);

    this.toast.deletevideo(file.id).subscribe(res => {
      console.log(res);
    });
  }

  deleteThumbnails(index) {
    this.links.splice(index, 1);
  }

  hideLinkForm(event) {}

  UploadVideo(obj) {
    console.log(obj);
  }

  saveVideos() {
    this.VideoTOSend = new VideoTOSend();
    this.saveInProgress = true;
    document.getElementById("overlay").style.display = "block";
    this.VideoTOSend.BusinessOpportunityId = this.BussniessId;
    this.VideoTOSend.CompanyId = this.CompanyId;
    let count = 0;

    this.links.forEach(element => {
      this.VideoTOSend.YoutubeLink.push(element);
    });

    this.files.forEach(element => {
      if (element instanceof File) {
        this.s3.uploadFile("product/catelogues/", element).subscribe(res => {
          if (res.percentage === 0 && res.data !== null && res.error === "") {
            this.VideoTOSend.Links.push(res.data.Location);

            if (
              this.files.length + this.links.length ===
              this.VideoTOSend.Links.length
            ) {
              this.toast.saveVideos(this.VideoTOSend).subscribe(res => {
                this.saveInProgress = false;
                document.getElementById("overlay").style.display = "none";

                this.alertfy.success("Added Successfully");
              });
            } else if (this.VideoLinks) {
              this.toast.saveVideos(this.VideoTOSend).subscribe(res => {
                this.saveInProgress = false;
                document.getElementById("overlay").style.display = "none";

                this.alertfy.success("Added Successfully");
              });
            }
            {
            }
          }
        });
      }
    });
    if (this.VideoTOSend.YoutubeLink.length > 0) {
      this.toast.saveVideos(this.VideoTOSend).subscribe(res => {
        this.saveInProgress = false;
        document.getElementById("overlay").style.display = "none";

        this.alertfy.success("Added Successfully");
      });
    }
  }
}

export class VideoTOSend {
  Links: string[] = [];
  BusinessOpportunityId: number;
  YoutubeLink: string[] = [];
  CompanyId: number;
}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { BulkService } from './bulk.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Bulk } from './bulk';
import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-bulk',
  templateUrl: './bulk.component.html',
  styleUrls: ['./bulk.component.scss'],

  animations: [
    trigger('notificationBottom', [
      state(
        'an-off, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'an-animate',
        style({
          overflow: 'visible',
          height: AUTO_STYLE
        })
      ),
      transition('an-off <=> an-animate', [animate('400ms ease-in-out')])
    ]),
    trigger('slideInOut', [
      state(
        'in',
        style({
          width: '280px'
          // transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        'out',
        style({
          width: '0'
          // transform: 'translate3d(100%, 0, 0)'
        })
      ),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('mobileHeaderNavRight', [
      state(
        'nav-off, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'nav-on',
        style({
          height: AUTO_STYLE
        })
      ),
      transition('nav-off <=> nav-on', [animate('400ms ease-in-out')])
    ]),
    trigger('fadeInOutTranslate', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms ease-in-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ transform: 'translate(0)' }), animate('400ms ease-in-out', style({ opacity: 0 }))])
    ]),
    trigger('mobileMenuTop', [
      state(
        'no-block, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'yes-block',
        style({
          height: AUTO_STYLE
        })
      ),
      transition('no-block <=> yes-block', [animate('400ms ease-in-out')])
    ])
  ]
})
export class BulkComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  configOpenRightBar: string;

  filterOptions = [
    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Specialities/GetSpecialities',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Specialty',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'Specialtiesids'
    },
    {
      index: 3,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/AdminScientificNames/SearchScientificNames?PageSize=10&PageNumber=1&Name=',
      order: 4,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Scientific Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'Scientificesids'
    },
     {
      index: 4,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Companies/GetListOfCompaniesLimited?Name=',
      order: 5,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Company Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'CompanyIds'
    }, 
   
  ];



 
  displayedColumns: any = ['Action','productName','SubTitle', 'manufacturerName', 'specialty', 'scientificName'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selection: SelectionModel<any>;
  filtersRes = '';
  pageIndex = 1;
  length = 10;
  pageSize = 10;
  searchKey = '';
  typingTimer: any;     
  doneTypingInterval = 500;
  filteredString = '';

  constructor(private service : BulkService,private route: Router) { }

  ngOnInit() {
    this.loadBulk(this.pageIndex , this.filtersRes);
  }

  
  loadBulk(pageIndex,filtersRes){
    this.service.getdata(pageIndex,filtersRes).pipe(takeUntil(this.onDestroy$)).subscribe(res=>{
      this.dataSource = new MatTableDataSource<any>(res);
this.length = res[0].totalRows;

    })
  }
  updaterow(event) {
    this.route.navigate([]).then(result => { window.open('/Bulk/Product/' + event.id  , '_blank'); });;
  }
  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;
    this.service.SearchBulkProd(this.pageIndex,this.searchKey, this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderData(res);
    });
    }

    keyUpapplyFilter(event) {

      this.searchKey = event;
      let _this = this;
      clearTimeout(this.typingTimer);
      window.this = _this;
      this.typingTimer = setTimeout(_this.filterData, _this.doneTypingInterval);
  
    }
  
    keyDownfilter(event) {
      clearTimeout(this.typingTimer);
    }
  
    filterData() {
      window.this.service.SearchBulkProd(window.this.pageIndex,window.this.searchKey,window.this.filteredString).pipe(takeUntil(window.this.onDestroy$)).subscribe(res => {
  
  
        window.this.renderData(res)
  
      });
    }

    renderData(data) {
      if (data.length < this.pageSize) {
        this.length = data.length;
      } else {
        this.length = data[0].totalRows;
      }
  
      this.dataSource = new MatTableDataSource<Bulk>(data);
  
    }
    handleDataFromFilters(event) {
      
      this.filterOptions.forEach(element => {
        if (element.type === 'multi-select') {
          element.searchQuery = element.selected.join('');
        }
      });
      this.filteredString = '';
      this.filterOptions.forEach(element => {
        this.filteredString += element.searchQuery;
      });
    
        this.service.SearchBulkProd(this.pageIndex, '', this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
          this.renderData(res);
        });


      }

   
}




declare global {
  interface Window { this: any; }
}
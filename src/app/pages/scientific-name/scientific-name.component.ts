import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from "@angular/core";
import { MatTableDataSource, MatDialog, MatPaginator } from "@angular/material";
import { of, ReplaySubject } from "rxjs";
import { take, tap, switchMap, takeUntil } from "rxjs/operators";

import { ScientificNameModel, SciNamesSearchParams } from "./model/scientific-name.model";
import { ScientificNameService } from "./services/scientific-name.service";
import { ScientificNamesFilterOptions } from "./filters/scientific-name.filter";
import { DeleteConfirmDialogComponent } from "src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component";
import { ScientificNameListComponent } from "./scientific-name-list/scientific-name-list.component";
import { ViewAllModalComponent } from "./view-all-modal/view-all-modal.component";
import { MergeScientificNameComponent } from "./merge-scientific-name/merge-scientific-name.component";
import { AddScientificNameComponent } from "./add-scientific-name/add-scientific-name.component";
import { IFilterOption } from "src/app/filter/filters.model";
import { ToastService } from "src/app/_services/toast.service";
import { NgForm } from "@angular/forms";
// import { TranslateComponent } from "./translate/translate.component";

@Component({
  selector: "app-scientific-name",
  templateUrl: "./scientific-name.component.html",
  styleUrls: ["./scientific-name.component.scss"],
})
export class ScientificNameComponent implements OnInit, OnDestroy, AfterViewInit {


  displayedColumns: Array<string> = [
    "actions",
    "id",
    "scientificName",
    "medicalLine",
    "createdDate",
    "CreatedBy",
    "viewpicture",
    "speciality",
    "relatedScientificName",
    "narrowerScientificName",
    "broaderScientificName",
    "distributornumber",
    "manufacturernumber",
    "productnumber",
    // 'createdby',
    // 'marketanalysis'
  ];
  dataSource: MatTableDataSource<ScientificNameModel>;
  searchParams: SciNamesSearchParams = new SciNamesSearchParams();
  onDestroy$ = new ReplaySubject<void>();
  filterOptions: Array<IFilterOption> = ScientificNamesFilterOptions;
  @ViewChild(MatPaginator ) paginator: MatPaginator;

  pageSize: number = 10;
  totalItems: number = 0;
  filteredString: string = "";
  pageType: string = "Scientific Name";
  isLoading: boolean;



  constructor(
    private toast: ToastService, 
    private service: ScientificNameService,
    public dialog: MatDialog
  ) {}



  ngOnInit() {
    this.pageType = "scientific name";
    this.getScientificNames(); 
  }

  ngAfterViewInit() {
    console.log(this.paginator);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  getScientificNames(): void {
    this.isLoading = true;
    this.service
      .getall(this.searchParams, this.filteredString)
      .pipe(
        take(1),
        tap((res: any) => {
          this.renderData(res);
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }


  renderData(res: any) {
    if(res && res.data) {
      this.totalItems = res.data.count;
      this.dataSource = new MatTableDataSource<ScientificNameModel>(res.data.items);
    } else {
      this.totalItems = 0;
      this.dataSource = new MatTableDataSource<ScientificNameModel>([]);
    }

    this.isLoading = false;
  }


  /**
   * @function searchSubmit
   * @description search for sci names
   */
  searchSubmit(form: NgForm) {

    if(this.paginator) this.paginator.firstPage(); 
    this.searchParams.PageNumber = 1;

    if(!form.value.id && !form.value.name) {
      this.searchParams.Id = '';
      this.searchParams.Name = '';
      this.getScientificNames();
      return;
    } 

    this.searchParams.Id = form.value.id ? String(form.value.id).trim() : '';
    this.searchParams.Name = form.value.name ? String(form.value.name).trim() : '';
    this.getScientificNames();
  }



  pageChange(event) {
    this.pageSize = event.pageSize;
    this.searchParams.PageSize = event.pageSize;
    this.searchParams.PageNumber = event.pageIndex + 1;
    this.getScientificNames();
  }


  // TODO: assign each table displayedCol for each
  actionTabChange(event) {
    switch (event.nextId) {
      case "1":
        this.pageType = "scientific name";
        this.searchParams.PageNumber = 1;
        this.searchParams.Id = '';
        this.searchParams.Name = '';
        this.getScientificNames();
        break;
      case "2":
        this.pageType = "suggestioned scientific name";
        // this.pageIndex = 1;
        // this.getSuggestedScientificNames();
        break;
    }
  }

  // clearSearchParams(form: any) {
  //   form.value.id = null;
  //   form.value.name = null;
  //   this.getScientificNames();
  // }

  showlist(row: ScientificNameModel) {
    this.dialog.open(ScientificNameListComponent, {
      width: "65%",
      data: { ScientificnameId: row },
    });
  }

  OpenAddUpdate(row: ScientificNameModel) {
    const dialogRef = this.dialog.open(AddScientificNameComponent, {
      width: "65%",
      data: { row }
    });

    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }


  viewAll(data: any[], pageTitle: string): void {
    let dialogeData: any = {};
    dialogeData.displayData = data;
    dialogeData.title = pageTitle;
    dialogeData.displayedColumns = ["id", "name"];

    this.dialog.open(ViewAllModalComponent, {
      data: dialogeData,
    });
  }

  onApprove(id: number): void {
    this.service
      .approveScientificName(id)
      .pipe(
        take(1),
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }

  onUnApprove(id: number): void {
    this.service
      .UnapproveScientificName(id)
      .pipe(
        take(1),
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }

  onDelete(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: "65%",
      data: {
        title: "Scientific Name",
        id: id,
      },
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted
            ? this.service.deleteScientificName(id)
            : of(undefined);
        }),
        tap((result) => {
          if (result) {
            if(result.success) {
              this.toast.success("Deleted successfully!");
              this.getScientificNames();
            } else this.toast.error("something wrong happened");
          }


        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }

  MergeScient(Sciente) {
    const dialogRef = this.dialog.open(MergeScientificNameComponent, {
      width: "65%",
      // height: "50%",
      data: { Sciente },
    });
    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }



  handleDataFromFilters(event) {
    this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;

    if (event === 0) {
      this.filteredString = "";
      this.getScientificNames();
      return;
    }

    this.filterOptions.forEach((element) => {
      if (element.type === "multi-select") {
        element.searchQuery = element.selected.join("");
      }
    });

    this.filteredString = "";
    this.filterOptions.forEach((element) => {
      this.filteredString += element.searchQuery;
    });

    this.getScientificNames();
  }


}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';

import { LanguagesService } from './languages.service';
import { AddLanguageComponent } from './addLanguage/addLanguage.component';
import * as _ from 'lodash';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from '../_services/toast.service';



@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private serv: LanguagesService, public dialog: MatDialog, private toast: ToastService) { }

  ngOnInit() {
    this.loadLanguages();

  }

  displayedColumns = [
    'actions',
    'id',
    'key',
    'language',
    'value',
    'page',
    'moduleId',
  ];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  length = 10;
  pageIndex = 1;
  pageSize = 10;
  SearchKey: string;

  dataSource: MatTableDataSource<any>;
  languages: any = [];
  loadLanguages() {
    this.serv.getfullLanguage().pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.languages = res;
      this.renderDataSource(res);
    });
  }

  pageChange(event) {
    /*  this.pageIndex = event.pageIndex + 1;
 
     this.serv.getUsers(this.pageIndex).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
       this.renderDataSource(res);
     }); */
  }

  renderDataSource(data) {
    /* if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.length = data[0].totalRows;
    } */
    this.dataSource = new MatTableDataSource<any>(data);
  }

  add() {
    const dialogRef = this.dialog.open(AddLanguageComponent, {
      width: '75%',
      height: '75%'
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      console.log('The dialog was closed');
      this.loadLanguages();
    });
  }

  DeleteLanguage(id) {
    this.serv.delLanguage(id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      if (res) {
        this.toast.success('Deleted Successfully');
      }
      else {
        this.toast.success('Something Wrong');
      }
    })
  }

  generateJson() {
    let latestArray = [];
    let languageTypes = _.groupBy(this.languages, 'language');
    let languagesKeys = Object.keys(languageTypes);
    languagesKeys.forEach(lang => {

      let languagesJson = _.groupBy(languageTypes[lang], 'pageId');
      let pageKeys = Object.keys(languagesJson);
      let finalObject = {
      };
      pageKeys.forEach(element => {

        let arraySection = languagesJson[element].filter(x => x.groupId !== null && x.type === 2);
        let langugaesNotGrouped = languagesJson[element].filter(x => x.groupId !== null && x.type === 0);
        langugaesNotGrouped.forEach(notGr => {
          if (finalObject[notGr.key]) {
            let key = notGr.key + '_' + notGr.pageName;
            finalObject[key.trim()] = {};
            finalObject[key.trim()][lang] = notGr.value;
          } else {
            finalObject[notGr.key] = {}
            finalObject[notGr.key][lang] = notGr.value;
          }

        });
        let sections = _.groupBy(arraySection, 'typeName');
        let sectionKey = Object.keys(sections);
        sectionKey.forEach(item => {
          let section = _.groupBy(sections[item], 'groupId');
          let data = Object.keys(section);


          if (finalObject[item] === undefined) {
            finalObject[item] = [];
          }
          data.forEach(da => {
            let semiObject = {}
            let finalSection = section[da];
            finalSection.forEach(ff => {
              semiObject[ff.key] = {};
              semiObject[ff.key][lang] = ff.value;
              semiObject[ff.key]["order"] = ff.orderId;
              semiObject[ff.key]["flag"] = ff.flag;
            });
            finalObject[item].push(semiObject);
          });


        });

        // will execute every 30 seconds

      });
      latestArray.push({ pageName: lang, data: finalObject });
    });
    latestArray.forEach((element, index) => {
      this.serv.generateFile({ FileName: element.pageName, Data: element.data }).subscribe(res => {
        
      });
    });
  }


}


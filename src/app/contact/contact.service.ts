import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  APIEndpoint = environment.ApiUrl;
constructor(private http: HttpClient) { }

getContacts(pageindex) : Observable<any[]>{
  return this.http.get<any[]>(this.APIEndpoint + '/api/AdminBulkContact/Search?PageSize=10&PageNumber='+pageindex);
}
getContactsall() : Observable<any[]>{
 return this.http.get<any[]>(this.APIEndpoint + '/api/AdminBulkContact/Search?PageSize=10&PageNumber=1');
}

saveEdit(param){
  return this.http.post(this.APIEndpoint + '/api/AdminBulkContact/AddUpdate', param);
}

getCountry(): Observable<any[]> {
  return this.http.get<any[]>(
    this.APIEndpoint + '/api/Countries'
  );

}


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialityComponent } from './speciality.component';
import {
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatInputModule,
  MatChipsModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { SpecialityRoutingModule } from './speciality-routing.module';
import { MListComponent } from './mList/mList.component';
import { AddSpecComponent } from './addSpec/addSpec.component';
import { FilterModule } from '../filter/filter.module';

@NgModule({
  imports: [
    CommonModule,
    SpecialityRoutingModule,
    MatCardModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatChipsModule,
    FilterModule
  ],
  declarations: [SpecialityComponent, MListComponent, AddSpecComponent],
  entryComponents: [MListComponent, AddSpecComponent]
})
export class SpecialityModule {}

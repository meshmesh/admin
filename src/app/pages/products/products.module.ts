import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FilterModule } from "../../filter/filter.module";
import { SharedModule } from "../../shared/shared.module";

import { QuillEditorModule } from "ngx-quill-editor";
import { AddEditCatalogComponent } from "./components/add-edit-catalog/add-edit-catalog.component";
import { MatDialogModule } from "@angular/material";
import { ProductsComponent } from "./products.component";
import { SeoProductComponent } from "./components/seo-product/seo-product.component";
import { ProductlistComponent } from "./components/productlist/productlist.component";
import { AddSubProductComponent } from "./components/addSubProduct/addSubProduct.component";
import { AddUpdateProductComponent } from "./components/add-update-product/add-update-product.component";
import { ProductsRoutingModule } from "./products-routing.module";
import { SeoNewProductComponent } from "./components/seo-new-product/seo-new-product.component";
import { ProductCatalogService } from "./services/products-catalog.service";
import { ProductService } from "./services/product.service";

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    SharedModule,
    FilterModule,
    QuillEditorModule,
    MatDialogModule
  ],
  declarations: [
    ProductsComponent,
    SeoProductComponent,
    ProductlistComponent,
    AddSubProductComponent,
    AddEditCatalogComponent,
    AddUpdateProductComponent,
    SeoNewProductComponent
  ],
  entryComponents: [
    AddUpdateProductComponent,
    ProductlistComponent,
    AddEditCatalogComponent,
    AddSubProductComponent,
    SeoProductComponent,
    SeoNewProductComponent
  ],
  providers: [
    ProductCatalogService,
    ProductService
  ]
})
export class ProductsModule {}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../_services/auth.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { first, takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit , OnDestroy {

  onDestroy$ = new ReplaySubject<void>();
  LoginForm: FormGroup;
  returnUrl: string;
  error = '';
  submitLoader: boolean;
  

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private authService: AuthService,
    private toast: ToastService,
    private fb: FormBuilder
  ) { 
    this.submitLoader = false;
  }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.createLoginForm();
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }


  createLoginForm() {
    this.LoginForm =
      this.fb.group({
        Email:
          new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
        Password:
          new FormControl('', Validators.required)
      });
  }


  get f() {
    return this.LoginForm.controls;
  }

  login() {
    this.submitLoader = true;
    this.authService.login(this.f.Email.value, this.f.Password.value)
      .pipe(first())
      .pipe(takeUntil(this.onDestroy$)).subscribe(
        res => {
          debugger
          this.submitLoader = false;
          if(res.success) {
            this.router.navigate(['/']);
          } else {
            this.toast.error(res.errorCode || "Oops! Invalid Credentials.");
          }
        },
        error => {
          this.submitLoader = false;
          this.toast.error("Oops! Something wrong happened.");
        }
      );
  }














}

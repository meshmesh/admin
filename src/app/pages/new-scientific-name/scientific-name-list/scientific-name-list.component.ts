import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ScientificNameService } from '../services/scientific-name.service';

@Component({
  selector: 'app-scientific-name-list',
  templateUrl: './scientific-name-list.component.html',
  styleUrls: ['./scientific-name-list.component.scss']
})

export class ScientificNameListComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  onDestroy$ = new ReplaySubject<void>();
  displayedColumns: string[] = ['id', 'name', 'countryName'];
  displayedManuColumns: string[] = ['id', 'name', 'countryName'];
  displayedProdColumns: string[] = ['id', 'Title'];
  displayedSpecialityColumns = ['id', 'name'];
  List: any;
  ListProd: any;
  ListManu: any;
  specialityProd: any;
  dialogType: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public service: ScientificNameService) { }

  ngOnInit() {

    if(this.data.type === 1) {
      this.dialogType = 1;
      this.service.getManuInScientificname(this.data.ScientificnameId).pipe(takeUntil(this.onDestroy$)).subscribe((resp: any) => {
        this.ListManu = resp.data;
      })
      this.service.getDistInScientificname(this.data.ScientificnameId).pipe(takeUntil(this.onDestroy$)).subscribe((resp: any) => {
        this.List = resp.data;
      })
      this.service.getProdInScientificname(this.data.ScientificnameId).pipe(takeUntil(this.onDestroy$)).subscribe((resp: any) => {
        this.ListProd = resp.data;
      })
    }

    else if(this.data.type === 3) {
      this.dialogType = 3;
      this.specialityProd = this.data.ScientificnameId.mainSpeciality;
    }



  }


}

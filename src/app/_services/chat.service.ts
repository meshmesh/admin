import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import Pusher from 'pusher-js';
import { CurrentUser } from '../_model/CurrentUser';
import { AuthService } from './auth.service';
export interface MessageTemplate {
  id: number;
  name: string;
  field_name: string;
  field_type: 'input' | 'date';
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  pusher: Pusher;
  currentUser: CurrentUser;
  constructor(private http: HttpClient, private authService: AuthService) {
    this.currentUser = this.authService.currentUserValue;
    if (this.currentUser) {
      this.init();
    }


  }

  GetConversations() {
    return this.http.get(environment.ApiUrl + 'Group/GetAll');
  }

  init() {
    let _this = this;
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      authEndpoint: environment.ApiUrl + 'Channel/ChannelAuthAsync',
      auth: {
        headers: {
          'authorization': 'Bearer ' + _this.currentUser.jwt
        }
      }
    });
  }

  createMessage(messageObject, groupId) {
    messageObject.SocketId = this.pusher.connection.socket_id;
    messageObject.GroupId = groupId;
    return this.http.post(environment.ApiUrl + 'Message/Create', messageObject);
  }
  GetMessages(conversationId) {
    return this.http.get(environment.ApiUrl + 'Message/GetById/' + conversationId);
  }
  GetUserLog() {
    return this.http.get(environment.ApiUrl + 'Message/GetUserLog');
  }

  SubscribeToNewGroup() {
    return this.pusher.subscribe('group_chat')

  }
  SubscripeToChannel(groupId) {
    return this.pusher.subscribe('private-' + groupId);
  }
  NewGroupCreated() {
    this.SubscribeToNewGroup().bind('new_group', (data) => {
      this.SubscripeToChannel(data.ID).bind('new_message', (message) => {

      });
    });
  }

}

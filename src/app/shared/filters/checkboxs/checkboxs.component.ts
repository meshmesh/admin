import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkboxs',
  templateUrl: './checkboxs.component.html',
  styleUrls: [
    './checkboxs.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class CheckboxsComponent implements OnInit {
  @Input() mainData;
  tempData: any[] = [];
  @Output() dataFromCheck = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log('data on check box', this.mainData);
  }

  handleCheckBox(event) {
    const name = event.target.name;
    const value = event.target.value;
    const obj = {
      name: name,
      value: value
    };
    console.log('value', event.target.value);
    console.log('name', event.target.checked);
    if (event.target.checked) {
      this.tempData.push(event.target.value);
      this.mainData.value = this.tempData;
    } else {
      const index = this.tempData.indexOf(event.target.value);
      this.tempData.splice(index, 1);
      this.mainData.value = this.tempData;
    }
    // if (value) {
    //   this.tempData.push(obj);
    //   // this.mainData.value.push(obj);
    // } else {
    //   const index = this.tempData.indexOf({ name: name, value: !value });
    //   this.tempData.splice(index, 1);
    //   // this.mainData.value.splice(index, 1);
    // }
    // this.mainData.value = this.tempData;

    // this.mainData.value = { name: name, value: value };
    this.dataFromCheck.emit(this.mainData);
  }
}

import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SignedAgreementResponse, SASearchParams, SACreate } from './business.models';



@Injectable()
export class SignedAgreementsService {

  APIEndpoint = environment.ApiUrlV3 + 'AdminSignAgreement';

  constructor(private http: HttpClient) {}


  /**
   * @function getSignedAgreement
   * @param {SASearchParams} data 
   */
  getSignedAgreement(data: SASearchParams): Observable<SignedAgreementResponse>   {
    let URL = this.APIEndpoint + `/Search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}`;

    // Optional search params
    // URL += data.Status ? `&Status=${data.Status}` : '&Status=1';
    // URL += data.ManfacturerName ? `&ManfacturerName=${data.ManfacturerName}` : '';
    // URL += data.DistributorName ? `&DistributorName=${data.DistributorName}` : '';
    // URL += data.Website ? `&Website=${data.Website}` : '';
    // URL += data.NextAction ? `&NextAction=${data.NextAction}` : '';
    // URL += data.LastAction ? `&LastAction=${data.LastAction}` : '';
    // URL += data.Source ? `&Source=${data.Source}` : '';
    // URL += data.CreatedFrom ? `&CreatedFrom=${data.CreatedFrom}` : '';
    // URL += data.CreatedTo ? `&CreatedTo=${data.CreatedTo}` : '';
    return this.http.get<SignedAgreementResponse>(URL);
  }

  getCountries(): Observable<any[]> {
    return this.http.get<any[]>(environment.ApiUrlV3 + 'Countries');
  }

  add(data: SACreate) {
    return this.http.post<any>(this.APIEndpoint, data);
  }

  edit(data: any) {
    return this.http.put<any>(this.APIEndpoint, data);
  }


  delete(data: any) {
    let URL = this.APIEndpoint + `/AdminSignAgreement?ManufactureId=${data.ManufactureId}&DistributorId=${data.DistributorId}`;
    return this.http.delete<any>(URL);
  }

}
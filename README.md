# Aumet Admin Panel from AbleProV607Demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component aum-component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deploy to UAT test servers

Run `npm run prepare ` to execute command required for preparing files for UAT build. It runs the following:
- `git pull` and make sure you are on a branch named as release-{your-branch-name}
- `npm run build` 
- zip files inside `dist/aumet-admin` folder and name it `{date}-release-{release-name}.zip`
- Copy the zip file into Backup folder `C:\inetpub\wwwroot\UAT\__BACKUPS`;
- Delete content of folder `C:\inetpub\wwwroot\UAT\Publish_Admin`
- Unzip `{date}-release-{release-name}.zip` to path `C:\inetpub\wwwroot\UAT\Publish_Admin`

## 🐞 Issue bug template

### Subject of the issue
Describe your issue here.

### 🌍 Your environment
* version of angular
* which browser and its version

### Steps to reproduce
Tell us how to reproduce this issue. 

### Expected behaviour
Tell us what should happen

### Actual behaviour
Tell us what happens instead



## Publish

By: Shadi Namrouti 24-06-2020
Note: I used ((node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/)) before (ng build) to work around a javascript heap issue when using ((--prod)). This occured in Angular 7 and older.


1- Open the folder using MS-Code
2- Open Terminal -> New Terminal
3- npm install
4- Test: 

node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build --deleteOutputPath --outputPath="C:\websites\publish-admin.aumet.me\"

5- Prod:
node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build --deleteOutputPath --prod --outputPath="C:\websites\publish-admin.aumet.me\"
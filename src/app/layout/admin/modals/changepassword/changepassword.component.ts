import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChangepasswordService } from './changepassword.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ChangepasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public serv :ChangepasswordService) { }
    hide = true;
    hide2 = true;
    passowrd: string;
    confirm:string;
    pass : pass;
    Invalid = false;
    Invalid2 = false;
  close(data): void {
    this.dialogRef.close(data);
  }


  ngOnInit() {

    this.pass = new pass();
  }

  Save(pass){
    
    if(pass.passowrd.length > 8){
      if (pass.passowrd === pass.confirm ) {
        console.log(pass);
        this.Invalid = false;
  this.serv.changepassword(pass.passowrd).subscribe(res =>{
    console.log(true);
  });
      } else {
        this.Invalid = true;
      }
     
    }else {
      this.Invalid2 = true;
    }    
 

  }

}

export class pass{
  passowrd: string;
  confirm:string;
}

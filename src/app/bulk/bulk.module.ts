import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BulkComponent } from './bulk.component';
import { SharedModule } from '../shared/shared.module';
import { BulkRoutingModule } from './bulk-routing.module';
import { AddEditBulkComponent } from './add-edit-bulk/add-edit-bulk.component';
import { QuillEditorModule } from 'ngx-quill-editor';
import { MatAutocompleteModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FilterModule } from '../filter/filter.module';

@NgModule({
  imports: [
    CommonModule,
    BulkRoutingModule,
    SharedModule,
    QuillEditorModule, MatAutocompleteModule,
    FormsModule,
    FilterModule

  ],
  declarations: [BulkComponent, AddEditBulkComponent]
})
export class BulkModule { }

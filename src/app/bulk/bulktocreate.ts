export class Bulktocreate {
    BulkId: number; 
    productTitle: string;
    productSubtitle: string;
    baseImage: string; 
    productDescription: string; 
    specialityId: number; 
    manufacturerId: number;
    madeIn: number;
    SoldToIds: number[] = [];
    SellingToIds: number[] = [];
    ScientificIds: number[] = [];
    CatalogURL: string; 
    Images:  string[] = []; 
    logoImage :  string;
    medicalexpoUrl : string;
    refererUrl: string;
    subSpecifications: string;
    seo : string;
    
}

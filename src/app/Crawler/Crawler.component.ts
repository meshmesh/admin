import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  MatTableDataSource,
  MatTabChangeEvent,
  MatDialog,
  MatPaginator,
  MatChipInputEvent
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { Observable, of } from "rxjs";
import { CrawlerService } from "./Crawler.service";
import { CrawleractionsComponent } from "./crawleractions/crawleractions.component";
import { environment } from "src/environments/environment";
import { ENTER, COMMA } from "@angular/cdk/keycodes";
import { ToastService } from "../_services/toast.service";

@Component({
  selector: "app-Crawler",
  templateUrl: "./Crawler.component.html",
  styleUrls: ["./Crawler.component.scss"]
})
export class CrawlerComponent implements OnInit {
  @ViewChild("scientificinput") scientificinput: ElementRef<HTMLInputElement>;
  @ViewChild("specialityinput") specialityinput: ElementRef<HTMLInputElement>;
  @ViewChild("medicalinput") medicalinput: ElementRef<HTMLInputElement>;
  @ViewChild("companyinput") companyinput: ElementRef<HTMLInputElement>;
  @ViewChild("paginator") paginator: MatPaginator;

  displayedColumns: string[] = [
    "companyid",
    "company",
    "link",
    "isproduct",
    "emails",
    "phone"
  ];
  NewSerachdisplayedColumns: string[] = ["link"];

  URLMustcards: any[] = [];

  seconddisplayedColumns: string[] = [
    "scientificName",
    "speciality",
    "Action",
    "numberofcompanies"
  ];
  ContactdisplayedColumns: string[] = ["Email", "Phone", "Action"];
  dataSource: MatTableDataSource<any>;
  NewSearchdataSource: MatTableDataSource<any[]>;

  SeconddataSource: MatTableDataSource<any>;
  ByCompanydataSource: MatTableDataSource<any>;
  EmailSelected: any;
  PhoneSelected: any;
  ContactDataSource: any[] = [];
  mustcounter: number = 0;
  shouldcounter: number = 0;

  searchobject: searchobject;
  scientificnamesearch: scientificnamesearch;
  selected: any;
  selectedsearch: any;
  selectedAdd: any;
  AddType: any;
  pageSize: number = 10;
  SecondPageSize: number = 10;

  pageNumber: number = 0;
  length: number;
  NewSearchlength: number;
  dynamicObject: any;
  searchdynamicObject: any;

  disabled = false;
  removable = true;
  selectedMedical: any[] = [];
  medicalsFilteration: Observable<any>;
  medicals: any = [];
  selectedMedicalId: any[] = [];
  Parent = false;

  seletedscientificNames: any[] = [];
  scientificNames: any = [];
  filteredscientificNames: Observable<any>;
  selectedScientificId: any[] = [];

  selectedspecialities: any[] = [];
  specialities: any = [];
  filteredspecialities: Observable<any>;
  selectedspecialitiesIds: any[] = [];

  selectedCompanies: any[] = [];
  Companies: any = [];
  filteredCompanies: Observable<any>;
  selectedCompaniesIds: any[] = [];

  hidespeciality = false;
  hidescientific = false;
  hideMedcail = false;
  hideCompany = false;
  hideCompanyandmedical = false;
  isLink = false;
  length2: void;
  addType: any;
  manufacturerFilter: Observable<any>;
  filterdmanufacturer: Observable<any>;
  manufacturer: any[] = [];
  seletedmanufacturer: any[] = [];
  manufacturerName: any;
  visible = true;
  selectable = true;
  addOnBlur = true;
  wildcards: any[] = [];
  shouldcards: any[] = [];
  URLwildcards: any[] = [];
  URLshouldcards: any[] = [];
  MustUrls: any[] = [];

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  CompanyURL: any;
  addscientificnameform = false;
  addcompanyform = false;
  secondpagenumber: number = 0;
  both: boolean;
  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private serv: CrawlerService,
    private toast: ToastService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.searchobject = new searchobject();
    this.scientificnamesearch = new scientificnamesearch();
    this.showMedical();
    this.showSpecialities();
    this.serv.getScientificNames("a").subscribe((response: any) => {
      this.scientificNames = response;
      this.filteredscientificNames = of(response);
    });

    this.manufacturerFilter = this.serv.getManufacturersuggestions("a");
    this.filteredCompanies = this.serv.getManufacturer("a");
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.wildcards.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }
  addMustUrls(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.MustUrls.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }
  addUrlWild(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.URLwildcards.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }
  addshould(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.shouldcards.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }
  addshouldURl(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.URLshouldcards.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }
  addmustURl(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || "").trim()) {
      this.URLMustcards.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }

  remove(wildcard): void {
    const index = this.wildcards.indexOf(wildcard);

    if (index >= 0) {
      this.wildcards.splice(index, 1);
    }
  }
  removeMustUrls(wildcard): void {
    const index = this.MustUrls.indexOf(wildcard);

    if (index >= 0) {
      this.MustUrls.splice(index, 1);
    }
  }
  removeMustURl(wildcard): void {
    const index = this.URLwildcards.indexOf(wildcard);

    if (index >= 0) {
      this.URLwildcards.splice(index, 1);
    }
  }

  removeShould(wildcard): void {
    const index = this.shouldcards.indexOf(wildcard);

    if (index >= 0) {
      this.shouldcards.splice(index, 1);
    }
  }
  removeShouldUrl(wildcard): void {
    const index = this.URLshouldcards.indexOf(wildcard);

    if (index >= 0) {
      this.URLshouldcards.splice(index, 1);
    }
  }
  removemustUrl(wildcard): void {
    const index = this.URLMustcards.indexOf(wildcard);
    if (index >= 0) {
      this.URLMustcards.splice(index, 1);
    }
  }

  changeAdd(event) {
    if (event.value == 1) {
      this.addscientificnameform = true;
      this.addcompanyform = false;
    } else {
      this.addscientificnameform = false;
      this.addcompanyform = true;
    }
  }
  openaction(obj): void {
    const dialogRef = this.dialog.open(CrawleractionsComponent, {
      width: "80%",
      height: "80%",
      data: obj
    });
    dialogRef
      .afterClosed()

      .subscribe(d => {
        this.submit(this.searchobject);
      });
  }

  handleTextChanging(event) {
    this.renderCompany(event);
  }

  renderCompany(searchvalue: string) {
    this.serv.getManufacturersuggestions(searchvalue).subscribe(res => {
      this.manufacturerFilter = of(res);
      this.displayFnManufacturer(res);
    });
  }

  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  geturl(event) {
    this.serv
      .getManufacturerById(event.option.value.id)
      .subscribe((res: any) => {
        this.CompanyURL = res[0].websiteUrl;
        console.log(this.CompanyURL);
      });
  }

  IgnoreResult(object) {
    this.toast.error("Ignore Not Working Yet");

    /*
this.serv.AddCompnayScientificNameFromCrawler(object.scientificNameId,object.companyIds).subscribe(res => {

}) */
  }
  GetContacts() {
    this.searchdynamicObject = {
      size: this.pageSize,
      _source: false
    };
    this.searchdynamicObject["query"] = {};
    this.searchdynamicObject["query"]["term"] = {};
    this.searchdynamicObject["query"]["term"]["companyId"] = {
      value: this.selectedCompaniesIds[0]
    };
    this.searchdynamicObject["aggs"] = {};
    this.searchdynamicObject["aggs"]["distinct_emails"] = {};
    this.searchdynamicObject["aggs"]["distinct_emails"]["terms"] = {
      field: "emails.keyword"
    };
    this.searchdynamicObject["aggs"]["distinct_phones"] = {};
    this.searchdynamicObject["aggs"]["distinct_phones"]["terms"] = {
      field: "phones.keyword"
    };

    this.serv.Search(this.searchdynamicObject).subscribe((res: any) => {
      if (res.error == null || res.error == undefined) {
        this.ContactDataSource = [
          {
            Email: res.aggregations.distinct_emails,
            Phone: res.aggregations.distinct_phones
          }
        ];

        // this.ContactDataSource = _.unionBy(this.ContactDataSource,"doc_count")
      } else this.toast.error(res.message);
    });
  }

  actionTabChange(event: MatTabChangeEvent) {
    this.selectedMedical = [];
    this.selectedMedicalId = [];
    this.selectedScientificId = [];
    this.seletedscientificNames = [];
    this.selectedspecialities = [];
    this.selectedspecialitiesIds = [];
    this.selectedCompaniesIds = [];
    this.selectedCompanies = [];
  }
  _filterManufacturer(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return this.serv.getManufacturersuggestions(filterValue);
  }
  SaveScientificName() {
    var obj: any = {};
    obj.scientificNameId = this.selectedScientificId[0];
    obj.scientificName = this.seletedscientificNames[0].name;
    obj.specialityId = this.selectedspecialitiesIds[0];
    obj.speciality = this.selectedspecialities[0].name;

    obj.MedicalLine = this.selectedMedical[0].name;
    obj.status = 1;
    this.serv.AddScientificName(obj).subscribe((res: any) => {
      if (res.error == null || res.error == undefined) {
        this.toast.success("Added successfully ");
      } else this.toast.error(res.message);
    });
  }

  SaveCompany(form) {
    var obj: any = {};

    obj.companyId = form.manufacturerName.id;
    obj.companyName = form.manufacturerName.name;
    obj.companyWebsite = this.CompanyURL;
    obj.status = 1;
    this.serv.AddCompany(obj).subscribe((res: any) => {
      if (res.error == null || res.error == undefined) {
        this.toast.success("Added successfully ");
      } else this.toast.error(res.message);
    });
  }
  removeMedicalPoints(i) {
    let temp = this.selectedMedical[i];
    let tempIds = this.selectedMedicalId[i];
    this.medicals.push(temp);
    this.selectedMedical.splice(i, 1);
    this.selectedMedicalId.splice(i, 1);
  }

  removespcialities(i) {
    const temp = this.selectedspecialities[i];
    const tempIds = this.selectedspecialitiesIds[i];
    this.specialities.push(temp);
    this.selectedspecialities.splice(i, 1);
    this.selectedspecialitiesIds.splice(i, 1);
  }

  removeCompanies(i) {
    const temp = this.selectedCompanies[i];
    const tempIds = this.selectedCompaniesIds[i];
    this.Companies.push(temp);
    this.selectedCompanies.splice(i, 1);
    this.selectedCompaniesIds.splice(i, 1);
  }

  _filterscientificNames(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return this.serv.getScientificNames(value);
  }
  _filterCompanies(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return this.serv.getManufacturer(value);
  }

  AddSelectedscientificNames(event) {
    this.seletedscientificNames.push(event.option.value);
    this.selectedScientificId.push(event.option.value.id);
    const index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);
      this.scientificinput.nativeElement.value = "";
    }
    this.scientificinput.nativeElement.value = "";
  }

  removescientificNames(i) {
    const temp = this.seletedscientificNames[i];
    const tempIds = this.selectedScientificId[i];
    this.scientificNames.push(temp);
    this.seletedscientificNames.splice(i, 1);
    this.selectedScientificId.splice(i, 1);
  }
  AddSelectedCompanies(event) {
    this.selectedCompanies.push(event.option.value);
    this.selectedCompaniesIds.push(event.option.value.id);
    const index = this.Companies.indexOf(event.option.value);
    if (index > -1) {
      this.Companies.splice(index, 1);
      this.companyinput.nativeElement.value = "";
    }
    this.companyinput.nativeElement.value = "";
  }
  addtocompanyscientificname(object, data) {
    let obj: any = {};
    obj.ScientificNameId = object.scientificNameId;
    obj.CompanyId = data.companyId;
    obj.Links = data.links;
    console.log(obj);

    this.serv.AddCompnayScientificNameFromCrawler(obj).subscribe(res => {
      this.toast.success("Imported Successfully ");
    });
  }
  AddSelectedSpecialities(event) {
    this.selectedspecialities.push(event.option.value);
    this.selectedspecialitiesIds.push(event.option.value.id);
    const index = this.specialities.indexOf(event.option.value);
    if (index > -1) {
      this.specialities.splice(index, 1);
      this.specialityinput.nativeElement.value = "";
    }
    this.specialityinput.nativeElement.value = "";
  }
  AddSelectedMedical(event) {
    this.selectedMedical.push(event.option.value);
    this.selectedMedicalId.push(event.option.value.id);
    let index = this.medicals.indexOf(event.option.value);
    if (index > -1) {
      this.medicals.splice(index, 1);
      this.medicalinput.nativeElement.value = "";
    }
    this.medicalinput.nativeElement.value = "";
  }

  _filterMedical(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(
      this.medicals.filter(
        item => item.name.toLowerCase().indexOf(filterValue) === 0
      )
    );
  }
  _filterspeciality(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(
      this.specialities.filter(
        item => item.name.toLowerCase().indexOf(filterValue) === 0
      )
    );
  }

  showMedical() {
    this.serv.getmedicalline().subscribe(
      (response: any) => {
        this.medicals = response;
        this.medicalsFilteration = of(this.medicals);
      },
      error => console.log(error)
    );
  }
  showSpecialities() {
    this.serv.getspecialities().subscribe(
      (response: any) => {
        this.specialities = response;
        this.filteredspecialities = of(this.specialities);
      },
      error => console.log(error)
    );
  }

  submitnewserach(form) {
    
    this.dynamicObject = {
      size: 100,
      _source: false,
      explain: false
    };
    this.dynamicObject["query"] = {};
    this.dynamicObject["query"]["bool"] = {};
    this.dynamicObject["query"]["bool"]["must"] = [];

    if (this.searchobject.Name) {
      this.dynamicObject["query"]["bool"]["must"][0] = {};
      this.dynamicObject["query"]["bool"]["must"][0]["match_phrase"] = {
        page: { query: this.searchobject.Name }
      };
    }

    if (this.selectedCompaniesIds.length > 0) {
      this.dynamicObject["query"]["bool"]["must"][1] = {};
      this.dynamicObject["query"]["bool"]["must"][1] = {
        term: { companyId: this.selectedCompaniesIds[0] }
      };
    }

    if (this.MustUrls.length > 0) {
      for (let index = 0; index < this.MustUrls.length; index++) {
        if (!Array.isArray(this.dynamicObject["query"]["bool"]["must"])) {
          this.dynamicObject["query"]["bool"]["must"][0] = [];
        }
        let indexToAdd = this.dynamicObject["query"]["bool"]["must"].length;
        const element = this.MustUrls[index];
        this.dynamicObject["query"]["bool"]["must"][indexToAdd] = {};
        this.dynamicObject["query"]["bool"]["must"][indexToAdd] = {};
        this.dynamicObject["query"]["bool"]["must"][indexToAdd] = {
          wildcard: { "link.keyword": "*" + element.name + "*" }
        };
      }
    }

    if (this.wildcards.length > 0) {
      this.dynamicObject["query"]["bool"]["must_not"] = [];
      for (let index = 0; index < this.wildcards.length; index++) {
        const element = this.wildcards[index];
        this.dynamicObject["query"]["bool"]["must_not"][index] = {};
        this.dynamicObject["query"]["bool"]["must_not"][index] = {
          wildcard: { "link.keyword": "*" + element.name + "*" }
        };
      }
    }
    if (this.URLwildcards.length > 0) {
      for (let index = 0; index < this.URLwildcards.length; index++) {
        const element = this.URLwildcards[index];
        if (!Array.isArray(this.dynamicObject["query"]["bool"]["must_not"])) {
          this.dynamicObject["query"]["bool"]["must_not"] = [];
        }
        let indexToAdd = this.dynamicObject["query"]["bool"]["must_not"].length;
        this.dynamicObject["query"]["bool"]["must_not"][indexToAdd] = {};
        this.dynamicObject["query"]["bool"]["must_not"][indexToAdd][
          "match_phrase"
        ] = {};
        this.dynamicObject["query"]["bool"]["must_not"][indexToAdd][
          "match_phrase"
        ] = {
          page: { query: element.name }
        };
      }
    }

    if (this.shouldcards.length > 0) {
      this.dynamicObject["query"]["bool"]["should"] = [];
      for (let index = 0; index < this.shouldcards.length; index++) {
        const element = this.shouldcards[index];
        this.dynamicObject["query"]["bool"]["should"][index] = {};
        this.dynamicObject["query"]["bool"]["should"][index][
          "match_phrase"
        ] = {};
        this.dynamicObject["query"]["bool"]["should"][index]["match_phrase"] = {
          page: { query: element.name }
        };
      }
    }
    if (this.URLshouldcards.length > 0) {
      for (let index = 0; index < this.URLshouldcards.length; index++) {
        if (!Array.isArray(this.dynamicObject["query"]["bool"]["should"])) {
          this.dynamicObject["query"]["bool"]["should"] = [];
        }
        let indexToAdd = this.dynamicObject["query"]["bool"]["should"].length;
        const element = this.URLshouldcards[index];
        this.dynamicObject["query"]["bool"]["should"][indexToAdd] = {};
        this.dynamicObject["query"]["bool"]["should"][indexToAdd] = {};
        this.dynamicObject["query"]["bool"]["should"][indexToAdd] = {
          wildcard: { "link.keyword": "*" + element.name + "*" }
        };
      }
    }

    this.dynamicObject["aggs"] = {};
    this.dynamicObject["aggs"]["distinct_ids"] = {};

    this.dynamicObject["aggs"]["distinct_ids"]["terms"] = {
      field: "companyId",
      size: 1000000
    };

    this.dynamicObject["aggs"]["distinct_ids"]["aggs"] = {};
    this.dynamicObject["aggs"]["distinct_ids"]["aggs"]["distinct_names"] = {};
    this.dynamicObject["aggs"]["distinct_ids"]["aggs"]["distinct_names"][
      "terms"
    ] = {
      field: "companyName.keyword"
    };

    this.dynamicObject["aggs"]["distinct_ids"]["aggs"]["distinct_names"][
      "aggs"
    ] = {};
    this.dynamicObject["aggs"]["distinct_ids"]["aggs"]["distinct_names"][
      "aggs"
    ]["distinct_links"] = {
      terms: { field: "link.keyword" }
    };
    this.http
      .post(environment.ApiUrl + "/api/Crawling/Search", this.dynamicObject)
      .subscribe((res: any) => {
        let keys = Object.keys(res.aggregations);
        let resualt: any = [];
        if (keys.length > 0) {
          keys.forEach(element => {
            if (this.isObject(res.aggregations[element])) {
              let subObjectKeys = Object.keys(res.aggregations[element]);
              if (subObjectKeys.length > 0) {
                subObjectKeys.forEach(item => {
                  if (Array.isArray(res.aggregations[element][item])) {
                    res.aggregations[element][item].forEach(supitem => {
                      if (this.isObject(supitem)) {
                        let supchildKeys = Object.keys(supitem);
                        supchildKeys.forEach(sup => {
                          if (this.isObject(supitem[sup])) {
                            let lastroot = Object.keys(supitem[sup]);
                            lastroot.forEach(root => {
                              if (Array.isArray(supitem[sup][root])) {
                                supitem[sup][root].forEach(child => {
                                  let object = { key: child.key, result: [] };
                                  let childroot = Object.keys(child);
                                  childroot.forEach(last => {
                                    if (this.isObject(child[last])) {
                                      let lastKeys = Object.keys(child[last]);
                                      lastKeys.forEach(data => {
                                        if (Array.isArray(child[last][data])) {
                                          object.result = [
                                            ...object.result,
                                            ...child[last][data]
                                          ];
                                        }
                                      });
                                      resualt.push(object);
                                    }
                                  });
                                });
                              }
                            });
                          }
                        });
                        // check if sub
                      }
                    });
                  }
                });
              }
            }
          });

          this.NewSearchdataSource = resualt;
          this.NewSearchlength = res.hits.total.value;
        }
      });
  }

  submit(form) {
    if (this.searchobject.Type === "3") {
      this.dynamicObject = {
        size: this.pageSize,
        from: this.pageNumber
      };
      this.dynamicObject["_source"] = ["companyName", "link", "companyId"];
      this.dynamicObject["query"] = {};
      this.dynamicObject["query"]["bool"] = {};
      this.dynamicObject["query"]["bool"]["must"] = [];
      this.dynamicObject["query"]["bool"]["must"][0] = {};
      this.dynamicObject["query"]["bool"]["must"][1] = {};
      if (this.searchobject.slop) {
        this.dynamicObject["query"]["bool"]["must"][0]["match_phrase"] = {
          page: { query: this.searchobject.Name, slop: this.searchobject.slop }
        };
        this.dynamicObject["query"]["bool"]["must"][1] = {
          term: { companyId: this.selectedCompaniesIds[0] }
        };
      } else {
        this.dynamicObject["query"]["bool"]["must"][0]["match_phrase"] = {
          page: { query: this.searchobject.Name, slop: 2 }
        };
        this.dynamicObject["query"]["bool"]["must"][1]["term"] = {};
        this.dynamicObject["query"]["bool"]["must"][1]["term"] = {
          companyId: this.selectedCompaniesIds[0]
        };
      }
    } else {
      this.dynamicObject = {
        size: this.pageSize,
        from: this.pageNumber
      };
      this.dynamicObject["query"] = {};
      if (this.searchobject.Type === "1") {
        if (this.searchobject.Operator) {
          this.dynamicObject["query"]["match"] = {};
          this.dynamicObject["query"]["match"]["page"] = {
            slop: this.searchobject.Operator,
            query: this.searchobject.Name
          };
        } else if (this.searchobject.slop) {
          this.dynamicObject["query"]["match_phrase"] = {};
          this.dynamicObject["query"]["match_phrase"]["page"] = {
            slop: this.searchobject.slop,
            query: this.searchobject.Name
          };
        } else {
          this.dynamicObject["query"]["match"]["page"] = {
            operator: this.searchobject.Operator
          };
        }
      } else if (this.searchobject.Type === "2") {
        if (this.searchobject.Operator) {
          this.dynamicObject["query"]["match"] = {
            operator: this.searchobject.Operator,
            companyName: this.searchobject.Name
          };
        } else if (this.searchobject.slop) {
          this.dynamicObject["query"]["match_phrase"] = {
            slop: this.searchobject.slop,
            companyName: this.searchobject.Name
          };
        } else {
          this.dynamicObject["query"]["match_phrase"] = {
            companyName: {
              query: this.searchobject.Name
            }
          };
        }
      } else if (this.searchobject.Type === "3") {
        this.dynamicObject["query"]["term"] = {
          manufacturerId: this.searchobject.Type
        };
      } else if (this.searchobject.Type === "4") {
        this.dynamicObject["query"]["match"] = {};
        this.dynamicObject["query"]["match"]["url"] = {
          query: this.searchobject.Type
        };
      }
    }

    this.http
      .post(environment.ApiUrl + "/api/Crawling/Search", this.dynamicObject)
      .subscribe((res: any) => {
        this.dataSource = res.hits.hits;
        this.length = res.hits.total.value;
      });
  }

  searchscientificnames(form) {
    this.secondpagenumber = 0;
    this.paginator.pageIndex = 0;
    if (form.ScientifitType == 1) {
      if (this.Parent) {
        let ids = [];
        if (this.seletedscientificNames.length > 0) {
          if (this.scientificinput.nativeElement.value != "") {
            ids = this.selectedScientificId;

            this.serv
              .GetScientificNameIds(this.scientificinput.nativeElement.value)
              .subscribe(res => {
                ids = [...ids, ...res];
              });
          } else {
            ids = this.selectedScientificId;
          }

          if (ids.length === this.seletedscientificNames.length) {
            let querystring = "";
            ids.forEach(element => {
              querystring += "&Ids=" + element;
            });
            this.serv
              .GetScientificNameByParentId(querystring, 1, this.SecondPageSize)
              .subscribe((res: any) => {
                if (res.error == null || res.error == undefined) {
                  if (res.content != []) {
                    this.SeconddataSource = res.content;

                    this.length2 = res.totalElements;
                  } else {
                    this.toast.error("No data to show");
                  }
                } else this.toast.error(res.message);
              });
          }
        } else {
          if (this.scientificnamesearch.scientificname !== "") {
            this.serv
              .GetScientificNameIds(this.scientificnamesearch.scientificname)
              .subscribe(res => {
                ids = [...ids, ...res];
                let querystring = "";
                ids.forEach(element => {
                  querystring += "&Ids=" + element;
                });
                this.serv
                  .GetScientificNameByParentId(
                    querystring,
                    1,
                    this.SecondPageSize
                  )
                  .subscribe((res: any) => {
                    if (res.error == null || res.error == undefined) {
                      if (res.content != []) {
                        this.SeconddataSource = res.content;
                        this.length2 = res.totalElements;
                      } else {
                        this.toast.error("No data to show");
                      }
                    } else this.toast.error(res.message);
                  });
              });
          }
        }
      } else {
        let ids = [];
        if (this.seletedscientificNames.length > 0) {
          if (this.scientificinput.nativeElement.value != "") {
            ids = this.selectedScientificId;

            this.serv
              .GetScientificNameIds(this.scientificinput.nativeElement.value)
              .subscribe(res => {
                ids = [...ids, ...res];
              });
          } else {
            ids = this.selectedScientificId;
          }

          if (ids.length === this.seletedscientificNames.length) {
            this.serv.Crawelerscientificnames(ids).subscribe((res: any) => {
              if (res.error == null || res.error == undefined) {
                if (res.content != []) {
                  this.SeconddataSource = res.content;
                  this.length2 = res.totalElements;
                } else {
                  this.toast.error("No data to show");
                }
              } else this.toast.error(res.message);
            });
          }
        } else {
          if (this.scientificnamesearch.scientificname !== "") {
            this.serv
              .GetScientificNameIds(this.scientificnamesearch.scientificname)
              .subscribe(res => {
                ids = [...ids, ...res];
                this.serv.Crawelerscientificnames(ids).subscribe((res: any) => {
                  if (res.error == null || res.error == undefined) {
                    if (res.content != []) {
                      this.SeconddataSource = res.content;
                      this.length2 = res.totalElements;
                    } else {
                      this.toast.error("No data to show");
                    }
                  } else this.toast.error(res.message);
                });
              });
          }
        }
      }
    } else if (form.ScientifitType == 2) {
      let querystring = "";
      this.selectedspecialitiesIds.forEach(element => {
        querystring += "&Ids=" + element;
      });
      this.serv
        .CrawelerscientificnamesbySpeciality(
          querystring,
          1,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            if (res.content != []) {
              this.SeconddataSource = res.content;
              this.length2 = res.totalElements;
            } else {
              this.toast.error("No data to show");
            }
          } else this.toast.error(res.message);
        });
    } else if (form.ScientifitType == 3) {
      let querystring = "";
      this.selectedMedicalId.forEach(element => {
        querystring += "&Ids=" + element;
      });
      this.serv
        .CrawelerscientificnamesbyMedicalline(
          querystring,
          1,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            if (res.content != []) {
              this.SeconddataSource = res.content;
              this.length2 = res.totalElements;
            } else {
              this.toast.error("No data to show");
            }
          } else this.toast.error(res.message);
        });
    } else if (form.ScientifitType == 4) {
      this.serv
        .GetScientificNameByCompanyId(
          this.selectedCompaniesIds,
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            if (res.content != []) {
              this.ByCompanydataSource = res.content;
              this.length2 = res.totalElements;
            } else {
              this.toast.error("No data to show");
            }
          } else this.toast.error(res.message);
        });
    } else if (form.ScientifitType == 5) {
      this.serv
        .FilterDataByCompanyIdAndMedicalLine(
          this.selectedMedicalId[0],
          this.selectedCompaniesIds[0],
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            if (res.content != []) {
              this.ByCompanydataSource = res.content;
              this.length2 = res.totalElements;
            } else {
              this.toast.error("No data to show");
            }
          } else this.toast.error(res.message);
        });
    }
  }

  SecondpageChange(event) {
    this.secondpagenumber = event.pageIndex;
    this.SecondPageSize = event.pageSize;

    if (this.hideCompany == true) {
      this.serv
        .GetScientificNameByCompanyId(
          this.selectedCompaniesIds[0],
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            this.ByCompanydataSource = res.content;
            this.length2 = res.totalElements;
          } else this.toast.error(res.message);
        });
    } else if (this.hidespeciality == true) {
      let querystring = "";
      this.selectedspecialitiesIds.forEach(element => {
        querystring += "&Ids=" + element;
      });
      this.serv
        .CrawelerscientificnamesbySpeciality(
          querystring,
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            this.SeconddataSource = res.content;
            this.length2 = res.totalElements;
          } else this.toast.error(res.message);
        });
    } else if (this.hideMedcail == true) {
      let querystring = "";
      this.selectedMedicalId.forEach(element => {
        querystring += "&Ids=" + element;
      });
      this.serv
        .CrawelerscientificnamesbyMedicalline(
          querystring,
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            this.SeconddataSource = res.content;
            this.length2 = res.totalElements;
          } else this.toast.error(res.message);
        });
    } else if (this.hideCompanyandmedical) {
      this.serv
        .FilterDataByCompanyIdAndMedicalLine(
          this.selectedMedicalId[0],
          this.selectedCompaniesIds[0],
          this.secondpagenumber,
          this.SecondPageSize
        )
        .subscribe((res: any) => {
          if (res.error == null || res.error == undefined) {
            this.ByCompanydataSource = res.content;
            this.length2 = res.totalElements;
          } else this.toast.error(res.message);
        });
    }
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  clearOperator() {
    this.searchobject.Operator = null;
  }
  clearSlop() {
    this.searchobject.slop = null;
  }
  changesearch(event) {
    this.SeconddataSource = new MatTableDataSource<any>();
    this.paginator.pageSize = 10;
    this.SecondPageSize = 10;
    if (event.value == 1) {
      this.hidescientific = true;
      this.hideMedcail = false;
      this.hidespeciality = false;
      this.hideCompany = false;
      this.hideCompanyandmedical = false;
    } else if (event.value == 2) {
      this.hidescientific = false;
      this.hideMedcail = false;
      this.hidespeciality = true;
      this.hideCompany = false;
      this.hideCompanyandmedical = false;
    } else if (event.value == 3) {
      this.hidescientific = false;
      this.hideMedcail = true;
      this.hidespeciality = false;
      this.hideCompany = false;
      this.hideCompanyandmedical = false;
    } else if (event.value == 4) {
      this.hidescientific = false;
      this.hideMedcail = false;
      this.hidespeciality = false;
      this.hideCompany = true;
      this.hideCompanyandmedical = false;
    } else {
      this.hideCompanyandmedical = true;
      this.hidescientific = false;
      this.hideMedcail = false;
      this.hidespeciality = false;
      this.hideCompany = true;
    }
  }

  enable(event) {
    if (event.value == 2) {
      this.disabled = true;
      this.searchobject.slop = null;
      this.searchobject.Operator = null;
      this.both = false;
      this.isLink = false;
    } else if (event.value == 1) {
      this.disabled = false;
      this.both = false;
      this.isLink = false;
    } else if (event.value == 3) {
      this.disabled = false;
      this.both = true;
      this.isLink = false;
    } else {
      this.disabled = true;
      this.both = false;
      this.isLink = true;
    }
  }

  pageChange(event) {
    this.pageNumber = event.pageIndex * 10;

    this.submit(this.searchobject);
  }
  AddContact() {
    this.serv
      .AddContactFromCrawling(
        this.EmailSelected,
        this.PhoneSelected,
        this.selectedCompaniesIds
      )
      .subscribe((res: any) => {
        this.toast.success(res.message);
      });
    console.log(
      this.PhoneSelected,
      this.EmailSelected,
      this.selectedCompaniesIds
    );
  }
  isObject(n) {
    return Object.prototype.toString.call(n) === "[object Object]";
  }
}
export class searchobject {
  Type: string;
  Operator: string;
  slop: number;
  Name: string;
  wildcards: any[] = [];
}

export class scientificnamesearch {
  ScientifitType: string;
  scientificname: string;
  speciality: number;
  medicalline: string;
  Companies: string;
  Parent: boolean;
}

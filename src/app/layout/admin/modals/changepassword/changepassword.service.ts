import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangepasswordService {

constructor(private http: HttpClient) { }

changepassword(password) {
  return  this.http.get(  environment.ApiUrl + "/api/AdminUsers/ChangePassword?password="+ password);
  }
}

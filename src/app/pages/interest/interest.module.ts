import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatButtonModule, MatTableModule, MatCheckboxModule, MatInputModule, MatDialogModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { InterestComponent } from './interest.component';
import { InterestRoutingModule } from './interest-routing.module';
import { AddCommentComponent } from './addComment/addComment.component';
// import { InterestService } from './interest.service';
import { AddUpdateComponent } from './addUpdate/addUpdate.component';
import { ViewCommentComponent } from './ViewComment/ViewComment.component';
import { FilterModule } from '../../filter/filter.module';





@NgModule({
  imports: [
    CommonModule,
    InterestRoutingModule,
    MatCardModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    FilterModule,
    MatDialogModule
  ],
  exports:[ViewCommentComponent],
  providers: [],
  declarations: [InterestComponent, AddUpdateComponent, AddCommentComponent , ViewCommentComponent],
  entryComponents: [AddUpdateComponent, AddCommentComponent , ViewCommentComponent]
})
export class InterestModule { }
 
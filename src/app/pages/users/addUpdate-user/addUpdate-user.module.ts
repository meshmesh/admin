import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { AddUpdateUserComponent } from './addUpdate-user.component';
import { AddUpdateUserRoutingModule } from './addUpdate-user-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [CommonModule, FormsModule, AddUpdateUserRoutingModule, SharedModule],
  exports: [AddUpdateUserComponent],
  declarations: [AddUpdateUserComponent],
  entryComponents: [AddUpdateUserComponent]
})
export class AddUpdateUserModule {}

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InterestService } from '../interest.service';
import { Interest } from '../interest';
import { of, ReplaySubject, throwError, Observable } from 'rxjs';
import { takeUntil, catchError } from 'rxjs/operators';
import { S3 } from 'aws-sdk/clients/all';
import { ToastService } from 'src/app/_services/toast.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-addUpdate',
  templateUrl: './addUpdate.component.html',
  styleUrls: ['./addUpdate.component.scss']
})
export class AddUpdateComponent implements OnInit, OnDestroy {


  intForm: Interest;
  SCi: any[] = [];
  medline: any[] = [];
  specialities: any[] = [];
  modalData: any;
  title: string = '';
  imagePreview = [];
  localImages: File[] = [];
  submitLoader: boolean;
  basic_title: string = 'Add';

  onDestroy$ = new ReplaySubject<void>();

  constructor(public dialogRef: MatDialogRef<AddUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private serv: InterestService, private toast: ToastService,
  ) { }



  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnInit() {
    this.intForm = new Interest();
    this.renderMedicleLine();
    this.renderScientificName('');
    this.title = this.data.type;
    this.modalData = this.data.items;
    if (this.modalData && this.modalData.id) {

      this.basic_title = 'Update';
      this.intForm.Id = this.modalData.id;
      this.displayScientific(this.modalData.scientificName);
      this.displaySpeciality(this.modalData.specialty);
      this.intForm.Scientific = this.modalData.scientificName;
      this.intForm.MedicleLine = this.modalData.medicalLine;
      this.intForm.Specialities = this.modalData.specialty;
    }
  }

  handleBaseImageChange(e) {
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }

  displayMedicalLine(medLine) {
    return medLine ? medLine.name : undefined;
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  handleSpecialitiesChanging(e) {
    console.log(e)
  }

  async submit(form: NgForm) {
    if (form.invalid)
      return;
    this.submitLoader = true;
    if (this.data.type == "Interest") {
      // let formDataToSend = new Interests();
      let formDataToSend = {
        Id: 0,
        DistributorId: this.data.details.id,
        ScientificNameId: this.intForm.Scientific.id,
        MedicalLineId: this.intForm.MedicleLine.id,
        SpecialtyId: this.intForm.Specialities.id,
        distributorCountryId: this.data.details.country.id
      }
      if (this.intForm.Id != null || this.intForm.Id != 0) {
        formDataToSend.Id = this.intForm.Id;
      }
      // if (this.localImages[0] != null) {
      //   this.uploadImage(this.localImages[0], function (imgLocation) {
      //     formDataToSend.Logo = imgLocation;
      //   });
      // }

      // formDataToSend.interests.DistributorId = this.modalData.id;
      // formDataToSend.interests.ScientificNameId = this.intForm.Scientific.id;
      // formDataToSend.interests.MedicalLineId = this.intForm.MedicleLine;
      // formDataToSend.interests.SpecialtyId = this.intForm.Specialities.id;

      if (formDataToSend.Id > 0) {
        this.serv.EditDistributorInterest(formDataToSend).pipe(catchError(err => {
          this.toast.error(err);
          this.submitLoader = false;
          return throwError(err);
        })).subscribe(res => {
          this.dialogRef.close(true);
          this.submitLoader = false;
          this.toast.success("Updated Successfuly")
        },
          error => {
            this.submitLoader = false;
            this.toast.error(error);
          }
        );
      } else {
        this.serv.saveDistributorInterest(formDataToSend).pipe(catchError(err => {
          this.toast.error(err);
          this.submitLoader = false;
          return throwError(err);
        })).subscribe(res => {
          this.dialogRef.close(true);
          this.submitLoader = false;
          this.toast.success("Added Successfuly")
        },
          error => {
            this.toast.error(error);
            this.submitLoader = false;
          }
        );
      }
    } else if (this.data.type == "Experience") {
      // this.formDataToSend = new Experience();
      let formDataToSend = {
        Id: 0,
        DistributorId: this.data.details.id,
        ScientificNameId: this.intForm.Scientific.id,
        MedicalLineId: this.intForm.MedicleLine.id,
        SpecialtyId: this.intForm.Specialities.id,
        distributorCountryId: this.data.details.country.id
      }
      if (this.intForm.Id != null || this.intForm.Id != 0) {
        formDataToSend.Id = this.intForm.Id;
      }

      // if (this.localImages[0] != null) {
      //   this.uploadImage(this.localImages[0], function (imgLocation) {
      //     formDataToSend.Logo = imgLocation;
      //   });
      // }

      // this.formDataToSend.DistributorId = this.modalData.id;
      // this.formDataToSend.ScientificNameId = this.intForm.Scientific.id;
      // this.formDataToSend.MedicalLineId = this.intForm.MedicleLine;
      // this.formDataToSend.SpecialtyId = this.intForm.Specialities.id;

      if (formDataToSend.Id > 0) {
        this.serv.EditDistributorExperience(formDataToSend).pipe(catchError(err => {
          this.toast.error(err);
          this.submitLoader = false;
          return throwError(err);
        })).subscribe(res => {
          this.dialogRef.close(true);
          this.submitLoader = false;
          this.toast.success("Updated Successfuly")
        },
          error => {
            this.toast.error(error);
          }
        );
      } else {
        this.serv.saveDistributorExperience(formDataToSend).pipe(catchError(err => {
          this.toast.error(err);
          this.submitLoader = false;
          return throwError(err);
        })).subscribe(res => {
          this.dialogRef.close(true);
          this.submitLoader = false;
          this.toast.success("Added Successfuly")
        },
          error => {
            this.toast.error(error);
          }
        );
      }
    }
  }

  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      // Key: 'company/logo/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }


  handleScientificChanging(event) {
    let searchText = ''
    if (event.name !== undefined) {
      this.intForm.MedicleLine = event.medicalLine;
      this.intForm.Specialities = event.specialities[0];
      searchText = event.name;
    }
    else
      searchText = event;

    this.renderScientificName(searchText);
  }

  renderScientificName(searchvalue: string) {
    this.serv.getScientificNameSuggestion(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.SCi = res.data.items;
      this.displayScientific(res.data.items);
    });
  }

  displayScientific(Scient) {
    return Scient ? Scient.name : undefined;
  }

  renderMedicleLine() {
    this.serv.getMedicleLineSuggestion().pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.medline = res.data;
    });
  }

  displaySpeciality(spec) {
    return spec ? spec.name : undefined;
  }

  bindspecialities(e: any) {
    this.serv.getSpecialitySuggestion(e.option.value.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.specialities = res.data;
      this.displaySpeciality(res.data);
    });
  }

  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialities.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
}

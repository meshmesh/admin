import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CrawlerService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) {}

  getScientificNames(str) {
    const url =
      this.APIEndpoint +
      `/api/AdminScientificNames/ScientificnameFilter?PageSize=10&PageNumber=1&nameinput=${str}`;
    return this.http.get<any>(url);
  }
  getManufacturerById(id) {
    return this.http.get(this.APIEndpoint + '/api/AdminManufacturers/' + id);
  }
  getManufacturer(str): Observable<any> {
    const url = this.APIEndpoint + '/api/adminManufacturers/searchfilter?PageNumber=1&PageSize=10&companyType=manufacturer&companyName=' + str ;
    return this.http.get<any>(url);
  }
  GetScientificNameIds(str) {
    const url =
      this.APIEndpoint +
      `/api/AdminScientificNames/GetScientificNameIds?input=${str}`;
    return this.http.get<any>(url);
  }
  getCountrySuggestion(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/Countries/GetCountries'
    );
  }

  getmedicalline(): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + '/api/MedicalLines/GetMedicalLines'
    );
  }
  getManufacturersuggestions(str): Observable<any> {
    const url =
    this.APIEndpoint +
      '/api/adminManufacturers/searchfilter?PageNumber=1&PageSize=10&companyType=manufacturer&companyName=' +
      str 
      ;
    return this.http.get<any>(url);
  }
  getspecialities(): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + '/api/Specialities/GetSpecialities'
    );
  }

  getcompanysbyids(Ids,scientificnameid){
    return this.http.get(this.APIEndpoint + '/api/AdminCompany/getcompanysbyids?'+Ids+'&scientificnameid='+scientificnameid)
  }

  Crawelerscientificnames(param){
    return this.http.post(this.APIEndpoint + '/api/Crawling/GetScientificName', param)
  }
  CrawelerscientificnamesbySpeciality(param,page,size){
    return this.http.get(this.APIEndpoint + '/api/Crawling/GetScientificNameBySpecialty?'+ param+'&page='+page+'&size='+size  )
  }
  CrawelerscientificnamesbyMedicalline(param,page,size){
    return this.http.get(this.APIEndpoint + '/api/Crawling/GetScientificNameByMedicalLine?'+ param+'&page='+page+'&size='+size )
  }
  FilterDataByCompanyIdAndMedicalLine(medical,company,pagenumber,PageSize) : Observable<any>{
    return this.http.get<any>(this.APIEndpoint + '/api/Crawling/FilterDataByCompanyIdAndMedicalLine?medicalLineId='+medical
    +'&companyId='+company +'&page='+pagenumber+'&size='+PageSize)
}
  GetScientificNameByCompanyId(param,pagenumber,PageSize) : Observable<any>{
    return this.http.get<any>(this.APIEndpoint + '/api/Crawling/GetScientificNameByCompanyId?id='+param +'&page='+pagenumber+'&size='+PageSize)
    }
    AddCompany(param){
      return this.http.post(this.APIEndpoint + '/api/Crawling/AddCompany',param)
      }
    
      AddScientificName(param){
        return this.http.post(this.APIEndpoint + '/api/Crawling/AddScientificName',param)
        }
    
        Search(param){
          return this.http.post(this.APIEndpoint + '/api/Crawling/Search',param)
          }        
      
        
    GetScientificNameByParentId(param,pagenumber,PageSize) : Observable<any>{
      return this.http.get<any>(this.APIEndpoint + '/api/Crawling/GetScientificNameByParentId?'+param +'&page='+pagenumber+'&size='+PageSize)
      }
  
      AddContactFromCrawling(email,phone,companyid){
        return this.http.post(this.APIEndpoint + '/api/AdminManufacturers/AddContactFromCrawling?email='+email+'&phone='+phone+'&companyid='+companyid,{})
        }        
    
  AddCompnayScientificNameFromCrawler(obj){
    return this.http.post(this.APIEndpoint + '/api/AdminCompany/AddCompnayScientificNameFromCrawler', obj)
  }
}

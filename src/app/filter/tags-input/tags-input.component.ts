import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { IFilterOption } from '../filters.model';

@Component({
  selector: 'app-tags-input',
  templateUrl: './tags-input.component.html',
  styleUrls: ['./tags-input.component.scss']
})
export class TagsComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Input('option') option: IFilterOption;
  @Output() dataFromAutoComplete = new EventEmitter();
  @ViewChild('Input') Input: ElementRef<HTMLInputElement>;

  constructor() { }

  ngOnInit(): void { }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our items
    if ((value || '').trim()) {
      this.option.list.push(value);
      this.option.selected.push(this.option.querystring + '=' + value + '&');
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

  }

  remove(index: number): void {
    if (index >= 0) {
      this.option.list.splice(index, 1);
      this.option.selected.splice(index, 1);
    }
  }

  
}

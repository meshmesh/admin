import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManufacturerComponent } from '../../pages/manufacturer/manufacturer.component';

const routes: Routes = [
  {
    path: '',
    component: ManufacturerComponent,
    data: {
      title: 'Manufacturer',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }, 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManufacturerRoutingModule { }

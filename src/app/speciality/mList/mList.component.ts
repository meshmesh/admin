import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator } from '@angular/material';
import { SpecialityService } from '../speciality.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mList',
  templateUrl: './mList.component.html',
  styleUrls: ['./mList.component.scss']
})
export class MListComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(public dialogRef: MatDialogRef<MListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private serv: SpecialityService ) { }

    displayedColumnsD = ['Name', 'Country','Action'];
    displayedColumnsM = ['Name', 'Country','Action'];
    Dist: MatTableDataSource<any>;
    Manu: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    values: any[] = [];


  ngOnInit() {
    this.getDist(this.data.id);
  }


  renderDataSourceD(data) {
    this.Dist = new MatTableDataSource<any>(data);
  }

  renderDataSourceM(data) {
    this.Manu = new MatTableDataSource<any>(data);
  }

  paginDataSourceD(data) {
    this.Dist = new MatTableDataSource(data);
    this.Dist.paginator = this.paginator;

    this.Manu = new MatTableDataSource(data);
    this.Manu.paginator = this.paginator;
  }

  close(data): void {
    this.dialogRef.close(data);
  }


  actionTabChange(event) {
    switch (event.nextId) {
      case '1':
        this.getDist(this.data.id);
        break;
      case '2':
        this.getManu(this.data.id);

        break;

    }

  }


  
  getDist(id) {
    this.serv.getDistSpec(id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.Dist = response;
        this.renderDataSourceD(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  getManu(id) {
    this.serv.getManuSpec(id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.Manu = response;
        this.renderDataSourceM(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  viewDist(token, manuId) {
    let obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'distributor' }
    this.serv.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      const response: any = res;
      window.open(response.url);
    });;
  }



  viewManu(token, manuId) {
    let obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'manufacturer' }
    this.serv.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      const response: any = res;
      window.open(response.url);
    });
  }

}
declare global {
  interface Window { this: any; }
}
export class EmailTemplate {

    Id: number;

    Type: number;
    Title: string;

    Body: string;
    Keys: string;

    Portals: number[];
    Languages: number[];

    SystemEmail: boolean;

    CCEmails: string;
    Note: string;

    Deleted: boolean;
    createdAt: Date;
    updatedAt: Date;
}

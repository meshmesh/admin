import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ScientificNameService } from '../services/scientific-name.service';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from "src/app/_services/toast.service";

@Component({
  selector: 'app-merge-scientific-name',
  templateUrl: './merge-scientific-name.component.html',
  styleUrls: ['./merge-scientific-name.component.scss']
})
export class MergeScientificNameComponent implements OnInit, OnDestroy {


  onDestroy$ = new ReplaySubject<void>();
  Scientifics: any[] = [];
  Sci1: string = this.data.Sciente.name;
  Sci2: any;
  sciNameCtrl: Subject<string> = new Subject<string>();
  Scientifics$: any = [];
  submittedObj: any = {
    ScientificNameID: this.data.Sciente.id,
    OldScientificID: null
  }


  constructor(
    private toast: ToastService, 
    private serv: ScientificNameService, 
    public dialogRef: MatDialogRef<MergeScientificNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }




  ngOnInit() {
    this.onChanges();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  handleScientificChanging(event) {
    this.sciNameCtrl.next(event);
  }

  onChanges() {
    this.sciNameCtrl
      .debounceTime(500)
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe(value => {
        if (value !== '') {
          // Call your function which calls API or do anything you would like do after a lag of 1 sec
          this._filterScientificName(value);
        } else {
          this.Scientifics$ = [];
        }
      });

    // this.Scientifics$ = this.sciNameCtrl.valueChanges.pipe(
    //   startWith(''),
    //   // delay emits
    //   debounceTime(300),
    //   // use switch map so as to cancel previous subscribed events, before creating new once
    //   switchMap(value => {
    //     if (value !== '') {
    //       // lookup from github
    //       return this._filterScientificName(value);
    //     } else {
    //       // if no value is present, return null
    //       return of(null);
    //     }
    //   })
    // );
  }

  _filterScientificName(value: any) {
    if (value) {
      this.serv.getAllScientificName(typeof value === 'string' ? value.toLowerCase() : value.name).pipe(
        takeUntil(this.onDestroy$)
      ).subscribe(res => {
        this.Scientifics$ = res;
      });
    } else {
      this.Scientifics$ = [];
    }
  }

  displayScientific(Scient) {
    return Scient ? Scient.name : undefined;
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  async submit(obj) {
    if (obj.form.valid && obj.value.Sci2.id !== undefined) {
      this.submittedObj.OldScientificID = obj.value.Sci2.id;
      this.serv.mergScientific(this.submittedObj.ScientificNameID, this.submittedObj.OldScientificID).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
        
        if (res.success) {
          this.dialogRef.close(true);
          this.toast.success("Merged Successfully!");
        } else this.toast.error("something wrong happened");

      })
    } else {
      this.toast.error("Select scientific name from thr list!");
    }
  }
}
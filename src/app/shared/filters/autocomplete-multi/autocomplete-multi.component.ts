import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AutoCompleteService } from './autoComlete.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete-multi',
  templateUrl: './autocomplete-multi.component.html',
  styleUrls: ['./autocomplete-multi.component.scss'],
  providers: [AutoCompleteService]
})
export class AutocompleteMultiComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  @Input() mainData;
  dataToDisplay: any[] = [];
  value = 'id';
  display= 'name';
 
  // that = this;
  @Output() dataFromAutoComp = new EventEmitter();
  // validators = [this.include];

  selectedList: string[] = [];
  finalSelectedList: number[] = [];
  constructor(private autoCompleteService: AutoCompleteService) {}

  ngOnInit() {
    this.handleTextChanging('a');
    switch (this.mainData.subTitle) {
      case 'country':
        this.autoCompleteService.getCountryDll().pipe(takeUntil(this.onDestroy$)).subscribe(res=> {
          this.dataToDisplay = res;
        });
        break;
      case 'Specialty':
        this.autoCompleteService.getSpesialityDll().pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
          this.dataToDisplay = res;
        });
        break;
      case 'mLine':
        this.autoCompleteService.getMedicalLineDll().pipe(takeUntil(this.onDestroy$)).subscribe(res=>{
          this.dataToDisplay = res;
        });
        break;
      case 'NoOfEmployee':
        this.autoCompleteService.getNumberOfEmployeesDll().pipe(takeUntil(this.onDestroy$)).subscribe(res=>{
          this.dataToDisplay = res;
        });
        break;
      case 'AnnualSalesManu':
        this.autoCompleteService.getAnnualSalesManu().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
          this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
      case 'AnnualSalesDist':
        this.autoCompleteService.getAnnualSalesDist().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
      case 'currentStageM':
        this.autoCompleteService.getManufactureStage().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
      case 'satisfaction':
        this.autoCompleteService.getSatisfaction().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
      case 'currentStageD':
        this.autoCompleteService.getDistributerStage().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
         
          },
          error => console.log(error)
        );
        break;
      case 'supervisorD':
        this.autoCompleteService.getDistributerSupervisor().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
      case 'supervisorM':
        this.autoCompleteService.getManufacturerSupervisor().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
    }
  }

  handleTextChanging(event) {
    switch (this.mainData.subTitle) {
      case 'scientificName':
        this.autoCompleteService.getScientificNameSuggestion(event).pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;

      case 'country':
      this._filter(event);

        break;

      case 'Specialty':
      this._filter(event);
        break;

      case 'NoOfEmployee':
      this._filter(event);
        break;

      case 'mLine':
      this._filter(event);
        break;

      case 'company':
        this.autoCompleteService.getCompanySuggestion(event).pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;

      case 'soldTo':
        this.autoCompleteService.getSoldTo().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;

      case 'selingPoints':
        this.autoCompleteService.getSellingPoints().pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;

      case 'distributor':
        this.autoCompleteService.getDistributers(event).pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;

      case 'manufacturer':
        this.autoCompleteService.getManufacturer(event).pipe(takeUntil(this.onDestroy$)).subscribe(
          (response: any) => {
            this.dataToDisplay = response;
          },
          error => console.log(error)
        );
        break;
    }
  }

  handleAdd(event) {
    if(this.mainData.subTitle === 'NoOfEmployee' || this.mainData.subTitle === 'AnnualSalesManu' || this.mainData.subTitle === 'AnnualSalesDist') {
      this.finalSelectedList.push(event.name);
    } else {
      this.finalSelectedList.push(event.id);
    }

      this.mainData.value = this.finalSelectedList;
      this.dataFromAutoComp.emit(this.mainData);
   
  }

  handleRemove(event) {
    const idIndex = this.finalSelectedList.indexOf(event.id);
    this.finalSelectedList.splice(idIndex ,1 );
    this.mainData.value = this.finalSelectedList;
    this.dataFromAutoComp.emit(this.mainData);
  } 

  _filter(value: any): any[] {
    
    return this.dataToDisplay.filter(data => data.name.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }
} 


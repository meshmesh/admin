
import { Guid } from 'guid-typescript';



export
    interface CurrentUser {

    currentState:
    string;

    nextState:
    string;

    createdAt:
    Date;

    updatedAt:
    Date;

    companyId:
    number;

    isCompanyAdmin:
    boolean;

    isActive:
    boolean;

    is_qualified:
    boolean;

    current_targeting_id:
    number;

    existingUser:
    boolean;

    countryCode:
    string;

    id:
    number;

    firstName:
    string;

    lastName:
    string;

    email:
    string;

    personalPhoneWithCode:
    string;

    jobTitle:
    string;

    companyName:
    string;

    token:
    Guid;

    userType:
    string;

    jwt:
    string;

    previousState:
    string;

    businessOpportunitiesIDs: number[];

    companyToken:
    string;

}


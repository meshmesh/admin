import { Component, Input, ViewChild, ElementRef, OnChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { takeUntil } from 'rxjs/operators';
import { MatAutocompleteTrigger } from '@angular/material';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-auto-complete-multi',
  templateUrl: './auto-complete-multi.component.html',
  styleUrls: ['./auto-complete-multi.component.scss']
})
export class AutoCompleteMultiComponent implements OnChanges, OnDestroy {



  filteredList: Observable<any>;
  onDestroy$ = new ReplaySubject<void>();

  @Input('option') option: any = {};
  @Output() dataFromMultiAutoComplete = new EventEmitter();

  @ViewChild('Input') Input: ElementRef<HTMLInputElement>;
  @ViewChild('autotrigger', { read: MatAutocompleteTrigger }) autoTrigger: MatAutocompleteTrigger;


  constructor(private http: HttpClient,  private authenticationService: AuthService) { }

  ngOnChanges() {
    const currentUser = this.authenticationService.currentUserValue;
    const myHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.jwt });
    this.http.get<any[]>(environment.ApiUrlV3filter + this.option.apiurl,  { headers: myHeaders }).pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
      this.option.list = res.data;
      this.filteredList = of(this.option.list);
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  _filter(value) {
    const currentUser = this.authenticationService.currentUserValue;
    const myHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.jwt });
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value!.toLowerCase();
    }

    if (this.option.list.length > 0 && ( !this.option.hasPagination ||  this.option.hasPagination === undefined)) {
      return of(this.option.list.filter(item => item.name ? item.name.toLowerCase().indexOf(filterValue) === 0 : ''));
    } else {
      this.http.get<any[]>(environment.ApiUrlV3filter + this.option.apiurl + filterValue,  { headers: myHeaders }).pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
        this.option.list = res.data;
        this.filteredList = of(this.option.list);
      });
    }
  }

  AddSelected(event) {
    if (this.option.output === 'object') {
      this.option.selected.push(event.option.value);
      this.Input.nativeElement.value = "";
    }
    else {
      this.option.selected.push(this.option.querystring + '=' + event.option.value[this.option.output] + '&');
    }

    this.option.chipsList.push(event.option.value);
    let index = this.option.list.indexOf(event.option.value);
    if (index > -1) {
      this.option.list.splice(index, 1);
      this.Input.nativeElement.value = "";
    }
    //this.dataFromMultiAutoComplete.next({});
    this.Input.nativeElement.value = "";
    this.openPanels();
  }


  openPanels(): void {
    const self = this;
    setTimeout(function () {
      self.autoTrigger.openPanel();

 /*      self.Input.nativeElement.focus();
      self.Input.nativeElement. */
    }, 1);
  }


  remove(i) {
    let object = this.option.chipsList[i];
    this.option.chipsList.splice(i, 1);
    this.option.selected.splice(i, 1);
    this.option.list.push(object);
    // this.dataFromMultiAutoComplete.next({});
  }
}

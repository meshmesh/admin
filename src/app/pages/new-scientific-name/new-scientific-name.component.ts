import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatCheckboxChange } from "@angular/material";
import { Observable, of, ReplaySubject } from "rxjs";
import { take, tap, switchMap, takeUntil, debounceTime } from "rxjs/operators";

import { IScientificName, ScientificNameModel, SciNamesSearchParamsV2 } from "./model/scientific-name.model";
import { ScientificNameService } from "./services/scientific-name.service";
import { ScientificNamesFilterOptions } from "./filters/scientific-name.filter";
import { DeleteConfirmDialogComponent } from "src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component";
import { ViewAllModalComponent } from "./view-all-modal/view-all-modal.component";
import { MergeScientificNameComponent } from "./merge-scientific-name/merge-scientific-name.component";
import { AddScientificNameComponent } from "./add-scientific-name/add-scientific-name.component";
import { IFilterOption } from "src/app/filter/filters.model";
import { ToastService } from "src/app/_services/toast.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { animate, state, style, transition, trigger } from "@angular/animations";
import {merge} from 'rxjs';
import {catchError, map, startWith} from 'rxjs/operators';


@Component({
  selector: "app-new-scientific-name",
  templateUrl: "./new-scientific-name.component.html",
  styleUrls: ["./new-scientific-name.component.scss"],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class NewScientificNameComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  public Searchform: FormGroup;
  dataSource: MatTableDataSource<IScientificName>;
  dataSourceArray: IScientificName[];
  displayedRows = [
    'actions',
    'image',
    'id',
    'name', 
    'approved', 
    'checked',
    'medicalLine',
    'speciality',
    'subspeciality',
  ];
  pageSize: number = 10;
  totalItems: number = 0;


  searchParams: SciNamesSearchParamsV2 = new SciNamesSearchParamsV2();
  filterOptions: Array<IFilterOption> = ScientificNamesFilterOptions;
  filteredString: string = "";


  pageType: string = "Scientific Name";
  isLoading: boolean;
  specialityList: any;
  subSpecialityList: any;
  isSubLoading: boolean;
  submitLoader: boolean;
  isExpanded: boolean = false;

  onDestroy$ = new ReplaySubject<void>();

  constructor(
    private toast: ToastService, 
    private service: ScientificNameService,
    public dialog: MatDialog,
    public fb: FormBuilder
  ) {}


  // convenience getter for easy access to form fields
  get f() {
    return this.Searchform.controls;
  }

  ngOnInit() {
    this.pageType = "scientific name";

    this.buildSearchForm();


    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoading = true; 

          this.searchParams.SortBy = this.sort.active ? this.sort.active === 'name' ? 2 : 1 : null;
          this.searchParams.SortType = this.sort.direction ? this.sort.direction === 'asc' ? 2 : 1 : null; 

          if(this.searchParams.SortType === null) {
            this.searchParams.SortBy = null
          }
          
          this.searchParams.PageSize = this.paginator.pageSize ? this.paginator.pageSize : 10;
          this.searchParams.PageNumber = this.paginator.pageIndex + 1;

          return this.service.getallV2(this.searchParams, this.filteredString)
        }),
        map(res => {
          // Flip flag to show that loading has finished.
          this.isLoading = false;
          this.totalItems = res.data.count;
          this.pageSize =  this.paginator.pageSize;
          return res.data.items;
        }),
        catchError(() => {
          this.isLoading = false;
          return of([]);
        })
      ).subscribe(data => this.dataSourceArray = data);


  }



  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  private buildSearchForm(): void {
    this.Searchform = this.fb.group({
      idOrName: [''],
    });

    this.Searchform.valueChanges
    .pipe(
      debounceTime(250),
      takeUntil(this.onDestroy$)
    )
    .subscribe((value) => {
      this.searchSubmit();
    });
  }


  toggle(element, expandedElement) {
    element != expandedElement ? expandedElement = element : expandedElement = ''
  }

  searchSubmit() {

    if(this.Searchform.value.idOrName) {
      this.Searchform.value.idOrName.filter((a) => typeof a.id == 'string').map((a) => {
        if(Number(a.name)) {
          a.id = Number(a.name);
        } else {
          a.id = 0;
        }
      });
    }

    this.searchParams.Ids = this.Searchform.value.idOrName ? this.Searchform.value.idOrName.filter((a) => a.id !== 0).map(a => a.id) : [];
    this.searchParams.Names = this.Searchform.value.idOrName ? this.Searchform.value.idOrName.filter((a) => !Number(a.name)).map(a => a.name) : [];
    this.getScientificNames();
  }

  public requestAutocompleteItems = (text: string): Observable<Response> => {
    return this.service.getScientificNameSuggestion(text);
  };


  onUpdateItem(e: boolean, element, expandedElement) {
    if(e) {
      this.getScientificNames();
    }
  }

  trackByFn(index, item) {
    return item.id; // unique id corresponding to the item
  }


  getScientificNames(): void {
    this.isLoading = true;
    this.service
      .getallV2(this.searchParams, this.filteredString)
      .pipe(
        takeUntil(this.onDestroy$)
      ).subscribe((res) => {
        console.log(res, "Data to be rendered")
        this.renderData(res);
      });
  }

  renderData(res: any) {
    if(res && res.data) {
      this.totalItems = res.data.count;
      this.dataSource = new MatTableDataSource<IScientificName>(res.data.items);
      this.dataSourceArray = res.data.items;
    } else {
      this.totalItems = 0;
      this.dataSource = new MatTableDataSource<IScientificName>([]);
      this.dataSourceArray = [];
    }
    this.isLoading = false;
  }


  // TODO: assign each table displayedCol for each
  actionTabChange(event) {
    switch (event.nextId) {
      case "1":
        this.pageType = "scientific name";
        this.searchParams.PageNumber = 1;
        this.searchParams.Ids = null;
        this.searchParams.Names = null;
        this.getScientificNames();
        break;
      case "2":
        this.pageType = "suggestioned scientific name";
        // this.pageIndex = 1;
        // this.getSuggestedScientificNames();
        break;
    }
  }

  OnCheckedClick(e: MatCheckboxChange, item: IScientificName) {
    console.log(e, item)
    this.service
    .checkScientificName(item.id, e.checked)
    .pipe(
      take(1),
      tap((result: any) => {
        if(result.success) {
          this.toast.success("Success!")
          this.getScientificNames();
        } else {
          this.toast.error("Oops! Something wrong happened!")
        }
      }, err => this.toast.error("Oops! Something wrong happened!"))
    )
    .pipe(takeUntil(this.onDestroy$))
    .subscribe();
  }


  OnApprovedClick(e: MatCheckboxChange, item: IScientificName) {
    console.log(e, item);
    this.service
    .approveScientificNameGeneral(item.id, e.checked)
    .pipe(
      take(1),
      tap((result: any) => {
        if(result.success) {
          this.toast.success("Success!")
          this.getScientificNames();
        } else {
          this.toast.error("Oops! Something wrong happened!")
        }
      }, err => this.toast.error("Oops! Something wrong happened!"))
    )
    .pipe(takeUntil(this.onDestroy$))
    .subscribe();
  }

  // showlist(row: ScientificNameModel) {
  //   this.dialog.open(ScientificNameListComponent, {
  //     width: "65%",
  //     data: { ScientificnameId: row, type: 1 },
  //   });
  // }

  // showSpecialityList(row: ScientificNameModel) {
  //   this.dialog.open(ScientificNameListComponent, {
  //     width: "65%",
  //     data: { ScientificnameId: row, type: 3 },
  //   });
  // }

  OpenAddUpdate(row?: IScientificName) {
    const dialogRef = this.dialog.open(AddScientificNameComponent, {
      width: "65%",
      data: { row: row ? row : null }
    });

    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }


  viewAll(data: any[], pageTitle: string): void {
    let dialogeData: any = {};
    dialogeData.displayData = data;
    dialogeData.title = pageTitle;
    dialogeData.displayedColumns = ["id", "name"];

    this.dialog.open(ViewAllModalComponent, {
      data: dialogeData,
    });
  }

  onDelete(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: "65%",
      data: {
        title: "Scientific Name",
        id: id,
      },
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted
            ? this.service.deleteScientificName(id)
            : of(undefined);
        }),
        tap((result) => {
          if (result) {
            if(result.success) {
              this.toast.success("Deleted successfully!");
              this.getScientificNames();
            } else this.toast.error("something wrong happened");
          }


        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }

  MergeScient(Sciente) {
    const dialogRef = this.dialog.open(MergeScientificNameComponent, {
      width: "65%",
      // height: "50%",
      data: { Sciente },
    });
    dialogRef
      .afterClosed()
      .pipe(
        tap((result) => {
          if(result) {
            this.getScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }



  handleDataFromFilters(event) {
    this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;
    debugger
    if (event === 0) {
      this.filteredString = "";
      this.getScientificNames();
      return;
    }

    this.filterOptions.forEach((element) => {
      if (element.type === "multi-select") {
        element.searchQuery = element.selected.join("");
      }
      if (element.type === "tags-select") {
        element.searchQuery = element.selected.join("");
      }
    });

    this.filteredString = "";
    this.filterOptions.forEach((element) => {
      this.filteredString += element.searchQuery;
    });

    this.getScientificNames();
  }


}



import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpecialityService } from './speciality.service';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { MListComponent } from './mList/mList.component';
import { AddSpecComponent } from './addSpec/addSpec.component';
import { tap, takeUntil } from 'rxjs/operators';
import { TranslateComponent } from '../translate/translate.component';

@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.scss'],
})
export class SpecialityComponent implements OnInit, OnDestroy {
  configOpenRightBar: string;

  filterOptions = [
    {
      index: 6,
      type: 'rangeDate',
      order: 7,
      value: '',
      dateValue: '',
      placeholder: 'Created From Date',
      default: '1/1/2000',

      searchQuery: '',
      querystring: 'FromDate',


    },
    {
      index: 6,
      type: 'rangeDate',
      order: 7,
      value: '',
      placeholder: 'Created To Date',
      default: '1/1/2090',

      searchQuery: '',
      querystring: 'ToDate',
      dateValue: '',
    },
    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Specialities/GetSpecialities',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Specialty',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'Specialtiesids'
    },
    /* {
      index: 1,
      type: 'range',
      order: 2,
      max: 1000,
      value1: '',
      value2: '',
      selected: [],
      placeholder: 'Number Of Distibutor',
      searchQuery: '',
      querystring1: 'from',
      querystring2: 'to'
    },
    {
      index: 2,
      type: 'range',
      order: 3,
      max: 1000,
      value1: '',
      value2: '',
      selected: [],
      placeholder: 'Number Of Manufacturer',
      searchQuery: '',
      querystring1: 'from',
      querystring2: 'to'
    } */
  ];
  dataSource: MatTableDataSource<any>;
  displayedColumns = [
    'Actions',
    'ID',
    'Name',
    'Alt',
    'MedicalLine',
    'numberoOfDistInt',
    'numberoOfDistExp',
    'numberOfDistributors',
    'numberOfManu'
  ];
  values: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageIndex = 1;
  length = 10;
  pageSize = 10;
  filteredString = '';
  onDestroy$ = new Subject<void>();

  toggleRightbar() {
    this.configOpenRightBar = this.configOpenRightBar === 'open' ? '' : 'open';
  }

  constructor(
    private http: HttpClient,
    private serv: SpecialityService,
    private rout: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loadSpec();
  }

  loadSpec() {
    this.serv.getSpec(this.pageIndex).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.values = res;
      this.renderDataSource(res);
      this.length = res[0].totalNumber;
    });
  }

  renderDataSource(data) {
    this.dataSource = new MatTableDataSource<any>(data);
  }

  view(id) {
    const dialogRef = this.dialog.open(MListComponent, {
      data: { id: id },
      width: '75%',
      height: '75%'
    });
  }

  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;

    this.serv.getSpec(this.pageIndex).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }

  handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });
    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });
    this.serv.FilterData(this.pageIndex, this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }

  Add() {
    const dialogRef = this.dialog.open(AddSpecComponent, {
      width: '65%',
      height: '65%'
    });
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        tap(() => {

        })
      )
      .subscribe(res => {
        if (res) {
          this.loadSpec();
        }
      });
  }

  update(row) {
    const dialogref = this.dialog.open(AddSpecComponent, {
      data: row,
      width: '65%',
      height: '65%'
    });
    dialogref
      .afterClosed()
      .pipe(
        takeUntil(this.onDestroy$),
        tap(() => {
 
        })
      )
      .subscribe(res => {
        if (res  ) {
          this.loadSpec();
        }
      });
  }
  
  translate(obj) {
    const dialogRef = this.dialog.open(TranslateComponent, {
      width: '85%',
      height: '70%',
      data: {
        key: obj.speciality, placeholders: ['Speciality Name Translation in',
          'None Descriptor Translation in'], title: obj.speciality, keyID: obj.id,
      }
    }); dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$),
      tap(() => {

      })
    )
    .subscribe(res => {
      if (res) {
        this.loadSpec();
      }
    });
}
  

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}

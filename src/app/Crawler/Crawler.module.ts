import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrawlerComponent } from './Crawler.component';
import { CrawlerRoutingModule } from './Crawler-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MatRadioModule } from '@angular/material';
import { CrawleractionsComponent } from './crawleractions/crawleractions.component';
import { CellHeightModule } from '../shared/cell-height/cell-height.module';

@NgModule({
  imports: [
    CommonModule,
    CrawlerRoutingModule,
    SharedModule,
    MatRadioModule,
    CellHeightModule
  ],
  declarations: [CrawlerComponent,CrawleractionsComponent],
  entryComponents : [CrawleractionsComponent] 
})
export class CrawlerModule { }

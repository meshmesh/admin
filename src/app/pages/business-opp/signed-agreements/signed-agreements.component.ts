import { Component, OnInit, ViewChild } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { SignedAgreement, SASearchParams } from '../business.models';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SignedAgreementsService } from '../signed-agreements.service';

@Component({
  selector: 'app-signed-agreements',
  templateUrl: './signed-agreements.component.html',
  styleUrls: ['./signed-agreements.component.scss']
})
export class SignedAgreementsComponent implements OnInit {


  @ViewChild('tableSA', { read: MatSort }) sortSA: MatSort;
  @ViewChild('tableSA', { read: MatPaginator }) paginatorSA: MatPaginator;

  pageIndexSA: number;
  pageSizeSA: number;
  //
  signedAgreementsSearchParams: SASearchParams = new SASearchParams();


  signedAgreementsDisplayedColumns: string[] = [
    "distributorName",
    "manufactureName",
    "signedDate",

  ];
  signedAgreementsDataSource;
  signedAgreementTotalItems: number;
  
  onDestroy$: ReplaySubject<void> = new ReplaySubject();

  constructor(private signedAgreementsService: SignedAgreementsService ) { }

  ngOnInit(): void { 
    this.getTableData();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }


  getTableData() {
    this.signedAgreementsService.getSignedAgreement(this.signedAgreementsSearchParams).subscribe((res) => {
      this.signedAgreementTotalItems = res.data.count;
      // set length for paginator
      this.renderSignedAgreementData(res.data.items);
    });
  }


  renderSignedAgreementData(data: Array<SignedAgreement>) {
    this.signedAgreementsDataSource = new MatTableDataSource<SignedAgreement>(data);
    this.signedAgreementsDataSource.paginator = this.paginatorSA;
    this.signedAgreementsDataSource.sort = this.sortSA;
  }

  pageChangeSA(event) {
    this.pageIndexSA = event.pageIndex + 1;
    this.pageSizeSA = event.pageSize;

    this.signedAgreementsSearchParams.PageSize = event.pageSize;
    this.signedAgreementsSearchParams.PageNumber = this.pageIndexSA;
    
    this.signedAgreementsService.getSignedAgreement(this.signedAgreementsSearchParams).subscribe((res) => {
      this.signedAgreementTotalItems = res.data.count;
      // set length for paginator
      this.renderSignedAgreementData(res.data.items);
    });
  }


  openBottomSheet(element) {
    console.log(element)
  }
}

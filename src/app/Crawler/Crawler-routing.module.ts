 import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrawlerComponent } from './Crawler.component';
const routes: Routes = [
  {
    path: '',
    component: CrawlerComponent,
    data: {
      title: 'Crawler',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrawlerRoutingModule {}
 
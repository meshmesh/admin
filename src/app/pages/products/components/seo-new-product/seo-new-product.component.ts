import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seo-new-product',
  templateUrl: './seo-new-product.component.html',
  styleUrls: ['./seo-new-product.component.scss']
})
export class SeoNewProductComponent implements OnInit {
  @Input() seoData;
  @Output() fromSeoToAddEdit = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log('SEO DATA: ', this.seoData);
  }
  handleSeo() {
    this.fromSeoToAddEdit.emit(this.seoData);
  }
}

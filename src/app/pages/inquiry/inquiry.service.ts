import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { InquirySearchParams, InquiryResponse, UpdateInquiry, AddEmailRequest } from "./inquiry.model";

@Injectable({
  providedIn: "root",
})
export class InquiryService {
  APIEndpoint = environment.ApiUrlV3;

  constructor(private http: HttpClient) {}

  getAll(data: InquirySearchParams, queryParams: string): Observable<InquiryResponse>   {
    let URL = this.APIEndpoint + `AdminInquiries/SearchInqiries?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;

    URL += data.Status ? `Status=${data.Status}&` : 'Status=1&';
    URL += queryParams; 
    URL = URL.lastIndexOf('&') === URL.length - 1 ? URL.slice(0, -1) : URL; 

    return this.http.get<InquiryResponse>(URL);
  }

  getScientificName(Id) {
    return this.http.get<any[]>(this.APIEndpoint + "AdminInquiries/ScientificName?Id=" + Id);
  }

  getMedicalLine(Id) {
    return this.http.get<any[]>(this.APIEndpoint + "AdminInquiries/MedicalLine?Id=" + Id);
  }

  getSpecialty(Id) {
    return this.http.get<any[]>(this.APIEndpoint + "AdminInquiries/Specialty?Id=" + Id);
  }

  editInquiries(data: UpdateInquiry) {
    return this.http.post(this.APIEndpoint + "AdminInquiries", data);
  }

  getMessages(InquirId) {
    return this.http.get(this.APIEndpoint + "AdminInquiries/Messagess?InquirId=" + InquirId);
  }

  // Manufactures
  getSciNamesForManu(id: number) {
    return this.http.get<any[]>(this.APIEndpoint + "Manufacturers/" + id + "/Scientificnames");
  }

  getMedicalLinesForManu(id: number) {
    return this.http.get<any[]>(this.APIEndpoint + "Manufacturers/" + id + "/medicalLines");
  }

  getSpecialitiesForManu(id: number) {
    return this.http.get<any[]>(this.APIEndpoint + "Manufacturers/" + id + "/specialities");
  }

  addEmail(data: AddEmailRequest) {
    return this.http.post<any[]>(this.APIEndpoint + "AdminInquiries/AddEmail", data);
  }

}

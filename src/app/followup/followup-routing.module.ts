import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FollowupComponent } from './followup.component';

const routes: Routes = [
  {
    path: '',
    component: FollowupComponent,
    data: {
      title: 'Follow Up',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FollowupRoutingModule {}

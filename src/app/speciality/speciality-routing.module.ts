import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpecialityComponent } from './speciality.component';

const routes: Routes = [
  {
    path: '',
    component: SpecialityComponent,

    data: {
      title: 'speciality',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialityRoutingModule { }

import { Guid } from "guid-typescript"

export class Specialiy {
    id: number
    speciality: string;
    MedicalLineIds: number[] = [];
    Keywords : any[] = [];
    iconImg: string;
    KeywordsID: number[] = [];
    isActive = true;
    createdAt: Date;
    updatedAt: Date;
    ParentId: Number;
}


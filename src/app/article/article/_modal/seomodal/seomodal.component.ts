import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-seomodal',
  templateUrl: './seomodal.component.html',
  styleUrls: ['./seomodal.component.scss']
})
export class SeomodalComponent implements OnInit {
  articleseo: any;
  generalSeo: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<SeomodalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }



  ngOnInit() {

    this.articleseo = this.data.data;

 
  }
  handleSeo() {

  }
  close(data) {
    this.dialogRef.close(data);
  }

  save(){
    this.close(this.articleseo);
  }
}

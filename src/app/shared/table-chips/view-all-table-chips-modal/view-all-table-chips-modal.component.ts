import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-all-table-chips-modal',
  template: `
    <mat-chip-list class="mat-chip-list-stacked" aria-orientation="vertical">
      <mat-chip *ngFor="let name of data" class="my-1" disabled="true">{{ name }}</mat-chip>
    </mat-chip-list>
  `
})
export class ViewAllTableChipsModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ViewAllTableChipsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string[]
  ) {}

  ngOnInit() {}
}

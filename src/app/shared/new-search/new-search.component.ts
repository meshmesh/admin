import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-new-search',
  templateUrl: './new-search.component.html',
  styleUrls: ['./new-search.component.scss']
})
export class NewSearchComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  constructor() {}

  debounceTime = environment.debounceTime;
  @Output() search = new EventEmitter<string>();

  searchInput: FormControl;

  ngOnInit() {
    this.searchInput = new FormControl('');
    this._search();
  }

  _search(): void {
    this.searchInput.valueChanges.pipe(debounceTime(this.debounceTime), takeUntil(this.onDestroy$)).subscribe((input: string) => {
      this.search.emit(input);
    });
  }
}

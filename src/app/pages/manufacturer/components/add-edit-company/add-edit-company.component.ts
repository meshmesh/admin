
import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';
import { ManufacturerService } from '../../services/manufacturer.service';

import { of, Observable, ReplaySubject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';
import { CompanyModel } from '../../model/manufacturer.model';

@Component({
  selector: 'app-add-edit-company.component',
  templateUrl: './add-edit-company.component.html',
  styleUrls: ['./add-edit-company.component.scss']
})
export class AddEditCompanyComponent implements OnInit, OnDestroy {

  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  onDestroy$ = new ReplaySubject<void>();

  basic_title: string = 'Add';
  removable = true;
  imagePreview = [];
  localImages: File[] = [];
  countries: any = [];
  countriesFilteration: Observable<any>;
  compForm: CompanyModel;
  selectedSpeciality: any[] = [];
  selectedSpecialityIds: any[] = [];
  annuals: any = [];

  employee: any[] = [];
  selectedMedicalName: any[] = [];
  selectedMedicalId: any[] = [];
  Type: any[] = [];
  Medicals: any[] = [];
  medicallineslist: any[] = [];
  specialities: any[] = [];
  filteredSpec: Observable<any>;
  imgurl: any = 'https://aumet-data.s3.amazonaws.com/company/logo';
  urlContains: any = 'https://aumet-data';

  Disabled = false;


  constructor(
    private serv: ManufacturerService,
    public dialogRef: MatDialogRef<AddEditCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: HttpClient,
    private toast: ToastService,
  ) { }

  

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  /*  imgurl: any = 'https://s3-us-west-2.amazonaws.com/company/avatar';
   testurl: any = 'https://aumet-data.s3.us-west-2.amazonaws.com/company/avatar/'; */
  ngOnInit() {

    this.compForm = new CompanyModel();

    this.serv.getCountry().pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.countries = res.data;
      this.countriesFilteration = of(this.countries);
    });

    if (this.data && this.data.id) {
      this.basic_title = 'Update';
      this.compForm.CompanyName = this.data.name;
      this.compForm.country = this.data.country;
      this.compForm.Description = this.data.description;
      this.compForm.WebsiteUrl = this.data.website;

      this.compForm.AumetComments = this.data.aumetComments;
      this.compForm.Logo = this.data.logo;
      if (this.compForm.Logo != null) {
        if (this.compForm.Logo.includes(this.urlContains)) {
          this.imagePreview[0] = this.compForm.Logo;
        } else {
          this.imagePreview[0] = this.imgurl + this.compForm.Logo;
        }
      }
    }
  }

  displayMedicalLine(medical) {
    return medical ? medical.name : undefined;
  }

  displayFnCounty(country?: any): string | undefined {
    return country ? country.name : undefined;
  }

  _filterCountry(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.country;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.countries.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }

  AddSelectedMedicals(event) {
    const value = event.value;
    let index = this.Medicals.map(function (x) { return x.id }).indexOf(event.option.value.id);
    if (index === -1) {
      this.selectedMedicalName.push(event.option.value)
      this.selectedMedicalId.push(event.option.value.id);
      this.searchInput.nativeElement.value = '';

    }
  }

  removeMedicalsPoints(i) {
    let temp = this.selectedMedicalName[i];
    let tempIds = this.selectedMedicalId[i];
    this.selectedMedicalName.splice(i, 1);
    this.selectedMedicalId.splice(i, 1);
  }

  handleBaseImageChange(e) {
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }

  handleImageError(i) {
    this.imagePreview.splice(i, 1);
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }

  handleBaseImgError() {
    this.imagePreview.pop();
  }

  uploadCompanyLogo(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'company/logo/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  close(data) {
    this.dialogRef.close(data);
  }

  async submit(companydata) {
    this.Disabled = true;
    if (this.data.id) {
      let formdata = {
          Id: this.data.id,
          Description: this.compForm.Description,
          Name: this.compForm.CompanyName,
          WebsiteUrl: this.compForm.WebsiteUrl,
          CountryId: this.compForm.country.id,
          Logo: ''
      }

      if (this.localImages.length > 0) {
        // update and get the new link

        const _thisProductImage = this;
        if (this.localImages[0] != null) {
          await this.uploadCompanyLogo(this.localImages[0], function (data) {
            _thisProductImage.compForm.Logo = data;
            formdata.Logo = data;
            _thisProductImage.updateFunction(formdata, _thisProductImage);
          }
          );
        } else {
          this.updateFunction(formdata, this);
        }
      } else {
        // upadte 
        this.updateFunction(formdata, this);
      }
    }
    else {
      let formdata = {
          "Description": this.compForm.Description,
          "Name": this.compForm.CompanyName,
          "WebsiteUrl": this.compForm.WebsiteUrl,
          "CountryId": this.compForm.country.id,
          "Logo": ''
        }

      if (this.localImages.length > 0) {
        const _thisUserImage = this;
        await this.uploadCompanyLogo(this.localImages[0], function (data) {
          _thisUserImage.compForm.Logo = data;
          formdata.Logo = data;
          _thisUserImage.addFunction(formdata, _thisUserImage);
        });
      } else {
        this.addFunction(formdata, this);
      }
    }
  }

  addFunction(form, ref) {
    this.Disabled = false;
    ref.http.post(environment.ApiUrlV3 + 'AdminManufacturer/', form).pipe(takeUntil(ref.onDestroy$)).subscribe(res => {
      setTimeout(function () {
        ref.Disabled = false;
        ref.toast.success('Added Succesfuly');
        ref.close(true);
      }, 500);
    });
  }

  updateFunction(form, ref) {
    this.Disabled = false;
    ref.http.put(environment.ApiUrlV3 + 'AdminManufacturer/', form).pipe(takeUntil(ref.onDestroy$)).subscribe(res => {
      setTimeout(function () {
        ref.Disabled = false;
        ref.toast.success('Updated Succesfuly');
        ref.close(true);
      }, 500);

    });
  }

  removeSpecialitiesPoints(i) {
    let temp = this.selectedSpeciality[i];
    let tempIds = this.selectedSpecialityIds[i];
    this.selectedSpeciality.splice(i, 1);
    this.selectedSpecialityIds.splice(i, 1);
  }

  _filterSpec(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialities.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }

  modelChanged(event) {
    // if(event == 'manufacturer'){
    //   this.getAnuualSales(15);
    // }
    // else{
    //   this.getAnuualSales(5);
    // }
  }

  AddSelectedSpecialitites(event) {
    this.selectedSpeciality.push(event.option.value);
    this.selectedSpecialityIds.push(event.option.value.id);
    let index = this.specialities.indexOf(event.option.value);
    if (index > -1) {
      this.specialities.splice(index, 1);
    }
  }
}
import { Injectable } from '@angular/core';
import { ToastrService, ComponentType } from 'ngx-toastr';

class ActionButton {
  text: string;
  action: Function; 
}


class NgToastConfig {
  /**
   * disable both timeOut and extendedTimeOut
   * default: false
   */
  disableTimeOut?: boolean;
  /**
   * toast time to live in milliseconds
   * default: 5000
   */
  timeOut?: number;
  /**
   * toast show close button
   * default: false
   */
  closeButton?: boolean;
  /**
   * time to close after a user hovers over toast
   * default: 1000
   */
  extendedTimeOut?: number;
  /**
   * show toast progress bar
   * default: false
   */
  progressBar?: boolean;
  /**
   * changes toast progress bar animation
   * default: decreasing
   */
  progressAnimation?: 'increasing' | 'decreasing';
  /**
   * render html in toast message (possibly unsafe)
   * default: false
   */
  enableHtml?: boolean;
  /**
   * css class on toast component
   * default: toast
   */
  toastClass?: string;
  /**
   * css class on toast container
   * default: toast-top-right
   */
  positionClass?: string;
  /**
   * css class on toast title
   * default: toast-title
   */
  titleClass?: string;
  /**
   * css class on toast message
   * default: toast-message
   */
  messageClass?: string;
  /**
   * animation easing on toast
   * default: ease-in
   */
  easing?: string;
  /**
   * animation ease time on toast
   * default: 300
   */
  easeTime?: string | number;
  /**
   * clicking on toast dismisses it
   * default: true
   */
  tapToDismiss?: boolean;
  /**
   * Angular toast component to be shown
   * default: Toast
   */
  toastComponent?: ComponentType<any>;
  /**
   * Helps show toast from a websocket or from event outside Angular
   * default: false
   */
  onActivateTick?: boolean;

  /**
   * 
   */
  buttons?: Array<ActionButton>;
}


@Injectable()
export class ToastService {

  constructor(private toastr: ToastrService) {}

  /**
   * @function info
   * To show toast message in case of info
   * @param text 
   * @param configs 
   */
  info(text: string, configs?: NgToastConfig) {
    this.toastr.info(text, '', configs);
  }

  /**
   * @function success
   * To show toast message in case of success
   * @param text 
   * @param configs 
   */
  success(text: string, configs?: NgToastConfig) {
    this.toastr.success(text, '', configs);
  }

  /**
   * @function warning
   * To show toast message in case of warning
   * @param text 
   * @param configs 
   */
  warning(text: string, configs?: NgToastConfig) {
    this.toastr.warning(text, '', configs);
  }

  /**
   * @function error
   * To show toast message in case of error
   * @param text 
   * @param configs 
   */
  error(text: string, configs?: NgToastConfig) {
    this.toastr.error(text, '', configs);
  }

}
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

const APIEndpoint = environment.ApiUrl;
@Injectable()

export class SearchService {
    constructor(private http: HttpClient) { }

    getMainCategory(pageType) {
        if (pageType === 'Article') {
            return this.http.get(APIEndpoint + '/api/AdminArticles/GetListOfCategories?MainOnly=true');
        }
    }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleTableComponent } from './articleTable.component';
const routes: Routes = [
  {
    path: '',
    component: ArticleTableComponent,
    data: {
      title: 'Articles1',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleTableRoutingModule {}

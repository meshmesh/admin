export class Catalogview {
    CatalogId: number;
    Name: string;
    Client: boolean;
    NumberOfProducts: number[];
    CreatedAtFrom: string;
    CreatedAtTo: string;
    CompanyIds: number[];
    CountryIds: number[];
    PageNumber: number;
    PageSize: number;
}

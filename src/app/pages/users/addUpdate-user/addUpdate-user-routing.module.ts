import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddUpdateUserComponent } from './addUpdate-user.component';

const routes: Routes = [
  {
    path: '',
    component: AddUpdateUserComponent,

    data: {
      title: 'Add/Update User',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddUpdateUserRoutingModule { }
 
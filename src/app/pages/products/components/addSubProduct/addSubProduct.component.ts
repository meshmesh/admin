import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MatDialogRef, MatTableDataSource, MatPaginator, MAT_DIALOG_DATA } from '@angular/material';
import { S3 } from 'aws-sdk/clients/all';
import { Guid } from 'guid-typescript';
import { SubProduct } from './SubProduct';
import { S3Service } from 'src/app/_services/s3.service';
import { ProductService } from '../../services/product.service';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-addSubProduct',
  templateUrl: './addSubProduct.component.html',
  styleUrls: ['./addSubProduct.component.scss']
})
export class AddSubProductComponent implements OnInit {
  seletedSellingPoints: any[] = [];
  removable = true;
  filteredSellingPoints: Observable<any>;
  sellingPoints: any[] = [];
  @ViewChild('sellingpointinput') sellingpointinput: ElementRef<HTMLInputElement>;
  localBaseImage: File[] = [];
  ProductToCreate: ProductToCreate;
  taskCount = 0;
  totalCount: number;
  needimage = false;
  baseImagePreview = [];
  localImages: File[] = [];
  imagePreview = [];
  dataSource: MatTableDataSource<any>;
  paginator: MatPaginator;
  displayedColumns: any[] = ['Name', 'subTitle'];
  length = 1
  pageSize = 10;
  attrib: any = [];
  subForm: SubProduct;
  seletedscientificNames: any[] = [];
  scientificNames: any[] = [];
  filteredscientificNames: Observable<any>;
  @ViewChild('scientificnameInput') scientificnameInput: ElementRef<HTMLInputElement>;
  specialitiesFilter: Observable<any>;
  specialities: any[] = [];
  Tags: any = [];

  constructor(
    public dialogRef: MatDialogRef<AddSubProductComponent>, 
    private serv: ProductService,
    public toast: ToastService, 
    private s3: S3Service,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.subForm = new SubProduct();
    this.ProductToCreate = new ProductToCreate();
    if (this.data.id != null || this.data.id != undefined) {
      this.serv.GetProductViewByID(this.data.id).subscribe((res: any) => {
        
        this.dataSource = new MatTableDataSource<any>(res);

      })
    }
    this.serv.getSellingPoints().subscribe((response: any) => {
      this.sellingPoints = response;
      this.filteredSellingPoints = of(this.sellingPoints);
    })

    this.serv.getSpeciality().subscribe(
      (response: any) => {
        this.specialities = response;
        this.specialitiesFilter = of(this.specialities);
      })

  }

  _filterscientificNamesOnSpeciality(value: any) {
    this.serv
      .getScientificNameSuggestion(
        `&SpecialityId=${this.subForm.speciality.id ? this.subForm.speciality.id : ''}&Name=`
      )
      .subscribe(res => {
        this.scientificNames = res;
        this.filteredscientificNames = of(this.scientificNames);
      });
  }



  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.speciality;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(this.specialities.filter(item => item.speciality.toLowerCase().indexOf(filterValue) === 0));
  }


  AddSelectedscientificNames(event) {
    this.seletedscientificNames.push(event.option.value);
    const index1 = this.seletedscientificNames.indexOf(event.option.value)
    const index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);
    }
    this.scientificnameInput.nativeElement.value = '';

    if (this.seletedscientificNames[index1].attributes.length > 0) {
      this.seletedscientificNames[index1].attributes.forEach(element => {
        this.attrib.push({ "att": element, "scie": event.option.value })
      });

    }
  }


  _filterscientificNames(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return this.serv.getScientificNameSuggestion(
      `&SpecialityId=${this.subForm.speciality.id ? this.subForm.speciality.id : ''}&Name=${filterValue}`
    );
  }

  removescientificNames(i) {
    const temp = this.seletedscientificNames[i];
    this.scientificNames.push(temp);
    this.seletedscientificNames.splice(i, 1);
  }

  removeSellingPoints(i) {
    const temp = this.seletedSellingPoints[i];
    this.sellingPoints.push(temp);
    this.seletedSellingPoints.splice(i, 1);
  }
  _filterSellingPoints(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(this.sellingPoints.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }


  AddSelectedSellingPoints(event) {
    this.seletedSellingPoints.push(event.option.value);
    const index = this.sellingPoints.indexOf(event.option.value);
    if (index > -1) {
      this.sellingPoints.splice(index, 1);
      this.sellingpointinput.nativeElement.value = '';
    }
  }


  handleBaseImageChange(e) {
    this.localBaseImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.baseImagePreview.pop();
        this.baseImagePreview.push(reader.result);
      };
    }
  }

  handleImageChange(e) {
    for (let i = 0; i < e.target.files.length; i++) {
      this.localImages.push(e.target.files[i]);
      if (e.target.files && e.target.files[i]) {
        const reader = new FileReader();
        reader.readAsDataURL(e.target.files[i]);
        reader.onload = e => {
          this.imagePreview.push({ url: reader.result });
        };
      }
    }
  }


  hideBaseImage() {
    this.baseImagePreview.pop();
    this.localBaseImage.pop();
  }


  handleBaseImgError() {
    this.baseImagePreview.pop();
  }

  hideimage(i) {
    this.imagePreview.splice(i, 1);
    this.localImages.splice(i, 1);
  }

  handleImageError(i) {
    this.imagePreview.splice(i, 1);
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }


  renderTable(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  pageChange(event) {
    this.length = event.pageIndex + 1;

    /* this.serv.getCOuntryPage(this.length,this.pageSize).subscribe((res: any[]) => {
      this.renderTable([res]);
    }); */
  }


  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }


  async uploadProductImage(image, imageLocation) {
    
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'product/images/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    await _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }



  async submit(form) {
    
    if (form.invalid) {

      if (this.baseImagePreview[0] == null || this.baseImagePreview[0] == undefined || this.baseImagePreview[0].length == 0) {
        this.needimage = true;
        return;
      }

      return;
    }
    if (this.baseImagePreview[0] == null || this.baseImagePreview[0] == undefined || this.baseImagePreview[0].length == 0) {
      this.needimage = true;
      return;
    }
    
    console.log(form, this.data)
    this.totalCount = this.localBaseImage.length + this.localImages.length;

    let obj: any = {};
    this.ProductToCreate.productTitle = form.value.Name;
    this.ProductToCreate.productSubtitle = form.value.subTitle;
    this.ProductToCreate.specialityId = form.value.speciality.id;
    this.seletedscientificNames.forEach(element => {
      this.ProductToCreate.ScientificIds.push(element.id);

    });
    this.seletedSellingPoints.forEach(element => {
      this.ProductToCreate.SellingToIds.push(element.id);

    });
    this.ProductToCreate.ParentProductID = this.data.id;
    let _thisProductImage = this;
    let taskCount = 0;

    await this.s3.uploadFile('product/catelogues/', this.localBaseImage[0]).subscribe(res => {
      if (res.percentage === 0 && res.data !== null && res.error === '') {
        taskCount++;
        this.ProductToCreate.baseImage = res.data.Location
        if (this.totalCount === taskCount) {
          this.addUpdateFunction(this.ProductToCreate);
        }
      };
    });

    if (this.imagePreview.length > 0) {
      this.localImages.forEach(async element => {
        await this.s3.uploadFile('product/catelogues/', element).subscribe(res => {

          if (res.percentage === 0 && res.data !== null && res.error === '') {
            taskCount++;

            this.ProductToCreate.Images.push(res.data.Location);

            if (this.totalCount === taskCount) {
              this.addUpdateFunction(this.ProductToCreate);
            }
          };
        });

      });

    }

    if (this.totalCount === taskCount) {
      this.addUpdateFunction(this.ProductToCreate);
    }

    console.log(this.ProductToCreate);

  }

  addUpdateFunction(data) {
    this.serv.addUpdateProduct(data).subscribe(
      (response: any) => {
        this.toast.success("Added Successfuly");
        this.dialogRef.close();
      },
      error => console.log('>>>', error)
    );
  }

}


class ProductToCreate {
  id: number;
  productTitle: string;
  productSubtitle: string;
  baseImage: string;
  productDescription: string;
  specialityId: number;
  SellingToIds: number[] = [];
  ScientificIds: number[] = [];
  Images: string[] = [];
  ParentProductID: number;
}
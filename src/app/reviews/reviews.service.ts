import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) { }

  getReviews(pagenumber, pagesize): Observable<any> {
    const url =
      this.APIEndpoint +
      '/api/AdminArticles/SearchAumetReview?pagesize=' +
      pagesize +
      '&pagenumber=' +
      pagenumber;

    return this.http.get<any>(url);
  }
  addUpdateReviews(obj) {
    return this.http.post(
      this.APIEndpoint + '/api/AdminArticles/addupdatereview',
      obj
    );
  }
  DeleteReview(id) {
    return this.http.get(
      this.APIEndpoint + '/api/AdminArticles/DeleteReview?id=' + id
    );
  }
  PublishReiew(id) {
    return this.http.post(
      this.APIEndpoint + '/api/AdminArticles/PublishReiew', id
    );
  }
  unPublishReiew(id) {
    return this.http.get(
      this.APIEndpoint + '/api/AdminArticles/unPublishReiew?Id=' + id
    );
  }
}

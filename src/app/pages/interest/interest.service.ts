import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterestService {

  APIEndpoint = environment.ApiUrlV3;
  constructor(private http: HttpClient) { }

  getInterest(Pagenumber, data = '', text = ''): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminInterest/Search?PageSize=10&PageNumber=' + Pagenumber + '&FullInput=' + text + data);
  }

  getInterest2(Pagenumber, data, text): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminInterest/Search?PageSize=10&PageNumber=' + Pagenumber + '&FullInput=' + text + data);
  }

  getInterestFilteration(Pagenumber, filterres): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminInterest/Search?PageSize=10&PageNumber=' + Pagenumber + '&' + filterres);
  }

  saveDistributorInterest(param) {
    return this.http.post(this.APIEndpoint + 'AdminDistributorInterest', param);
  }

  EditDistributorInterest(param) {
    return this.http.put(this.APIEndpoint + 'AdminDistributorInterest', param);
  }

  saveDistributorExperience(param) {
    return this.http.post(this.APIEndpoint + 'AdminDistributorExperience', param);
  }

  EditDistributorExperience(param) {
    return this.http.put(this.APIEndpoint + 'AdminDistributorExperience', param);
  }
  GetInterestById(param) {
    return this.http.get(this.APIEndpoint + 'DistributorIntreset/' + param);
  }

  getAdminbyID(param): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '' + param);
  }

  viewProfile(obj) {
    return this.http.post(this.APIEndpoint + 'Users/AddRequestLogin', obj);
  }

  getRegions() {
    return this.http.get(this.APIEndpoint + 'countries/getregions');
  }

  getCOmpanies(name, type): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'Companies/GetListOfCompaniesLimited?Name=' +
      name + '&Type=' + type);
  }


  getComp(param) {
    return this.http.get<any[]>(this.APIEndpoint + 'companies/GetCompanyView?id=' + param)
  }

  getComments(param) {
    return this.http.get(this.APIEndpoint + 'AdminInterest/GetComment?ObjectId=' + param + '&ObjectType=3');
  }

  getIgnoreComment(id, compid) {
    return this.http.get(this.APIEndpoint + 'AdminInterest/GetIgnoreComment?scientificnameid=' + id + '&DistributorId=' + compid);
  }

  saveComment(param) {
    return this.http.post(this.APIEndpoint + 'DistributorIntreset/Create', param);
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url =
      this.APIEndpoint +
      `AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url);
  }

  getMedicleLineSuggestion(): Observable<any> {
    const url =
      this.APIEndpoint +
      `MedicalLines?PageNumber=1&PageSize=100`;
    return this.http.get<any>(url);
  }

  getSpecialitySuggestion(medId): Observable<any> {
    const url =
      this.APIEndpoint +
      'Specialties/Search?ForAdmin=true&MedicalLineId=' + medId;
    return this.http.get<any>(url);
  }

  delInterest(param) {
    return this.http.delete(this.APIEndpoint + 'DistributorIntreset?Id=' + param);
  }
}

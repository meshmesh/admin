import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DynamicPageTableComponent } from './dynamicPageTable.component';
const routes: Routes = [
  {
    path: '',
    component: DynamicPageTableComponent,
    data: {
      title: 'Dynamic1',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicPageTableRoutingModule {}

/* import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MessageTemplate } from 'src/app/_services/chat.service';

@Component({
  selector: 'app-template-modal',
  templateUrl: './template-modal.component.html',
  styleUrls: ['./template-modal.component.scss']
})
export class TemplateModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<TemplateModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MessageTemplate
  ) {}

  input: FormControl;

  ngOnInit() {
    this.input = new FormControl('', Validators.required);
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  onCreate(): void {
    if (this.input.valid) {
      const message = this.createMessage();
      this.dialogRef.close(message);
    } else {
      this.input.markAsTouched();
    }
  }

  createMessage(): string {
    let inputValue = this.input.value;
    const datePipe = new DatePipe('en-US');
    switch (this.data.id) {
      case 1:
        return `Please send us a sample of ${inputValue}`;
      case 2:
        return `Please send the catalogue of ${inputValue}`;
      case 3:
        inputValue = datePipe.transform(inputValue, 'short');
        return `Can we please have a skype call on ${inputValue}`;
    }
  }
}
 */
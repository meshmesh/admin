import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchCellHeightDirective } from './cell-height.directive';
import { MatchCellHeightService } from './cell-height.service';



@NgModule({
  imports: [
    CommonModule,


  ],
  declarations: [MatchCellHeightDirective],
  exports: [MatchCellHeightDirective],
  providers: [MatchCellHeightService]


})
export class CellHeightModule { }


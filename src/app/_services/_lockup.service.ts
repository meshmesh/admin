import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
const APIEndpoint = environment.ApiUrl;
@Injectable({
  providedIn: 'root'
})
export class _lockupService {
  constructor(private http: HttpClient) {}

  getLockUps(majorCode: number) {
    const url = APIEndpoint + '/api/Lookups/Get?MajorCode=' + majorCode;
    return this.http.get<any>(url);
  }
}

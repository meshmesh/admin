import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator } from '@angular/material';
import { InterestService } from '../interest.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-ViewComment',
  templateUrl: './ViewComment.component.html',
  styleUrls: ['./ViewComment.component.scss']
})
export class ViewCommentComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(public dialogRef: MatDialogRef<ViewCommentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private serv: InterestService) { }
    displayedColumns=['id','AddedBy','Text']

    dataSource: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    values: any[]=[];
    comment: any[]=[];
    
  ngOnInit() {
     this.loadComments(); 
     this.LoadIgnoreComment();
     
     

  }

  close(data){
    this.dialogRef.close(data);
  }
  
  renderDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
  }

 

loadComments() {
  
    this.serv.getComments(this.data.row.id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values.push(response) ;
    
        this.renderDataSource(this.values);
      });
      
  }
  LoadIgnoreComment() {
    this.serv.getIgnoreComment(this.data.row.interestScientificName.id,this.data.row.company.id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
 
        this.comment = response;

      });
      
  }


}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterestComponent } from './interest.component';


const routes: Routes = [
  {
    path: '',
    component: InterestComponent,

    data: {
      title: 'viewInterest',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
]; 
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterestRoutingModule { }

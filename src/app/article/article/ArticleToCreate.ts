export class ArticleToCreate {

    Id: number;
    Title: string;
    Articleimage: string;
    Details: string;
    Description: string;
    Subimages: string[] = [];
    Subtitle: string;
    Seo: string;
    Tags: string[] = [];
    Slug: string;
    Orderid: number;
    numberofhits: number;
    Portals: number[] = []
    CategoryId: number;
    Categories: number[] = [];
    Published: boolean;
    subCategory: any = {};

}

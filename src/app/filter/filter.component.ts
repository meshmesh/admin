import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  AUTO_STYLE,
  animate,
} from "@angular/animations";
import * as moment from "moment";
import { Options } from "ng5-slider";
import { IFilterOption } from "./filters.model";

@Component({
  selector: "app-filter",
  templateUrl: "./filter.component.html",
  styleUrls: ["./filter.component.scss"],
  animations: [
    trigger("notificationBottom", [
      state(
        "an-off, void",
        style({
          overflow: "hidden",
          height: "0px",
        })
      ),
      state(
        "an-animate",
        style({
          overflow: "visible",
          height: AUTO_STYLE,
        })
      ),
      transition("an-off <=> an-animate", [animate("400ms ease-in-out")]),
    ]),
    trigger("slideInOut", [
      state(
        "in",
        style({
          width: "280px",
          // transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        "out",
        style({
          width: "0",
          // transform: 'translate3d(100%, 0, 0)'
        })
      ),
      transition("in => out", animate("400ms ease-in-out")),
      transition("out => in", animate("400ms ease-in-out")),
    ]),
    trigger("mobileHeaderNavRight", [
      state(
        "nav-off, void",
        style({
          overflow: "hidden",
          height: "0px",
        })
      ),
      state(
        "nav-on",
        style({
          height: AUTO_STYLE,
        })
      ),
      transition("nav-off <=> nav-on", [animate("400ms ease-in-out")]),
    ]),
    trigger("fadeInOutTranslate", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate("400ms ease-in-out", style({ opacity: 1 })),
      ]),
      transition(":leave", [
        style({ transform: "translate(0)" }),
        animate("400ms ease-in-out", style({ opacity: 0 })),
      ]),
    ]),
    trigger("mobileMenuTop", [
      state(
        "no-block, void",
        style({
          overflow: "hidden",
          height: "0px",
        })
      ),
      state(
        "yes-block",
        style({
          height: AUTO_STYLE,
        })
      ),
      transition("no-block <=> yes-block", [animate("400ms ease-in-out")]),
    ]),
  ],
})
export class FilterComponent implements OnInit {
  @Input() filterOptions: IFilterOption[] = [];
  @Output() dataFromFilters = new EventEmitter();
  public configOpenRightBar: string;
  public config: any;
  constructor() {}

  rangeSliderOptions: Options = {
    floor: 0,
    ceil: 100,
  };

  ngOnInit() {}

  dataChange(event, option) {
    debugger
    if (option.type === "toggel") {
      option.value = event
    }

    if (option.type === "Matchingtoggel") {
      option.value = !option.value;
    }

    if (option.type === "rangeDate") {

      if(option.dateValue) {
        let date = new Date(option.dateValue);
        let formatted_date = "";
        let month = "";
        let day = "";
        if (date.getDate() < 9) {
          day = "0" + date.getDate().toString();
        } else {
          day = date.getDate().toString();
        }
        if (date.getMonth() + 1 > 9) {
          month = (date.getMonth() + 1).toString();
        } else {
          month = "0" + (date.getMonth() + 1);
        }
        formatted_date = day + "/" + month + "/" + date.getFullYear();
        option.value = formatted_date;
      } else {
        option.value = '';
      }

    }

    if (option.type === "checkbox") {
      if (option.value == false) {
        option.value = "";
      }
    }

    if(option.type === 'select') {
      if (option.value === 'none') {
        option.value = "";
      }
    }



    if(option.value) {
      option.searchQuery = "&" + option.querystring + "=" + option.value;
    } else {
      option.searchQuery = '';
    }

   
  }


  toggleRightbar() {
    this.configOpenRightBar = this.configOpenRightBar === "open" ? "" : "open";
  }


  clearFilter() {
    console.log(this.filterOptions);

    for(let option of this.filterOptions) {


      if(option.type === 'multi-select') {
        option.chipsList = [];
        option.value = {};
        option.selected = [];
        option.list = [];
        option.searchQuery = '';
      }

      if(option.type === 'tags-select') {
        option.chipsList = [];
        option.value = '';
        option.selected = [];
        option.list = [];
        option.searchQuery = '';
      }

      if(option.type === 'rangeDate') {
        option.value = '';
        option.dateValue = '';
        // option.value = "";
        // option.dateValue = option.default;
      }

      option.searchQuery = '';
      option.value = '';
    }


    console.log("after", this.filterOptions);
    this.dataFromFilters.next(0);
    this.toggleRightbar()
  }

  ApplyFilter() {
    this.dataFromFilters.next({});
    this.toggleRightbar();
  }

  dataChangeAutoComplate(event) {
    this.dataFromFilters.next({});
  }

  valuechanges(event, option) {
    let _this = this;
    setTimeout(function () {
      if (option.type === "range") {
        option.searchQuery =
          "&" +
          option.querystring1 +
          "=" +
          option.rangeSliderMinValue +
          "&" +
          option.querystring2 +
          "=" +
          option.rangeSliderMaxValue;
        console.log(option.searchQuery);
      }
    }, 100);
  }


  ClearDate(event, option) {

    this.dataChange(event, option);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewsComponent } from './reviews.component';
import { ReviewsRoutingModule } from './reviews-routing.module';
import { ReviewsService } from './reviews.service';
import { SharedModule } from '../shared/shared.module';
import { QuillEditorModule } from 'ngx-quill-editor';
import { TagInputModule } from 'ngx-chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatDialogModule, MatSliderModule, MatTableModule } from '@angular/material';
import { AddReviewsComponent } from './addReviews/addReviews.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    QuillEditorModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    ReviewsRoutingModule,
    MatInputModule,
    MatDialogModule,
    MatSliderModule,
    MatTableModule
  ],
  providers: [ReviewsService],
  exports: [ReviewsComponent],
  declarations: [ReviewsComponent, AddReviewsComponent],
  entryComponents: [AddReviewsComponent]
})
export class ReviewsModule { }

export class IFilterOption {
  index: number;
  order: number;
  type?: string;
  output?: string;
  apiurl?: string;
  value?: any;
  hasPagination?: boolean;
  selected?: Array<any>
  placeholder?: string;
  chipsList?: Array<any>
  list?: Array<any>
  searchQuery: string;
  querystring: string;
  default?: string;
  dateValue?: string;
}
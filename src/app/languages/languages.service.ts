import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) { }

getLangugaesId(MajorCode){
 return this.http.get<any>(this.APIEndpoint + '/api/AdminLookups/get?MajorCode=' + MajorCode);
}

addUpdate(param){
  return this.http.post<any>(this.APIEndpoint + '/api/Languages/AddUpdate/', param);
}

getfullLanguage(){
  return this.http.get(this.APIEndpoint + '/api/Languages/');
}

delLanguage(id){
  return this.http.get(this.APIEndpoint + '/api/Languages/Delete?id='+ id);
}

generateFile(data){
  return this.http.post(this.APIEndpoint + '/api/Languages/SaveFile' , data);
}

}

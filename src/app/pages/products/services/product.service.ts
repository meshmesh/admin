import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/_services/auth.service';

const APIEndpoint = environment.ApiUrl;
const newAPIEndpoint = environment.ApiUrlV3;



export class ProductSearchParams {
  PageSize: number = 10;
  PageNumber: number = 1;
  Name: string;
  Id: number;
  sciNames: string;
}



@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient ,   private authenticationService: AuthService) {}

  GetScientificNames(str) {
    const url =
    newAPIEndpoint +
      `AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url);
  }

  addUpdateProduct(obj) {    
    const url = APIEndpoint + '/api/AdminProducts/AddUpdate';
    return this.http.post(url, obj, {
      headers: new HttpHeaders({
      'Authorization': 'Bearer '+ this.authenticationService.currentUserValue.jwt ,
      'Accept': 'application/json'
    })
   });
  }

  GetProductViewByID(id) {
    return this.http.get(APIEndpoint + '/api/AdminProducts/getsubproducts?id=' + id);
  }

  getall(pageNumber) {
    return this.http.get(newAPIEndpoint + 'AdminProduct?PageSize=10&PageNumber=' + pageNumber);
  }

  deleteCatalog(id) {
    const url = APIEndpoint + '/api/AdminCatalog/Delete?Id=' + id;
    return this.http.post(url, {});
  }

  updateSeo(obj) {
    const url = APIEndpoint + '/api/AdminProducts/Update';
    return this.http.post(url, obj);
  }

  SearchCatalog( pageNumber, data  , text) {
    return this.http.get(
      APIEndpoint +
        '/api/AdminCatalog/SearchCatalogs?PageSize=10&PageNumber=' +
        pageNumber +
        '&Name=' +
        text +
        data
    );
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url = APIEndpoint + `/api/AdminScientificNames/SearchScientificNames?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url);
  }

  getSpeciality() {
    const url = APIEndpoint + '/api/Specialities/GetSpecialities';
    return this.http.get(url);
  }
  
  getSellingPoints() {
    const url = APIEndpoint + '/api/Lookups/Get?MajorCode=2';
    return this.http.get(url);
  }


  getallCatalog(pageNumber) {
    return this.http.get(APIEndpoint + '/api/AdminCatalog/SearchCatalogs?PageSize=10&PageNumber=' + pageNumber);
  }

  searchCatalog(pageNumber, args) {
    const url = APIEndpoint + '/api/AdminCatalog/SearchCatalogs?PageSize=10&PageNumber=' + pageNumber + '&' + args;
    return this.http.get(url);
  }

  deleteProduct(id) {
    const url = newAPIEndpoint + 'AdminProduct?Id=' + id;
    return this.http.delete(url);
  }

  publishProduct(productId) {
    const url = newAPIEndpoint + 'AdminProduct/Puplish';
    return this.http.put(url, {productId: productId});
  }

  searchfilter(pageNumber, fiter) {
    const url =
      newAPIEndpoint +
      'AdminProduct?PageSize=10&PageNumber=' +
      pageNumber +
      fiter;
    return this.http.get(url);
  }

  getcatalogbyid(id) {
    const url = APIEndpoint + '/api/AdminCatalog/' + id;
    return this.http.get(url);
  }

  getProductById(id) {
    const url = newAPIEndpoint + 'AdminProduct/GetProductPopup?ProductId=' + id;
    return this.http.get(url);
  }

  getPortals() {
    return this.http.get(APIEndpoint + '/api/portals/getportals');
  }
}

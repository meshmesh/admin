import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './users.service';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { User } from './addUpdate-user/user';
import { AddUpdateUserComponent } from './addUpdate-user/addUpdate-user.component';
import { partition, catchError, takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [
    trigger('notificationBottom', [
      state(
        'an-off, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'an-animate',
        style({
          overflow: 'visible',
          height: AUTO_STYLE
        })
      ),
      transition('an-off <=> an-animate', [animate('400ms ease-in-out')])
    ]),
    trigger('slideInOut', [
      state(
        'in',
        style({
          width: '280px'
          // transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        'out',
        style({
          width: '0'
          // transform: 'translate3d(100%, 0, 0)'
        })
      ),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('mobileHeaderNavRight', [
      state(
        'nav-off, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'nav-on',
        style({
          height: AUTO_STYLE
        })
      ),
      transition('nav-off <=> nav-on', [animate('400ms ease-in-out')])
    ]),
    trigger('fadeInOutTranslate', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms ease-in-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ transform: 'translate(0)' }), animate('400ms ease-in-out', style({ opacity: 0 }))])
    ]),
    trigger('mobileMenuTop', [
      state(
        'no-block, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'yes-block',
        style({
          height: AUTO_STYLE
        })
      ),
      transition('no-block <=> yes-block', [animate('400ms ease-in-out')])
    ])
  ]
})
export class UsersComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  configOpenRightBar: string;
  filteredString = '';

  filterOptions = [

    {
      index: 5,
      type: 'multi-select',
      output: 'value',
      apiurl: '/api/Lookups/Get?MajorCode=8',
      order: 6,
      value: {},
      selected: [],
      placeholder: 'Stages',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'StageInput'
    },
    {
      index: 5,
      type: 'checkbox',
      order: 6,
      value: false,
      selected: [],
      placeholder: 'Verified ?',
      searchQuery: '',
      querystring: 'IsVerifiedInput'
    },
    {
      index: 5,
      type: 'checkbox',
      order: 6,
      value: false,
      selected: [],
      placeholder: 'With Company ?',
      searchQuery: '',
      querystring: 'WithCompany'
    },
    {
      index: 5,
      type: 'checkbox',
      order: 6,
      value: false,
      selected: [],
      placeholder: 'Active',
      searchQuery: '',
      querystring: 'ActiveInput'
    }
  ];

  toggleRightbar() {
    this.configOpenRightBar = this.configOpenRightBar === 'open' ? '' : 'open';
  }

  constructor(private http: HttpClient, private serv: UsersService, private rout: Router, public dialog: MatDialog) {}

  values: any[] = [];
  dataSource: MatTableDataSource<any>;
  SearchKey: string;

  displayedColumns = [
    'actions',
    'Id',
    'Name',
    'Country',
    'jobTitle',
    'Email',
    'Phone',
    'Type',
    'Company',
    'Stage',
    'IsAdmin',
    'IsVerified',
    'RegisterationDate',
  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  length = 10;
  pageIndex = 1;
  pageSize = 10;
  selected: any;
  userbyID: any;
  checked: boolean;
  
  typingTimer: any; //timer identifier
  doneTypingInterval = 500;
  searchKey = '';
  userForm: User;

  ngOnInit() {
    this.loadUsers();
  }

  handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });
    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });
    this.serv.usersFilter(this.pageIndex, this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }
  loadUsers() {
    this.serv.getUsers(this.pageIndex).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        this.renderDataSource(response);
        this.length = response[0].totalRows;
        /*         this.checked = response.isAdmin;s
         */
      },
      error => {
        console.log(error);
      }
    );
  }

  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;

    this.serv.usersFilter2(this.pageIndex,this.searchKey,this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataSource(res);
    });
  }

   loadusersdata(){
    
     this.serv.usersFilter2(this.pageIndex, this.searchKey , this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
       this.renderDataSource(res);
     })

   }
  navto(row) {
    this.rout.navigate([]).then(result => {
      window.open('/Setting/Users/add/' + row.id, '_blank');
    });
  }

  renderDataSource(data) {
    if (data.length < this.pageSize) {
      this.length = data.length;
    } else {
      this.length = data[0].totalRows;
    }
    this.dataSource = new MatTableDataSource<any>(data);
  }

  /*
  Filter(SearchKey) {
    this.dataSource.filter = this.SearchKey.trim().toLowerCase();
  } */

  keyUpapplyFilter(event) {
    this.searchKey = event;
    const _this = this;
    clearTimeout(this.typingTimer);
    window.this = _this;
    this.typingTimer = setTimeout(_this.filterData, _this.doneTypingInterval);
  }

  keyDownfilter(event) {
    clearTimeout(this.typingTimer);
  }

  filterData() {
    window.this.serv.getUsers(window.this.pageIndex, window.this.filtersRes, window.this.searchKey).pipe(takeUntil(window.this.onDestroy$)).subscribe(res => {
      window.this.renderDataSource(res);
    });
  }

  /*  setadmin(obj){

    if (this.checked = false){
      this.userForm.isCompanyAdmin = true;
    }
    else{
      this.userForm.isCompanyAdmin = false;
    }

this.serv.AddUpdateUser(obj).subscribe(res => {
  console.log(res)
  });
} */


  Del(id) {
    this.serv.deleteUser(id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.serv.usersFilter2(this.pageIndex, this.searchKey , this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
        this.renderDataSource(res);
      })
    });
  }
}
declare global {
  interface Window {
    this: any;
  }
}

import { IFilterOption } from "src/app/filter/filters.model";

export class AddEmailRequest {
  email: string;
  inquiryId: number
}

export enum StatusEnum {
  None = 1,
  EmailNeed = 2,
  WrongEnail = 3,
  Stopped = 4
}

export class ICountry {
  countryID: number;
  countryFlagPath: string;
  countryName: string;
}

export class InquiryUnit {
  distributorId: number;
  distributorName: string;
  distributorToken: string;
  id: number;
  lastAction: number; // last stage
  lastActionDate: string; // last stage date
  manufacturerId: number;
  manufacturerMedicalLineCount: number;
  manufacturerName: string;
  manufacturerScientificNameCount: number;
  manufacturerSpecialtyCount: number;
  manufacturerToken: string;
  medicalLineCount: number;
  nextAction: number; // next stage
  nextActionDate: string; // next stage date
  scientificNameCount: number;
  specialtyCount: number;
  status: number;
  statusDate: string;
  users: Array<any>;
  country: ICountry;
  createdAt: "2020-07-06T10:10:56.954413"
  inquiryMedicalLines: Array<string>;
  inquiryScientificNames: Array<string>;
  inquirySpecialities: Array<string>;
  lastActionText: string;
  manufacturerWebsite: string;
  nextActionText: string;
  reason: string;
  source: string;
}

export class InquirySearchParams {
  // required and default
  PageNumber: number = 1;
  PageSize: number = 10;

  Status: StatusEnum;
  ManfacturerName: string;
  DistributorName: string;
  Website: string;
  NextAction: string;
  LastAction: string;
  Source: string;
  CreatedFrom: string;
  CreatedTo: string;

  // TODO: ARRAYS to be set
  CountryIds: string;
  ScientificNameIds: string;
  SpecialtyIds: string;
  MedicalLineIds;
  ManufacturerScientificNameIds;
  ManufacturerSpecialtyIds;
  ManufacturerMedicalLineIds;

}

export class UpdateInquiry {
  id: number;
  status: number
}


export class InqData {

}

export class InquiryResponse {
  data: {
    count: number;
    items: Array<InquiryUnit>;
  };
  errorCode: number;
  status: boolean;
}


export const InquiryFilterOptions: Array<IFilterOption> = [
  {
    index: 0,
    order: 1,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/Countries',
    list: [],
    chipsList: [],
    value: {},
    placeholder: 'Countries',
    selected: [],
    searchQuery: '',
    querystring: 'CountryIds'
  },
  {
    index: 1,
    order: 2,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/Specialties/GetByName?Name=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Manufacturer specialties',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'ManufacturerSpecialtyIds'
  },
  {
    index: 2,
    order: 3,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/MedicalLines',
    value: {},
    selected: [],
    placeholder: 'Manufacturer medical lines',
    chipsList: [],
    list: [],
    querystring: 'ManufacturerMedicalLineIds',
    searchQuery: ''
  },
  {
    index: 3,
    order: 4,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminScientificNames/List?PageSize=10&PageNumber=1&Key=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Manufacturer scientific names',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'ManufacturerScientificNameIds'
  },
  {
    index: 4,
    order: 5,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminScientificNames/List?PageSize=10&PageNumber=1&Key=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Scientific name sent for',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'ScientificNameIds'
  },
  {
    index: 5,
    order: 6,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminCompany/CompanyList?Type=manufacturer&NameOrSlug=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Manfacturer Name',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'ManfacturerIds'
  },
  {
    index: 6,
    order: 7,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/AdminCompany/CompanyList?Type=distributor&NameOrSlug=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Distributor Name',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'DistributorIds'
  },
  {
    index: 7,
    order: 8,
    type: 'text',
    value: '',
    placeholder: 'Website',
    searchQuery: '',
    querystring: 'Website',
  },
  {
    index: 8,
    order: 9,
    type: 'select',
    list: [
      {
        name: 'In Progress',
        id: 1
      },
      {
        name: 'First Email',
        id: 2
      },
      {
        name: 'Seconed Email',
        id: 3
      },
      {
        name: 'Pipe Drive',
        id: 4
      }
    ],
    value: '',
    placeholder: 'Last stage',
    searchQuery: '',
    querystring: 'LastAction',
  },
  {
    index: 9,
    order: 10,
    type: 'select',
    list: [
      {
        name: 'In Progress',
        id: 1
      },
      {
        name: 'First Email',
        id: 2
      },
      {
        name: 'Seconed Email',
        id: 3
      },
      {
        name: 'Pipe Drive',
        id: 4
      }
    ],
    value: '',
    placeholder: 'Next stage',
    searchQuery: '',
    querystring: 'NextAction',
  },
  {
    index: 10,
    order: 11,
    type: 'rangeDate',
    value: [],
    placeholder: 'Sent Date From',
    default: '1/1/2000',
    searchQuery: '',
    querystring: 'CreatedFrom',
  },
  {
    index: 11,
    order: 12,
    type: 'rangeDate',
    value: [],
    placeholder: 'Sent Date to',
    default: '1/1/2090',
    searchQuery: '',
    querystring: 'CreatedTo',
  },

  {
    index: 12,
    order: 13,
    type: 'text',
    value: '',
    placeholder: 'Person Name',
    searchQuery: '',
    querystring: 'UserName',
  },
  {
    index: 13,
    order: 14,
    type: 'text',
    value: '',
    placeholder: 'Person Email',
    searchQuery: '',
    querystring: 'UserEmail',
  },
];
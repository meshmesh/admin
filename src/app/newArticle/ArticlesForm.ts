export class ArticlesForm {
    Id:any;
    Title: string;
    Articleimage: any;
    Details: string;
    Description: string;
    Subimages: any[] = [];
    Subtitle: string;
    Seo: string;
    Sort: number;
    RelatedArtical: any[] = [];
    Slug: string;
    Orderid: number;
    numberofhits: number;
    Portals: any[] = [];
    Type: number
    Feature: boolean;
    ContentType: number;
    URL: string;
    Published: boolean;
    maincategory: number;
    Tag : string;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  APIEndpoint = environment.ApiUrlV3;

  constructor(private http: HttpClient) {}

  getToken(key): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'Auth/MetaBase');
      // return this.http.post<any[]>("http://localhost:5000/api/AuthAPI/AumetDashboardToken", {"SECRET_KEY":key});
  }
}

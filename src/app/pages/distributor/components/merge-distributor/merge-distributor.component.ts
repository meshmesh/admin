
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { DistributorService } from '../../services/distributor.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-merge-distributor',
  templateUrl: './merge-distributor.component.html',
  styleUrls: ['./merge-distributor.component.scss']
})
export class MergeDistributorComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private toast: ToastService, private serv: DistributorService, public dialogRef: MatDialogRef<MergeDistributorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ofCompany: any;
  company1: string = this.data.comp.name;
  company2: any;
  submittedObj: any = {
    CompanyID: this.data.comp.id,
    OldCompanyId: null
  }
  ngOnInit() {

  }



  handleTextChanging(event) {
    this.renderCompany(event);
  }


  renderCompany(searchvalue: any) {
    if (searchvalue.name === undefined) {
      this.serv.getCOmpanies(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
        this.ofCompany = res.data.items;
        this.displayCompany(res.data.items);
      });
    }
  }

  displayCompany(company) {
    return company ? company.name : undefined;
  }


  close(data): void {
    this.dialogRef.close(data);
  }

  async submit(obj) {
    let data = {
      SourceDistId : this.submittedObj.OldCompanyId,
      DestDistId: this.submittedObj.CompanyID
    }
    // this.submittedObj.OldCompanyId = obj.value.company2.id;
    this.serv.mergCompanies(data).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.dialogRef.close(true);
      this.toast.success("Merged Successfuly")
    })
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DynamicComponent } from './dynamic.component';
import { DynamictableComponent } from './dynamictable/dynamictable/dynamictable.component';

const router: Routes = [
    {
        path: '',
        component: DynamicComponent,
        data: {
            title: 'Dynamic',
            icon: 'icon-layout-cta-right',
            caption: 'my landing',
            status: true
        }
    },
    
        {
            path: 'table',
            component: DynamictableComponent,
            data: {
                title: 'Dynamictable',
                icon: 'icon-layout-cta-right',
                caption: 'my landing',
                status: true
            }
          }
    
];
@NgModule({
    imports: [RouterModule.forChild(router)],
    exports: [RouterModule]
})

export class DynamicRoutingModule { }

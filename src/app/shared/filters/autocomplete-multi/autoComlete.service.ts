import { Injectable, OnDestroy } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class AutoCompleteService implements OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  stringList: string[] = [];
  idList: number[] = [];

  constructor(private http: HttpClient) {}

  getScientificNameSuggestion(str) {
    const url =
      APIEndpoint +
      '/api/AdminScientificNames/SearchScientificNamesAdmin?PageSize=10&PageNumber=1&Name=';
    return this.http.get(url + str);
  }

  getSpesialitySuggestion() {
    const url = APIEndpoint + '/api/Specialities/GetSpecialities';
    this.http.get<any>(url).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        for (let i = 0; i < response.length; i++) {
          this.stringList.push(response[i].speciality);
          this.idList.push(response[i].id);
        }
      },
      error => console.log(error)
    );
  }

  getSpesialityDll() {
    const url = APIEndpoint + '/api/Specialities/GetSpecialities';
    return this.http.get<any>(url);
  }

  getMedicalLineSuggestion() {
    const url = APIEndpoint + '/api/MedicalLines/GetMedicalLines';
    this.http.get<any>(url).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        for (let i = 0; i < response.length; i++) {
          this.stringList.push(response[i].name);
          this.idList.push(response[i].id);
        }
      },
      error => console.log(error)
    );
  }

  getMedicalLineDll() {
    const url = APIEndpoint + '/api/MedicalLines/GetMedicalLines';
    return this.http.get<any>(url);
  }
  getCountrySuggestion() {
    const url = APIEndpoint + '/api/Countries/GetCountries';
    this.http.get(url).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        for (let i = 0; i < response.length; i++) {
          this.stringList.push(response[i].country);
          this.idList.push(response[i].id);
        }
      },
      error => console.log(error)
    );
  }
  getCountryDll() {
    const url = APIEndpoint + '/api/Countries/GetCountries';
    return this.http.get<any>(url);
  }
  getNumberOfEmployees() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=6';
    this.http.get(url).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        for (let i = 0; i < response.length; i++) {
          this.stringList.push(response[i].name);
          this.idList.push(response[i].name);
        }
      },
      error => console.log(error)
    );
  }

  getNumberOfEmployeesDll() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=6';
   return this.http.get<any>(url);
  }

  getCompanySuggestion(str) {
    const url =
      APIEndpoint + '/api/Companies/GetListOfCompaniesLimited?Name=' + str;
    return this.http.get(url);
  }

  getDistributers(str) {
    const url =
      APIEndpoint +
      '/api/Companies/GetListOfCompaniesLimited?Name=' +
      str +
      '&Type=distributor';
    return this.http.get(url);
  }

  getManufacturer(str) {
    const url =
      APIEndpoint +
      '/api/Companies/GetListOfCompaniesLimited?Name=' +
      str +
      '&Type=manufacturer';
    return this.http.get(url);
  }

  getSoldTo() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=3';
    return this.http.get(url);
  }

  getSellingPoints() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=2';
    return this.http.get(url);
  }

  getCompanys(str) {
    const url =
      APIEndpoint +
      '/api/Companies/GetCompanies?CompanyName=' +
      str +
      '&PageSize=10&PageNumber=1';
    return this.http.get(url);
  }

  getAnnualSalesDist() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=5';
    return this.http.get(url);
  }

  getAnnualSalesManu() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=15';
    return this.http.get(url);
  }

  getManufactureStage() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=7';
    return this.http.get(url);
  }

  getDistributerStage() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=8';
    return this.http.get(url);
  }

  getSatisfaction() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=13';
    return this.http.get(url);
  }

  getDistributerSupervisor() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=11';
    return this.http.get(url);
  }

  getManufacturerSupervisor() {
    const url = APIEndpoint + '/api/Lookups/GetAdmin?MajorCode=10';
    return this.http.get(url);
  }
}

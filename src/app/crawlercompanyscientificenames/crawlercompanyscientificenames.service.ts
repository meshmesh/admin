import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const APIEndpoint = environment.ApiUrl;

@Injectable(
  {
    providedIn: 'root'
  }
)
export class CrawlerCompanyScientificeNamesService {
  constructor(private http: HttpClient) {}

  GetCompanyScientificnames(pageNumber, pageSize,param) {
    return this.http.get(APIEndpoint + '/api/AdminManufacturers/CrawlerScientificNames?pagenumber=' + pageNumber + '&pagesize=' + pageSize +'&'+ param);
  }
  DeleteScientificname(data) {
    return this.http.post(APIEndpoint + '/api/AdminManufacturers/DeleteAdminScientificname', data);
  }

}

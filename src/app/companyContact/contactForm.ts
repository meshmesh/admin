export class ContactForm {
    FirstName: any
    LastName: any
    PhoneNumber: any
    CountryId: number
    Email: any
    CompanyId: number
    Note: any
    JobTitle: any
    Id: number
    UserID: number
    Invite: boolean
    CurrentStage: number
    NextStage: number
    CreatedAt: Date
    DeletedAt: Date
}

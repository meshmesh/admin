import { Component, OnInit, OnDestroy } from '@angular/core';
import { FollowUpService } from '../_services/followup.service';
import { Observable, ReplaySubject } from 'rxjs';
import { FollowUp, User, Action, Company } from '../_model/followup';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { tap, takeUntil } from 'rxjs/operators';
import { UserInfoDialogComponent } from './user-info-dialog/user-info-dialog.component';
import { ActionsLogModalComponent } from './actions-log-modal/actions-log-modal.component';
import { followUpFilters } from './followup.filters';

@Component({
  selector: 'app-followup',
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.scss']
})
export class FollowupComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  constructor(private followUpService: FollowUpService, private dialog: MatDialog) {}

  followUp$: Observable<FollowUp[]>;
  dataSource: MatTableDataSource<FollowUp>;

  filters = followUpFilters;
  displayedColumns: string[] = [
    'Actions',
    'CompanyType',
    'CompanyName',
    'User',
    'Country',
    'Stage',
    'Process',
    'LastActionName',
    'LastActionDate',
    'NextActionName',
    'NextActionDate',
    'AssignedAdmin'
  ];

  ngOnInit() {
    this.getFollowUps();
    this.renderData();
  }

  getFollowUps(): void {
    this.followUp$ = this.followUpService.getFollowUp().pipe();
  }

  renderData(): void {
    this.followUp$
      .pipe(
        tap(data => (this.dataSource = new MatTableDataSource(data))),
        takeUntil(this.onDestroy$)
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  getCountryFlag(countryCode: string): string {
    return '../../../../assets/images/flags24/' + countryCode.toUpperCase() + '.png';
  }

  viewUser(user: User): void {
    const userDialog = this.dialog.open(UserInfoDialogComponent, {
      data: user
    });
  }

  viewLog(company: Company): void {
    const logDialog = this.dialog.open(ActionsLogModalComponent, {
      data: company,
      minWidth: '1350px',
      minHeight: '350px'
    });
  }
}

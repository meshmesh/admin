import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor() {}

  getNotifications(): Observable<Notification[]> {
    return of([]);
  }
}

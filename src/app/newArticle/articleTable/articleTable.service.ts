import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class ArticleTableService {
  constructor(private http: HttpClient) { }

  search(args, pageNumber) {
    let url = APIEndpoint + '/api/AdminArticles/SearchArticals?PageSize=10&PageNumber=' + pageNumber;
    for (let i = 0; i < args.length; i = i + 2) {
      url = url + '&' + args[i] + '=' + args[i + 1];
    }
    return this.http.get<any>(url);
  }

  searchArticles(pageNumber) {
    return this.http.get(APIEndpoint + '/api/AdminArticles/SearchArticals?PageSize=10&PageNumber=' + pageNumber);
  }
  SearchCategory(categoryid) {
    return this.http.get(APIEndpoint + '/api/AdminArticles/SearchArticals?PageSize=100&PageNumber=1&Category='+categoryid );
  }


  getFilteredArticles(args: {}): Observable<any> {
    let url: string = APIEndpoint + '/api/AdminArticles/SearchArticals';
    Object.keys(args).forEach((key, i) => {
      if (i === 0) {
        url = url + '?' + key.toString() + '=' + args[key];
      } else {
        url = url + '&' + key.toString() + '=' + args[key];
      }
    });
    return this.http.get<any>(url);
  }


  puplishArticle(id) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/Publish?Id=' + id, {});
  }

  UnpuplishArticle(id) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/UnPublish?Id=' + id, {});
  }

  deleteArticle(id) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/Delete?Id=' + id, {});
  }

  getArticlesById(id): Observable<any> {
    const url = APIEndpoint + '/api/AdminArticles/' + id;
    return this.http.get<any>(url);
  }

  getMainCategory() {
    return this.http.get<any>(APIEndpoint + '/api/AdminArticles/GetListOfCategories?MainOnly=true');
  }

  getRelatedArticles(id) {
    return this.http.get<any>(APIEndpoint + '/api/AdminArticles/GetRelatedArticals?Id=' + id);
  }

  getSubCategory(id) {
    const url = APIEndpoint + '/api/AdminArticles/GetListOfCategories?MainCategory=' + id;
    return this.http.get<any>(url);
  }

  getPortal() {
    return this.http.get<any>(APIEndpoint + '/api/portals/getportals');
  }

  createUpdateArticle(obj) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/AddUpdate', obj);
  }
  UpdateSEO(obj) {
    return this.http.post(APIEndpoint + '/api/AdminArticles/Update', obj);
  }

  getPortals() {
    return this.http.get(APIEndpoint + '/api/portals/getportals');
  }

  getArticleById(id) {
    return this.http.get(APIEndpoint + '/api/AdminArticles/' + id);
  }


}

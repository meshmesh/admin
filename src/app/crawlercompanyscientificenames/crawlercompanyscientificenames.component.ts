import { Component, OnInit } from '@angular/core';
import { AdminComponent } from '../layout/admin/admin.component';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { CrawlerCompanyScientificeNamesService } from './crawlercompanyscientificenames.service';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastService } from '../_services/toast.service';

@Component({
  selector: 'app-crawlercompanyscientificenames',
  templateUrl: './crawlercompanyscientificenames.component.html',
  styleUrls: ['./crawlercompanyscientificenames.component.scss']
})
export class CrawlercompanyscientificenamesComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  paginator: MatPaginator;
  displayedColumns: any[] = ['select', 'ScientificName', 'Company', 'Actions'];

  filterOptions = [
    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/Specialities/GetSpecialities',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Specialty',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'SpecialtyIds'
    },
    {
      index: 2,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/MedicalLines/GetMedicalLines',
      order: 3,
      value: {},
      selected: [],
      placeholder: 'Medical Line',
      chipsList: [],
      list: [],
      querystring: 'FI_MedicalLineIds',
      searchQuery: ''
    },
    {
      index: 3,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/AdminScientificNames/SearchScientificNames?PageSize=10&PageNumber=1&Name=',
      order: 4,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Scientific Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'ScientificIds'
    },
    {
      index: 4,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/adminManufacturers/searchfilter?PageNumber=1&PageSize=10&companyType=manufacturer&companyName=',
      order: 1,
      hasPagination: true,
      value: {},
      selected: [],
      placeholder: 'Manufacturer Name',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'companyIds',
    }
  ];
  filteredString = '';
  pagenumber = 1;
  pagesize = 10;
  selection = new SelectionModel<Element>(true, []);

  length: number;
  data: any;

  constructor(
    private adminComponent: AdminComponent, 
    private toast: ToastService,
    public serv: CrawlerCompanyScientificeNamesService) { }

  ngOnInit() {
    // setTimeout(() => {
    //   this.adminComponent.verticalNavType = 'expanded';
    //   this.adminComponent.toggleOpened('');
    // });
    this.loaddata(this.pagenumber, this.pagesize, '');

  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.data.forEach(row => this.selection.select(row));
  }

  deleted($event) {
    //  delete (this.dataSource.data.forEach(row => this.selection.select(row));)
  }

  removeSelectedRows() {
    console.log(this.selection);
    let counter = 0;
    this.selection.selected.forEach(element => {

      counter++
      this.serv.DeleteScientificname(element.id).subscribe(res => {


        
        if (counter == this.selection.selected.length) {

          this.toast.success("Deleted Successfully ");
          this.loaddata(this.pagenumber, this.pagesize, this.filteredString);

        }
      })

    });
  }




  loaddata(pagenumber, pagesize, param) {
    this.serv.GetCompanyScientificnames(pagenumber, pagesize, param).subscribe((res: any) => {
      this.dataSource = res;
      this.data = res;
      if(res.length == 0) this.length = 0; 
      else this.length = res[0].totalnumber;
      
    });
  }
  handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });
    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });

    this.loaddata(this.pagenumber, this.pagesize, this.filteredString);
  }
  Delete(row) {
    this.serv.DeleteScientificname(row.id).subscribe(res => {
      this.loaddata(this.pagenumber, this.pagesize, this.filteredString);

    })
  }

  pageChange(event) {
    this.pagenumber = event.pageIndex + 1;

    this.loaddata(this.pagenumber, this.pagesize, this.filteredString);

  }
}

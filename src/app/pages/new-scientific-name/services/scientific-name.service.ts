import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from 'src/environments/environment';
import { IParentSpeciality, ISpeciality, SciNameListV2Response, SciNamesSearchParams, SciNamesSearchParamsV2 } from '../model/scientific-name.model';
import { concatMap, pluck } from 'rxjs/operators';
import { SciNameUpdateRequest } from '../model/add-scientific-name.model';

const APIEndpoint = environment.ApiUrlV3;


const extract = (obj, ...keys) => {
  const newObject = Object.assign({});
  Object.keys(obj).forEach((key) => {
     if(keys.includes(key)){
        newObject[key] = obj[key];
        delete obj[key];
     };
  });
  return newObject;
};


@Injectable({
  providedIn: 'root'
})
export class ScientificNameService {
  constructor(private http: HttpClient) {}

  getall(data: SciNamesSearchParams, queryParams: string) {
    let URL = APIEndpoint + `AdminScientificNames/Search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;
    URL += queryParams;
    // append data search params 
    URL += data.Name ? `&Name=${data.Name}` : '';
    URL += data.Id ? `&Id=${data.Id}` : '';
    return this.http.get(URL);
  }
  

  getallV2(data: SciNamesSearchParamsV2, queryParams: string): Observable<any> {
    let URL = APIEndpoint + `AdminScientificNames/SearchV2?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;
    URL += queryParams;
    // append data search params 

    if(data.Names && data.Names.length) {
      data.Names.forEach((element: string) => {
        URL += `&Names=${element}`;
      });
    }


    if(data.Ids && data.Ids.length) {
      data.Ids.forEach((element: number) => {
        URL += `&Ids=${element}`;
      });
    }



    URL += data.SortType ? `&SortType=${data.SortType}` : '';
    URL += data.SortBy ? `&SortBy=${data.SortBy}` : '';

    return this.http.get(URL);
  }

    /**
     * .pipe(

      concatMap((res: SciNameListV2Response) => {
        
        if(res.data && res.data.items.length) {
          for(let trip of res.data.items) {
              
            trip.mainParentSpeciality =  trip.mainSpeciality.map((a: ISpeciality) => ({
              id: a.parentID, 
              name: a.parentName,
            })) || [];
            trip.mainChildSpeciality = trip.mainSpeciality.map((a: ISpeciality) => ({
              id: a.id, 
              name: a.name,
            })) || [];

            // trip.subPecialityController.push(
            //   {
            //     parent : 
            //   }
            // )

            trip.parentSubSpeciality = this.getUnique(trip.specialities.filter((a) => a.subSpecialty).map((a) => ({id: a.parentID, name: a.parentName})), 'id');
            trip.childSubSpeciality = trip.specialities.filter((a) => a.subSpecialty).map((a) => ({id: a.id, name: a.name}));
            
          }
        }
        return of(res);
      })
    );
     */
    



  private getUnique(arr, comp) {

    // store the comparison  values in array
    const unique =  arr.map(e => e[comp])

      // store the indexes of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the false indexes & return unique objects
    .filter((e) => arr[e]).map(e => arr[e]);

    return unique;
  }

  add(data: SciNameUpdateRequest) {
    let URL = APIEndpoint + `AdminScientificNames`;
    return this.http.post(URL, data);
  }

  update(data: SciNameUpdateRequest) {
    let URL = APIEndpoint + `AdminScientificNames/Update`;
    return this.http.post(URL, data);
  }


  
  searchSpeciality(medicalLineId: number = null, isParent: string = '', text: string = '', isChild: string = '', withSciname: string = ''): Observable<IParentSpeciality[]> {
    let URL = APIEndpoint + 'Specialties/Search?'; // ForAdmin=true
    URL += isParent ? `&ParentOnly=${isParent}` : '';
    URL += isChild ? `&Childonly=${isChild}` : '';
    URL += withSciname ? `&HasScientificOnly=${withSciname}` : '';
    URL += medicalLineId ? `&MedicalLineId=${medicalLineId}` : '';
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));
  }

  getMedicleLineSuggestion(): Observable<any> {
    const url =
      APIEndpoint +
      `MedicalLines?PageNumber=1&PageSize=100`;
    return this.http.get<any>(url);
  }

  getSubSpecialitySugg(parentId: number, text: string = '') {
    let URL = APIEndpoint + 'Specialties/GetChild?'  ;
    URL += parentId ? `&SpecialtyId=${parentId}` : '';
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));;

  }
  getSubSpecialitySuggestion(parentId: number): Observable<any> {
    let URL = APIEndpoint + 'Specialties/GetChild?'  ;
    URL += parentId ? `&SpecialtyId=${parentId}` : '';
    return this.http.get<any>(URL);
  }

  getManuInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=1';
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/Manufacturers");
  }

  getDistInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=6';
    
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/Distributors");
  }

  getProdInScientificname(ObjectId) {
    // const url = APIEndpointOld + '/api/AdminProducts/GetProductByScientificName?ScientificId=' + ObjectId;
    return this.http.get(APIEndpoint + "AdminScientificNames/"+ ObjectId +"/products");
  }

  deleteScientificName(id) {
    console.log(id);
    return this.http.post(APIEndpoint + 'AdminScientificNames/Delete?Id=' + id, {});
  }

  approveScientificName(id: number): Observable<any> {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Approve?Id=' + id + '&Approved='+ true, {});
  }

  UnapproveScientificName(id: number): Observable<any> {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Approve?Id=' + id + '&Approved='+ false, {});
    // return this.http.post(APIEndpoint + 'AdminScientificNames/UnApprove?Id=' + id, {});
  }


  approveScientificNameGeneral(id: number, status: boolean): Observable<any> {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Approve?Id=' + id + '&Approved='+ status, {});
  }

  checkScientificName(id, status: boolean) {
    let data = {
      "scientificNameId": id,
      "check": status
    }
    return this.http.post(APIEndpoint + 'AdminScientificNames/Check', data);
  }

  getAdminScientificNameSuggestion(str: string = ''): Observable<any> {
    const url =
      APIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url).pipe(pluck('data', 'items'));
  }



  getScientificNameSuggestion(key: string = ''): Observable<any> {
    const url =
      APIEndpoint +
      `ScientificNames/SearchElsatic?Pagesize=10&PageNumber=1&Key=${key}`;
    return this.http.get(url).pipe(pluck('data'));
  }

  getMedicalSugg(text: string = ''): Observable<any> {
    let URL = APIEndpoint + `MedicalLines?PageNumber=1&PageSize=10`;
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));
  }


  getAllScientificName(name = ''): Observable<any> {
    let URL =  environment.ApiUrlV3 + 'AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=' + name ;
    return this.http.get<any>(URL).pipe(pluck('data', 'items'));
  }

   mergScientific(originalId, alternateId) {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Merge', {
      'new': alternateId,
      'old': originalId
    });
  }

  
}

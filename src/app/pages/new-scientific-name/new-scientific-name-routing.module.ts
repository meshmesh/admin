import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewScientificNameComponent } from './new-scientific-name.component';

const routes: Routes = [
  {
    path: '',
    component: NewScientificNameComponent,
    data: {
      title: 'Scientific Name',
      icon: 'icon-layout-cta-right',
      caption: 'Scientific Name Page',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewScientificNameRoutingModule { }

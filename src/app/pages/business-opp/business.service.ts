import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { BusinessOppSearchParams, BusinessOppResponse, BusinessOpp } from './business.models';
import { pluck } from 'rxjs/operators';
import { AuthService } from 'src/app/_services/auth.service';


export class IIUser {
  constructor(public id: number, public name: string) {}
}

@Injectable()
export class BusinessService {

  APIEndpoint = environment.ApiUrlV3 + 'AdminDistributorOpportunities';

  constructor(private http: HttpClient, private authenticationService: AuthService) {}


  /**
   * @function getBusinessOpp
   * @param {BusinessOppSearchParams} data 
   */
  getBusinessOpp(data: BusinessOppSearchParams): Observable<BusinessOppResponse> {
    const currentUser = this.authenticationService.currentUserValue;
    const myHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.jwt });
    let URL = this.APIEndpoint + `/Search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}`;

    // Optional search params
    // URL += data.Status ? `&Status=${data.Status}` : '&Status=1';
    // URL += data.ManfacturerName ? `&ManfacturerName=${data.ManfacturerName}` : '';
    // URL += data.DistributorName ? `&DistributorName=${data.DistributorName}` : '';
    // URL += data.Website ? `&Website=${data.Website}` : '';
    // URL += data.NextAction ? `&NextAction=${data.NextAction}` : '';
    // URL += data.LastAction ? `&LastAction=${data.LastAction}` : '';
    // URL += data.Source ? `&Source=${data.Source}` : '';
    // URL += data.CreatedFrom ? `&CreatedFrom=${data.CreatedFrom}` : '';
    // URL += data.CreatedTo ? `&CreatedTo=${data.CreatedTo}` : '';
    return this.http.get<BusinessOppResponse>(URL, { headers: myHeaders });
  }



  getBusinessOppFilter(data: BusinessOppSearchParams, url: string): Observable<BusinessOppResponse> {
    const currentUser = this.authenticationService.currentUserValue;
    const myHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.jwt });
    let URL = this.APIEndpoint + `/Search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;

    // Optional search params
    URL += url;
    return this.http.get<BusinessOppResponse>(URL, { headers: myHeaders }); 
  }


  /**
   * @function getAllDistributors
   * get list of all Distributors
   * todo: move to Distributors Module
   * @param name 
   * @return {Observable} Array of Distributors
   */
  getAllDistributors(name = ''): Observable<any> {
    let URL =  environment.ApiUrlV3 + 'AdminDistributor/Search?Filter.PageNumber=1&Filter.PageSize=10&Filter.CompanyName=' + name;
    return this.http.get<any>(URL).pipe(pluck('data', 'items'));
  }


  /**
   * @function getAllManu
   * get list of all manufacturers
   * todo: move to Manufacturer Module
   * @param name 
   * @return {Observable} Array of manufacturers
   */
  getAllManu(name = ''): Observable<any> {
    let URL =  environment.ApiUrlV3 + 'Manufacturers/Clients?PageNumber=1&PageSize=10&Name=' + name ;
    return this.http.get<any>(URL).pipe(pluck('data'));
  }



  getCountries(): Observable<any>  {
    return this.http.get<any>(environment.ApiUrlV3 + 'Countries').pipe(pluck('data'));
  }



  getAllBusinessOpp(data: BusinessOpp) {
    let URL = this.APIEndpoint + `/All?DistributorId=${data.distributorID}`;
    return this.http.get<BusinessOppResponse>(URL).pipe(pluck('data')); 
  }

  getNextBusinessOpp(data: BusinessOpp) {
    let URL = this.APIEndpoint + `/Next?DistributorId=${data.distributorID}`;
    return this.http.get<BusinessOppResponse>(URL); 
  }

  getClient(distributorId, key: string) {
    let URL = `${this.APIEndpoint}/GetClient?DistributorID=${distributorId}&Key=${key}`;
    return this.http.get<BusinessOppResponse>(URL); 
  }

  sendBusinessOpp(data) {
    let URL = this.APIEndpoint + `/Add`;
    return this.http.post<BusinessOppResponse>(URL, data); 
  }

  deleteBusinessOpp(id: number) {
    let URL = this.APIEndpoint + `/Delete`;
    let data = { id }
    return this.http.post<BusinessOppResponse>(URL, data); 
  }


  deleteCertificate(id: number) {
    let URL = environment.ApiUrlV3 + 'Companies' + '/DeleteFile';
    let data = { id }
    return this.http.post<BusinessOppResponse>(URL, data); 
  }

  addCommentBusinessOpp(data) {
    let URL = environment.ApiUrlV3 + 'AdminDistributor' + '/Comment';
    return this.http.put<BusinessOppResponse>(URL, data); 
  }
}
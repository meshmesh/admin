import { IFilterOption } from "src/app/filter/filters.model";

export const ScientificNamesFilterOptions: Array<IFilterOption> = [
  {
    index: 0,
    order: 1,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/MedicalLines',
    value: {},
    selected: [],
    placeholder: 'Medical Line',
    chipsList: [],
    list: [],
    querystring: 'MedicalLineIds',
    searchQuery: ''
  },
  {
    index: 1,
    order: 2,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/Specialties/Search?ParentOnly=true&Name=',
    value: {},
    hasPagination: true,
    selected: [],
    placeholder: 'Specialty',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'MainSpecialityIds'
  },
  {
    index: 2,
    order: 3,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/Specialties/GetChild?Name=',

    value: {},
    hasPagination: true,
    selected: [],
    placeholder: 'Sub Specialty',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'SpecialityIds'
  },

  {
    index: 2,
    order: 3,
    type: 'rangeDate',
    value: [],
    placeholder: 'Creation From Date',
    default: '1/1/2000',
    searchQuery: '',
    querystring: 'CreationFromDate',
  },
  {
    index: 3,
    order: 4,
    type: 'rangeDate',
    value: [],
    placeholder: 'Creation To Date',
    default: '1/1/2090',
    searchQuery: '',
    querystring: 'CreationToDate',
  },
  {
    index: 4,
    order: 5,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/ScientificNames/SearchElsatic?PageSize=10&PageNumber=1&Key=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Related Terms',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'RTIds'
  },

  {
    index: 4,
    order: 5,
    type: 'tags-select',
    output: 'id',
    value: [],
    selected: [],
    placeholder: 'Alternatives',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'Alternatives'
  },

  {
    index: 5,
    order: 6,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/ScientificNames/SearchElsatic?PageSize=10&PageNumber=1&Key=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Broader Term',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'BTIds'
  },
  {
    index: 6,
    order: 7,
    type: 'multi-select',
    output: 'id',
    apiurl: '/api/ScientificNames/SearchElsatic?PageSize=10&PageNumber=1&Key=',
    hasPagination: true,
    value: {},
    selected: [],
    placeholder: 'Narrow Term',
    chipsList: [],
    list: [],
    searchQuery: '',
    querystring: 'NTIds'
  },

  {
    index: 7,
    order: 8,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Is Approved',
    searchQuery: '',
    querystring: 'Approved',
  },


  {
    index: 8,
    order: 9,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Image',
    searchQuery: '',
    querystring: 'IsPictureFilled',
  },

  {
    index: 9,
    order: 10,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Subspecialty',
    searchQuery: '',
    querystring: 'HaveSubspecialty',
  },

  {
    index: 10,
    order: 11,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Description',
    searchQuery: '',
    querystring: 'HaveDescription',
  },

  {
    index: 11,
    order: 12,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Alternative',
    searchQuery: '',
    querystring: 'HaveAlternative',
  },

  {
    index: 12,
    order: 13,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Narrow Terms',
    searchQuery: '',
    querystring: 'HaveNT',
  },

  {
    index: 13,
    order: 14,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Related Terms',
    searchQuery: '',
    querystring: 'HaveRT',
  },

  {
    index: 14,
    order: 15,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have Broader Terms',
    searchQuery: '',
    querystring: 'HaveBT',
  }, 


  {
    index: 15,
    order: 16,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Have References',
    searchQuery: '',
    querystring: 'HaveReferences',
  }, 
  {
    index: 16,
    order: 17,
    type: 'select',
    list: [
      {
        name: 'Yes',
        id: 'true'
      },
      {
        name: 'No',
        id: 'false'
      },
    ],
    value: '',
    placeholder: 'Checked',
    searchQuery: '',
    querystring: 'Checked',
  }, 


];

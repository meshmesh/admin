import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { MatChipInputEvent, MAT_DIALOG_DATA, MatDialogRef, MatAutocompleteSelectedEvent } from '@angular/material';
import * as S3 from 'aws-sdk/clients/s3';
import { Observable, of, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Guid } from 'guid-typescript';

import { AddScientificNameService } from '../services/add-scientific-name.service';
import { ScientificNameModel, AddScientificNameModel, UpdateScientificNameModel } from '../model/add-scientific-name.model';
import { ScientificNameService } from '../services/scientific-name.service';
import { ToastService } from 'src/app/_services/toast.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-scientific-name',
  templateUrl: './add-scientific-name.component.html',
  styleUrls: ['./add-scientific-name.component.scss']
})
export class AddScientificNameComponent implements OnInit {

  item;
  basic_title
  constructor(
    private scientificNameService: AddScientificNameService,
    private service: ScientificNameService,
    private toast: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddScientificNameComponent>
  ) { }



  ngOnInit() {
    this.basic_title = "Add";
    this.item = this.data.row;
  }

  onUpdateItem(e: boolean) {

    if(e == null) {
      return;
    }

    if(this.dialogRef) {
      this.close(e)
    } 
  }

  close(data) {
    this.dialogRef.close(data);
  }

}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataListService } from './dataList.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dataList',
  templateUrl: './dataList.component.html',
  styleUrls: ['./dataList.component.scss']
})
export class DataListComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  			
  displayedColumns = ['ID', 'Name','Type', 'Value','Order', 'Company', 'Status', 'Actions'];
  constructor(private serv: DataListService) { }
  dataSource: MatTableDataSource<any>;
  values: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
    this.loadBuyers();
  }


  
  loadBuyers() {
    this.serv.getData().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        this.renderDataSource(response);
        this.paginDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }
  renderDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
  }

  paginDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  Approve(row){
    this.serv.approval(row.id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      
    });
  }


}
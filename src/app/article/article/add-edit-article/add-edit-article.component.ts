import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../../article.service';
import { Guid } from 'guid-typescript';
import * as S3 from 'aws-sdk/clients/s3';
import { ArticleToCreate } from '../ArticleToCreate';
import { of, Observable } from 'rxjs';
import { MatChipInputEvent, MatDialog } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { SeomodalComponent } from '../_modal/seomodal/seomodal.component';
import * as _ from 'lodash';
import { ToastService } from 'src/app/_services/toast.service';


@Component({
  selector: 'app-add-edit-article',
  templateUrl: './add-edit-article.component.html',
  styleUrls: ['./add-edit-article.component.scss', '../../../../assets/icon/icofont/css/icofont.scss']
})
export class AddEditArticleComponent implements OnInit {
  articleForm: ArticleToCreate;
  seletedSubCategories: any[] = [];
  SubCategories: any[] = [];
  filteredSubCategories: Observable<any>;
  seletedTags: any[] = [];
  Tags: any[] = [];
  categorys: any[] = [];
  showsub: boolean;
  removable = true;
  subImagePreview: any[] = [];
  totalTasks = 0;
  totalCount = 0;
  imagePreview: any;
  files: File[] = [];
  file: File;
  @Output() fromSeoToAddEdit = new EventEmitter();
  @Input() seoData;
  generalSeo = [];
  specialSeo = [];
  outputSeo: any;
  finalSeoData: any[] = [];
  articleSeo: any[] = [];
  public show = false;
  public buttonName: any = 'Show';
/*   type: any [] = [{name:'distributor'}, {name: 'manufacturer'}];
 */
type: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService,
    private http: HttpClient,
    private toast: ToastService,
    public dialog: MatDialog
  ) {}


  openSEO() {
    const dialogRef = this.dialog.open(SeomodalComponent, {
      data: this.finalSeoData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.length > 0) {
        this.finalSeoData = result;
      }
    });
  }
  removeTag(i) {
    this.seletedTags.splice(i, 1);
  }

  removeSubCategories(i) {
    const temp = this.seletedSubCategories[i];
    this.SubCategories.push(temp);
    this.seletedSubCategories.splice(i, 1);
  }

  addTags(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.seletedTags.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  filtersubcategory() {
    this.articleService.getSubCategory(this.articleForm.CategoryId).subscribe(
      (response: any) => {
        this.SubCategories = response;
        if (response.length > 0) {
          this.filteredSubCategories = of(this.SubCategories);
          this.showsub = true;
        } else {
          this.showsub = false;
        }
      },

      error => console.log(error)
    );
  }

  _filterSubCategories(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(this.SubCategories.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
  AddSelectedSubCategoriess(event) {
    this.seletedSubCategories.push(event.option.value);
    const index = this.SubCategories.indexOf(event.option.value);
    if (index > -1) {
      this.SubCategories.splice(index, 1);
    }
  }
  async submit(form) {
    if (form.invalid) {
      return;
    }
    if (this.imagePreview === null) {
      return;
    }

    this.totalCount = this.files.length;
    this.seletedTags.forEach(element => {
      this.articleForm.Tags.push(element.name);
    });
    this.seletedSubCategories.forEach(element => {
      this.articleForm.Categories.push(element.id);
    });
    if (this.totalCount === 0) {
      this.articleForm.Subimages = [];

      this.subImagePreview.forEach(element => {
        this.articleForm.Subimages.push(element);
      });

      if (!this.file && this.articleForm.Id) {
        this.articleForm.Articleimage = this.imagePreview;
        this.addUpdateFunction(this.articleForm);
      }
    }

    if (!this.file && !this.articleForm.Id) {
      return;
    } else if (this.file) {
      const _thisfile = this;
      this.totalCount++;

      this.uploadImage(this.file, function(result) {
        _thisfile.articleForm.Articleimage = result;
        _thisfile.totalTasks++;
        if (_thisfile.totalTasks === _thisfile.totalCount) {
          _thisfile.addUpdateFunction(_thisfile.articleForm);
        }
      });
    }

    if (this.files.length > 0) {
      this.files.forEach(element => {
        const _thisfiles = this;
        this.uploadImage(element, function(result) {
          _thisfiles.articleForm.Subimages.push(result);
          _thisfiles.totalTasks++;
          if (_thisfiles.totalTasks === _thisfiles.totalCount) {
            _thisfiles.addUpdateFunction(_thisfiles.articleForm);
          }
        });
      });
    }
  }
  addUpdateFunction(articleForm) {
    const _thisTimer = this;
    this.http.post(environment.ApiUrl + '/api/AdminArticles/AddUpdate', articleForm).subscribe(res => {
      this.toast.success('Inserted Successfully ');

      setTimeout(function() {
        _thisTimer.router.navigate(['/article-page']);
      }, 1500);
    });
  }
  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function(err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  onImageChange(event) {
    this.file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = event => {
        this.imagePreview = reader.result;
      };
    }
  }

  onsubImageChange(event) {
    for (let i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
      if (event.target.files && event.target.files[i]) {
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[i]);
        // tslint:disable-next-line:no-shadowed-variable
        reader.onload = event => {
          this.subImagePreview.push(reader.result);
        };
      }
    }
  }
  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'product/Article/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function(resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  LoadSeo(updateMood) {
    this.articleService.getPortal().subscribe(
      (response: any) => {
        // console.log('portalssss', response);
        this.seoData = response;
        this.filterSeo(updateMood);
      },
      error => console.log(error)
    );
  }

  filterSeo(updateMood) {
    this.finalSeoData = [];
    this.seoData.forEach(element => {
      if (updateMood) {
        if (this.articleSeo.length > 0) {
          this.articleSeo.forEach(item => {
            if (element.id === item.id) {
              element.metaDescription = item.metaDescription;
              element.metaKeyWord = item.metaKeyWord;
              element.metaTitle = item.metaTitle;
              element.name = item.name;
            }
            const index = this.finalSeoData
              .map(function(x) {
                return x.id;
              })
              .indexOf(element.id);
            if (index === -1) {
              this.finalSeoData.push(element);
            }
          });
        } else {
          this.finalSeoData.push(element);
        }
      } else {
        this.finalSeoData.push(element);
      }
    });
  }
  hideImage() {
    this.imagePreview = null;
    this.file = null;
    this.articleForm.Articleimage = null;
  }
  hideSubImage(i) {
    this.subImagePreview.splice(i, 1);
    this.files.splice(i, 1);
    this.articleForm.Subimages.splice(i, 1);
  }

  ngOnInit() {
    this.articleForm = new ArticleToCreate();
    this.articleForm.Id = this.route.snapshot.params['id'];

    this.articleService.getMainCategory().subscribe(
      (response: any) => {
        this.categorys = response;
      },
      error => console.log(error)
    );

    if (this.articleForm.Id) {
      this.articleService.getArticlesById(this.articleForm.Id).subscribe((response: any) => {
        this.articleForm.Articleimage = _.cloneDeep(response.articleimage);
        this.articleForm.Title = response.title;
        this.articleForm.Subtitle = response.subtitle;
        this.articleForm.Subimages = _.cloneDeep(response.subimages);
        this.articleForm.CategoryId = response.maincategory;
        this.articleSeo = JSON.parse(response.seo);
        this.LoadSeo(true);
        this.articleForm.Categories = response.categories;
        this.articleForm.Published = response.published;
        this.subImagePreview = response.subimages;
        this.imagePreview = response.articleimage;
        response.tags.forEach(element => {
          _.cloneDeep(this.seletedTags.push({ name: element }));
        });
        this.articleForm.Description = response.description;
        this.articleForm.Details = response.details;
        this.articleService.getSubCategory(this.articleForm.CategoryId).subscribe(
          (res: any) => {
            this.SubCategories = res;
            if (res.length > 0) {
              this.filteredSubCategories = of(this.SubCategories);
              response.categories.forEach(element => {
                const index = this.SubCategories.map(function(x) {
                  return x.id;
                }).indexOf(element);
                this.seletedSubCategories.push(this.SubCategories[index]);
              });
              this.showsub = true;
            } else {
              this.showsub = false;
            }
          },

          error => console.log(error)
        );
      });
    } else {
      this.LoadSeo(false);
    }
  }
}

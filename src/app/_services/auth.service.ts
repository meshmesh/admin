import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CurrentUser } from '../_model/CurrentUser';
import { Router } from '@angular/router';
@Injectable()
export class AuthService {
  currentUserSubject: BehaviorSubject<CurrentUser>;
  public currentUser: Observable<CurrentUser>;
  ApiUrl: string = environment.ApiUrl;
  helper = new JwtHelperService();
  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<CurrentUser>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): CurrentUser {
    return this.currentUserSubject.value;
  }
  public setCurrentUserValue(user: CurrentUser) {
    this.currentUserSubject.next(user);
  }
  public updateCurrentUserState(
    currentSate: string,
    nextState: string,
    companyToken: string = null,
    companyId: number = null
  ) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    currentUser.currentState = currentSate;
    currentUser.nextState = nextState;
    currentUser.companyToken = companyToken;
    currentUser.companyId = companyId;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.currentUserSubject.next(currentUser);
  }
  public updateCurrentUserbusinessOpportunities(Ids: number[]) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    currentUser.businessOpportunitiesIDs = Ids;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.currentUserSubject.next(currentUser);
  }


  login(email: string, password: string) {
    return this.http
      .post<CurrentUser>(environment.ApiUrlV3 + 'Auth/Login/Admin', {
        email: email,
        password: password
      })
      .pipe(
        map((res: any) => {
          debugger

          if(res && res.success) {
            // login successful if there's a jwt token in the response
            if (res.data && res.data.jwt) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('currentUser', JSON.stringify(res.data));
              this.currentUserSubject.next(res.data);
            }
          }
 
          return res;
        })
      );
  }

  // login(email: string, password: string) {
  //   return this.http
  //     .post<CurrentUser>(this.ApiUrl + '/api/auth/loginAdmin', {
  //       Email: email,
  //       Password: password
  //     })
  //     .pipe(
  //       map(user => {
  //         // login successful if there's a jwt token in the response
  //         if (user && user.jwt) {
  //           // store user details and jwt token in local storage to keep user logged in between page refreshes
  //           localStorage.setItem('currentUser', JSON.stringify(user));
  //           this.currentUserSubject.next(user);
  //         }

  //         return user;
  //       })
  //     );
  // }

  getUserDetail() {
    return localStorage.getItem('currentUser');
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['login']);
  }

  decodedToken(token) {
    return this.helper.decodeToken(token);
  }
  expirationDate(token) {
    return this.helper.getTokenExpirationDate(token);
  }
  isExpired(token) {
    return this.helper.isTokenExpired(token);
  }

  convertDate(inputFormat) {
    function pad(s) {
      return s < 10 ? '0' + s : s;
    }
    const d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
  }
  getSeo() {
    const url =  this.ApiUrl + '/api/portals/getportals';
    return this.http.get(url);
  }
  
}

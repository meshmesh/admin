import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ToggleFullScreenDirective } from "./fullscreen/toggle-fullscreen.directive";
import { AccordionAnchorDirective } from "./accordion/accordionanchor.directive";
import { AccordionLinkDirective } from "./accordion/accordionlink.directive";
import { AccordionDirective } from "./accordion/accordion.directive";

import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from "ngx-perfect-scrollbar";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { TitleComponent } from "../layout/admin/title/title.component";
import { CardComponent } from "./card/card.component";
import { CardToggleDirective } from "./card/card-toggle.directive";
import { ModalBasicComponent } from "./modal-basic/modal-basic.component";
import { ModalAnimationComponent } from "./modal-animation/modal-animation.component";

import { ClickOutsideModule } from "ng-click-outside";
import { DataFilterPipe } from "./elements/data-filter.pipe";
import { parsePhoneNumber } from "libphonenumber-js";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FiltersComponent } from "./filters/filters.component";
import { RangeDateComponent } from "./filters/range-date/range-date.component";
import { CheckboxsComponent } from "./filters/checkboxs/checkboxs.component";
import { SingleSelectComponent } from "./filters/single-select/single-select.component";
import { MultiSelectComponent } from "./filters/multi-select/multi-select.component";
import { TextComponent } from "./filters/text/text.component";
import { ToggelComponent } from "./filters/toggel/toggel.component";
import { OrderModule } from "ngx-order-pipe";
import { MatExpansionModule } from "@angular/material/expansion";
import { SelectModule } from "ng-select";
import { FileUploadModule } from "ng2-file-upload";
import { UiSwitchModule } from "ng2-ui-switch";
import { TagInputModule } from "ngx-chips";
import { DataTableModule } from "angular2-datatable";
import { MatMenuModule } from "@angular/material/menu";
import { SearchComponent } from "./search/search.component";
import { AutocompleteMultiComponent } from "./filters/autocomplete-multi/autocomplete-multi.component";
import { NumberComponent } from "./filters/number/number.component";
import { AutocompletesingleComponent } from "./filters/autocompletesingle/autocompletesingle.component";
import { DeletedPuplishPopupComponent } from "./deleted-puplish-popup/deleted-puplish-popup.component";
import {
  MatDatepickerModule,
  MatInputModule,
  MatFormFieldModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatBadgeModule,
  MatNativeDateModule,
  MatGridListModule,
  MatButtonModule,
  MatSelectModule,
  MatToolbarModule,
  MatCheckboxModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatListModule,
  MatTabsModule,
  MatBottomSheetModule,
} from "@angular/material";
import { SingleDateComponent } from "./filters/single-date/single-date.component";
import { SortingComponent } from "./filters/sorting/sorting.component";
import { UniquePipe } from "../../app/pipes/unique.pipe";

import { ChangepasswordComponent } from "../layout/admin/modals/changepassword/changepassword.component";
import { TableComponent } from "./table/table.component";
import { CdkTableModule } from "@angular/cdk/table";
import { DeleteConfirmDialogComponent } from "./delete-confirm-dialog/delete-confirm-dialog.component";
import { NewSearchComponent } from "./new-search/new-search.component";
import { NgSelectModule } from "@ng-select/ng-select";

import { QuillEditorModule } from "ngx-quill-editor";

import { TranslateComponent } from "../translate/translate.component";
import { NewSeoModalComponent } from "./seo-modal/seo-modal.component";
import { ImageUploadComponent } from "./image-upload/image-upload.component";
import { BooleanPipe } from "../_pipes/boolean.pipe";
import { TableChipsComponent } from "./table-chips/table-chips.component";
import { ViewAllTableChipsModalComponent } from "./table-chips/view-all-table-chips-modal/view-all-table-chips-modal.component";
import { CheckedIconComponent } from "./checked-icon/checked-icon.component";
import { SeoFormComponent } from "./seo-modal/seo-form/seo-form.component";

import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { EllipsisPipe } from "./pipes/ellipses.pipe";
import { SafePipe } from "./pipes/safe.pipe";
import { ManufacturerDetailComponent } from "../pages/manufacturer/manufacturer-detail/manufacturer-detail.component";
import { DistributorDetailsComponent } from "../pages/distributor/distributor-details/distributor-details.component";


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const materialModules = [
  MatPaginatorModule,
  MatTableModule,
  MatMenuModule,
  MatFormFieldModule,
  MatChipsModule,
  MatExpansionModule,
  MatMenuModule,
  MatInputModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
  CdkTableModule,
  MatBadgeModule,
  MatNativeDateModule,
  MatGridListModule,
  MatButtonModule,
  MatSelectModule,
  MatToolbarModule,
  MatCheckboxModule,
  MatListModule,
  MatTabsModule,
  MatBottomSheetModule,
];

const modals = [
  DeleteConfirmDialogComponent,
  ViewAllTableChipsModalComponent,
  NewSeoModalComponent,
];
const components = [
  TableChipsComponent,
  NewSearchComponent,
  BooleanPipe,
  CheckedIconComponent,
  ImageUploadComponent,
  SeoFormComponent,
];
@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    PerfectScrollbarModule,
    ClickOutsideModule,
    ReactiveFormsModule,
    FormsModule,
    OrderModule,
    SelectModule,
    FileUploadModule,
    UiSwitchModule,
    TagInputModule,
    DataTableModule,
    NgxUiLoaderModule,
    ...materialModules,
    NgSelectModule,
    QuillEditorModule,
    NgxMaterialTimepickerModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ToggleFullScreenDirective,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    CardToggleDirective,
    PerfectScrollbarModule,
    TitleComponent,
    CardComponent,
    ModalBasicComponent,
    ModalAnimationComponent,
    // SpinnerComponent,
    ClickOutsideModule,
    DataFilterPipe,
    FiltersComponent,
    SearchComponent,
    DeletedPuplishPopupComponent,
    NgxUiLoaderModule,
    UniquePipe,
    TableComponent,
    // ContactTableComponent,
    ...materialModules,
    ...modals,
    ...components,
    NewSearchComponent,
    FileUploadModule,
    NgSelectModule,
    // AddCompanyComponent,
    NgxMaterialTimepickerModule,
    EllipsisPipe,
    SafePipe
  ],
  declarations: [
    ToggleFullScreenDirective,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    CardToggleDirective,
    TitleComponent,
    CardComponent,
    ModalBasicComponent,
    ModalAnimationComponent,
    // SpinnerComponent,
    DataFilterPipe,
    FiltersComponent,
    RangeDateComponent,
    CheckboxsComponent,
    SingleSelectComponent,
    MultiSelectComponent,
    // AddCompanyComponent,
    TextComponent,
    ToggelComponent,
    SearchComponent,
    AutocompleteMultiComponent,
    NumberComponent,
    AutocompletesingleComponent,
    DeletedPuplishPopupComponent,
    SingleDateComponent,
    SortingComponent,
    UniquePipe,
    ChangepasswordComponent,
    TableComponent,
    // ContactTableComponent,
    ...modals,
    ...components,
    NewSearchComponent,
     DistributorDetailsComponent,
    ManufacturerDetailComponent,
    TranslateComponent,
    // SendEmailComponent,
    EllipsisPipe,
    SafePipe
  ],
  entryComponents: [
    ChangepasswordComponent,
    ...modals,
    // AddCompanyComponent,
    DistributorDetailsComponent,
    ManufacturerDetailComponent,
    // SendEmailComponent,
  ],

  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },

    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: { autoFocus: false, hasBackdrop: true },
    },
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class SharedModule {}
export function ValidatePhoneNumber(countryCode: string, phoneNumber: string) {
  try {
    const pareser = parsePhoneNumber(countryCode + phoneNumber);
    return pareser.isValid();
  } catch (error) {
    return error;
  }
}

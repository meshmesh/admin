export interface DeleteConfirmDialogData {
  title: string;
  id: number;
}

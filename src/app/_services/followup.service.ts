import { Injectable } from '@angular/core';
import { FollowUp, followUps, FollowUpLog, log, actionNames } from '../_model/followup';
import { of, Observable } from 'rxjs';

@Injectable()
export class FollowUpService {
  constructor() {}

  getFollowUp(): Observable<FollowUp[]> {
    return of(followUps);
  }

  getLog(id: number): Observable<FollowUpLog> {
    return of(log);
  }

  getActionNames(): Observable<string[]> {
    return of(actionNames);
  }
}

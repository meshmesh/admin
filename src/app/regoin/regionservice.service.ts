import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegionserviceService {

constructor(private http: HttpClient) { }

searchfilter(pageNumber, fiter) {
  let url =
    environment.ApiUrl +
    '/api/AdminRegion/Search?&PageNumber=' + pageNumber + '&'+ fiter;
  return this.http.get(url);
}
getall(pageNumber) {
  let url =
    environment.ApiUrl +
    '/api/AdminRegion/Search?&PageNumber=' + pageNumber;
  return this.http.get(url);
}
getManuinRegions(ObjectId){
 let url = environment.ApiUrl +'/api/AdminCompany/GetCompanybySource?ObjectId='+ObjectId+'&SourceId=5'  
  return this.http.get(url);
}
getDistinRegions(ObjectId){
 let url = environment.ApiUrl +'/api/AdminCompany/GetCompanybySource?ObjectId='+ObjectId+'&SourceId=11'  
  return this.http.get(url);
}
}

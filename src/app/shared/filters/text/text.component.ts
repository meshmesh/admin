import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  textValue = '';
  @Input() mainData;
  @Output() dataFromText = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  inputHandler(event) {
    this.mainData.value = event.target.value;
    this.dataFromText.emit(this.mainData);
  }
}

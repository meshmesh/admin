import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConversationsService } from './conversations.service';
import { AuthService } from '../_services/auth.service';





@Injectable()
export class ConversationsResolver implements Resolve<Array<any>> {
    constructor(private conversationService: ConversationsService,
        private authService: AuthService,
        private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): Observable<Array<any>> {
        if (this.authService.currentUserValue) {
            return this.conversationService.GetConversations().pipe(
                catchError(error => {
                    this.router.navigate(['']);
                    return of(null);
                })
            );
        } else {
            return of([]);
        }

    }
}



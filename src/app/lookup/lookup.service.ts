import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LookupService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) {}

  getMajors(): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '/api/Lookups/GetAll');
  }

  saveEdit(param) {
    return this.http.post(this.APIEndpoint + '/api/Lookups/AddUpdate', param);
  }

  approveMajor(id: number): Observable<any> {
    return this.http.get(this.APIEndpoint + '/api/AdminLookups/Approve?Id=' + id);
  }
}

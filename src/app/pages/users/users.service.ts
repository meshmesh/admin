import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  APIEndpoint = environment.ApiUrlV3;
  constructor(private http: HttpClient) { }

  getUsers(Pagenumber, data = '', text = ''): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'users/search?PageSize=10&PageNumber=' + Pagenumber + '&FullInput=' + text + data);
  }

  usersFilter(Pagenumber, data): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'users/search?PageSize=10&PageNumber=' + Pagenumber + '&' + data);
  }
  usersFilter2(Pagenumber,text, data): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'users/search?PageSize=10&PageNumber=' + Pagenumber +'&'  + '&FullInput=' + text+'&' + data);
  }


  getCOmpanies(name, type): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'Companies/GetListOfCompaniesLimited?Name=' +
      name + '&Type=' + type);
  }


  AddUpdateUser(param) {
    return this.http.post(this.APIEndpoint + 'Users/UpdateUserInfo', param);
  }

  ActivateDeactivateUser(param) {
    return this.http.put(this.APIEndpoint + 'Users/ActivateUser', param);
  }

  getCountry(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + 'Countries'
    );

  }


  getJobTitle(param, type): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'JobTitles?search?Params=' +
      param + '&type=' + type + '&limit=10');
  }


  getUserbyId(param): Observable<any> {
    return this.http.get<any[]>(this.APIEndpoint + 'Users/Load?id=' + param);
  }

  deleteUser(param){
return this.http.get<any[]>(this.APIEndpoint + 'Users/Delete?Id=' + param);
  }

}
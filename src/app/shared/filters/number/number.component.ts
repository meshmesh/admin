import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss']
})
export class NumberComponent implements OnInit {
  numberValue = 0;
  @Input() mainData;
  @Output() dataFromNumber = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  inputHandler(event) {
    console.log('message', event.target.value);
    this.mainData.value = event.target.value;
    this.dataFromNumber.emit(this.mainData);
  }

}

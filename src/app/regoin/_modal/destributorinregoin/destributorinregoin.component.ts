import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegionserviceService } from '../../regionservice.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-destributorinregoin',
  templateUrl: './destributorinregoin.component.html',
  styleUrls: ['./destributorinregoin.component.scss']
})
export class DestributorinregoinComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(public dialogRef: MatDialogRef<DestributorinregoinComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,public service : RegionserviceService) { }

  List :any;
  ListDist :any;
  displayedManuColumns: string[] = ['id', 'name', 'countryName', 'annualSales']
  displayedDistColumns: string[] = ['id', 'name', 'countryName', 'annualSales']

  ngOnInit() {
    
    this.service.getManuinRegions(this.data.data).pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
      
      this.List = res;
    })
    this.service.getDistinRegions(this.data.data).pipe(takeUntil(this.onDestroy$)).subscribe(res =>{
      

      this.ListDist = res;
    })
  }
  close(data): void {
    this.dialogRef.close(data);
  }

}

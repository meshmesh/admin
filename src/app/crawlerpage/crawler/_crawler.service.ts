import { OptionView } from './../../shared/optionView';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { pluck, map } from 'rxjs/operators';
import { CompanyScientificName, CompanyScientificNameRequest } from './companyScientificName';
import { IParentSpeciality } from 'src/app/pages/new-scientific-name/model/scientific-name.model';


@Injectable({
  providedIn: 'root'
})
export class _crawlerService {
  apiUrl: string = environment.ApiUrlV3;
  constructor(private http: HttpClient) { }


  
  getCompanyManfacturerByNameAndType(NameOrSlug: string, type: string): Observable<OptionView[]> {
    return this.http.get<OptionView[]>(`${this.apiUrl}AdminCompany/CompanyList?NameOrSlug=${NameOrSlug}&Type=${type}`).pipe(pluck('data'));
  }


  getScientificNames(
    value: string,
    selectedIds: number[]
  ): Observable<OptionView[]> {
    selectedIds = selectedIds.filter(id => id !== null && id !== 0);
    return this.http.get<OptionView[]>(
      `${this.apiUrl}ScientificNames/SearchElsatic?Key=${value}${
      selectedIds.length > 0 ? '&SelectedIds=' : ''
      }${selectedIds.join('&SelectedIds=')}`
    ).pipe(pluck('data'),

    );
  }

  getSubSpecialitySuggestion(parentId: number, text: string = ''): Observable<any> {
    let URL = this.apiUrl + 'Specialties/GetChild?'  ;
    URL += parentId ? `&SpecialtyId=${parentId}` : '';
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));;
  }

  getParentSpecialtySuggestion(text: string) {
    let URL = this.apiUrl + 'Specialties/Search?ParentOnly=true'; // 
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));
  }


  searchSpeciality(medicalLineId: number = null,isParent: string = '', isChild: string = '', withSciname: string = ''): Observable<IParentSpeciality[]> {
    let URL = this.apiUrl + 'Specialties/Search?'; // ForAdmin=true
    URL += isParent ? `&ParentOnly=${isParent}` : '';
    URL += isChild ? `&Childonly=${isChild}` : '';
    URL += withSciname ? `&HasScientificOnly=${withSciname}` : '';
    URL += medicalLineId ? `&MedicalLineId=${medicalLineId}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));
  }




  getSubSpecialty(text: string) {
    let URL = this.apiUrl + 'Specialties/Search?Childonly=true'; // 
    URL += text ? `&Name=${text}` : '';
    return this.http.get<any>(URL).pipe(pluck('data'));
  }
  

  getScientificNamesSuggestions(value: string) {
    // selectedIds = selectedIds.filter(id => id !== null && id !== 0);
    // return this.http.get<OptionView[]>(
    //   `${this.apiUrl}ScientificNames/SearchElsatic?Key=${value}${
    //   selectedIds.length > 0 ? '&SelectedIds=' : ''
    //   }${selectedIds.join('&SelectedIds=')}`
    // ).pipe(pluck('data'));
    return this.http.get<OptionView[]>(
      `${this.apiUrl}ScientificNames/SearchElsatic?Key=${value}`
    ).pipe(pluck('data'));
  }

  getCountries(): Observable<OptionView[]> {
    return this.http.get<OptionView[]>(this.apiUrl + 'Countries').pipe(pluck('data'));
  }


  getCompanyScientificName(data: CompanyScientificNameRequest): Observable<CompanyScientificName[]> {
    return this.http.post<CompanyScientificName[]>(this.apiUrl + 'ProspectedCompanyScientificNames/GetCompanyScientificNames', data).pipe(pluck('data'));
  }

  approveDisApprove(companyScientificNames: number[], status: boolean) {
    let data = {
      companyScientificNames: companyScientificNames,
      status: status
    }
    return this.http.put<any>(this.apiUrl + 'ProspectedCompanyScientificNames', data);
  }

  checkUncheck(item: CompanyScientificName, status: boolean) {
    let data = {
      prospectedCompanyId: item.prospectedCompanyScientificNameId,
      scientificNameId: item.scientificNameID,
      check: status
    }
    return this.http.post<any>(this.apiUrl + 'ProspectedCompanyScientificNames/Check', data);
  }


  getMedicleLineSuggestion(): Observable<any> {
    const url =
    this.apiUrl +
      `MedicalLines?PageNumber=1&PageSize=100`;
    return this.http.get<any>(url);
  }


}

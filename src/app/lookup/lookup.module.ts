import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatButtonModule, MatTableModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { LookupComponent } from './lookup.component';
import { LookupRoutingModule } from './lookup-routing.module';


@NgModule({
  imports: [
    CommonModule,
    LookupRoutingModule,
    MatCardModule,
    
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule

  ],
  declarations: [LookupComponent]
})
export class LookupModule { }

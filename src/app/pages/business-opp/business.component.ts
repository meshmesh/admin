import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatTableDataSource, MatSort, MatDialog, MatBottomSheet } from "@angular/material";
import { MatPaginator } from "@angular/material/paginator";

import { BusinessOppSearchParams, BusinessOpp, BusinessOppFilterOptions } from "./business.models";
import { BusinessService } from "./business.service";
import { ReplaySubject } from "rxjs";

import { ActionsPopupComponent } from "./actions-popup/actions-popup.component";
import { ToastService } from "src/app/_services/toast.service";
import { takeUntil } from "rxjs/operators";
import { CreateSignedAgreementComponent } from "./create-signed-agreement/create-signed-agreement.component";
import { ManufacturerDetailComponent } from "../manufacturer/manufacturer-detail/manufacturer-detail.component";
import { DistributorService } from "../distributor/services/distributor.service";
import { ManufacturerService } from "../manufacturer/services/manufacturer.service";
import { CreateBusinessOppComponent } from "./create-business-opp/create-business-opp.component";
import { DistributorDetailsComponent } from "../distributor/distributor-details/distributor-details.component";
// import { DistributorService } from '../services/distributor.service';



@Component({
  selector: "app-business",
  templateUrl: "./business.component.html",
  styleUrls: ["./business.component.scss"],
})
export class BusinessComponent implements OnInit, AfterViewInit {

  @ViewChild('table', { read: MatSort }) sort: MatSort;
  // @ViewChild('table', { read: MatPaginator }) paginator: MatPaginator;
  @ViewChild(MatPaginator ) paginator: MatPaginator;

  // flags
  currentPage: number = 1;
  totalItems: number = 0;
  pageNumber: number = 1;
  pageSize: number = 10;


  pageType: string = 'businessOpp';
  businessSearchParams: BusinessOppSearchParams = new BusinessOppSearchParams();
  businessOppList: Array<BusinessOpp>;
  dataSource;
  isLoading: boolean = false;
  onDestroy$: ReplaySubject<void> = new ReplaySubject();

  filterOptions: any[] = BusinessOppFilterOptions;
  filteredString: string = '';


  displayedColumns: string[] = [
    "Actions",
    "distributorName",
    "manufacturerName",
    "opportunitySendDate",
    "opportunityFinshedDate",
    "NextBusiness",
    "distributorCertificates",
    "distributorComment",
    "users"
  ];

  constructor(
    private manuService: ManufacturerService,
    private distributorService: DistributorService,
    private toast: ToastService, 
    private dialog: MatDialog, 
    private businessOppService: BusinessService, 
    private bottomSheet: MatBottomSheet) {}

  ngOnInit() {
    this.getTableDataWithPreservedParams()
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.dialog.closeAll();
  }

  ngAfterViewInit() {
    console.log(this.paginator);
  }


  getTableDataWithPreservedParams() {
    this.isLoading = true;
    this.businessOppService.getBusinessOppFilter(this.businessSearchParams, this.filteredString).subscribe((res) => {
      if(res && res.data) {
        this.totalItems = res.data.count;
        this.renderData(res.data.items);
      } else {
        this.totalItems = 0;
        this.renderData([]);
      }
    });
  }

  renderData(data: Array<BusinessOpp>) {
    this.dataSource = new MatTableDataSource<BusinessOpp>(data);
    console.log(this.paginator)
    console.log(this.dataSource.paginator)
    // check if no data source, then hide pagination 
    this.isLoading = false;
  }


  onTabChange(event) {
    switch (event.nextId) {
      case 1:
        this.pageType = 'businessOpp';
        break;
      case 2:
        this.pageType = 'signedAgreements'
        break;
    }
  }

  pageChange(event) {
    debugger
    this.pageSize = event.pageSize;
    this.businessSearchParams.PageSize = event.pageSize;
    this.businessSearchParams.PageNumber = event.pageIndex + 1;
    this.getTableDataWithPreservedParams();
  }


  handleDataFromFilters(event) {
    
    this.paginator.pageIndex = 0;
    this.businessSearchParams.PageNumber = 1;

    if(event === 0) {
      this.filteredString = '';
      this.getTableDataWithPreservedParams();
      return;
    }

    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });
    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });

    this.getTableDataWithPreservedParams();

  }




  // refactor based on type
  openLink(element: BusinessOpp, type: number) {
    switch (type) {
      // Reload
      case 1:
        this.onReload(element);
        break;

      // Remove
      case 2:
        this.onDelete(element);
        break;

      // Send BO
      case 3:
        this.onSend(element);
        break;

      // Log
      case 4:
        this.onLog(element);
        break;

      // View & Reject the Company certificate
      case 5:
        this.onViewCertificate(element);
        break;

      // Add comments
      case 6:
        this.onAddComments(element);
        break;

      // show Users
      case 7:
        this.showUsers(element);
        break;

      // show comments
      case 8:
        this.showComments(element);
        break;
    }
  }
  

  showComments(element) {
    this.dialog.open(ActionsPopupComponent, {
      width: '65%',
      // height: '70%',
      data: { 
        element: element, 
        type: 8
      }
    });
  }

  showUsers(element) {
    const dialogRef = this.dialog.open(ActionsPopupComponent, {
      width: '65%',
      // height: '70%',
      data: { 
        element: element, 
        type: 7 
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.toast.success('Deleted Successfully!');
      }
    });
  }
  
  onViewCertificate(element: BusinessOpp) {
    const dialogRef = this.dialog.open(ActionsPopupComponent, {
      width: '65%',
      // height: '70%',
      data: { 
        element: element, 
        type: 5 
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getTableDataWithPreservedParams();
      } 
    });
  }

  onReload(element: BusinessOpp) {
    const dialogRef = this.dialog.open(ActionsPopupComponent, {
      // width: '1250px',
      // height: '70%',
      data: { 
        element: element, 
        type: 1 
      }
    });
  }

  onDelete(element: BusinessOpp) {
     const dialogRef = this.dialog.open(ActionsPopupComponent, {
      // width: '1250px',
      // height: '70%',
      data: { 
        element: element, 
        type: 2 
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.deleteRowDataTable(element);
      }
    });

  }


  private deleteRowDataTable(element: BusinessOpp) {
    const itemIndex = this.dataSource.data.findIndex(obj => obj.opportunityID === element.opportunityID);
    if(itemIndex > -1) this.dataSource.data.splice(itemIndex, 1);
    this.dataSource.paginator = this.paginator;
  }

  addBusinessOpp() {
    const dialogRef = this.dialog.open(CreateBusinessOppComponent, {
      width: '65%',
      // height: '70%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getTableDataWithPreservedParams();
      } 
    });
  }


  onSend(element: BusinessOpp) {
    const dialogRef = this.dialog.open(ActionsPopupComponent, {
      width: '45%',
      // height: '70%',
      data: { 
        element: element, 
        type: 3 
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getTableDataWithPreservedParams();
      }
    });
  }


  onLog(element) {
    this.dialog.open(ActionsPopupComponent, {
      width: '65%',
      // height: '70%',
      data: { 
        element: element, 
        type: 4 
      }
    });
  }

  onAddComments(element) {
    // TODO: open popup
    const dialogRef = this.dialog.open(ActionsPopupComponent, {
      width: '45%',
      // height: '70%',
      data: { 
        element: element, 
        type: 6 
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result >> ", result);

      // should reload page
      if(result) {
        this.getTableDataWithPreservedParams();
      }
    });
  }


  Add() {
    const dialogRef = this.dialog.open(CreateSignedAgreementComponent, {
      width: '65%',
      // height: '70%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result >> ", result)
    });
  }


  displayDistributor(element) {
    this.distributorService.get(element.distributorID).pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
      this.dialog.open(DistributorDetailsComponent, {
        data: res.data[0],
        width: '85%',
        height:'80%',
      });
    });
  }

  displayManufacturer(element) {
    this.manuService.getManufacturerById(element.manufacturerID).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const dialogRef = this.dialog.open(ManufacturerDetailComponent, {
        data: res.data,
        width: '85%',
        height: '80%'
      });
    });
  }

}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  APIEndpoint = environment.ApiUrl;

constructor(private http: HttpClient) { }


getLangugaesId(MajorCode){
  return this.http.get<any>(this.APIEndpoint + '/api/AdminLookups/get?MajorCode=' + MajorCode);
 }
}

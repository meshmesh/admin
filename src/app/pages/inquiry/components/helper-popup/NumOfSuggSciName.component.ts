import { Component, OnInit, Inject, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InquiryService } from '../../inquiry.service';
import { InquiryUnit, AddEmailRequest } from '../../inquiry.model';
import { ToastService } from "src/app/_services/toast.service";

export class InquiryDataTypeSource {
  row: InquiryUnit
  type: number
}

export class IMessageBody {
  body: string;
  createdAt: string;
  id: number;
}

@Component({
  selector: 'app-NumOfSuggSciName',
  templateUrl: './NumOfSuggSciName.component.html',
  styleUrls: ['./NumOfSuggSciName.component.scss']
})
export class NumOfSuggSciNameComponent implements OnInit , OnDestroy, AfterViewInit {

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  onDestroy$ = new ReplaySubject<void>();
  dialogTitle: string;
  displayedColumns: Array<string> = ['id', 'name'];
  dialogType: number;
  email: string;
  messages: Array<IMessageBody> = [];
  medicalLineSource: Array<string>;
  specialitiesSource: Array<string>;
  sciNamesSource: Array<string>;
  isLoading: boolean;
  private EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  constructor(
    private toast: ToastService, 
    public dialogRef: MatDialogRef<NumOfSuggSciNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InquiryDataTypeSource, 
    private serv: InquiryService) { }
 

  ngOnDestroy(): void {
    this.onDestroy$.next(); 
  }
  
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    
    if(this.data.type === 1) {
      this.dialogType = 1;
      this.dialogTitle = 'Inquiry Source';
      this.medicalLineSource = this.data.row.inquiryMedicalLines;
      this.specialitiesSource = this.data.row.inquirySpecialities
      this.sciNamesSource = this.data.row.inquiryScientificNames;
    } 
    else if (this.data.type === 2) {
      this.dialogType = 2;
      this.displayedColumns = ['id', 'name'];
      this.dialogTitle = 'All Scientific Name';
      this.serv.getSciNamesForManu(this.data.row.manufacturerId).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) =>{
        this.renderData(res.data);
      });
    } 
    else if(this.data.type === 3) {
      this.dialogType = 3;
      this.displayedColumns = ['id', 'name'];
      this.dialogTitle = 'All specialities';
      this.serv.getSpecialitiesForManu(this.data.row.manufacturerId).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) =>{
        this.renderData(res.data);
      });
    } 
    else if (this.data.type === 4) {
      this.dialogType = 4;
      this.displayedColumns = ['id', 'name'];
      this.dialogTitle = 'All Medical Line';
      this.serv.getMedicalLinesForManu(this.data.row.manufacturerId).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) =>{
        this.renderData(res.data);
      }); 
    } 
    else if (this.data.type === 5) {
      this.dialogType = 5;
      this.displayedColumns = ['id', 'firstName', 'email'];
      this.dialogTitle = 'Users';
      this.renderData(this.data.row.users);
    } 
    else if (this.data.type === 6) {
      this.dialogType = 6;
      this.dialogTitle = 'Add Email';
    }
    else if (this.data.type === 7) {
      this.isLoading = true;
      this.dialogType = 7;
      this.dialogTitle = 'Inquiries Messages';
      this.serv.getMessages(this.data.row.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) =>{
        this.messages = res.data || [];
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
    }

  }

  renderData(data) {
    this.dataSource = new MatTableDataSource<any>(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  close(data){
    this.dialogRef.close(data);
  }


  addemail() {

    if (!this.EMAIL_REGEX.test(this.email)) {
      this.toast.error('Invalid email');
    } else  {
      let data: AddEmailRequest = {
        email: this.email,
        inquiryId: this.data.row.id
      }
      this.serv.addEmail(data).subscribe((res: any) => {
        this.dialogRef.close(res);
      });
    }



  }

}

export class Languages {
    id: number;
    pageId: number;
    moduleId: number;
    userId: number;
    key: string;
    language: string;
    value: string;
    pageName: string;
    path: string;
    deletedAt: Date;
    createdAt: Date;
    updatedAt: Date;
    languageId: number;
    langs: any;
}

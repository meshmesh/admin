import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, } from '@angular/forms';
import { BulkService } from '../bulk.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, ReplaySubject } from 'rxjs';
import { Bulk } from '../bulk';

import { Bulktocreate } from '../bulktocreate';
import { TooltipPosition } from '@angular/material';

import { takeUntil } from 'rxjs/operators';
import { ToastService } from 'src/app/_services/toast.service';


@Component({
  selector: 'app-add-edit-bulk',
  templateUrl: './add-edit-bulk.component.html',
  styleUrls: ['./add-edit-bulk.component.scss']
})
export class AddEditBulkComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  removable = true;
  bulktocreate: Bulktocreate;
  position: TooltipPosition = 'above';

  bulkForm: Bulk;
  specialities: any[] = [];
  specialitiesFilter: Observable<any>;
  manufacturerFilter: Observable<any>;
  filterdmanufacturer: Observable<any>;
  manufacturer: any[] = [];
  seletedmanufacturer: any[] = [];
  filteredscientificNames: Observable<any>;
  scientificNames: any[] = [];
  seletedscientificNames: any = [];
  countriesFilteration: Observable<any>;
  countries: any[] = [];
  filteredSellingPoints: Observable<any>;
  sellingPoints: any[] = [];
  seletedSellingPoints: any[] = [];
  baseImagePreview = [];
  imagePreview: any[];
  showmore: boolean = false;
  smbutton: any = 'Show';
  filteredBuyers: Observable<any>;
  buyers: any[] = [];
  seletedBuyers: any[] = [];
  catalogPreview = [];
  logoPreview = [];
  localBaseImage: File[] = [];
  logoFileName: any;
  images: any[];
  baseImageFileName: any;
  localImages: File[] = [];
  logoImage: File[] = [];
  catalogImage: File[] = [];
  catalogname: any;
  specialityList  :any[] = [];
  public editor;
  manufacturerlist: any[];
  public editorContent: string;
  public editorConfig = {
    placeholder: 'Description'
  };
  showspecilaity:any;
  showscientificname:any;
  constructor(
    private fb: FormBuilder, 
    private service: BulkService, 
    private router: ActivatedRoute,
    private toast: ToastService,
  ) { }

  ngOnInit() {

    this.bulkForm = new Bulk();
    this.bulkForm.Id = this.router.snapshot.params['id'];
   this.service.getSpeciality().pipe(takeUntil(this.onDestroy$)).subscribe((res:any )=>{
    this.specialityList = res;
   })
    this.service.getMadeIn().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {


        this.countries = response;
        this.countriesFilteration = of(this.countries);
        this.service.getbyid(this.bulkForm.Id).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
          this.showscientificname = res[0].scientificName.display
          this.showspecilaity = res[0].specialities.name
          this.service.getScientificNameById(res[0].scientificNameId).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
            this.bulkForm.scientificname = result[0];
            this.bulkForm.scientificname.nameDisplay = this.bulkForm.scientificname.scientificName + '-' + this.bulkForm.scientificname.speciality.name;
            this.seletedscientificNames.push(this.bulkForm.scientificname);

          })

          this.bulkForm.productName = res[0].productName;
          this.bulkForm.productSubtitle = res[0].productSubtitle;
          this.bulkForm.productLink = [res[0].productLink];
          this.bulkForm.subSpecifications = res[0].subSpecifications;
          this.service.GetSpecialityById(res[0].specialtyId).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
            this.bulkForm.specialty = result;
            this.specialities = this.bulkForm.specialty;
            this.displayFnSpicality(this.specialities)
          })

          this.bulkForm.madeIn = this.countries.find(c => c.id === res[0].madeIn);
          this.bulkForm.scientificNameId = res[0].scientificNameId;
          this.bulkForm.description = res[0].description;
          this.bulkForm.refererLink = [res[0].refererLink];
          this.baseImagePreview = res[0].baseImageFileName;
          this.localBaseImage = res[0].baseImageFileName;
          this.logoPreview = res[0].logoFileName;
          this.logoFileName = res[0].logoFileName;
          this.images = JSON.parse(res[0].images);
          this.baseImageFileName = res[0].baseImageFileName;
          this.catalogPreview = res[0].catalogueLink;
          this.catalogname = res[0].catalogueLink;
          this.catalogImage = res[0].catalogueLink;

          this.imagePreview = JSON.parse(res[0].images);


        });

      },
      error => console.log(error)
    );





    this.service.getBuyers().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.buyers = response;
        this.filteredBuyers = of(this.buyers);
      },
      error => console.log(error)
    );


    this.manufacturerFilter = this.service.getManufacturersuggestions('a');

    this.service.getSpeciality().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.specialities = response;
        this.specialitiesFilter = of(this.specialities);
      },
      error => console.log(error)
    );

    this.filteredscientificNames = this.service.getScientificNameSuggestion('a');

    this.service.getSellingPoints().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.sellingPoints = response;
        this.filteredSellingPoints = of(this.sellingPoints);
      },
      error => console.log(error)
    );


  }
  AddSelectedBuyers(event) {

    this.seletedBuyers.push(event.option.value);
    let index = this.buyers.indexOf(event.option.value);
    if (index > -1) {
      this.buyers.splice(index, 1);

    }
  }
  removeBuyers(i) {
    let temp = this.seletedBuyers[i];
    this.buyers.push(temp);
    this.seletedBuyers.splice(i, 1);
  }
  _filterBuyers(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }


    return of(this.buyers.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
  _filterManufacturer(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();

    }


    return this.service.getManufacturersuggestions(filterValue);
  }

  handleTextChanging(event) {
    this.renderCompany(event);
  }

 

  renderCompany(searchvalue: string) {

    this.service.getManufacturersuggestions(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe(res => {

      this.manufacturerFilter = of(res);
      this.displayFnManufacturer(res);
    });

  }




  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.speciality;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialityList.filter(data => data.speciality.toLowerCase().indexOf(filterValue) === 0));
  }

  AddSelectedscientificNames(event) {
    this.seletedscientificNames.push(event.option.value);
    let index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);

    }
  }
  AddSelectedSellingPoints(event) {
    this.seletedSellingPoints.push(event.option.value);
    let index = this.sellingPoints.indexOf(event.option.value);
    if (index > -1) {
      this.sellingPoints.splice(index, 1);
    }
  }
  _filterSellingPoints(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }


    return of(this.sellingPoints.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }
  _filterCountry(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.country;
    } else {
      filterValue = value.toLowerCase();
    }


    return of(this.countries.filter(item => item.country.toLowerCase().indexOf(filterValue) === 0));
  }
  _filterscientificNames(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return this.service.getScientificNameSuggestion(`${filterValue}`)
  }
  removescientificNames(i) {
    let temp = this.seletedscientificNames[i];
    this.scientificNames.push(temp);
    this.seletedscientificNames.splice(i, 1);
  }

  removeSellingPoints(i) {
    let temp = this.seletedSellingPoints[i];
    this.sellingPoints.push(temp);
    this.seletedSellingPoints.splice(i, 1);
  }
  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }

  displayFnSpicality(speciality?: any): string | undefined {
    return speciality ? speciality.speciality : undefined;
  }
  displayFnScientificname(scientificName?: any): string | undefined {
    return scientificName ? scientificName.scientificName : undefined;
  }
  displayFnCounty(country?: any): string | undefined {
    return country ? country.country : undefined;
  }




  onEditorBlured(quill) {
    console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
    console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
  }

  moreoptions() {
    this.showmore = !this.showmore;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.showmore)
      this.smbutton = "Hide";
    else
      this.smbutton = "Show";
  };


  handleBaseImageChange(e) {
    this.localBaseImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.baseImagePreview.splice(1,e);
        this.baseImagePreview.push(reader.result);
      };
    }
  }
  handleLogoImageChange(e) {
    this.logoImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.logoPreview.pop();
        this.logoPreview.push(reader.result);
      };
    }
  }
  handleCatalogChange(e) {
    this.catalogImage[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.catalogPreview.pop();
        this.catalogPreview.push(reader.result);
      };
    }
  }
  hideimage(i) {
    this.imagePreview.splice(i, 1);
    
    this.localImages.splice(i, 1);
  }
  hideCatalog(i) {
    this.catalogPreview= [];
    this.catalogImage= [];

  }
  hidelogo(i) {
    this.logoPreview= [];
    this.logoPreview= [];
  }
  handleImageError(i) {
    this.imagePreview= [];
  }
  handleBaseImgError(i) {
  }
  handleCatalogError(i) {
    this.catalogPreview= [];
  }
 
  hideBaseImage() {
    this.baseImagePreview = [];
    this.localBaseImage = [];
  }

  handleImageChange(e) {

    for (let i = 0; i < e.target.files.length; i++) {
      this.localImages.push(e.target.files[i]);
      if (e.target.files && e.target.files[i]) {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[i]);
        reader.onload = e => {
          this.imagePreview.push({ url: reader.result });
        };
      }
    }
  }




  async submit(form) {
   
      
      if (form.invalid) {    
        return;
      }
      
    this.bulktocreate = new Bulktocreate();
    this.bulktocreate.BulkId = this.bulkForm.Id;
    if (this.bulkForm.manufacturerName) {
      this.bulktocreate.manufacturerId = this.bulkForm.manufacturerName.id;    }

    this.bulktocreate.productDescription = this.bulkForm.description;
    this.bulktocreate.productTitle = this.bulkForm.productName;
    this.bulktocreate.productSubtitle = this.bulkForm.productSubtitle;
    this.bulktocreate.CatalogURL = this.bulkForm.productCatalogueLink;
    this.bulktocreate.refererUrl = this.bulkForm.refererLink[0];
    if (this.bulkForm.specialty) {
      this.bulktocreate.specialityId = this.bulkForm.specialty.id;
    }
    this.seletedscientificNames.forEach(element => {
      this.bulktocreate.ScientificIds.push(element.id)
    });
    this.seletedBuyers.forEach(element => {
      this.bulktocreate.SoldToIds.push(element.id)
    });
    this.seletedSellingPoints.forEach(element => {
      this.bulktocreate.SellingToIds.push(element.id)
    });
    if (this.bulkForm.madeIn) {
      this.bulktocreate.madeIn = this.bulkForm.madeIn.id;
    }
    this.bulktocreate.baseImage = this.baseImageFileName;
    this.images
    this.images.forEach(element => {
      this.bulktocreate.Images.push(element.url) ;
    });
  
    this.bulktocreate.logoImage = this.logoFileName;
    this.bulktocreate.CatalogURL = this.catalogname;



     this.service.addUpdateProduct(this.bulktocreate).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.toast.success("Added Successfully");
      setTimeout(() => {
        this.Cancel();
      }, 1500);
    
    }) 


  }
  Cancel(){
    window.close();

  }
}

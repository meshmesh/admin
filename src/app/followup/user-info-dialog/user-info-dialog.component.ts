import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from 'src/app/_model/followup';

@Component({
  selector: 'app-user-info-dialog',
  templateUrl: './user-info-dialog.component.html',
  styleUrls: ['./user-info-dialog.component.scss']
})
export class UserInfoDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<UserInfoDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: User) {}

  ngOnInit() {}
}

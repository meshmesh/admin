import { Component, OnInit, Input } from '@angular/core';
import { SEO } from 'src/app/_model/SEO';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-seo-form',
  templateUrl: './seo-form.component.html',
  styleUrls: ['./seo-form.component.scss']
})
export class SeoFormComponent implements OnInit {
  constructor() {}

  @Input() seo: SEO;
  seoForm: FormGroup;

  ngOnInit() {
    this.seoForm = this.createForm();
    if (this.seo) {
      this.fillValues();
    }
  }

  createForm(): FormGroup {
    const group = new FormGroup({
      metaDescription: new FormControl(''),
      metaKeyWord: new FormControl(''),
      metaTitle: new FormControl('')
    });
    return group;
  }

  fillValues(): void {
    this.seoForm.setValue({
      metaDescription: this.seo.metaDescription,
      metaKeyWord: this.seo.metaKeyWord,
      metaTitle: this.seo.metaTitle
    });
  }

  createObject(): SEO {
    const data = this.seoForm.getRawValue();
    return { ...data, id: this.seo.id, name: this.seo.name };
  }
}

import { Component, OnInit } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ConversationsService } from './conversations.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  onDestroy$ = new ReplaySubject<void>();

  conversations: any[];
  selectedConversation: number;
  group_channel: any;
  constructor(private route: ActivatedRoute,
    private conversationService: ConversationsService, private router: Router) { }
  ngOnDestroy() {

    this.onDestroy$.next();
  }
  ngOnInit() {
    this.conversationService.init();
    this.route.data.pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      if (res.conversations) {
        res.conversations = _.uniqBy(res.conversations[0].groups, 'ID');
        this.conversations = res.conversations;
        this.conversations.forEach(element => {
          element._users = _.uniqBy(element._users, 'ID');
          this.subscribeToChannel(element.ID);
        });

      }

    });
    this.route.params.pipe(takeUntil(this.onDestroy$)).subscribe(param => {
      let groupId = parseInt(window.location.href.substring(window.location.href.lastIndexOf('/') + 1, window.location.href.length));
      if (!isNaN(groupId)) {
        this.selectedConversation = groupId;
      }
    });
  }


  subscribeToChannel(groupId) {
    let _this = this;
    this.conversationService.pusher.unsubscribe('private-' + groupId); //unsubscribe
    this.group_channel = this.conversationService.pusher.subscribe('private-' + groupId);
    this.group_channel.bind('new_message', function (data) {
      // do the ordering depends on date ;

    });
  }
  selectConversation(id) {
    this.selectedConversation = id;

    this.router.navigate(['inbox', id]);
  }
}

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SuggestedScientificNameService } from '../../services/suggested-scientific-name.service';
@Component({
  selector: 'app-replace-suggested-name',
  templateUrl: './replace-suggested-name.component.html',
  styleUrls: ['./replace-suggested-name.component.scss']
})
export class ReplaceSuggestedNameComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private serv: SuggestedScientificNameService, public dialogRef: MatDialogRef<ReplaceSuggestedNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,) { }
  Scientifics: any[] = [];
  Sci1: string = this.data.Sciente.valueItem;
  Sci2: any;

  submittedObj: any = {
    ScientificNameID: this.data.Sciente.id,
    OldScientificID: null
  }

  ngOnInit() {
    this.renderScientificName('');
  }

  handleScientificChanging(event) {
    this.renderScientificName(event);
  }

  renderScientificName(searchvalue: string) {
    this.serv.getScientificNameSuggestion(searchvalue).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.Scientifics = res.data.items;
      this.displayScientific(res.data.items);
    });
  }

  displayScientific(Scient) {
    return Scient ? Scient.name : undefined;
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  async submit(obj) {
    this.submittedObj.OldScientificID = obj.value.Sci2.id;
    this.serv.mergScientific(this.submittedObj.ScientificNameID, this.submittedObj.OldScientificID).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.close(null);
    })
  }
}
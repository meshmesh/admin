export class MedicalLine {
    id: number;
    imagePath: string;
    name: string;
}

export class ISpeciality {
    id: number;
    imagePath: string;
    name: string;
}

export class ScientificNameModel {
    Id: number;
    ScientificName: string;
    SpecialityId: number;
    IconImg: string;
    IsApproved: boolean;
    Note: string;
    Rank: number;
    MedicalLineId: number;
    RelatedTerms: number[] = [];
    BroaderTerms: number[] = [];
    NarrowerTerms: number[] = [];
    NonpreferredTerms: any[] = [];
    CategoryId: number;
    AttributeType: number[] = [];
    ScientificClassId: number[] = [];
    CreatedAt: string;
    
    CompanyIds: number[] = [];
    Nonpreferredtermsid: number[] = [];
    borderTerm: any = {};
    narrowTerm: any = {};
    relatedTerm: any = {};
    manufacturer: number[] = [];
    manufacturerId: number;
    

    alterNative: string;
    approvedAt: string;
    approvedBy: string;
    bt: Array<any>;
    createdAt: string;
    distributorCount: 0
    id: number;
    imagePath: string;
    isApproved: boolean;
    manufacturerCount: number;
    medicalLine: MedicalLine;
    name: string;
    nt: Array<any>;
    productCount: number;
    rt: Array<any>;
    specialities: Array<ISpeciality>;

    // AdminComment: string;
    // Details: string;
    // UsedFor: string;
    // ScientificClass: any = {};
    // attribute: any = {};
    // attributename: any[] = [];
    // ScientificClassname: any[] = [];
    // MarketAnalysis: boolean;
}


export class SciNamesSearchParams {
    PageSize: number = 10;
    PageNumber: number = 1;

    Id: string;
    Name: string;
    Approved: boolean;
    IsPictureFilled: boolean;
    SpecialityIds: any;
    MedicalLineIds: any;
    RTIds: any;
    BTIds: any;
    NTIds: any;
    CreationFromDate: any;
    CreationToDate: any;
    Alternatives: any;
}
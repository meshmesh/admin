import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Observable, of, ReplaySubject } from 'rxjs';
import { startWith, map, takeUntil } from 'rxjs/operators';
import { DynamicService } from './dynamic.service';
import { AutoCompleteService } from '../shared/filters/autocomplete-multi/autoComlete.service';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.scss']
})

export class DynamicComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  @ViewChild('psecialityInput') psecialityInput: ElementRef<HTMLInputElement>;
  @ViewChild('manufacturerInput') manufacturerInput: ElementRef<HTMLInputElement>;
  @ViewChild('distributorInput') distributorInput: ElementRef<HTMLInputElement>;




  countriesList: any[] = [];
  removable = true;
  value: string;
  viewValue: string;
  dynamicForm: FormGroup;
  tempForm: FormGroup;
  show = false;
  show2 = false;
  show3 = false;
  show4 = false;
  temp1: boolean;
  temp2: boolean;
  temp3: boolean;
  temp4: boolean;
  all = false;
  type: any[] = [
    { value: '1', viewValue: 'Product' },
    { value: '2', viewValue: 'Manufacturer' },
    { value: '3', viewValue: 'Distributor' },
    { value: '4', viewValue: 'Healthcare' }
  ];


  public editor;
  public editorContent: string;
  public editorConfig = {
    placeholder: 'Description'
  };
  
  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  selectedSpecialitiesList: any[] = [];
  
  scientificnamelist: any[] = [];
  scientificnameFilter: Observable<any>;
  selectedscientificname: any[] = [];
  medicallineslist: any[] = [];
  medicallinefilter: Observable<any>;
  selectedmedicalline : any[]=[];
  manufacturers: any[] = [];
  filteredmanufacturers: Observable<any>;
  selectedmanufacturers: any[] = [];
  filtereddistributors: Observable<any>;
  distributors: any[] = [];
  seleteddistributors: any[] = [];
  


  constructor(private fb: FormBuilder, private _service : DynamicService,
    private autoCompleteService: AutoCompleteService
     ) { }

  createCategoryForm() {
    this.dynamicForm = this.fb.group({
      title: new FormControl(''),
      subtitle: new FormControl(''),
      description: new FormControl(''),
      products: this.fb.group({
        product: new FormControl(''),
        scientificname: new FormControl(''),
        speciality: new FormControl(''),
        medicalline: new FormControl(''),
        country: new FormControl(''),
        regoin: new FormControl(''),
      }),
      manufacturers: this.fb.group({
        manufacturer: new FormControl(''),
        scientificname: new FormControl(''),
        speciality: new FormControl(''),
        medicalline: new FormControl(''),
        country: new FormControl(''),
        regoin: new FormControl(''),
      }),
      distributors: this.fb.group({
        distributor: new FormControl(''),
        scientificname: new FormControl(''),
        speciality: new FormControl(''),
        medicalline: new FormControl(''),
        country: new FormControl(''),
        regoin: new FormControl(''),
      }),
      healthCares: this.fb.group({
        healthCare: new FormControl(''),
        country: new FormControl(''),
        regoin: new FormControl(''),
        subcategory: new FormControl('')

      })

    });
  }
  ngOnInit() {
    this.createCategoryForm();
    this.loadscientificname();
    this.loadmanufacturer();
    this.loaddistributor();

    this._service.getMedicalLines2().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.medicallineslist = respone;
      this.medicallinefilter = of(this.medicallineslist);
      console.log(respone);
    });

    this._service.getCountrySuggestion().pipe(takeUntil(this.onDestroy$)).subscribe(data => {
      data.forEach(elem => {
        this.countriesList.push({ name: elem.country, id: elem.id });
      });
    });


      this._service.getSpecalities().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
        this.specialitieslist = respone;
        this.tempForm = this.dynamicForm.controls.products as FormGroup;

        this.specialitiesFilter = this.tempForm.controls.speciality.valueChanges
          .pipe(
            startWith(''),
            map(state => state ? this._filterSpeciality(state) : this.specialitieslist.slice())
          );
  
      });
      this.tempForm = this.dynamicForm.controls.products as FormGroup;
      this.tempForm.controls.scientificname.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(term => {
        if(term !==''){
         this.scientificnameFilter = this.autoCompleteService.getScientificNameSuggestion(term);
        }
      })


      
   
  }

  onEditorBlured(quill) {
    console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
    console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
  }

  loadmanufacturer(){
    this.tempForm = this.dynamicForm.controls.manufacturers as FormGroup;


    this.tempForm.controls.manufacturer.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(data => {
      if (data.name !== undefined) {
        this.filteredmanufacturers = this._service.getListOfManufacturer(data.name);
      } else if (data !== '') {
        this.filteredmanufacturers = this._service.getListOfManufacturer(data);
      }
    });
  }


loaddistributor(){
  this.tempForm = this.dynamicForm.controls.distributors as FormGroup;


  this.tempForm.controls.distributor.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(data => {
    if (data.name !== undefined) {
      this.filtereddistributors = this._service.getListOfDistributors(data.name);
    } else if (data !== '') {
      this.filtereddistributors = this._service.getListOfDistributors(data);
    }
  });

}

  
loadscientificname(){
  this.autoCompleteService.getScientificNameSuggestion('a').pipe(takeUntil(this.onDestroy$)).subscribe(
    (response: any) => {
      this.scientificnamelist = response;
      this.scientificnameFilter = of(this.scientificnamelist);
    },
    error => console.log(error)
  );
}

Addseleteddistributors(event) {

  this.seleteddistributors.push(event.option.value);
  let index = this.distributors.indexOf(event.option.value);
  if (index > -1) {
    this.distributors.splice(index, 1);
    this.distributorInput.nativeElement.value = "";

  }
this.distributorInput.nativeElement.value = "";
}
removedistibutors(i) {
  let temp = this.seleteddistributors[i];
  this.distributors.push(temp);
  this.seleteddistributors.splice(i, 1);
}
removemanufacturer(i) {
  let temp = this.selectedmanufacturers[i];
  this.manufacturers.push(temp);
  this.selectedmanufacturers.splice(i, 1);
}

AddseletedManufacturer(event) {
  let exist = this.selectedmanufacturers.map(function (x) { return x.id; }).indexOf(event.option.value.id);
  if (exist === -1) {
    this.selectedmanufacturers.push(event.option.value);
  }

  let index = this.selectedmanufacturers.map(function (x) { return x.id; }).indexOf(event.option.value.id);
  if (index > -1) {
    this.manufacturers.splice(index, 1);
    this.manufacturerInput.nativeElement.value = "";
  }



}

  AddselectedScientificname(event) {

    let exist = this.selectedscientificname.map(function (x) { return x.id; }).indexOf(event.option.value.id);
    if (exist === -1) {
      this.selectedscientificname.push(event.option.value);
    }


    this.scientificnameFilter.pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.scientificnamelist = res;
      let index = this.scientificnamelist.map(function (x) { return x.id; }).indexOf(event.option.value.id);

      if (index > -1) {
        this.scientificnamelist.splice(index, 1);
/*         this.nameInput.nativeElement.value = ""; */

this.scientificnameFilter = of(this.scientificnamelist);
      }

    });




  }
  removeScientificname(i) {
    let temp = this.selectedscientificname[i];
    this.scientificnamelist.push(temp);
    this.selectedscientificname.splice(i, 1);
  }
  
  removeMedicalline(i) {
    let temp = this.selectedmedicalline[i];
    this.medicallineslist.push(temp);
    this.selectedmedicalline.splice(i, 1);
  }

  AddselectedSpeciality(event) {

    this.selectedSpecialitiesList.push(event.option.value);
    let index = this.specialitieslist.indexOf(event.option.value);
    if (index > -1) {
      this.specialitieslist.splice(index, 1);
       this.psecialityInput.nativeElement.value = "";
      this.tempForm = this.dynamicForm.controls.products as FormGroup;
    }

  }
  AddselectedMedicalline(event) {

    this.selectedmedicalline.push(event.option.value);
    let index = this.medicallineslist.indexOf(event.option.value);
    if (index > -1) {
      this.medicallineslist.splice(index, 1);
      this.tempForm = this.dynamicForm.controls.products as FormGroup;
    }

  }
  removeSpeciality(i) {
    let temp = this.selectedSpecialitiesList[i];
    this.specialitieslist.push(temp);
    this.selectedSpecialitiesList.splice(i, 1);
  }
  _filterSpeciality(value: any): any[] {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.speciality;
    } else {
      filterValue = value.toLowerCase();
    }
    return this.specialitieslist.filter(data => data.speciality.toLowerCase().indexOf(filterValue) === 0);
  }

  _filterMedicalLine(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.Name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.medicallineslist.filter(item => item.Name.toLowerCase().indexOf(filterValue) === 0));
  }

  prodradio(event) {
    this.show = !this.show;
  }

  manuRadio(event) {
    this.show2 = !this.show2;
  }

  distRadio(event) {
    this.show3 = !this.show3;
  }

  healthRadio(event) {
    this.show4 = !this.show4;
  }
  onchange(event) {

    this.f.category.patchValue(event.value)
  }
  get f() {
    return this.dynamicForm.controls;
  }
  get p() {
    return this.tempForm.controls;
  }
  async submit(){
   
  
  console.log(this.dynamicForm.value);
  }




}
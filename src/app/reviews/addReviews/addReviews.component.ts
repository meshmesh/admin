import { Component, OnInit, Inject } from "@angular/core";
import { ReviewsService } from "../reviews.service";
import { ReviewForm } from "./ReviewForm";
import { Guid } from "guid-typescript";
import { S3 } from "aws-sdk/clients/all";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
  // tslint:disable-next-line: component-selector
  selector: "app-addReviews",
  templateUrl: "./addReviews.component.html",
  styleUrls: ["./addReviews.component.scss"]
})
export class AddReviewsComponent implements OnInit {
  imgurl: any = "https://s3-us-west-2.amazonaws.com/company/avatar";
  testurl: any =
    "https://aumet-data.s3.us-west-2.amazonaws.com/company/avatar/";

  localImages: File[] = [];
  imagePreview = [];
  submitted = true;
  picture: string;
  name: string;
  position: string;
  title: string;
  rating: number;
  quotation: string;
  Published: boolean = null;
  Type: number;
  reviewForm: ReviewForm;
  showimagevalidator = false;
  selectedType: string;
  constructor(
    private serv: ReviewsService,
    protected dialogRef: MatDialogRef<AddReviewsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      dialogData: ReviewForm;
      dialogType: "create" | "edit";
    }
  ) {}

  ngOnInit() {
    this.reviewForm = new ReviewForm();
    if (this.data.dialogType === "edit") {
      this.reviewForm = this.data.dialogData;
      this.name = this.reviewForm.name;
      this.picture = this.reviewForm.picture;
      this.rating = this.reviewForm.rating;
      this.quotation = this.reviewForm.quotation;
      this.position = this.reviewForm.position;
      this.imagePreview[0] = this.reviewForm.picture;
      this.title = this.reviewForm.title;
      this.Type = this.reviewForm.type;
    }
  }

  uploadVideoImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: "aumet-data",
      Key:
        "company/avatar/" +
        Guid.create() +
        "." +
        this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function(resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function(err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  handleBaseImageChange(e) {
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }

  handleImageError(i) {
    this.imagePreview.splice(i, 1);
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }

  handleBaseImgError() {
    this.imagePreview.pop();
  }

  submit(ad) {
    if (this.imagePreview[0] == null || this.imagePreview[0] == undefined || this.imagePreview[0].length == 0) {
      this.showimagevalidator = true;
      return;
    }
    this.reviewForm.name = this.name;
    this.reviewForm.position = this.position;
    this.reviewForm.title = this.title;
    this.reviewForm.rating = this.rating;
    this.reviewForm.quotation = this.quotation;
    this.reviewForm.type = +this.Type;
    if (this.localImages.length > 0) {
      this.uploadVideoImage(this.localImages[0], e => {
        this.reviewForm.picture = e;
        this.submitted = false;
        this.serv.addUpdateReviews(this.reviewForm).subscribe(rese => {
          this.dialogRef.close();
        });
      });
    } else {
      this.serv
        .addUpdateReviews(this.reviewForm)
        .subscribe(e => this.dialogRef.close());
    }
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value > 5) {
      return Math.round(value / 5);
    } else if (value === 5) {
      return (value = 5);
    }

    return value;
  }

  close(data): void {
    this.dialogRef.close(data);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { DistributorModel, DistributorSearchParams } from '../model/distributor-model';
import { ManufacturerModel } from '../../../pages/manufacturer/model/manufacturer.model';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DistributorService {

  APIEndpoint = environment.ApiUrlV3;

  constructor(private http: HttpClient) {}


  getCOmpanies(name): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminManufacturer/search?PageNumber=1&PageSize=10&companyType=distributor&companyName=' +
      name);
  }


  getmanufacturer(id): Observable<any[]> {
    return this.http.get<ManufacturerModel[]>(
      this.APIEndpoint + 'AdminManufacturer/' + id
    );
  }


  GetScientificNameIds(str) {
    const url =
      this.APIEndpoint +
      `AdminScientificNames/GetScientificNameIds?input=${str}`;
    return this.http.get<any>(url);
  }


  GetScientificNames(str) {
    const url =
      this.APIEndpoint +
      `AdminScientificNames/ScientificnameFilter?PageSize=10&PageNumber=1&nameinput=${str}`;
    return this.http.get<any>(url);
  }


  getAll(data: DistributorSearchParams, queryParams: string) {
    let URL = this.APIEndpoint + `AdminDistributor/search?PageNumber=${data.PageNumber}&PageSize=${data.PageSize}&`;
    URL += queryParams;
    
    // append data search params 
    URL += data.CompanyName ? `&CompanyName=${encodeURIComponent(data.CompanyName)}` : '';
    URL += data.CompanyId ? `&CompanyId=${data.CompanyId}` : '';
    URL += data.CompanyRegistered ? `&CompanyRegistered=${data.CompanyRegistered}` : '';
    URL += data.IsCrawled ? `&IsCrawled=${data.IsCrawled}` : '';
    return this.http.get(URL);
  }

  GetIntrest(pageNumber, id): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminDistributorInterest?PageSize=10&PageNumber='+pageNumber+'&DistributorId=' + id );
  }
  GetBusinessOpp(pageNumber, id): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminDistributorOpportunities/All?PageSize=10&PageNumber='+pageNumber+'&DistributorId=' + id );
  }

  GetExperiance(pageNumber, id): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminDistributorExperience?PageSize=10&PageNumber='+pageNumber+'&DistributorId=' + id );
  }

  GetIntrest2(pagenumber): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + 'AdminInterest/Search?PageSize=10&PageNumber='+pagenumber);
  }
  
  filterDistQa(pageNumber, data): Observable<DistributorModel[]> {
    return this.http.get<DistributorModel[]>(
      this.APIEndpoint + 'AdminDistributor/Search?CompanyRegistered=false&PageSize=10&PageNumber=' + pageNumber +'&' + data
    );
  }

  get(id): Observable<any[]> {
    return this.http.get<DistributorModel[]>(this.APIEndpoint + 'AdminDistributor?Id=' + id);
  }

  ActivateDeactivateDistributor(data: any): Observable<any> {
    return this.http.put(this.APIEndpoint + "AdminDistributor/Activate", data);
  }

  getListOfManufacturer(name): Observable<any> {
    return this.http.get<any>(
      this.APIEndpoint + 'Companies/GetListOfCompaniesLimited?Name=' + name + '&Type=manufacturer'
    );
  }
  getSeo() {
    const url = this.APIEndpoint + 'portals/getportals';
    return this.http.get(url);
  }
  addShareProfile(data) {
    return this.http.post(this.APIEndpoint + 'AdminShardProfiles/Add', data);
  }

  viewProfile(obj) {
    return this.http.get(this.APIEndpoint + 'Auth/RedirectLinks'+ obj)
  }

  addAdminCrawllerdata(data) {
    return this.http.put(this.APIEndpoint + 'AdminCrawller', data);
  }

  DemoQualifications(obj) {
    return this.http.post(this.APIEndpoint + 'Users/AddRequestLoginDemo', obj);
  }
  deleteDistributor(id): Observable<any> {
    return this.http.delete<any>(this.APIEndpoint + 'AdminDistributor?DistributorId=' + id);
  }

  deleteDistributorExp(id): Observable<any> {
    return this.http.delete<any>(this.APIEndpoint + 'AdminDistributorExperience?Id=' + id);
  }

  deleteDistributorInt(id): Observable<any> {
    return this.http.delete<any>(this.APIEndpoint + 'AdminDistributorInterest?Id=' + id);
  }

  getSpecalities(): Observable<any> {
    return this.http.get<any>(this.APIEndpoint + 'Specialities/GetSpecialities');
  }
  getmedicalline(): Observable<any> {
    return this.http.get<any>(this.APIEndpoint + 'MedicalLines/GetMedicalLines');
  }

  addCommentdata(data) {
    return this.http.put(this.APIEndpoint + 'AdminDistributor/Comment', data);
  }

  loadCallTypes() {
    const url = this.APIEndpoint + 'Lookups/Get?MajorCode=21';
    return this.http.get(url);
  }

  addcalllog(data) {
    data.CallDate = new Date(data.CallDate, data.time);
    data.CallDate = data.date;
    return this.http.post(this.APIEndpoint + 'AdminCalls/Add', data);
  }

  // updateScientificName(scientificNamesIds: number[], token: any): Observable<any> {
  //   return this.http.post(`${this.APIEndpoint}AdminDistributors/UpdateSpecialitySceitificName`, {
  //     ScientificIds: scientificNamesIds,
  //     token: token
  //   });
  // }

  mergCompanies(data){
    return this.http.put(this.APIEndpoint + 'AdminDistributor/Merge',data);
  }
 
  
  getComp(param){
    return this.http.get<any[]>(this.APIEndpoint + 'companies/GetCompanyView?id=' + param)
  }

  // getContracts(id){
  //   return this.http.get(this.APIEndpoint + 'AdminDistributors/GetDistributorContracts?CompanyID=' + id)
  // }


  getManufacturersuggestions(str): Observable<any> {
    return this.http.get<any>(this.APIEndpoint + 'Companies/GetListOfCompaniesLimited?Name=' + str + '&Type=manufacturer');
  }
  updateSeo(obj) {
    const url = this.APIEndpoint + 'AdminManufacturer/Update';
    return this.http.post(url, obj);
  }
  // GetClientManufacturer(str): Observable<any> {
  //   return this.http.get<any>(this.APIEndpoint + 'AdminManufacturer/search?PageNumber=1&client=true&PageSize=10&companyType=manufacturer&companyName=' + str );
  // }

  getCountrySuggestion(): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + 'Countries/GetCountries'
    );
  }


  signDistributor(form){
    return this.http.post(this.APIEndpoint + 'AdminDistributors/AddDistributorClient', form);
  }

  getCompanyContact(CompanyId){
    return this.http.get(this.APIEndpoint + 'AdminCompanyContact/Get/' + CompanyId);
  }


  getCompanyContactbyid(id){
    return this.http.get(this.APIEndpoint + 'AdminDistributors/ClientById?id=' + id);
  }
}

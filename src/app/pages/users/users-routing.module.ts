import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {
      title: 'Users',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  },
  {
    path: 'add/:id',
    loadChildren: './addUpdate-user/addUpdate-user.module#AddUpdateUserModule'
  },
  {
    path: 'add',
    loadChildren: './addUpdate-user/addUpdate-user.module#AddUpdateUserModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}

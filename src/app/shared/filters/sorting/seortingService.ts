import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class SortingService {
    constructor(private http: HttpClient) { }

    getArticleLookup() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=16';
        return this.http.get(url);
    }

    getScientificNameLookups() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=16';
        return this.http.get(url);
    }

    getProductLookups() {
        const url = APIEndpoint + '/api/Lookups/Get?MajorCode=16';
        return this.http.get(url);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule, MatSelectModule, MatButtonModule, MatTableModule, MatInputModule, MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { FilterModule } from '../../filter/filter.module';


@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatCardModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,  
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule,
    FilterModule

 
  ],
  declarations: [UsersComponent,],

  
})
export class UsersModule { }

 
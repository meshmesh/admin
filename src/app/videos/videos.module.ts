import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideosComponent } from './videos.component';
import { SharedModule } from '../shared/shared.module';
import { VideosDirective } from './videos.directive';

@NgModule({
   imports: [
      CommonModule,
      SharedModule,
   ],
   declarations: [
      VideosComponent,
      VideosDirective
   ],
   exports: [
      VideosComponent
   ]
})
export class VideosModule { }

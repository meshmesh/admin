import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ContactService } from './contact.service';

@Injectable()
export class ContactResolver implements Resolve<Array<any>> {
  constructor(private data: ContactService, private router: Router) {}
  resolve(): Observable<Array<any>> {
    return this.data.getContactsall().pipe(
        catchError(error => {
            this.router.navigate(['']);
            return of(null);
        })
    );
}
}

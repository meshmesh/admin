import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-deleted-puplish-popup',
  templateUrl: './deleted-puplish-popup.component.html',
  styleUrls: ['./deleted-puplish-popup.component.scss']
})
export class DeletedPuplishPopupComponent implements OnInit {
  @Input() popupData;
  type: string;
  action: string;
  text:string;
  constructor() { }

  ngOnInit() {
    this.type = this.popupData.type;
    this.action = this.popupData.action;
    this.text = this.popupData.text;
  }

}

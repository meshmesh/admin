import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { take, tap, switchMap, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { of, ReplaySubject } from 'rxjs';

import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';
import { SuggestedScientificNameService } from '../services/suggested-scientific-name.service';
import { ScientificNameModel } from '../model/scientific-name.model';
import { ReplaceSuggestedNameComponent } from './replace-suggested-name/replace-suggested-name.component';
import { ApproveScientificNameComponent } from './approve-scientific-name/approve-scientific-name.component';

@Component({
  selector: 'app-suggested-scientific-name',
  templateUrl: './suggested-scientific-name.component.html',
  styleUrls: ['./suggested-scientific-name.component.scss']
})
export class SuggestedScientificNameComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator ) paginator: MatPaginator;


  onDestroy$ = new ReplaySubject<void>();
  displayedColumns: string[] = [
    'actions',
    'name',
    'company',
    'date'
  ];

  dataSource: MatTableDataSource<ScientificNameModel>;
  length: number;
  pageIndex = 1;
  pageSize = 10;
  isLoading: boolean;


  constructor(private service: SuggestedScientificNameService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getSuggestedScientificNames();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  getSuggestedScientificNames(): void {
    this.isLoading = true;
    this.service
      .getall(this.pageSize, this.pageIndex)
      .pipe(
        take(1),
        tap((res: any) => {
          this.isLoading = false;
          this.length = res.data.count;
          this.dataSource = new MatTableDataSource<any>(res.data.items);
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  onDelete(element): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Suggested Scientific Name',
        id: element.id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this.service.deleteSuggestedScientificName(element.id) : of(undefined);
        }),
        tap((res: number) => {
          if (res) {
            this.getSuggestedScientificNames();
          }
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  ReplaceSuggested(Sciente) {
    const dialogRef = this.dialog.open(ReplaceSuggestedNameComponent, {
      width: '65%',
      data: { Sciente }
    }); 
    dialogRef.afterClosed().pipe(
      tap(() => {
        this.getSuggestedScientificNames();
      })
    ).pipe(takeUntil(this.onDestroy$)).subscribe();
  }


  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getSuggestedScientificNames();
  }

  OpenAddUpdate(element) {
    const dialogRef = this.dialog.open(ApproveScientificNameComponent, {
      width: "65%",
      data: element,
    });

    dialogRef
      .afterClosed()
      .pipe(
        tap(() => {
          this.getSuggestedScientificNames();
        })
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe();
  }
}

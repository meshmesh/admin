import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ContactForm } from './contactForm';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material';

import { ManufacturerService } from '../pages/manufacturer/services/manufacturer.service';
import { ToastService } from '../_services/toast.service';

@Component({
  selector: 'app-companyContact',
  templateUrl: './companyContact.component.html',
  styleUrls: ['./companyContact.component.scss']
})
export class CompanyContactComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(
    private manuService: ManufacturerService, 
    @Inject(MAT_DIALOG_DATA) public data,
    private toast: ToastService ) { }

  countries: any = []
  titles: any = []
  cForm: ContactForm
  type: any;


  FirstName: any
  LastName: any
  PhoneNumber: any
  CountryId: number
  CompanyId: number
  Email: any
  Note: any
  JobTitle: any







  ngOnInit() {
    this.cForm = new ContactForm();

    this.manuService.getCountry().subscribe(x => {
      this.countries = x
    })
    if (this.data.Data != undefined  ) {
      
      this.CompanyId = this.data.Data.CompanyId;
      this.type = this.data.type;
      this.manuService.getContactId(this.data.Data.ContactId).subscribe((res:any) =>{

        this.cForm.CompanyId = this.CompanyId;
        this.CountryId = res.countryId;
        this.Email = res.email;
        this.FirstName = res.firstName;
        this.JobTitle = res.jobTitle;
        this.LastName = res.lastName;
        this.Note = res.note;
        this.PhoneNumber = res.phoneNumber;
        this.cForm.Id = res.id
      })
    }else if(this.data.company != undefined ) {
      this.type = this.data.type;
      this.CompanyId = this.data.company.id
 
    }


  }

  handleJobChanging(event) {
    this.renderJobTitle(event, false);
  }

  renderJobTitle(searchvalue: string, updateMode: boolean) {
    this.manuService.getJobTitle(searchvalue, this.type).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.titles = res;
    });
  }

  displayJob(job) {
    return job ? job : undefined;
  }

  submit() {
this.cForm.CompanyId = this.CompanyId
this.cForm.CountryId = this.CountryId
this.cForm.Email = this.Email
this.cForm.FirstName = this.FirstName
this.cForm.JobTitle = this.JobTitle
this.cForm.LastName = this.LastName
this.cForm.Note = this.Note
this.cForm.PhoneNumber = this.PhoneNumber

this.manuService.addCompanyContact(this.cForm).subscribe(x => {
  if(x == true){
    this.toast.success('Added Successfully');
  }
  else{
    this.toast.error('An error issued')
  }
})
  }


}

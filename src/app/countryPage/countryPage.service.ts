import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const APIEndpoint = environment.ApiUrl;

@Injectable()
export class CountryPageService {
  constructor(private http: HttpClient) {}

  getCOuntryPage(pageNumber, pageSize) {
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/SearchCountryPage?pagenumber=' + pageNumber + '&pagesize=' + pageSize);
  }
  addCOuntryPage(form) {
    return this.http.post(APIEndpoint + '/api/AdminCountryPage/addupdateCountry', form);
  }
  addStory(form) {
    return this.http.post(APIEndpoint + '/api/AdminCountryPage/addupdateCountrystory', form);
  }

  getCountryById(id) {
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/CountryById?Id=' + id);
  }

  GetPageStoryById(id) {
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/GetPageStoryById?Id=' + id);
  }

  getCountries() {
    return this.http.get(APIEndpoint + '/api/Countries');
  }

  GetStoryByCountryId(id) {
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/GetStoryByCountryId?Id=' + id);
  }
  DeleteCountry(id) {
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/DeleteCountry?Id=' + id);
  }

  DeleteCountryStory(id){
    return this.http.get(APIEndpoint + '/api/AdminCountryPage/DeleteCountryStory?Id=' + id);
  }

  UpdateSEO(seo): Observable<any[]>{
    return this.http.post<any[]>(APIEndpoint + '/api/AdminCountryPage/UpdateSeo',seo);
  }
  Publish(id) {
    return this.http.post(
      APIEndpoint + '/api/AdminCountryPage/Publish?Id='+id,{}    );
  }
  unPublish(id) {
    return this.http.post(
      APIEndpoint + '/api/AdminCountryPage/UnPublish?Id='+id,{}  
    );
  }
  
}

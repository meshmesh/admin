import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewMatchingComponent } from './newMatching.component';
import { NewMatchingRoutingModule } from './newmatching-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TagInputModule } from 'ngx-chips';
import { MatTooltipModule, MatExpansionModule, MatGridListModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatIconModule, MatToolbarModule, MatCheckboxModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteService } from '../shared/filters/autocomplete-multi/autoComlete.service';
import { NewdistributorViewComponent } from './newdistributor-view/newdistributor-view.component';
import { NewmanufacturerViewComponent } from './newmanufacturer-view/newmanufacturer-view.component';

@NgModule({
  imports: [
    CommonModule,
    NewMatchingRoutingModule,
    
    SharedModule,
    TagInputModule,
    MatTooltipModule,
    MatExpansionModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    MatIconModule,
    MatToolbarModule,
    MatCheckboxModule,
    NgxMaterialTimepickerModule.forRoot(),
    DataTableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [NewMatchingComponent,NewdistributorViewComponent,NewmanufacturerViewComponent],
  providers: [AutoCompleteService]

})
export class NewMatchingModule { }

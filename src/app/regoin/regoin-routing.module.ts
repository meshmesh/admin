import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegoinComponent } from './regoin.component';
const routes: Routes = [
  {
    path: '',
    component: RegoinComponent,
    data: {
      title: 'Region',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    }
  
  }
 


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegoinRoutingModule { }

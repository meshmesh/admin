export class ProductToCreate {
    id: number; 
    productTitle: string;
    productSubtitle: string;
    baseImage: string; 
    productDescription: string; 
    specialityId: number; 
    slug: string;
    productrangeId: number;
    manufacturerId: number;
    createdAt: Date; 
    updatedAt: Date; 
    madeIn: number;
    aumetComment: string;
    published: boolean;
    seo: string; 
    SoldToIds: number[] = [];
    SellingToIds: number[] = [];
    ScientificId: number;
    SEOOptimized: boolean;
    Ecommerce: boolean;
    CatalogURL: string; 
    Images:  string[] = []; 
}


import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { LanguagesService } from '../languages.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Languages } from '../languages';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-addLanguage',
  templateUrl: './addLanguage.component.html',
  styleUrls: ['./addLanguage.component.scss']
})
export class AddLanguageComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private serv: LanguagesService, public dialogRef: MatDialogRef<AddLanguageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  languageList: any = [];
  language: Languages;

  ngOnInit() {
    this.language = new Languages();

    this.serv.getLangugaesId(50).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.languageList = res;
      const lan = this.languageList.find(c => c.langs === this.language.langs);
      this.language.langs = lan;
    });
  }

  async submit(obj) {
    if (obj.invalid) {
      return;
    }

    this.language.languageId = this.language.langs.langId;
    this.language.language = this.language.langs.name;

    this.serv.addUpdate(this.language).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      
      this.dialogRef.close('success');

    });
  }

  close(data): void {
    this.dialogRef.close(data);
  }

}

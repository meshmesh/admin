import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManufacturerRoutingModule } from "../manufacturer/manufacturer-routing.module";
import { SharedModule } from "../../shared/shared.module";

import { QuillEditorModule } from "ngx-quill-editor";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxCarouselModule } from "ngx-carousel";

import { AutoCompleteService } from "../../shared/filters/autocompletesingle/autocompletesingle.service";
import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { TagInputModule } from "ngx-chips";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { ClipboardModule } from "ngx-clipboard";
import { FilterModule } from "../../filter/filter.module";
import { ManufacturerActionsComponent } from "./components/manufacturer-actions/manufacturer-actions.component";
import { HttpClientModule } from "@angular/common/http";
import { AddUpdateUserComponent } from "../users/addUpdate-user/addUpdate-user.component";
import { MatTooltipModule, MatDialogModule } from "@angular/material";
import { AddUpdateUserModule } from "../users/addUpdate-user/addUpdate-user.module";
import { ManufacturerComponent } from "./manufacturer.component";
import { DataTableModule } from "angular2-datatable";
import { MergeManuComponent } from "./components/merge-manu/merge-manu.component";
import { AddEditCompanyComponent } from "./components/add-edit-company/add-edit-company.component";
import { AddEditScinameComponent } from "./components/add-edit-sciname/add-edit-sciname.component";
import { AddManuClientComponent } from "./components/add-manu-client/add-manu-client.component";



@NgModule({
  imports: [
    CommonModule,
    ManufacturerRoutingModule,
    SharedModule,
    QuillEditorModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTableModule,
    NgxCarouselModule,
    NgxMaterialTimepickerModule.forRoot(),
    TagInputModule,
    NgxMatSelectSearchModule,
    NgxMaterialTimepickerModule.forRoot(),
    ClipboardModule,
    FilterModule,
    MatTooltipModule,
    AddUpdateUserModule,
    MatDialogModule,
  ],
  exports: [    
    ManufacturerActionsComponent,
    AddUpdateUserComponent,
    MergeManuComponent,
    AddManuClientComponent,
    AddEditCompanyComponent,
    AddEditScinameComponent],

  declarations: [
    ManufacturerActionsComponent,
    MergeManuComponent,
    AddManuClientComponent,
    ManufacturerComponent,
    AddEditCompanyComponent,
    AddEditScinameComponent,

  ],
  entryComponents: [
    ManufacturerActionsComponent,
    AddUpdateUserComponent,
    MergeManuComponent,
    AddManuClientComponent,
    AddEditCompanyComponent,
    AddEditScinameComponent,
  ],

  providers: [
     AutoCompleteService],
})
export class ManufacturerModule { }

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SeomodalComponent } from './seomodal.component';

describe('SeomodalComponent', () => {
  let component: SeomodalComponent;
  let fixture: ComponentFixture<SeomodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeomodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeomodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class CompanyFile {
  Id: number;
  Link: string;
  Type: number;
  Description: string;
  Exists: boolean;
  YouTube: string;
  Caption: string;
}

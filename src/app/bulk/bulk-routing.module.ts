import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkComponent } from './bulk.component';
import { AddEditBulkComponent } from './add-edit-bulk/add-edit-bulk.component';

const router: Routes = [
    {
        path: '',
        component: BulkComponent,
        data: {
            title: 'Bulk',
            icon: 'icon-layout-cta-right',
            caption: 'my landing',
            status: true
        }
        
    },
    {
        path: ':id',
        component: AddEditBulkComponent,
        data: {
          title: 'Add-Bulk',
          icon: 'icon-layout-cta-right',
          caption: 'my landing',
          status: true
        }
      },
        
    
];
@NgModule({
    imports: [RouterModule.forChild(router)],
    exports: [RouterModule]
})

export class BulkRoutingModule { }

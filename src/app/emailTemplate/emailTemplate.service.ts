import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService {

  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) { }

  getTemplates(): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '/api/AdminEmailTemplate/Getall');
  }

  getEmailbyID(id){
    return this.http.get<any>(this.APIEndpoint + '/api/AdminEmailTemplate/Get/'+ id);
  }

  addUpdateEmail(obj){
return this.http.post(this.APIEndpoint + '/api/AdminEmailTemplate/AddUpdate/', obj);
  }
  
  DeleteEmail(param){
return this.http.get(this.APIEndpoint + '/api/AdminEmailTemplate/Delete?Id='+param);
  }

}
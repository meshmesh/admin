import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../_services/auth.service';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.jwt) {
            request = request.clone({
                setHeaders: {
                  Authorization: `Bearer ${currentUser.jwt}`,
                  Language : 'en'
                }
            });
        }

        return next.handle(request);
    }


    cleanUrl(dirtyUrl: string): string {

        let sa = Array.from(dirtyUrl);
        let lastindex = sa[sa.length-1];
        let lastindexOfAnd = sa.lastIndexOf('&');
    
    
        // handle in case of extra '&' and operator
        if( lastindex == '&') {
          sa.splice(sa.lastIndexOf('&'), 1);
          return sa.join("");
        } 
    
        // handle at case of Key={empty}
        // you may check if diff in index of and last element is more than 0
        else if(lastindex == '=') {
          let diff = Number(lastindex) - lastindexOfAnd;
          // remove element start from '&' index end at last element
          sa.splice(diff,2);
    
          // return remaning elements and join 
          return sa.join("");
        }
    
        else {
          return dirtyUrl;
        }
        
      }
}
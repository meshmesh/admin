import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatButtonModule, MatTableModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { EmailTemplateComponent } from './emailTemplate.component';
import { EmailTemplateRoutingModule } from './emailTemplate-routing.module';
import { EditEmailComponent } from './editEmail/editEmail.component';
import { QuillEditorModule } from 'ngx-quill-editor';


@NgModule({
  imports: [
    CommonModule,
    EmailTemplateRoutingModule,
    MatCardModule,
    
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    QuillEditorModule

  ],
  declarations: [EmailTemplateComponent, EditEmailComponent],
  entryComponents: [EditEmailComponent]
})
export class EmailTemplateModule { }

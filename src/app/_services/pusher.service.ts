import { Injectable, OnDestroy } from '@angular/core';
import { environment } from '../../environments/environment';
import Pusher from 'pusher-js';
import { HttpClient } from '@angular/common/http';
import { CurrentUser } from '../_model/CurrentUser';
import { AuthService } from './auth.service';
import { BehaviorSubject, of, Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PusherService implements OnDestroy {
 ngOnDestroy(): void {
   this.onDestroy$.next();
 }
 onDestroy$ = new ReplaySubject<void>();
  pusher: Pusher;
  channel: any;

  groups: any[] = [];
  currentUser: CurrentUser;

  socketId: any = '';
  messagesSubject: BehaviorSubject<any[]>;
  public currentMessages: Observable<any[]>;
  constructor(private http: HttpClient, private authService: AuthService) {
    this.currentUser = this.authService.currentUserValue;
    this.messagesSubject = new BehaviorSubject<any[]>([]);
    this.init();
  }

  getConversation() {
    return this.http.get(environment.ApiUrl + '/api/Group/GetAll');
  }


  init() {
    let _this = this;
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      authEndpoint: environment.ApiUrl + '/api/Channel/ChannelAuthAsync',
      auth: {
        headers: {
          'authorization': 'Bearer ' + _this.currentUser.jwt
        }
      }
    });
  }
  createGroup(groupObject) {
    return this.http.post(environment.ApiUrl + '/api/Group/Create', groupObject);
  }

  bindGroups() {
    let _this = this;
    this.channel = this.pusher.pipe(takeUntil(this.onDestroy$)).subscribe('group_chat');
    this.channel.bind('new_group', function (data) {
      _this.groups.push(data.new_group);

    });
  }
  createMessage(messageObject, groupId) {
    messageObject.SocketId = this.pusher.connection.socket_id;
    this.socketId = this.pusher.connection.socket_id;
    messageObject.GroupId = groupId;
    return this.http.post(environment.ApiUrl + '/api/Message/Create', messageObject);
  }
  getAllMessages(groupId) {
    return this.http.get(environment.ApiUrl + '/api/Message/GetById/' + groupId);
  }
 
  getMessages(): Observable<any[]> {
    return this.messagesSubject;
  }

 
  unSubscribeToChannel(groupId) {
    this.channel.unsubscribe('private-' + groupId);
  }



}

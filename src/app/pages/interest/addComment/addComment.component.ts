import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { InterestService } from "../interest.service";
import { Commentt } from "./Commentt";
import { ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ToastService } from "src/app/_services/toast.service";

@Component({
  selector: "app-addComment",
  templateUrl: "./addComment.component.html",
  styleUrls: ["./addComment.component.scss"],
})
export class AddCommentComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(
    public dialogRef: MatDialogRef<AddCommentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private serv: InterestService,
    private toast: ToastService
  ) {}

  cForm: Commentt;
  ngOnInit() {
    this.cForm = new Commentt();
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  async submit(obj) {
    if (obj.invalid) return;
    this.cForm.ObjectType = 3;
    this.cForm.ObjectId = this.data.row.id;
    this.serv
      .saveComment(this.cForm)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res) => {
        this.close(null);
        this.toast.success("Added Successfully ");
      });
  }

  /* save(data){
     this.comment = this.data; 
   }*/
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox.component';
import { InboxRoutingModule } from './inbox-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MessagesPanelComponent } from './messages-panel/messages-panel.component';

const components = [InboxComponent,  MessagesPanelComponent];

@NgModule({
  imports: [CommonModule, InboxRoutingModule, SharedModule],
  declarations: [...components],
  entryComponents: []
})
export class InboxModule {}

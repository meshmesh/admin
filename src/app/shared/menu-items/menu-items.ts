import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}




const MENUITEMS = [
  {
    label: 'My Panel',
    main: [
      {
        state: 'dashboard',
        short_label: 'da',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont-dashboard-web'
      },

      {
        state: 'Companies',
        name: 'Companies',
        type: 'sub',
        icon: 'icofont-globe',
        children: [
          {
            state: 'Manufacturer',
            short_label: 'M',
            name: 'Manufacturers',
            type: 'link',
            icon: 'feather icon-arrow-right '
          },
          {
            state: 'Distributor',
            short_label: 'D',
            name: 'Distributers',
            type: 'link',
            icon: 'feather icon-arrow-right'
          }
        ]
      },
      {
        state: 'Crawling',
        name: 'Crawling',
        type: 'sub',
        icon: 'icofont-robot',
        children: [
          {
            state: 'Crawler',
            short_label: 'C',
            name: 'Crawler',
            type: 'link',
            icon: 'feather icon-arrow-right'
          },
          {
            state: 'new',
            short_label: 'N',
            name: 'Crawler Scientific Names',
            type: 'link',
            icon: 'feather icon-arrow-right'
          },
        ]
      },
      {
        state: 'Setup',
        name: 'Setup',
        type: 'sub',
        icon: 'icofont-gears',
        children: [
          {
            state: 'Scientificname',
            short_label: 'S',
            name: 'Scientific Name',
            type: 'link',
            icon: 'icofont-beaker'
          },
          {
            state: 'NewScientificname',
            short_label: 'S',
            name: 'New Scientific Name',
            type: 'link',
            icon: 'icofont-beaker'
          },
          {
            state: 'Product',
            short_label: 'P',
            name: 'Products',
            type: 'link',
            icon: 'icofont-medicine'
          },
        ]
      },

      {
        state: 'Inquiries',
        name: 'Inquiries',
        type: 'sub',
        icon: 'icofont-inbox',
        children: [

          {
            state: 'Inquiry',
            short_label: 'I',
            name: 'Inquiry History',
            type: 'link',
            icon: 'feather icon-arrow-right'
          }
        ]
      },
      {
        state: 'Business',
        name: 'Opportunities',
        type: 'sub',
        icon: 'icofont-briefcase',
        children: [
          {
            state: 'Opportunity',
            short_label: 'BO',
            name: 'Business Opportunities',
            type: 'link',
            icon: 'feather icon-arrow-right'
          }
        ]
      },
      {
        state: 'StaticPages',
        name: 'Static Pages',
        type: 'sub',
        icon: 'feather  icon-layers',
        children: [
          {
            state: 'Aumetknowledge',
            short_label: 'A',
            name: 'Aumet knowledge',
            type: 'sub',
            icon: 'feather icon-arrow-right',
            children: [
              {
                state: 'Articles1',
                short_label: 'A',
                name: 'Articles',
                type: 'link',
                icon: 'feather icon-arrow-right'
              },
              {
                state: 'Reviews',
                short_label: 'R',
                name: 'Reviews',
                type: 'link',
                icon: 'feather icon-arrow-right'
              },
              {
                state: 'Videos',
                short_label: 'V',
                name: 'Videos',
                type: 'link',
                icon: 'feather icon-file-text'
              }

            ]
          },
          {
            state: 'Dynamic1',
            short_label: 'D',
            name: 'Dynamic',
            type: 'link',
            icon: 'feather icon-arrow-right'
          },
          {
            state: 'Countries',
            short_label: 'C',
            name: 'Countries',
            type: 'link',
            icon: 'feather icon-file-text'
          }
        ]
      },
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}

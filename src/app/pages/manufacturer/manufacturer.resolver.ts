import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ManufacturerService } from './services/manufacturer.service';
import { ManufacturerModel, ManuListSearchParams } from './model/manufacturer.model';

@Injectable()
export class ManufacturerResolver implements Resolve<Array<ManufacturerModel>> {

    constructor(
        private service: ManufacturerService, 
        private router: Router ) { }
        
    resolve(): Observable<Array<ManufacturerModel>> {
        let data: ManuListSearchParams = new ManuListSearchParams();
        return this.service.getAll(data, '')
            .pipe(
                catchError(error => {
                    this.router.navigate(['']);
                    return of(null);
                })
            );
    }
}



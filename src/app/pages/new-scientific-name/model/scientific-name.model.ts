
export class ScientificNameModel {
    Id: number;
    ScientificName: string;
    SpecialityId: number;
    IconImg: string;
    IsApproved: boolean;
    Note: string;
    Rank: number;
    MedicalLineId: number;
    RelatedTerms: number[] = [];
    BroaderTerms: number[] = [];
    NarrowerTerms: number[] = [];
    NonpreferredTerms: any[] = [];
    CategoryId: number;
    AttributeType: number[] = [];
    ScientificClassId: number[] = [];
    CreatedAt: string;
    
    CompanyIds: number[] = [];
    Nonpreferredtermsid: number[] = [];
    borderTerm: any = {};
    narrowTerm: any = {};
    relatedTerm: any = {};
    manufacturer: number[] = [];
    manufacturerId: number;
    

    alterNative: string;
    approvedAt: string;
    approvedBy: string;
    bt: Array<any>;
    createdAt: string;
    distributorCount: 0
    id: number;
    imagePath: string;
    isApproved: boolean;
    manufacturerCount: number;
    medicalLine: IMedicalLine;
    name: string;
    nt: Array<any>;
    productCount: number;
    rt: Array<any>;
    specialities: Array<ISpeciality>;

    // AdminComment: string;
    // Details: string;
    // UsedFor: string;
    // ScientificClass: any = {};
    // attribute: any = {};
    // attributename: any[] = [];
    // ScientificClassname: any[] = [];
    // MarketAnalysis: boolean;
}

export class IDataItems {
  count: number;
  facets: number;
  items: Array<IScientificName>;
}

export class SciNameListV2Response {
  success: boolean;
  data: IDataItems;
  errorCode: number;
}
export class SciNamesSearchParams {
    PageSize: number = 10;
    PageNumber: number = 1;

    Id: string;
    Name: string;
    Approved: boolean;
    IsPictureFilled: boolean;
    SpecialityIds: any;
    MedicalLineIds: any;
    RTIds: any;
    BTIds: any;
    NTIds: any;
    CreationFromDate: any;
    CreationToDate: any;
    Alternatives: any;
}

export class SciNamesSearchParamsV2 {
    PageSize: number = 10;
    PageNumber: number = 1;

    Ids: Array<number>;
    Names: Array<string>;
    Approved: boolean;
    IsPictureFilled: boolean;
    MainSpecialityIds: Array<number>;
    SpecialityIds:  Array<number>;
    MedicalLineIds:  Array<number>;
    RTIds: Array<number>;
    BTIds: Array<number>;
    NTIds: Array<number>;
    CreationFromDate: string;
    CreationToDate: string;
    Alternatives: Array<string>;
    HaveSubspecialty: boolean;
    HaveDescription: boolean;
    HaveAlternative: boolean;
    HaveNT: boolean;
    HaveRT: boolean;
    HaveBT: boolean;
    HaveNotes: boolean;
    HaveReferences: boolean;
    Checked: boolean;
    SortBy: number;
    SortType: number;
}


export class IXSpeciality {
  id: number;
  name: string;
  imagePath?: string;
  parentID?: number;
  parentName?: string;
  subSpecialty?: boolean; 
}

export class IMedicalLine {
    id: number;
    imagePath: string;
    name: string;
  }
  
  export class ISpeciality {
    id: number;
    imagePath: string;
    name: string;
    parentID: number;
    parentName: string;
    subSpecialty: boolean;  
  }


  export class IParentSpeciality {
    id: number;
    name: string;
    key: string;
    imagePath: string;
    parentId: number;
    childCount: number;
  }

  export class IParentChild {
    child: Array<IXSpeciality>;
    parent: IXSpeciality;
  }
  export class IScientificName {
    alterNative: Array<string>;
    approvedAt: string;
    approvedBy: string;
    bt: Array<IScientificName>
    createdAt: string;
    distributorCount: number;
    id: number;
    name: string;
    imagePath: string;
    isApproved: boolean;
    manufacturerCount: number;
    medicalLine: IMedicalLine;
    nt: Array<IScientificName>
    productCount: number;
    rt: Array<IScientificName>

    // mainSpeciality: Array<ISpeciality>;
    
    references: Array<string>;
    description: Array<string>;


    mainSpeciality: IParentChild;
    specialities: Array<IParentChild>;


    // calculated
    mainParentSpeciality: Array<IXSpeciality>;
    mainChildSpeciality: Array<IXSpeciality>;
    parentSubSpeciality: Array<IXSpeciality>;
    childSubSpeciality: Array<IXSpeciality>;
  }
  
export class ReviewForm {
  id: number;
  picture: string;
  name: string;
  position: string;
  title: string;
  rating: number;
  quotation: string;
  type?: number;
}

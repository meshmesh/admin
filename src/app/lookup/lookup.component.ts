import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LookupService } from './lookup.service';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource, MatPaginator, MatSelectChange } from '@angular/material';
import { _lockupService } from '../_services/_lockup.service';
import { take, takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss']
})
export class LookupComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  constructor(private serv: LookupService, private _lookup: _lockupService) {}

  values: any[] = [];
  dataSource: MatTableDataSource<any>;

  SearchKey: string;
  MajorLookup: any[] = [];
  displayedColumns = ['actions', 'id', 'major', 'value', 'order'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  adding: any = {};
  disabled = false;
  obj: any = {
    Id: null,
    MajorId: null,
    Name: '',
    Deleted: false
  };

  ngOnInit() {
    this.loadmajor();
  }

  AddUpdate(form) {
  this.obj.Name = this.adding.name;
    this.obj.MajorId = this.adding.majorId;
    this.obj.Id = this.adding.id;
    this.obj.Deleted = false;
    this.serv.saveEdit(this.obj).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.loadmajor();
    this.adding = {};
    });
  }

  del(form) {
    this.obj.Name = this.adding.name;
    this.obj.MajorId = this.adding.majorId;
    this.obj.Value = this.adding.value;
    this.obj.Id = this.adding.id;
    this.obj.Deleted = true;
    this.serv.saveEdit(this.obj).pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.loadmajor();
      this.adding = {};
      
    });
  }

  loadmajor() {
    this.serv.getMajors().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        this.renderDataSource(response);
        this.paginDataSource(response);
      },
      error => {
        // console.log(error);
      }
    );

    this._lookup.getLockUps(0).pipe(takeUntil(this.onDestroy$)).subscribe(response => {
      this.MajorLookup = response;
    });
  }

  renderDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
  }

  paginDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  Filter(SearchKey) {
    this.dataSource.filter = this.SearchKey.trim().toLowerCase();
  }

  onOptionsSelected(event: MatSelectChange) {
    if (event.value === 0) {
      const filterData = this.values;
      this.renderDataSource(filterData);
      this.paginDataSource(filterData);
    } else {
      const filterData = this.values.filter(t => t.majorId === event.value);
      this.renderDataSource(filterData);
      this.paginDataSource(filterData);
    }
  }

  displaymajors(row) {
    this.adding = row;
  }

  onApprove(id: number): void {
    this.serv
      .approveMajor(id)
      .pipe(take(1))
      .pipe(takeUntil(this.onDestroy$)).subscribe(d => {
        this.loadmajor();
      });
  }
}

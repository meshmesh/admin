import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from "@angular/core";
import { Observable, of } from "rxjs";
import { DynamicPageTableService } from "../dynamicPageTable/dynamicPageTable.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { bool } from "aws-sdk/clients/signer";
import { debug } from "util";
import { Dynamic } from "../dynamicPageTable/Dynamic";
import { map } from "rxjs/operators";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-addDynamic",
  templateUrl: "./addDynamic.component.html",
  styleUrls: ["./addDynamic.component.scss"]
})
export class AddDynamicComponent implements OnInit {
  seletedscientificNames: any[] = [];
  scientificNames: any = [];
  filteredscientificNames: Observable<any>;
  @ViewChild("scientificnameInput") scientificnameInput: ElementRef<
    HTMLInputElement
  >;
  removable = true;
  selectedMedical: any[] = [];
  medicalsFilteration: Observable<any>;
  medicals: any = [];
  selectedMedicalId: any[] = [];
  selectedScientificId: any[] = [];
  selectedCountries: any[] = [];
  country: any;
  countriesFilteration: Observable<any>;
  selectedCountriesId: any[] = [];
  countries: any = [];
  Published: boolean;
  dynForm: Dynamic;
  Type : any;
  Title: any;
  Slug: any;
  Description: any;
  countrys: any;
  medical: any;
  scientificNamess: any;

  constructor(
    private serv: DynamicPageTableService,
    public dialogRef: MatDialogRef<AddDynamicComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) { }

  ngOnInit() {
    this.dynForm = new Dynamic();
    this.serv.getScientificNames("a").subscribe((response: any) => {
      this.scientificNames = response;
      this.filteredscientificNames = of(response);
    });
    this.showCountry();
    this.showMedical();

    //   console.log(this.dialogData);

    if (this.dialogData.id) {
      //   console.log(this.dialogData);
      //   console.log(this.selectedCountries);
      this.serv
        .getDynamicPageById(this.dialogData.id, 1)
        .pipe()
        .subscribe((x: any) => {
          
          this.Type = parseInt(x[0].categoryId);
       

          this.selectedCountries = x[0].countries;
          
          this.selectedMedical = x[0].medicalLines;
          this.seletedscientificNames = x[0].narrowTerms;


          this.selectedCountries.forEach(element => {
            this.selectedCountriesId.push(element.id);
          });  
           this.selectedMedical.forEach(element => {
            this.selectedMedicalId.push(element.id);
          });  
           this.seletedscientificNames.forEach(element => {
            this.selectedScientificId.push(element.id);
          });
        //  this.Type = x[0].type;
          this.Slug = x[0].slug;
          this.Title = x[0].title;
          this.Published = x[0].published;
          this.Description = x[0].description;
          
          console.log(this.Type);
          /////////////////////////////////
      this.dynForm.CategoryId = x[0].type;
      this.selectedMedical.forEach(element => {
        this.dynForm.MedicalLines.push(element.id)
      });
      this.seletedscientificNames.forEach(element => {
        this.dynForm.ScientificIds.push(element.id)
      });
      this.selectedCountries.forEach(element => {
        this.dynForm.Countries.push(element.id)
      });
        });
    }
  }

  removescientificNames(i) {
    const temp = this.seletedscientificNames[i];
    const tempIds = this.selectedScientificId[i];
    this.scientificNames.push(temp, tempIds);
    this.seletedscientificNames.splice(i, 1);
    this.selectedScientificId.splice(i, 1);
  }

  removeMedicalPoints(i) {
    let temp = this.selectedMedical[i];
    let tempIds = this.selectedMedicalId[i];
    this.medicals.push(temp, tempIds);
    this.selectedMedical.splice(i, 1);
    this.selectedMedicalId.splice(i, 1);
  }

  _filterscientificNames(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return this.serv.getScientificNames(value);
  }

  _filterMedical(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    return of(
      this.medicals.filter(
        item => item.name.toLowerCase().indexOf(filterValue) === 0
      )
    );
  }

  AddSelectedscientificNames(event) {
    this.seletedscientificNames.push(event.option.value);
    this.selectedScientificId.push(event.option.value.id);
    const index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);
    }
  }

  AddSelectedMedical(event) {
    this.selectedMedical.push(event.option.value);
    this.selectedMedicalId.push(event.option.value.id);
    let index = this.medicals.indexOf(event.option.value);
    if (index > -1) {
      this.medicals.splice(index, 1);
    }
  }

  removeRegionsPoints(i) {
    let temp = this.selectedCountries[i];
    let tempIds = this.selectedCountriesId[i];
    // this.countries.push(temp, tempIds);
    this.selectedCountries.splice(i, 1);
    this.selectedCountriesId.splice(i, 1);
  }

  _filterRegions(value: any): Observable<any[]> {
    let filterValue = "";
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    console.log(this.countries.filter(i => !i.country));
    return of(
      this.countries.filter(item =>
        item["country"].toLowerCase().startsWith(filterValue)
      )
    );
  }

  AddSelectedCountry(event) {
    this.selectedCountries.push(event.option.value);
    this.selectedCountriesId.push(event.option.value.id);
    let index = this.countries.indexOf(event.option.value);
    if (index > -1) {
      this.countries.splice(index, 1);
    }
  }

  showCountry() {
    this.serv.getCountrySuggestion().subscribe(
      (response: any) => {
        this.countries = response;
        this.countriesFilteration = of(this.countries);
      },
      error => console.log(error)
    );
  }

  showMedical() {
    this.serv.getmedicalline().subscribe(
      (response: any) => {
        this.medicals = response;
        this.medicalsFilteration = of(this.medicals);
      },
      error => console.log(error)
    );
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  submit(form) {
    
    if (form.invalid) {
      return;
    }
    if (this.dialogData.id) {
      this.dynForm.Id = this.dialogData.id;
      this.dynForm.CategoryId = parseInt(form.value.Type);
      this.dynForm.MedicalLines = this.selectedMedicalId;
      this.dynForm.ScientificIds = this.selectedScientificId;
      this.dynForm.Countries = this.selectedCountriesId;
      this.dynForm.Published = this.Published;
      this.dynForm.Description = form.value.Description;
      this.dynForm.slug = form.value.Slug;
      this.dynForm.title = form.value.Title;
      this.serv.addDynamicPage(this.dynForm).subscribe(x => {
        console.log(x);
        this.dialogRef.close(x);
      });
    }
    else {
      this.dynForm.CategoryId = parseInt(form.value.Type);
      this.dynForm.MedicalLines = this.selectedMedicalId;
      this.dynForm.ScientificIds = this.selectedScientificId;
      this.dynForm.Countries = this.selectedCountriesId;
      this.dynForm.Published = this.Published;
      this.dynForm.Description = form.value.Description;
      this.dynForm.slug = form.value.Slug;
      this.dynForm.title = form.value.Title;
      this.serv.addDynamicPage(this.dynForm).subscribe(x => {
        console.log(x);
        this.dialogRef.close(x);
      });
    }
  }
}

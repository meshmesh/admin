import { OnInit, ViewChild, Component, OnDestroy } from '@angular/core';
import { _lockupService } from '../_services/_lockup.service';

import { HttpClient } from '@angular/common/http';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ContactService } from './contact.service';
import { Contact } from './contact';
import * as _ from 'lodash'
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(private http: HttpClient, private serv: ContactService, private router : ActivatedRoute) { }
  pageIndex = 1;
  values: any[] = [];
  dataSource: MatTableDataSource<any>;
  SearchKey: string;
  MajorLookup: any[] = [];
  displayedColumns = ['id','companyName','contactName','Country','phoneNumber','email','webSite','address','source'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  contact = new Contact;
  adding: any ={};
  countries: any = [];
  cont: any = [];
  counter = 0;
  length : number;
  ngOnInit() {
    this.router.data.pipe(takeUntil(this.onDestroy$)).subscribe(res=> {
      
      this.renderDataSource(res.distributor);
      
    })
    this.serv.getCountry().pipe(takeUntil(this.onDestroy$)).subscribe(data => {
      this.countries = data;
    });

  }
  
  AddUpdate(form) {

this.contact.id = this.adding.id;
this.contact.ContactName = this.adding.contactName;
this.contact.CompanyName = this.adding.companyName;
this.contact.Country = this.adding.country.name;
this.contact.WebSite = this.adding.webSite;
this.contact.Comment = this.adding.comment;
this.contact.PhoneNumber = this.adding.phoneNumber;
this.contact.Email = this.adding.email;
this.contact.Address = this.adding.address;
this.contact.Source = this.adding.source;
this.serv.saveEdit(this.contact).pipe(takeUntil(this.onDestroy$)).subscribe(result =>{
  this.loadContact();
})
  }

 /* del(form){
    this.obj.Name = this.adding.name;
    this.obj.MajorId = this.adding.majorId;
    this.obj.Value = this.adding.value;
    this.obj.OrderId = this.adding.orderId;
    this.obj.Id = this.adding.id;
    this.obj.Deleted=true;
    this.serv.saveEdit(this.obj).pipe(takeUntil(this.onDestroy$)).pipe(takeUntil(this.onDestroy$)).subscribe(result =>{
    this.loadContact();
      })
  }*/


  loadContact() {
    this.serv.getContactsall().pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.values = response;
        this.renderDataSource(response);
        this.paginDataSource(response);
      },
      error => {
        console.log(error);
      }
      
    );

  }


  renderDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.length = data[0].totalRows;
  }

  paginDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  Filter(SearchKey) {
    this.dataSource.filter = this.SearchKey.trim().toLowerCase();
  }
  pageChange(event) {
    this.pageIndex = event.pageIndex + 1;
      this.serv.getContacts(event.pageIndex + 1).pipe(takeUntil(this.onDestroy$)).subscribe((res: any)=>{
        this.renderDataSource(res);
      });
    }

  displaymajors(row) {
     this.adding =_.cloneDeep(row);
     let Coun = this.countries.find(c => c.country === this.adding.country);
     this.adding.country = {};
     this.adding.country = Coun;
    }
} 

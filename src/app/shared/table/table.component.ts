
import {
  Component, OnInit,
  Output, EventEmitter,
  Input, ViewChild,
  OnChanges
} from
  '@angular/core';

import {
  MatTableDataSource, MatPaginator,
  MatSort
}
  from '@angular/material';

import {
  SelectionModel
} from
  '@angular/cdk/collections';





@Component({

  selector: 'app-table',

  templateUrl: './table.component.html',

  styleUrls: ['./table.component.scss']

})

export class TableComponent implements OnInit, OnChanges {

  @Output() update = new EventEmitter<number>();

  @Output() delete = new EventEmitter<number>();

  // tslint:disable-next-line:no-input-rename

  @Input('tableColumns') tableColumns = [];
  @Input('displaysize') displaysize = [];

  // tslint:disable-next-line:no-input-rename

  @Input('DataSource') DataSource: any[] = [];

  dataSource: MatTableDataSource<any>;

  selection: SelectionModel<any>;

  @ViewChild('paginator') paginator: MatPaginator;

  @ViewChild('table', { read: MatSort }) sort: MatSort;


  columns: any[];

  column: any;



  constructor() { }



  ngOnChanges() {
    this.renderTable(this.DataSource);

  }

  ngOnInit() {

    this.renderTable(this.DataSource);

  }



  applyFilter(filterValue:
    string) {

    this.dataSource.filter =
      filterValue.trim().toLowerCase();

  }



  renderTable(columns:
    any[]) {

    this.columns = columns;

    this.dataSource = new MatTableDataSource<any>(columns);

    this.dataSource.paginator = this.paginator;

    this.dataSource.sort = this.sort;

    this.selection = new SelectionModel<any>(true, []);

    this.dataSource.sortingDataAccessor = (sortData, sortHeaderId) => {

      if (!sortData[sortHeaderId]) {

        return this.sort.direction === 'asc' ? '3' : '1';

      }


      return /^\d+$/.test(sortData[sortHeaderId]) ? Number('2' + sortData[sortHeaderId]) : '2' + sortData[sortHeaderId].toString().toLocaleLowerCase();

    };

  }

  deleteRow(index: number) {
    this.delete.emit(index);
  }



  isAllSelected() {

    return this.selection.selected.length === this.dataSource.data.length;

  }


  updateRow(row: any) {

    this.column = row;
    this.update.emit(row);

  }

  getValue(element: any, column: any) {

    if (column.replace(/\s+/g, '').includes('Date')) {
      return this.formatDate(element[column.replace(/\s+/g, '')]);
    }

    else {
      return element[column.replace(/\s+/g, '')];
    }


  }





  formatDate(date) {

    var
      monthNames = [

        "January",
        "February", "March",

        "April",
        "May", "June",
        "July",

        "August",
        "September", "October",

        "November",
        "December"

      ];

    date =
      new Date(date);

    var
      day = date.getDate();

    var
      monthIndex = date.getMonth();

    var
      year = date.getFullYear();


    return day + ' ' + monthNames[monthIndex] + ' ' + year;

  }

}










import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicPageTableComponent } from './dynamicPageTable.component';
import { MatDialogModule } from '@angular/material';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddDynamicComponent } from '../addDynamic/addDynamic.component';
import { DynamicPageTableRoutingModule } from './dynamicPageTable-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    SharedModule,
    DynamicPageTableRoutingModule,
    ReactiveFormsModule,
    
  ],

  declarations: [DynamicPageTableComponent, AddDynamicComponent],
  entryComponents: [AddDynamicComponent]
})
export class DynamicPageTableModule { }

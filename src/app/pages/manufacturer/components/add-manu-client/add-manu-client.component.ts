import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { ReplaySubject } from "rxjs";
import { S3 } from "aws-sdk/clients/all";
import { Guid } from "guid-typescript";
import { ToastService } from "src/app/_services/toast.service";
import { ManufacturerService } from "../../services/manufacturer.service";
import { FormBuilder, FormGroup, NgForm, Validators } from "@angular/forms";


export class MODAL_DATA {
  element: any
}

export class IClientData {
  callerUrl: string;
  commercialTerms: string;
  description: string;
  endDate: string;
  id: number;
  image: string;
  introductionCount: number;
  isClient: true
  name: string;
  numOfUsedIntroduction: number;
  numOfUsedUnlookCountry: number;
  sellingPoints: string;
  signedDate: string;
  unLookCount: number;
}

export class Iresponse {
  data: Array<IClientData>;
  success: boolean;
}
export class UpdateClientRequest {
  image: string;
  description: string;
  subtitle: string;
  companyId: number;
  endDate: string;
  unlookCount: number;
  introductionCall: number;
  commercialTerms: string;
  sellingPoints: string;
  callerUrl: string;
}

@Component({
  selector: "app-add-manu-client",
  templateUrl: "./add-manu-client.component.html",
  styleUrls: ["./add-manu-client.component.scss"],
})
export class AddManuClientComponent implements OnInit, OnDestroy {

  public clientForm: FormGroup;
  public clientData: IClientData = new IClientData();
  public submitLoader: boolean = false;
  public file: File;
  public isLoading: boolean;
  public minDate = new Date(2000, 0, 1);

  onDestroy$ = new ReplaySubject<void>();

  constructor(
    public fb: FormBuilder,
    private service: ManufacturerService,
    @Inject(MAT_DIALOG_DATA) public data: MODAL_DATA,
    private toast: ToastService,
    public dialogRef: MatDialogRef<AddManuClientComponent>,
    public dialog: MatDialogRef<AddManuClientComponent>
  ) {}



  ngOnInit() {
    this.buildForm();
    const manuId = this.data.element ? this.data.element.companyId : null;
    this.isLoading = true;
    if(manuId) {
      this.service
      .getClient(manuId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: Iresponse) => {
        this.clientData = res.data && res.data.length ? res.data[0] : null;
        if(res.data) this.updateFormValue(res.data[0]);
        else this.isLoading = false;
      });
    }
  }


  ngOnDestroy(): void {
    this.onDestroy$.next();
  }


  get f() {
    return this.clientForm.controls;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.clientForm.controls[controlName].hasError(errorName);
  };


  buildForm() {
    this.clientForm = this.fb.group({
      subtitle: [
        null,
        [Validators.required, Validators.maxLength(100)],
      ],
      description: [
        null,
        Validators.maxLength(1000),
      ],
      imageUrl: [null],
      sellingPoints: [null],
      callerUrl: [null],
      introductionCall: [null, [Validators.min(0), Validators.pattern(/^[1-9]\d*$/)]],
      unlookCount: [null, [Validators.min(0), Validators.pattern(/^[1-9]\d*$/)]],
      endDate: [null],
      numOfUsedIntroduction: [{value: null, disabled: true}],
      numOfUsedUnlookCountry: [{value: null, disabled: true}],
      commercialTerms: [null],
    });

    this.isLoading = false;
  }

  updateFormValue(data: IClientData) {
    this.clientForm.patchValue({
      subtitle: data.name,
      description: data.description,
      imageUrl: data.image,
      sellingPoints: data.sellingPoints,
      commercialTerms: data.commercialTerms,
      callerUrl: data.callerUrl,
      introductionCall: data.introductionCount,
      unlookCount: data.unLookCount,
      endDate: new Date(data.endDate),
      numOfUsedIntroduction: data.numOfUsedIntroduction || 0,
      numOfUsedUnlookCountry: data.numOfUsedUnlookCountry || 0
    });
    this.clientForm.updateValueAndValidity();
    this.isLoading = false;
  }

  onFileChanged(e) {
    this.file = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.clientForm.patchValue({ imageUrl: reader.result });
        this.clientForm.get("imageUrl").updateValueAndValidity();
      };
    }
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: "aumet-data",
      Key:
        "company/logo/" +
        Guid.create() +
        "." +
        this.getFileExtension(image.name),
      Body: image,
    };
    const that = this;
    that.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: "AKIAQQRZF2VNB3KYOT4O",
      secretAccessKey: "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",
      region: "us-west-2",
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }



  submit() {
    console.log(this.clientForm);
    if (this.clientForm.invalid) {
      this.toast.error("Please check all required fields!");
      return;
    }

    this.submitLoader = true;
    let data: UpdateClientRequest = new UpdateClientRequest();
    data.companyId = this.data.element.companyId;
    data.description = this.clientForm.value.description;
    data.subtitle = this.clientForm.value.subtitle;
    data.endDate = this.clientForm.value.endDate;
    data.unlookCount = this.clientForm.value.unlookCount;
    data.introductionCall = this.clientForm.value.introductionCall;
    data.sellingPoints = this.clientForm.value.sellingPoints;
    data.callerUrl = this.clientForm.value.callerUrl;
    data.commercialTerms = this.clientForm.value.commercialTerms;



    if (this.file) {
      this.uploadImage(this.file, (result: string) => {
        data.image = result ? result : this.clientForm.value.imageUrl;
        this.updateSubmit(data);
      });
    } else {
      data.image = this.clientForm.value.imageUrl;
      this.updateSubmit(data);
    }

  }


  updateSubmit(data: UpdateClientRequest) {
    this.service
      .addClient(data)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((x) => {
        this.submitLoader = false;
        this.toast.success("Client has been added successfuly");
        this.close(true);
      }, err => {
        this.submitLoader = false;
        this.toast.success("Something wrong happened!");
        // this.close(true);
      });
  }



  close(data): void {
    this.dialogRef.close(data);
  }

  onCancel(): void {
    this.dialog.close();
  }
}

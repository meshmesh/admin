/* import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { trigger, state, style, transition, AUTO_STYLE, animate } from '@angular/animations';
import { MessagingService } from 'src/app/_services/messaging.service';
import { Observable, of } from 'rxjs';
import { FbService } from 'src/app/_services/fb.service';
import { map, tap, take } from 'rxjs/operators';

@Component({
  selector: 'app-notifications-menu',
  templateUrl: './notifications-menu.component.html',
  styleUrls: ['./notifications-menu.component.scss'],
  animations: [
    trigger('notificationBottom', [
      state(
        'an-off, void',
        style({
          overflow: 'hidden',
          height: '0px'
        })
      ),
      state(
        'an-animate',
        style({
          overflow: 'visible',
          height: AUTO_STYLE
        })
      ),
      transition('an-off <=> an-animate', [animate('400ms ease-in-out')])
    ])
  ]
})
export class NotificationsMenuComponent implements OnInit {
  constructor(private msgService: MessagingService, private fbService: FbService) {}

  @Output() close = new EventEmitter();
  @Output() open = new EventEmitter();
  @Input() show: string;
  @Input() class: string;

  notification$: Observable<any[]>;

  ngOnInit() {
    this.fbService.init();
    // this.notification$ = this.getNotifications();
  }

  _open(): void {
    this.open.emit();
  }

  _close(): void {
    this.close.emit();
  }

  // getNotifications(): Observable<any> {
  //   return this.fbService.modal.valueChanges().pipe(
  //     map(user => {
  //       return Object.values(user[0]['notifications']);
  //     })
  //   );
  // }
}
 */
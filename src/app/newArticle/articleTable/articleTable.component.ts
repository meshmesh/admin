import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ArticleTableService } from './articleTable.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { switchMap, tap, take } from 'rxjs/operators';
import { Article } from 'src/app/_model/article';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';
import { NewSeoModalComponent } from 'src/app/shared/seo-modal/seo-modal.component';
import { ArticlesComponent } from '../articles/articles.component';
import { Router } from '@angular/router';
export interface Arguments {
  PageSize: number;
  PageNumber: number;
  Published?: boolean;
  Name?: string;
}

@Component({
  selector: 'app-articleTable',
  templateUrl: './articleTable.component.html',
  styleUrls: ['./articleTable.component.scss']
})
export class ArticleTableComponent implements OnInit {

  filterOptions: any[] = [
    {
      index: 3,
      type: 'rangeDate',
      order: 1,
      value: '',
      title: 'creaition date',
      dbArg: 'CreatedAtFrom'
    },
    {
      index: 0,
      type: 'text',
      order: 2,
      title: 'title',
      value: '',
      dbArg: 'Name'
    },
    {
      index: 1,
      type: 'checkBoxs',
      order: 3,
      value: [],
      title: 'Sub Category',
      list: [],
      dbArg: ''
    },
    {
      index: 2,
      type: 'toggel',
      order: 4,
      value: false,
      name: 'puplished',
      title: 'puplished',
      dbArg: 'Published'
    },
    {
      type: 'sortBy',
      index: 12,
      order: 100,
      comp: 'Article',
      value: []
    }
  ];

  pagenumber = 1;
  filteredString = "";
  length = 10;
  mainCategory = 0;
  mainSearchText: string;
  mainCategories: { id: number; name: string }[] = [];
  dataFromFilters: any = [];
  dataSource: MatTableDataSource<any>;
  paginator: MatPaginator;
  searchPageNumber = 1;
  showFilters = false;
  articlesArray: any;
  displayedColumns: any [] = ["actions","id","title","category","Related","published"]


  constructor(private serv: ArticleTableService, private dialog: MatDialog,private router: Router) { }

  ngOnInit() {
    
      this.loadArticles();
  }


  loadArticles(){
  this.serv
    .searchArticles(this.pagenumber)
    .subscribe(res => {
      this.renderTable(res);
    });
  }

  renderTable(data) {
    if (data.length < 10) {
      this.length = data.length;
    } else {
      this.length = data[0].totalNumber;
    }
    this.dataSource = new MatTableDataSource<any>(data);
  }


  

onDelete(id: number): void {
  const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
    data: {
      title: 'Article',
      id: id
    }
  });
  dialogRef
    .afterClosed()
    .pipe(
      take(1),
      switchMap((deleted: boolean) => {
        return deleted ? this.serv.deleteArticle(id) : of(null);
      }),
    )
    .subscribe(x => {
      this.loadArticles();
    });
}


onOpenSEO(row: Article): void {
  
  const dialogRef = this.dialog.open(NewSeoModalComponent, {
    data: row.seo,
    minWidth: '650px',
    minHeight: '500px'
  });

  dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap(seo => {
          if (seo) {
            const s = JSON.stringify(seo);
            const obj = {
              Id: row.id,
              Items: [
                {
                  Key: 'seo',
                  Value: s
                }
              ]
            };
            return this.serv.UpdateSEO(obj);
          } else {
            return of(null);
          }
        }),
        tap(res => {
          if (res) {
        this.loadArticles();
          }
        })
      )
      .subscribe();
}

onPublish(id: number): void {
  this.serv
    .puplishArticle(id)
    .subscribe(x => {
      this.loadArticles();
    });
}

onUnPublish(id: number): void {
  this.serv
    .UnpuplishArticle(id)
    .subscribe(x => {
      this.loadArticles();
    });
}

onCreateArticle(){
 const dialogRef = this.dialog.open(ArticlesComponent, {
    width: '50%',
    height: '80%',
  })
  dialogRef.afterClosed()
  .subscribe(result => {
    console.log('The dialog was closed');
    this.loadArticles();
  });
}

onEditArticle(item){
  const dialogRef = this.dialog.open(ArticlesComponent, {
    width: '50%',
    height: '80%',
   data: item
  })
  dialogRef.afterClosed()
  .subscribe(result => {
    console.log('The dialog was closed');
    this.loadArticles();
  });
}





pageChange(event) {
  this.pagenumber = event.pageIndex + 1;

  this.serv
    .searchArticles(this.pagenumber)
    .subscribe(res => {
      this.renderTable(res);
    });
  
}










}

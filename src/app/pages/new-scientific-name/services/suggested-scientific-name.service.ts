import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const APIEndpoint = environment.ApiUrlV3;
@Injectable({
  providedIn: 'root'
})

export class SuggestedScientificNameService {

  constructor(private http: HttpClient) {}

  getall(pagesize, pageNumber) {
    const url =
    APIEndpoint +
      'AdminScientificNames/Suggestion?PageSize=' +
      pagesize +
      '&PageNumber=' +
      pageNumber;
      
    return this.http.get(url);
  }

  deleteSuggestedScientificName(id) {
    return this.http.get(APIEndpoint + 'AdminScientificNames/Suggestion?SuggestionId=' + id, {});
  }

  getScientificNameSuggestion(str): Observable<any> {
    const url = APIEndpoint + `AdminScientificNames/Search?PageSize=10&PageNumber=1&Name=${str}`;
    return this.http.get<any>(url);
  }

  mergScientific(suggestionId, scieId) {
    return this.http.post(APIEndpoint + 'AdminScientificNames/Suggestion/Replace', {
      'scientificNameId': scieId,
      'suggestionId': suggestionId
    });
  }
  
}

import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmailTemplate } from '../emailTemplate';
import { EmailTemplateService } from '../emailTemplate.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-editEmail',
  templateUrl: './editEmail.component.html',
  styleUrls: ['./editEmail.component.scss']
})
export class EditEmailComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();

  constructor(public dialog: MatDialogRef<EditEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any, private serv: EmailTemplateService) { }

emailT: EmailTemplate;

  ngOnInit() {
    this.emailT = new EmailTemplate();
  }

  close(data): void {
    this.dialog.close(data);
  }



  async submit(obj){
    this.emailT = obj.value;
    this.serv.addUpdateEmail(this.emailT).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      
    })
  }
}

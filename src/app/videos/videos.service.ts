import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class VideosService {
  service$: Observable<any>;
  public serviceSubject = new Subject<any>();

  
constructor(private http: HttpClient) { 
  this.service$ = this.serviceSubject.asObservable();
}
APIEndpoint = environment.ApiUrl;



shareData(data) {
  this.serviceSubject.next(data);
}
saveVideos(obj){
 return this.http.post(this.APIEndpoint + '/api/AdminBusinessOpportunities/AddVideo', obj);
 }
 deletevideo(id){
  return this.http.get<any>(this.APIEndpoint + '/api/AdminBusinessOpportunities/DeleteVideo?Id='+id);
  }
 
 
}

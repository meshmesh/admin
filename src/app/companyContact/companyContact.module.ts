
import { NgModule } from "@angular/core";

import { CompanyContactComponent } from "./companyContact.component";
import { MatTableModule, MatPaginatorModule, MatFormFieldModule, MatAutocompleteModule, MatSelectModule, MatDialogModule, MatInputModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    FormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    SharedModule
  ],

  declarations: [CompanyContactComponent],
  exports:[CompanyContactComponent]

})
export class companycontactModule { }


import { Component, OnInit, Inject, ViewChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatAutocomplete,
  MatDialog
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { NgxCarousel } from 'ngx-carousel';
import { ManufacturerService } from '../services/manufacturer.service';
import { Observable, of, ReplaySubject } from 'rxjs';
import { FormGroup, FormArray } from '@angular/forms';
import { AuthService } from '../../../_services/auth.service';
import { take, tap, takeUntil, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ClipboardService } from 'ngx-clipboard';
import { AddUpdateUserComponent } from '../../users/addUpdate-user/addUpdate-user.component';

import { UsersService } from '../../users/users.service';
import { S3 } from 'aws-sdk/clients/all';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';
import { Guid } from 'guid-typescript';
import { environment } from '../../../../environments/environment';
import { ToastService } from 'src/app/_services/toast.service';
import { AvailableModel, ScientificFormModel, ExistingModel, FilesViewModel } from '../model/manufacturer.model';
import { AddEditScinameComponent } from '../components/add-edit-sciname/add-edit-sciname.component';
// import { CompanyContactComponent } from '../../../companyContact/companyContact.component';
@Component({
  selector: 'app-manufacturer-detail',
  templateUrl: './manufacturer-detail.component.html',
  styleUrls: ['./manufacturer-detail.component.scss', '../../../../assets/icon/icofont/css/icofont.scss'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms ease-in-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ transform: 'translate(0)' }), animate('400ms ease-in-out', style({ opacity: 0 }))])
    ])
  ]
})
export class ManufacturerDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  openProfileLink: string = environment.OpenProfileLink;
  profilelink = '';
  manulink = '';
  comment = '';
  Website = '';
  Domain = '';
  onDestroy$ = new ReplaySubject<void>();
  Durations = [{ name: '15 Days', value: 15 }, { name: '30 Days', value: 30 }, { name: '90 Days', value: 90 }];
  callTypes: any = [];
  seoData: any = [];
  userName = this.auth.currentUserValue.firstName + ' ' + this.auth.currentUserValue.lastName;
  userId = this.auth.currentUserValue.id;
  filtereddistributors: Observable<any>;
  distributors: any[] = [];
  seleteddistributors: any[] = [];
  manufacturerRecuirment: any = {};
  displayedusresColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'currentStage', 'phone', 'Action'];
  showmore = false;
  shButton: any = 'Hide';
  narrowselected = false;
  targetingSource: MatTableDataSource<any>;
  exclusivitySource: MatTableDataSource<any>;
  vacantSource: MatTableDataSource<any>;
  callLogSource: MatTableDataSource<any>;
  contractSource: MatTableDataSource<any[]>;
  manufacturerSeoData: any = {};
  finalSeoData = [];
  description = [];
  imagePreview = [];
  localImages: File[] = [];
  removable = true;
  pageSize: number = 10;
  length: number;

  loadingSciNames: boolean;
  @ViewChild('table', { read: MatSort }) sortTargeting: MatSort;
  @ViewChild('tableVacant', { read: MatSort }) sortVacant: MatSort;
  @ViewChild('tableExclusivity', { read: MatSort }) sortExclusivity: MatSort;
  @ViewChild('tableCallLog', { read: MatSort }) sortCallLog: MatSort;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  @ViewChild('nameInput') nameInput: ElementRef<HTMLInputElement>;
  @ViewChild('medicalInput') medicalInput: ElementRef<HTMLInputElement>;
  // ContactdisplayedColumns: string[] = ['Id', 'FirstName', 'LastName', 'PhoneNumber', 'Email', 'JobTitle', 'Note', 'Invite', 'CurrentStage', 'NextStage', 'Action'];
  prospectedCompanyScientificNamesDisplayedColumns: string[] = ['Id', 'ScientificName', 'MedicalLine', 'Specialty', 'Action'];
  filesDisplayedColumns: string[] = ['Id', 'Name', 'Type', 'Path'];
  usersDisplayedColumns: string[] = ['Id', 'FirstName', 'LastName', 'Stage', 'Email', 'PhoneNumber', 'IsAuthorized', 'IsAdmin'];
  // basicInfoDisplayedColumns: string[] = ['Id','Logo','Name','Country','IsCrawled','website','annualSales','isRegistered','registrationDate','phone','description','numProducts','numScientificNames','numSignedAgreements','numInquries','numUsers'];

  ContactDataSource: any;
  inputTarget: any;
  buyerlist: any;
  editProfile = true;
  editProfileIcon = 'icofont-edit';
  subForm: FormGroup;
  editAbout = true;
  editAboutIcon = 'icofont-edit';
  Narrrow: any;
  Prods: any;
  filteredscientificNames: Observable<any>;
  scientificNames: any[] = [];
  seletedscientificNames: any = [];
  public carouselTileOne: NgxCarousel;
  selectionTargeting: SelectionModel<any>;
  selectionVacant: SelectionModel<any>;
  selectionExclusivity: SelectionModel<any>;
  selectionCallLog: SelectionModel<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginatorVacant') paginatorVacant: MatPaginator;
  @ViewChild('paginatorExclusivity') paginatorExclusivity: MatPaginator;
  @ViewChild('paginatorCallLog') paginatorCallLog: MatPaginator;
  medicalline: any[] = [];
  MedicallineFilter: Observable<any>;
  selectedMedicallineList: any[] = [];
  selectedMedicallineListid: any[] = [];

  public editor;
  public editorContent: string;
  public editorConfig = {
    placeholder: 'About Your Self'
  };

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  profitChartOption: any;
  countriesList: any[] = [];

  public show = false;
  public buttonName: any = 'Show';
  ManuScientificNames: any;

  public show2 = false;
  public buttonName2: any = 'Show';
  narrowtermlist: any;
  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  selectedSpecialitiesList: any[] = [];
  selectedSpecialitiesListid: any[] = [];
  filter: any;
  countries: any = [];
  scientificnamelist: any[] = [];
  scientificnameFilter: Observable<any>;
  selectedscientificname: any[] = [];
  selectedscientificid: any[] = [];
  // share profile fileds
  shareProfileForm: FormGroup;
  Scientific: ScientificFormModel;
  specialityscientificnameForm: FormGroup;
  medicallinenameForm: FormGroup;
  commentForm: FormGroup;
  contractForm: FormGroup;
  callForm: FormGroup;
  updateform: FormGroup;
  AddUpdateForm: FormGroup;
  timeNow = new Date().getHours() + ':' + new Date().getMinutes();
  seoDataToSend: any;
  @ViewChild('autoCompletescientificname') autoCompletescientificname: MatAutocomplete;
  @ViewChild('autoCompletespeciality') autoCompletespeciality: MatAutocomplete;
  @ViewChild('table', { read: MatSort }) sort: MatSort;

  Existing: MatTableDataSource<ExistingModel>;
  Available: MatTableDataSource<AvailableModel>;
  NotExist: MatTableDataSource<FilesViewModel>;
  Existselection: SelectionModel<ExistingModel>;
  filesSource: MatTableDataSource<FilesViewModel>;
  selection: SelectionModel<FilesViewModel>;
  selectionExs: SelectionModel<ExistingModel>;
  selectionAv: SelectionModel<AvailableModel>;

  
  updatedfileslist: any[] = [];
  form: FormGroup;
  items: FormArray;
  filteredOptions: {};
  checked = false;
  buyers: any[] = [];
  filteredSoldto: Observable<any>;
  Soldto: any[] = [];
  selectedSoldto: any[] = [];
  arrayControl: Observable<any>;
  SCi: any[] = [];
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);

  constructor(
    public dialogRef: MatDialogRef<ManufacturerDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    public dialog: MatDialog,
    private _manuService: ManufacturerService,
    private auth: AuthService,
    private http: HttpClient,
    private toast: ToastService,
    private _clipboardService: ClipboardService,
    private _userService: UsersService
  ) { }


  ngOnInit() {
    this.comment = this.dialogData[0].comment;
    this.Website = this.dialogData[0].website;
    this.Domain = this.dialogData[0].domain;
    this.renderScientificName('');
    this.onViewRequirement(this.dialogData);
    this._manuService.getCountry().pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.countries = res.data;
    });
    
    // this._manuService.getProspectedSciName(this.dialogData[0].prospectedCompanyId).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
    //   this.scientificnamelist = res.data.items;
    // });

    this.GetProspectedSciName(1);
  }


  onTabChange(event) {
    switch (event.nextId) {
      case '2':
        this.getManufacturerRequirments();
        break;
    }
  }


  getManufacturerRequirments() {
 
    let id = this.dialogData[0].id;
    this._manuService.getManuNotRequirments(id).pipe(takeUntil(this.onDestroy$)).subscribe(
      (response: any) => {
        this.manufacturerRecuirment = response.data;
      },
      error => {
        console.log(error);
      });
  }

  checkNarrowTerms(object) {
    let list = this.updateform.get('SubProduct').value;
    let listid = this.updateform.controls.SubProducts.value;
    if (list.length === 0) {
      this.updateform.controls.SubProduct.patchValue([object]);
      this.Prods = [object];
      this.updateform.controls.SubProducts.patchValue([object.id]);
    } else {
      let i = list.indexOf(object);
      if (i === -1) {
        list.push(object);
        listid.push(object.id);
      } else {
        list.splice(i, 1);
        listid.splice(i, 1);
      }

      this.updateform.controls.SubProduct.patchValue(list);
      this.Prods = list;
      this.updateform.controls.SubProducts.patchValue(listid);

    }
  }

  moreOptions() {
    if (this.showmore == false)
      this.showmore = true;
  }

  AddSelectedscientificNames(event) {
    this.seletedscientificNames = [];

    this.seletedscientificNames.push(event.option.value);
    this._manuService.getSCiwithNarrow(event.option.value.id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.Narrrow = res[0];

      let selectedBuyers = [];

      this.updateform.controls.buyers.patchValue(selectedBuyers);
    });
    const index = this.scientificNames.indexOf(event.option.value);
    if (index > -1) {
      this.scientificNames.splice(index, 1);
    }

  }

  removescientificNames(i) {
    const temp = this.seletedscientificNames[i];
    this.scientificNames.push(temp);
    this.seletedscientificNames.splice(i, 1);
  }

  _filterscientificNames(value: any): Observable<any[]> {
    let filterValue = '';
    if (typeof value === "object") {
      filterValue = value.name;
      return this._manuService.getScientificNameList(filterValue);
    } else if (typeof value === 'string') {
      filterValue = value.toLowerCase();
      return this._manuService.getScientificNameList(filterValue);
    } else {
      return of([]);
    }
  }


  openAddUser(details): void {
    const dialogRef = this.dialog.open(AddUpdateUserComponent, {
      width: '45%',
      data: {
        items:{},
        details:details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    });
  }

  EditUser(data, details) {
    const dialogRef = this.dialog.open(AddUpdateUserComponent, {
      width: '45%',
      data: {
        items: data,
        details:details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.renderInfo();
    }
    );
  }

  openAddScie(data): void {
    const dialogRef = this.dialog.open(AddEditScinameComponent, {
      width: '45%',
      // height: '50%',
      data: {
        type: 'prosciename',
        items: {},
        details: data
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      if(result) {
        this.GetProspectedSciName(1);
      }
    });
  }

  EditScie(data, details) {
    const dialogRef = this.dialog.open(AddEditScinameComponent, {
      width: '45%',
      // height: '50%',
      data: {
        type: 'prosciename',
        items: data,
        details: details
      }
    });
    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      if(result) {
        this.GetProspectedSciName(1);
      }
    });
  }

  DeleteScie(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: '45%',
      data: {
        title: 'Prospected Scientific Name',
        id: id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this._manuService.deleteManufacturersProspecSciName(id) : of(undefined);
        }),
        tap((res: number) => {
          this.GetProspectedSciName(1);
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  onDeactivate(id: number): void {
    let Activate = {
      IsActive: false,
      id: id
    }

    this.activateDeactivate(Activate);
  }

  onActivate(id: number): void {
    let Activate = {
      IsActive: true,
      id: id
    }
    this.activateDeactivate(Activate);
  }

  activateDeactivate(Activate) {
    this._userService
      .ActivateDeactivateUser(Activate)
      .pipe(
        take(1),
        tap((res) => {
          this.renderInfo();
        })
      )
      .pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  onAddComment(formData) {
    if (!formData.invalid) {
      let data = {
        Text: formData.value.comment,
        ManufacturerId: this.dialogData[0].id
      }
      this._manuService
        .addCommentdata(data)
        .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
          this.toast.success('Has been added successfully');
        });
    } else {
      Object.keys(formData.controls).forEach(key => {
        if (formData.getControl(key) != null)
          formData.getControl(key).markAsTouched();
      });
    }
  }


  onAddProduct(formData) {
    if (!formData.invalid) {
      let data = {
        Id: 0,
        CompanyID: this.dialogData[0].companyID,
        Image: '',
        ProductDescription: formData.value.Description,
        ScientificId: formData.value.Scientific.id,
        CountryID: formData.value.Country.id,
        SubTitle: formData.value.SubTitle,
        Title: formData.value.Name
      }

      if (this.localImages[0] != null) {
        this.uploadProductImage(this.localImages[0], function (imgLocation) {
          data.Image = imgLocation;
        });
      }

      this._manuService
        .addProduct(data)
        .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
          formData.reset();
          this.toast.success('Successfully added');
        });
    } else {
      Object.keys(formData.controls).forEach(key => {
        if (formData.getControl(key) != null)
          formData.getControl(key).markAsTouched();
      });
    }
  }

  uploadProductImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'product/images/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  onAddCrawler(formData) {
    if (!formData.invalid) {
      let data = {
        CompanyID: this.dialogData[0].companyID,
        Domain: formData.value.Domain,
        Website: formData.value.Website
      }
      this._manuService
        .addAdminCrawllerdata(data)
        .pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
          this.toast.success('Has been added successfully');
        });
    } else {
      Object.keys(formData.controls).forEach(key => {
        if (formData.getControl(key) != null)
          formData.getControl(key).markAsTouched();
      });
    }
  }

  ngAfterViewInit() {
  }


  GetProspectedSciName(pagenumber: number) {
    this.loadingSciNames = true;
    this._manuService.getProspectedSciName(this.dialogData[0].prospectedCompanyId, pagenumber, this.pageSize).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.scientificnamelist = res.data.items;
      debugger
      this.length = res.data.count;
      this.loadingSciNames = false;
    });
  }

  pageChange(e) {
    this.GetProspectedSciName(e.pageIndex + 1);
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  renderInfo() {
    this._manuService.getManufacturerById(this.dialogData[0].id).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      this.dataSource = new MatTableDataSource<any>(res.data);
      this.dialogData = res.data;
      this.comment = res.data[0].comment;
      this.Website = res.data[0].website;
      this.Domain = res.data[0].domain;
    });
  }


  renderScientificName(searchvalue: string) {
    let searchText = '';
    if (searchvalue != '' && searchvalue != null) {
      searchText = '&Name=' + searchvalue;
    }
    this._manuService.getScientificNameSuggestion(searchText).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.SCi = res.data.items;
      this.displayScientific(res.data.items);
    });
  }

  handleScientificChanging(searchvalue) {
    if (searchvalue.name == undefined) {
      this.renderScientificName(searchvalue);
    }
  }

  displayScientific(Scient) {
    return Scient ? Scient.name : undefined;
  }

  handleBaseImageChange(e) {
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }


  onViewRequirement(data) {
    let token = data[0].token,
      manuId = data[0].companyID;
    const obj = '?CompanyToken=' + token + '&CompanyId=' + manuId + '&CompanyType=manufacturer';
    this._manuService.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const response: any = res;
      this.profilelink = res.data.find(x => x.key.toLowerCase() == 'profile').link;
      this.manulink = res.data.find(x => x.key.toLowerCase() == "manufacturerequirment").link;;
      // window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token + "&redirectUrl=" + response.nextStage);
    });
  }

  // EditViewRequirement(token, manuId) {
  //   const obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'manufacturer', RedirectType: 3 };
  //   this._manuService.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // onViewProfileQalification(token, manuId) {
  //   const obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'manufacturer', RedirectType: 2 };
  //   this._manuService.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     window.open(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }

  // copyLink(token, manuId) {
  //   const obj = { CompanyToken: token, CompanyId: manuId, CompanyType: 'manufacturer', RedirectType: 1 };
  //   this._manuService.viewProfile(obj).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
  //     const response: any = res;
  //     this._clipboardService.copyFromContent(response.url + "?redirectType=" + response.redirectType + "&token=" + response.token);
  //   });
  // }
}
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';

import { ReplaySubject } from 'rxjs';
import { AuthService } from '../../_services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { CurrentUser } from '../../_model/CurrentUser';
import { ConversationsService } from '../conversations.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'chat-messages-panel',
  templateUrl: './messages-panel.component.html',
  styleUrls: ['./messages-panel.component.scss']
})
export class MessagesPanelComponent implements OnInit, OnDestroy {

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.onDestroy$)).subscribe((param: any) => {
      this.groupId = Number(param.id);
      this.messagesService.GetMessages(Number(param.id)).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
        this.messages = res;
       setTimeout(() => { this.scrollDown() }, 500);
        this.flagMessages();
        this.subscribeToChannel(Number(param.id));
      });
    })
  }
  ngOnDestroy(): void {
    this.onDestroy$.next()
  }
  onDestroy$ = new ReplaySubject<void>();
  channel: any;
  messages: any = []
  text: string;
  groupId: number;
  @ViewChild('messagesScroll') messageboxcontainer: ElementRef<HTMLDivElement>;
  constructor(private authService: AuthService, private route: ActivatedRoute, private messagesService: ConversationsService) {
    this.currentUser = this.authService.currentUserValue;
  }
  currentUser: CurrentUser;
  flagMessages() {
    if (this.messages) {
      this.messages = this.messages.map(
        (message: any): any => {
          const flag: 'received' | 'sent' = message.userId === this.currentUser.id ? 'sent' : 'received';
          return { ...message, flag };
        }
      );
    }
  }
  subscribeToChannel(id) {
    let _this = this;

    this.channel = this.messagesService.pusher.subscribe('private-' + id);
    this.channel.bind('new_message', function (data) {
      if (data.messageView.userId === _this.currentUser.id) {
        data.messageView.flag = 'sent';
      } else {
        data.messageView.flag = 'received';
      }
      _this.messages.push(data.messageView);
      _this.scrollDown();
    });
  }
  sendMessage() {
    let object: any = {};
    object.Message = this.text;
    this.messagesService.createMessage(object, this.groupId).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.text = '';
    })
  }
  scrollDown() {
    const bottomPosition = this.messageboxcontainer.nativeElement.scrollHeight
    this.messageboxcontainer.nativeElement.scrollTo({top: bottomPosition, behavior: 'smooth'})
  }

}

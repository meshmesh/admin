import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguagesComponent } from './languages.component';
import { SharedModule } from '../shared/shared.module';
import { LanguagesRoutingModule } from './languages-routing.module';
import { AddLanguageComponent } from './addLanguage/addLanguage.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LanguagesRoutingModule,

  ],
  declarations: [LanguagesComponent, AddLanguageComponent],
  entryComponents: [AddLanguageComponent]
})
export class LanguagesModule { }

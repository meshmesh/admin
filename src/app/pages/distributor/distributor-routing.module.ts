import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistributorViewComponent } from './distributor.component';
import { DistributorResolver } from './distributor.resolver';


const routes: Routes = [
  {
    path: '',
    component: DistributorViewComponent,
    data: {
      title: 'Distributor',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    },
    resolve: {
      distributor: DistributorResolver 
    } 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistributorViewRoutingModule { }

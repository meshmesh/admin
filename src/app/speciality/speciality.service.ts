import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecialityService {
  APIEndpoint = environment.ApiUrl;
  constructor(private http: HttpClient) {}
  getSpecalities(): Observable<any> {
    return this.http.get<any>(this.APIEndpoint + '/api/Specialities/GetSpecialities');
  }

  getSpecality(id): Observable<any> { 
    return this.http.get<any>(this.APIEndpoint + '/api/AdminSpeciality/Get/'+ id);
  }
  getSpec(pageIndex): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '/api/AdminSpeciality/search?PageSize=10&PageNumber=' + pageIndex);
  }
  FilterData(pageIndex, data): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/AdminSpeciality/search?PageSize=10&PageNumber=' + pageIndex + '&' + data
    );
  }

  getManuSpec(ObjectId): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=2'
    );
  }

  getDistSpec(ObjectId): Observable<any[]> {
    return this.http.get<any[]>(
      this.APIEndpoint + '/api/AdminCompany/GetCompanybySource?ObjectId=' + ObjectId + '&SourceId=7'
    );
  }

  viewProfile(obj) {
    return this.http.post(this.APIEndpoint + '/api/Users/AddRequestLogin', obj);
  }

  AddSpec(param) {
    return this.http.post(this.APIEndpoint + '/api/AdminSpeciality/AddUpdate', param);
  }

  /* getMedicalLines() : Observable<any> {
  return this.http.get<any>(this.APIEndpoint + '/api/MedicalLines/GetMedicalLines');
 } */

  getMedicalLines(name): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '/api/MedicalLines/GetMedicalLines?Name=' + name);
  }

  getSpecByID(id): Observable<any[]> {
    return this.http.get<any[]>(this.APIEndpoint + '/api/AdminSpeciality/search?PageSize=10&Specialtiesids=' + id);
  }
}

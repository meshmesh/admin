import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  empty: any;
  tags:any[];
  @Input() filterOptions;
  @Output() dataFromFilters = new EventEmitter();
  fromDate: any = '';
  toDate: any = '';
  fromDateSingle: any = '';
  toDateSingle: any = '';

  constructor(private router: Router) {}

  ngOnInit() {
    this.empty = this.filterOptions;
    this.empty.push({});
  }

  dataResivedFromText(event) {
    this.dataFromFilters.emit(event);
  }
 
  dataResivedFromMultiText(event) {
    this.dataFromFilters.emit({isTags:true,data:this.tags.map(function(item){return item.value})});
  }

  dataResivedFromSelect(event) {
    this.dataFromFilters.emit(event);
  }

  dataResivedFromMultiSelect(event) {
    this.dataFromFilters.emit(event);
  }

  dataResivedFromCheck(event) {
    this.dataFromFilters.emit(event);
  }

  dataResivedFromToggel(event) {
    this.dataFromFilters.emit(event);
  }

  fromDataHandler(event) {
    this.fromDate = event;
    const obj = {
      from: this.fromDate,
      to: this.toDate
    };
    // this.dataFromFilters.emit({
    //   index: event[0].index,
    //   title: 'rangeDate',
    //   value: obj
    // });
  }

  toDataHandler(event) {
    this.toDate = event;
    const obj = {
      from: this.fromDate,
      to: this.toDate
    };
    this.dataFromFilters.emit({
      index: event[0].index,
      title: 'rangeDate',
      value: obj
    });
  }

  fromSingleDataHandler(event) {
    this.fromDateSingle = event;
    const obj = {
      from: this.fromDateSingle,
      to: this.toDateSingle
    };
    this.dataFromFilters.emit({
      index: 0,
      title: 'singleDate',
      value: obj
    });
  }

  toSingleDataHandler(event) {
    this.toDateSingle = event;
    const obj = {
      from: this.fromDateSingle,
      to: this.toDateSingle
    };
    this.dataFromFilters.emit({
      index: 0,
      title: 'singleDate',
      value: obj
    });
  }

  fromAutoCompHandler(event) {
    this.dataFromFilters.emit(event);
  }
  dataFromNumberHandler(event) {
    this.dataFromFilters.emit(event);
  }

  handleDataFromSort(e) {
    this.dataFromFilters.emit(e);
  }
}

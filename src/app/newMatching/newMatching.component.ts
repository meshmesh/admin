import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newMatching',
  templateUrl: './newMatching.component.html',
  styleUrls: ['./newMatching.component.scss']
})
export class NewMatchingComponent implements OnInit {
  public configOpenRightBar: string;
  public config: any;
  filtersRes = '';
  dataFromFilters: any[] = [];
  filterOptions = [
 
    {
      index: 0,
      type: 'toggel',
      subTitle: 'Client',
      order: 1,
      value: [],
      title: 'Client',
      apiTitle: '',
      dbArg: ''
    },
    {
      index: 1,
      type: 'toggel',
      subTitle: 'Closing',
      order: 2,
      value: [],
      title: 'Closing',
      apiTitle: '',
      dbArg: ''
    },
    {index : 2,
    type : 'autocomplete',
  order : 3,
    subTitle : 'manufacturer',
    value: [],
    title: 'Manufacturer',
    apiTitle : 'ManufacturIds'

  },
  { index : 3,
    type :'autocomplete',
    order: 4,
    subTitle : 'distributor',
    value : [],
    title : 'Distributor',
    apiTitle:'DistributorIds'

  },
  {
    index: 4,
    type: 'autocomplete',
    subTitle: 'mLine',
    order: 5,
    value: [],
    title: 'Medical Line',
    apiTitle: 'MedicalLineIds',
    list: [],
    dbArg: 'MedicalLineIds'
  },
  {
    index: 5,
    type: 'autocomplete',
    subTitle: 'Specialty',
    order: 6,
    value: [],
    title: 'Specialty',
    apiTitle: 'SpecialtyIds',
    list: [],
    dbArg: 'SpecialtyIds'
  },
  {
    index: 6,
    type: 'autocomplete',
    subTitle: 'scientificName',
    order: 7,
    value: [],
    title: 'Scientific Name',
    apiTitle: 'ScientificName',
    list: [],
    dbArg: 'ScientificName'
  },
   
    {
      index: 7,
      type: 'autocomplete',
      subTitle: 'country',
      order: 8,
      value: [],
      title: 'Country',
      apiTitle: 'ManufacturCountryIds',
      dbArg: 'ManufacturCountryIds'
    },
    
    {
      index: 8,
      type: 'autocomplete',
      order: 9,
      value: [],
      title:'Distributor Country',
      subTitle: 'country',
      apiTitle: 'DistributorCountryIds'
    }

  ];

  constructor() { }

  ngOnInit() {
  }

  
  handleDataFromFilters(event) {
    this.dataFromFilters[event.index] = event;
    this.filtersRes = '';
    this.dataFromFilters.forEach(elem => {
      if (elem) {
        if (Array.isArray(elem.value)) {
          for (const i in elem.value) {
            // for 'CheckBox' filters
            if (elem.type === 'checkBoxs') {
              this.filtersRes += elem.value[i];
            } else {
              // for 'Multi Select' filters
              this.filtersRes += '&' + elem.apiTitle + '=' + elem.value[i];
            }
          }
        } else {
          this.filtersRes += '&' +  elem.title  + '=' + elem.value;
        }
      }
    });
   
  }
  
  toggleRightbar() {
    this.configOpenRightBar = this.configOpenRightBar === 'open' ? '' : 'open';
  }
}

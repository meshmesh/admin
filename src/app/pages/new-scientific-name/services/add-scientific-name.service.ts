import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

const APIEndpoint = environment.ApiUrlV3;

@Injectable()
export class AddScientificNameService {
  
  onDestroy$ = new ReplaySubject<void>();
  specalities: { value: number; text: string }[] = [];
  medicalLines: { value: number; text: string }[] = [];
  companies: { value: number; text: string }[] = [];
  scientificStringList: any[];
  scientificIdList: any[];


  constructor(private http: HttpClient) { }

  search(args, pageNumber) {
    let url =
      APIEndpoint +
      'AdminScientificNames/Search?PageSize=10&PageNumber=' +
      pageNumber;
    for (let i = 0; i < args.length; i = i + 2) {
      url += '&' + args[i] + '=' + args[i + 1];
    }

    return this.http.get(url);
  }

  getSpecality(medId) {
    this.http.get(APIEndpoint + 'Specialties/Search?ForAdmin=true&MedicalLineId=' + medId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          if (this.specalities.length !== response.length) {
            for (let i = 0; i < response.length; i++) {
              this.specalities.push({
                value: response[i].id,
                text: response[i].speciality
              });
            }
          }
        },
        error => console.log(error)
      );
  }

  getSpecalities(medId): Observable<any> {
    return this.http.get<any>(APIEndpoint + 'Specialties/Search?ForAdmin=true&MedicalLineId=' + medId);
  }

  addManufacturer(data): Observable<any> {
    return this.http.post<any>(
      APIEndpoint + 'AdminScientificNames/Manufacturers', data
    );
  }

  removeManufacturer(CompanyId,ScientificNameId): Observable<any> {
    return this.http.get<any>(
      APIEndpoint + 'AdminScientificNames/Manufacturers/Delete?CompanyId=' + CompanyId + '&ScientificNameId=' + ScientificNameId
    );
  }

  getMedicalLineTabs() {
    return this.http.get<any>(
      APIEndpoint + 'MedicalLines'
    ); 
  }

  getSpecialityAccordion(data) {
    return this.http.get<any>(
      APIEndpoint + `Specialties/GetSpecialityWithChilds?MedicalLineId=${data.MedicalLineId}&SpecialtyId=${data.SpecialtyId ? data.SpecialtyId : ''}`
    );
  }


  getMedicalLines() {
    this.http
      .get(APIEndpoint + 'MedicalLines?PageNumber=1&PageSize=100')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          if (this.medicalLines.length !== response.length) {
            for (let i = 0; i < response.length; i++) {
              this.medicalLines.push({
                value: response[i].id,
                text: response[i].name
              });
            }
          }
        },
        error => console.log(error)
      );
  }

  getMedicalLines2(): Observable<any> {
    return this.http.get<any>(
      APIEndpoint + 'MedicalLines?PageNumber=1&PageSize=100'
    );
  }

  getCompanies() {
    this.http.get(APIEndpoint +'AdminCompany/CompanyList?NameOrSlug=test&Type=distributor')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (response: any) => {
          for (let i = 0; i < response.data.length; i++) {
            this.companies.push({
              value: response.data[i].id,
              text: response.data[i].name
            });
          }
        },
        error => console.log(error)
      );
      }


  getScientificNameSuggestion(key): Observable<any> {
    const url =
      APIEndpoint +
      `ScientificNames/SearchElsatic?Pagesize=30&PageNumber=1&Key=${key}`;
    return this.http.get(url);
  }
  getManufacturersuggestions(str): Observable<any> {
    const url =
      APIEndpoint +
      'AdminCompany/CompanyList?NameOrSlug=' +
      str +
      '&Type=manufacturer';
    return this.http.get<any>(url);
  }

  deleteScientificName(id) {
    return this.http.post(
      APIEndpoint + 'AdminScientificNames/Delete?Id=' + id,
      {}
    );
  }

  approveScientificName(id) {
    return this.http.post(
      APIEndpoint + 'AdminScientificNames/Approve?Id=' + id,
      {}
    );
  }

  addUpdate(obj) {
    let apiUrl = '';
    if (obj.Id > 0)
      apiUrl = APIEndpoint + 'AdminScientificNames/Update'
    else
      apiUrl = APIEndpoint + 'AdminScientificNames'
    return this.http.post(apiUrl,
      obj
    );
  }

  approveScieName(obj) {
    let apiUrl = '';
    if (obj.Id > 0)
      apiUrl = APIEndpoint + 'AdminScientificNames/Suggestion/Approve'
    else
      apiUrl = APIEndpoint + 'AdminScientificNames/Suggestion/Approve'
    return this.http.post(apiUrl,
      obj
    );
  }
}

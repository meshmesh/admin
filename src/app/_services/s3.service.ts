import { Injectable } from '@angular/core';
import { S3 } from 'aws-sdk/clients/all';
import { Observable } from 'rxjs';
import { Guid } from 'guid-typescript';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class S3Service {

  bucket = new S3({
    accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
    secretAccessKey: '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',
    region: 'us-west-2'
  });
  params = {
    Bucket: 'aumet-data',
    Key: '',
    Body: ''
  };
  constructor() { }
  uploadFile(folder: string, file: File, returnedObject: any = {}): Observable<FileUploadStatus> {
    return new Observable<FileUploadStatus>((observer) => {
      const fileData = {
        Bucket: 'aumet-data',
        Key: `${folder}/${file.name}` + Guid.create() + '.' +
          this.getFileExtension(file.name),
        ContentType: file.type,
        Body: file
      };
      const s3 = new S3(environment.S3_bucket);
      s3.upload(fileData, function (err, data) {
        if (err) {
          observer.next(new FileUploadStatus(file.name, 0, `${err.message}`, null, null));
          console.log(err.stack);
        }
        observer.next(new FileUploadStatus(file.name, 0, '', data, returnedObject));
      }).on('httpUploadProgress', function (progress) {
        const percentage = (progress.loaded) * 100 / progress.total;
        observer.next(new FileUploadStatus(file.name, percentage, '', null, null));
      });
    });
  }

  getFileExtension(filename) {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

}
export class FileUploadStatus {
  name: string;
  percentage: number;
  error: string;
  data: any;
  returnedObject: any;
  constructor(name: string, percentge: number, error: string, data: any, returnedObject: any) {
    this.name = name;
    this.percentage = percentge;
    this.error = error;
    this.data = data;
    this.returnedObject = returnedObject;
  }
}

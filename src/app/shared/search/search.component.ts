import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { SearchService } from './search.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit, OnChanges , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  isCollapsed = false;
  @Input() pageType;
  mainSearch: { id: string; value: string; text: string; api: string } = {
    id: '-1',
    value: 'All',
    text: '',
    api: ''
  };
  searchValue: { value: number; label: string } = { value: -1, label: '' };
  searchText: string;
  mainCategories: any[] = [];
  test: { value: string; id: number };
  @Output() dataFromSearch = new EventEmitter();
  constructor(private searchService: SearchService) {}

  ngOnInit() {
    if (this.pageType === 'scientificName') {
      this.mainCategories.push({
        id: -1,
        value: 'All'
      });
      this.mainCategories.push({
        id: 1,
        value: 'Approved'
      });
      this.mainCategories.push({
        id: 0,
        value: 'Not Aproved'
      });
    } else if (this.pageType === 'products') {
      this.mainCategories.push({
        id: 0,
        value: 'All'
      });
      this.mainCategories.push({
        id: 1,
        value: 'Clients'
      });
    } else if (this.pageType === 'catalog') {
      this.mainCategories.push({
        id: -1,
        value: 'All'
      });
      this.mainCategories.push({
        id: 1,
        value: 'Clients'
      });
      this.mainCategories.push({
        id: 0,
        value: 'Zero Products'
      });
    } else if (this.pageType === 'manufactorar') {
      this.mainCategories.push({
        id: -1,
        value: 'All'
      });
      this.mainCategories.push({
        id: 1,
        value: 'Registered',
        api: '&CompanyRegisted=true'
      });
      this.mainCategories.push({
        id: 2,
        value: 'Closing',
        api: '&CompanyClosing=true'
      });
      this.mainCategories.push({
        id: 3,
        value: 'Client',
        api: '&CompanyClient=true'
      });
      this.mainCategories.push({
        id: 4,
        value: 'Favorite',
        api: '&CompanyFav=true'
      });
    } else if (this.pageType === 'Distributor') {
      this.mainCategories.push({
        id: -1,
        value: 'All'
      });
      this.mainCategories.push({
        id: 0,
        value: 'Registered',
        api: '&Registed=true'
      });
      this.mainCategories.push({
        id: 1,
        value: 'Client',
        api: '&Client=true'
      });
      this.mainCategories.push({
        id: 2,
        value: 'Closing',
        api: '&Closing=true'
      });
      this.mainCategories.push({
        id: 3,
        value: 'Favorite',
        api: '&Fav=true'
      });
    } else {
      this.searchService.getMainCategory(this.pageType).pipe(takeUntil(this.onDestroy$)).subscribe(
        (response: any) => {
          for (let i = 0; i < response.length; i++) {
            this.mainCategories.push({
              id: response[i].id,
              value: response[i].label
            });
          }
          this.mainCategories.unshift({
            id: '-1',
            value: 'All'
          });
        },
        error => console.log(error)
      );
    }
  }

  handleButton(event) {
    // this.isCollapsed = false;
    this.mainSearch.id = event.target.value;
    this.mainSearch.value = event.target.name;
    if (event.target.api) {
      this.mainSearch.api = event.target.api;
    }
  }

  ngOnChanges() {}

  openCollapse() {
    this.isCollapsed = true;
  }

  closeCollapse() {
    this.isCollapsed = false;
  }

  handleCollapse() {
    // this.isCollapsed = true;
  }

  search() {
    console.log(this.mainSearch);
    this.dataFromSearch.emit(this.mainSearch);
  }
}

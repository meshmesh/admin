import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScientificNameComponent } from 'src/app/pages/scientific-name/scientific-name.component';

const routes: Routes = [
  {
    path: '',
    component: ScientificNameComponent,
    data: {
      title: 'Scientific Name',
      icon: 'icon-layout-cta-right',
      caption: 'Scientific Name Page',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScientificNameRoutingModule { }

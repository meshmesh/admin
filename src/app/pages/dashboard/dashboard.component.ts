import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from "src/environments/environment";
import { DashboardService } from './dashboard.service';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  // should be URL to lotty loader image
  iframeUrl: string = "assets/images/medical-care.jpg";
  METABASE_SITE_URL: string = environment.METABASE_SITE_URL;
  onDestroy$ = new ReplaySubject<void>();

  constructor(private service: DashboardService) { }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnInit() {
    this.service.getToken(environment.METABASE_SECRET_KEY).subscribe((response:any) => {
      this.iframeUrl = this.METABASE_SITE_URL + "/embed/dashboard/" + response.data + "#bordered=false&titled=false";
    }, error => {
    });
  }

}

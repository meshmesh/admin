import { Component, OnInit, OnDestroy } from '@angular/core';
import { ArticleService } from '../article.service';
import { Observable, of, ReplaySubject, Subject, BehaviorSubject } from 'rxjs';
import { Article } from 'src/app/_model/article';
import { tap, take, switchMap, takeUntil, map } from 'rxjs/operators';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { NewSeoModalComponent } from 'src/app/shared/seo-modal/seo-modal.component';
import { Router } from '@angular/router';
import { DeleteConfirmDialogComponent } from 'src/app/shared/delete-confirm-dialog/delete-confirm-dialog.component';

export interface Arguments {
  PageSize: number;
  PageNumber: number;
  Published: boolean;
  Name: string;
}

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, OnDestroy {
  // New Version
  displayedColumns = ['actions', 'id', 'title', 'Type', 'category', 'Related', 'published'];
  dataSource: MatTableDataSource<Article>;
  article$: Observable<Article[]>;
  arguments: BehaviorSubject<Arguments> = new BehaviorSubject({
    PageSize: 10,
    PageNumber: 1,
    Published: true,
    Name: ''
  });
  onDestroy$: ReplaySubject<void> = new ReplaySubject();
  // Old Version
  pageType = 'Article';
  slideWidth = '1px';
  searchPageNumber = 1;
  showFilters = false;
  articlesArray: any;
  resivedFromSearchComponent: any;
  dataFromFilters: any = [];
  mainCategory = 0;
  mainSearchText: string;
  mainCategories: { id: number; name: string }[] = [];
  subCatigory: any;
  filterOptions: any[] = [
    {
      index: 3,
      type: 'rangeDate',
      order: 1,
      value: '',
      title: 'creaition date',
      dbArg: 'CreatedAtFrom'
    },
    {
      index: 0,
      type: 'text',
      order: 2,
      title: 'title',
      value: '',
      dbArg: 'Name'
    },
    {
      index: 1,
      type: 'checkBoxs',
      order: 3,
      value: [],
      title: 'Sub Category',
      list: [],
      dbArg: ''
    },
    {
      index: 2,
      type: 'toggel',
      order: 4,
      value: false,
      name: 'puplished',
      title: 'puplished',
      dbArg: 'Published'
    },
    {
      type: 'sortBy',
      index: 12,
      order: 100,
      comp: 'Article',
      value: []
    }
  ];
  constructor(private articleService: ArticleService, private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.arguments
      .pipe(
        switchMap(args => {
          return this.articleService.getFilteredArticles(args);
        }),
        tap((articles: Article[]) => {
          this.dataSource = new MatTableDataSource(articles);
        })
      )
      .subscribe();
  }

  getArticles(): Observable<Article[]> {
    return this.articleService.getFilteredArticles(this.arguments);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  /**
   *  Subscribe for one time to the current arguments behabiour subject,
   * and then pass a new argument object to the subject, with the updated Name value
   * @param e Outout event from search bar, which is the search string
   */
  onSearch(e: string): void {
    this.arguments
      .pipe(
        take(1),
        map((args: Arguments) => {
          return { ...args, Name: e };
        })
      )
      .subscribe(args => this.arguments.next(args));
  }

  /**
   * Open diaog form for SEO to edit the selected article, if a new value returns an update followed by a refresh will occur
   * @param row The article to edit SEO for
   */
  onOpenSEO(row: Article): void {
    const dialogRef = this.dialog.open(NewSeoModalComponent, {
      data: row.seo,
      minWidth: '65%',
      // minHeight: '500px'
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap(seo => {
          if (seo) {
            const s = JSON.stringify(seo);
            const obj = {
              Id: row.id,
              Items: [
                {
                  Key: 'seo',
                  Value: s
                }
              ]
            };
            return this.articleService.createUpdateArticle(obj);
          } else {
            return of(null);
          }
        }),
        tap(res => {
          if (res) {
            this.article$ = this.getArticles();
          }
        })
      )
      .subscribe();
  }

  /**
   * Change article's published status to true
   * @param id aritcle's id
   */
  onPublish(id: number): void {
    this.articleService
      .puplishArticle(id)
      .pipe(
        take(1),
        tap(() => {
          this.article$ = this.getArticles();
        })
      )
      .subscribe();
  }
  onUnPublish(id: number): void {
    /* this.articleService
      .puplishArticle(id)
      .pipe(
        take(1),
        tap(() => {
          this.article$ = this.getArticles();
        })
      )
      .subscribe(); */
  }

  /**
   * Route to article form to create new article
   */
  onCreateArticle(): void {
    this.router.navigate(['Articles', 'form']);
  }

  /**
   * Route to article form to edit article
   * @param id Id of the article to be edited
   */
  onEditArticle(id: number): void {
    this.router.navigate(['Articles', 'form', id]);
  }

  /**
   * Opens a dialog to confirm the delete of an article,
   * if the dialog returns confirmation a delete request is sent followed by data refresh
   * @param id Id of the article to be deleted
   */
  onDelete(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      data: {
        title: 'Article',
        id: id
      }
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        switchMap((deleted: boolean) => {
          return deleted ? this.articleService.deleteArticle(id) : of(null);
        }),
        tap(res => {
          if (res) {
            this.article$ = this.getArticles();
            const _thisTimer = this;
            location.reload();
          }
        })
      )
      .subscribe();
  }

  private newMethod(response: any) {
    this.articlesArray = response;
  }

  dataFromFiltersHandler(event) {
    if (event.index === 3) {
      let monthto = event.value.to[1].month;
      let monthfrom = event.value.from[1].month;
      let dayto = event.value.to[1].day;
      let dayfrom = event.value.from[1].day;
      const yearto = event.value.to[1].year;
      const yearfrom = event.value.from[1].year;

      if (monthfrom < 10) {
        monthfrom = '0' + monthfrom;
      }
      if (monthto < 10) {
        monthto = '0' + monthto;
      }
      if (dayto < 10) {
        dayto = '0' + dayto;
      }
      if (dayfrom < 10) {
        dayfrom = '0' + dayfrom;
      }
      const to = dayto + '/' + monthto + '/' + yearto;
      const from = dayfrom + '/' + monthfrom + '/' + yearfrom;
      this.dataFromFilters[event.index] = {
        index: 3,
        from: from,
        to: to
      };
    } else {
      this.dataFromFilters[event.index] = event;
    }
    this.search(3);
  }

  search(type) {
    if (type === 'search') {
      this.dataFromFilters = [];
    }

    const length = this.dataFromFilters.length;
    const data = this.dataFromFilters;
    const args = [];

    if (this.mainCategory !== -1) {
      args.push('MainCategory');
      args.push(this.mainCategory);
    }
    if (this.mainSearchText) {
      args.push('Name');
      args.push(this.mainSearchText);
    }

    for (let i = 0; i < length; i++) {
      if (data[i] && data[i].type === 'text') {
        args.pop();
        args.pop();
        if (args.length === 4) {
          args.pop();
          args.pop();
        }
        args.push(data[i].dbArg);
        args.push(data[i].value);
      } else if (data[i] && data[i].type === 'toggel') {
        args.push('Puplished');
        args.push(data[i].value);
      } else if (data[i] && data[i].type === 'checkBoxs') {
        if (args.includes('MainCategory')) {
          args.shift();
          args.shift();
        }
        args.unshift(data[i].value[0].name);
        args.unshift('MainCategory');
      } else if (data[i] && data[i].index === 3) {
        args.push('CreationFromDate');
        args.push(data[i].from);
        args.push('CreationToDate');
        args.push(data[i].to);
      } else if (data[i] && data[i].index === 12) {
        console.log('ready to args', data[i].value);
        args.push('SortBy');
        args.push(data[i].value[1]);
        args.push('SortType');
        args.push(data[i].value[0]);
      }
    }

    this.articleService.search(args, this.searchPageNumber).subscribe(
      (response: any) => {
        this.articlesArray = response;
        this.showFilters = true;
      },
      error => {
        console.log(error);
      }
    );

    this.articleService.getSubCategory(this.mainCategory).subscribe(
      (response: any) => {
        const data = [];
        const sCat = response;
        const l = sCat.length;
        for (let i = 0; i < l; i++) {
          data.push({ id: sCat[i].id, name: sCat[i].name });
        }
        this.filterOptions[1].list = data;
      },
      error => console.log(error)
    );
  }

  handleDataFromSearch(event) {
    if (event.id !== '-1' || event.id !== -1) {
      this.mainCategory = event.id;
      this.mainSearchText = event.text;
    } else {
      this.mainCategory = 0;
      this.mainSearchText = event.text;
    }
    this.search('search');
  }

  handlePageNumber(event) {
    this.searchPageNumber = event;
    this.search('pageNomber');
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;


@Component({
  selector: 'app-range-date',
  templateUrl: './range-date.component.html',
  styleUrls: ['./range-date.component.scss']
})
export class RangeDateComponent implements OnInit {
  @Input() mainData;
  inputValue: string;
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  show = 'none';
  @Output() fromDateOut = new EventEmitter();
  @Output() toDateOut = new EventEmitter();
  constructor(public parserFormatter: NgbDateParserFormatter) { }

  ngOnInit() {
    console.log('man data in range date',this.mainData)
  }

  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.fromDateOut.emit([this.mainData, this.fromDate]);
      this.inputValue = 'from ' + this.parserFormatter.format(this.fromDate);
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
      this.inputValue = 'from ' + this.parserFormatter.format(this.fromDate) + ' to ' + this.parserFormatter.format(this.toDate);
      this.toDateOut.emit([this.mainData, this.toDate]);
    } else {
      this.toDate = null;
      this.fromDate = date;
      this.fromDateOut.emit([this.mainData, this.fromDate]);
      this.toDateOut.emit([this.mainData, this.toDate]);
      this.inputValue = 'from ' + this.parserFormatter.format(this.fromDate);
    }
    if (this.toDate && this.fromDate) {
      this.show = 'none';
      this.inputValue = 'from ' + this.parserFormatter.format(this.fromDate) + ' to ' + this.parserFormatter.format(this.toDate);
    }
  }


  showPicker() {
    if (this.show === 'none') {
      this.show = 'block';
    } else {
      this.show = 'none';
    }
  }
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);
  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);


}
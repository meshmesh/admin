import { Country } from './country';

export interface FollowUp {
  id: number;
  company: Company;
  last_action: Action;
  next_action: Action;
  relatedTo: string;
  user: User;
}

export interface FollowUpLog {
  company: Company;
  actions: Action[];
}

export interface Company {
  id: number;
  name: string;
  type: string;
  token: string;
  country: Country;
  stage: string;
  process: string;
  registrationDate: string;
}

export interface User {
  id: number;
  name: string;
  email: string;
  phone: string;
  remarketing: boolean;
}

export interface Action {
  id: number;
  date: string;
  name: string;
  friendlyName: string;
  relatedTo: string;
  process: Process;
  completed: boolean;
}

export interface Process {
  id: number;
  name: string;
}

const countries: Country[] = [
  { id: 1, code: 'NZ', country: 'New Zealand', display: 'New Zeland', logo: 'http://beta.aumet.me/flags24/BF.png' },
  { id: 2, code: 'PL', country: 'Poland', display: 'Poland', logo: 'http://beta.aumet.me/flags24/PL.png' }
];

const companyuser: User = {
  id: 1,
  name: 'Yazan Shannak',
  email: 'yazan@hotmail.com',
  phone: '0795236417',
  remarketing: false
};
const adminuser = 'Shadi AbuMathkoor';

const last_action: Action = {
  id: 1,
  date: '20/12/2018',
  name: 'Action Name',
  friendlyName: 'Action Friendly Name',
  relatedTo: adminuser,
  process: { id: 1, name: 'Registration' },
  completed: true
};
const next_action: Action = {
  id: 2,
  date: '5/2/2019',
  name: 'Action Name 2',
  friendlyName: 'Action Friendly Name 2 ',
  relatedTo: adminuser,
  process: { id: 1, name: 'Registration' },
  completed: false
};

export const companies: Company[] = [
  {
    id: 1,
    name: 'Company 1 ',
    type: 'Sometype',
    token: 'asdffdsa',
    stage: 'Registration',
    process: 'Fill Data',
    registrationDate: '10/7/2018',
    country: countries[0]
  }
];

export const followUps: FollowUp[] = [
  {
    id: 1,
    company: companies[0],
    last_action: last_action,
    next_action: next_action,
    user: companyuser,
    relatedTo: adminuser
  }
];

export const log: FollowUpLog = {
  company: companies[0],
  actions: [next_action, last_action]
};

export const actionNames: string[] = ['Action Name 1', 'Action Name 2'];

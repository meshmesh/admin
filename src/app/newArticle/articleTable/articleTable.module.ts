import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillEditorModule } from 'ngx-quill-editor';
import { TagInputModule } from 'ngx-chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ArticleTableRoutingModule } from './articleTable-routing.module';
import { ArticlesComponent } from '../articles/articles.component';
import { ArticleTableComponent } from './articleTable.component';
import { ArticleTableService } from './articleTable.service';
import { MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    QuillEditorModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
  
    ArticleTableRoutingModule,
    MatInputModule
  ],
  providers: [ArticleTableService],
  exports: [ArticleTableComponent],
  declarations: [ArticlesComponent, ArticleTableComponent],
  entryComponents: [ArticlesComponent]
})
export class ArticleTableModule {}

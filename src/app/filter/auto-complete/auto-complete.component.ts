import { Component, OnChanges, Input, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss']
})
export class AutoCompleteComponent implements OnChanges {
    
  @Input('option') option: any = {};
  @Output() dataFromAutoComplete = new EventEmitter();
  @ViewChild('Input') Input: ElementRef<HTMLInputElement>;


  filteredList: Observable<any>;


  constructor(private http: HttpClient) { }


  ngOnChanges() {
    this.filteredList = this.http.get<any[]>(environment.ApiUrlV3filter + this.option.apiurl);
  }

  _filter(value) {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value!.toLowerCase();
    }

    if (this.option.list.data.length > 0 && !this.option.hasPagination) {
      return of(this.option.list.data.filter(item => item.name ? item.name.toLowerCase().indexOf(filterValue) === 0 : ''));
    } else {
      return this.http.get<any[]>(environment.ApiUrlV3filter+this.option.apiurl + filterValue);
    }
  }

  AddSelected(event) {
   // this.dataFromAutoComplete.next({});
  }

}

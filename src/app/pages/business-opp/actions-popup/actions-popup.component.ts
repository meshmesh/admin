import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  OnDestroy,
  AfterViewInit,
} from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from "@angular/material";
import { ReplaySubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { BusinessService } from "../business.service";
import { BusinessOpp } from "../business.models";
import { ToastService } from "src/app/_services/toast.service";

class CustomDataType {
  type: number;
  element: BusinessOpp;
}

@Component({
  selector: "app-actions-popup",
  templateUrl: "./actions-popup.component.html",
  styleUrls: ["./actions-popup.component.scss"],
})
export class ActionsPopupComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  onDestroy$ = new ReplaySubject<void>();
  dialogTitle: string;
  displayedColumns: Array<string> = [
    "manufacturerName",
    "sendDate",
    "finshedDate",
    "scheduledCall",
  ];
  comment: string;
  modalType = 1;
  selected_distributor;
  selected_distributor_id;
  isActive: boolean = false;
  emptyData: boolean = false;
  commentText: string;
  // should assign Distributor data type
  distributorOptions: Array<any> = [];

  constructor(
    public dialogRef: MatDialogRef<ActionsPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CustomDataType,
    private service: BusinessService,
    private toast: ToastService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    switch (this.data.type) {
      // Reload
      case 1:
        this.displayedColumns = ["manufacturerName"];
        this.dialogTitle = "Reload Next opportunity";
        this.service
          .getNextBusinessOpp(this.data.element)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe((res: any) => {
            if (res && res.data && res.data.length > 0) {
              this.renderData(res.data);
            } else this.emptyData = true;
          });
        break;

      // Remove
      case 2:
        console.log(this.data.element);
        this.dialogTitle = "Delete Business opportunity";
        this.modalType = 2;
        break;

      // Send BO
      // check if BO is active 
      // if active show a confirmation popup
      // if not active, show the send popup
      case 3:
        console.log(this.data.element);
        this.dialogTitle = "Send Business opportunity";
        this.modalType = 3;
        this.isActive = this.data.element.active;
        this.getAllDistributors('');
        break;

      // Log
      case 4:
        console.log(this.data.element);
        this.dialogTitle = "All business opportunities sent previously";
        this.modalType = 4;
        this.service
          .getAllBusinessOpp(this.data.element)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe((res: any) => {
            this.renderData(res.items);
          });
        break;

      // View & Reject the Company certificate
      case 5:
        console.log(this.data.element);
        this.modalType = 5;
        this.dialogTitle = "Company Certificate";
        this.displayedColumns = ["certId", "certActions"];
        this.renderData(this.data.element.distributorCertificates);
        break;

      // Add comments
      case 6:
        console.log(this.data.element);
        this.dialogTitle = "Add comments";
        this.comment = this.data.element.distributorComment || "";
        this.modalType = 6;
        break;

      // show users
      case 7:
        console.log(this.data.element);
        this.dialogTitle = "Distributor Person";
        this.modalType = 7;
        this.displayedColumns = ["userName", "userEmail"];
        this.renderData(this.data.element.users);
        break;

      // add comments
      case 8:
        console.log(this.data.element);
        this.dialogTitle = "Comment";
        this.modalType = 8;
        this.commentText = this.data.element.distributorComment;
        break;
    }
  }

  rejectCertificate(element) {
    this.service
      .deleteCertificate(element.id)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          if (res.success) {
            this.dialogRef.close(true);
            this.toast.success("Rejected Successfully!");
          }
        },
        (err) => this.toast.error("something wrong happened")
      );
  }

  renderData(data) {
    this.dataSource = new MatTableDataSource<any>(data);
    this.dataSource.paginator = this.paginator;
    // if (this.modalType !== 5) this.dataSource.sort = this.sort;
  }

  onConfirmSendBO() {
    this.isActive = !this.isActive;
  }

  onDelete() {
    this.service
      .deleteBusinessOpp(this.data.element.opportunityID)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          if (res.success) {
            this.dialogRef.close(true);
          } else this.toast.error("something wrong happened");
        },
        (err) => this.toast.error("something wrong happened")
      );
  }

  onSelectionChanged(e) {
    this.selected_distributor = e.option.value.name;
    this.selected_distributor_id = e.option.value.id;
  }

  onAddComment() {
    if (this.comment === "" || this.comment.length <= 3) {
      this.toast.error("Please enter a proper comment");
      return;
    }
    let data = {
      distributorId: this.data.element.distributorID,
      text: this.comment,
    };
    this.service
      .addCommentBusinessOpp(data)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          if (res.success) {
            this.dialogRef.close(true);
            this.toast.success("Added Successfully!");
          } else this.toast.error("something wrong happened");
        },
        (err) => this.toast.error("something wrong happened")
      );
  }

  getAllDistributors(searchText) {
    this.service
      .getClient(this.data.element.distributorID, searchText)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: any) => {
        this.distributorOptions = res.data;
      });
  }

  handleTextChanging(searchText) {
    this.getAllDistributors(searchText);
  }

  onSendBusinessOpp() {
    if (this.selected_distributor) {
      let data = {
        distributorId: this.data.element.distributorID,
        manufactureId: this.selected_distributor_id,
      };
      this.service
        .sendBusinessOpp(data)
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(
          (res: any) => {
            if (res.success) {
              this.dialogRef.close(true);
              this.toast.success("Sent Successfully!");
            } else this.toast.error("something wrong happened");
          },
          (err) => this.toast.error("something wrong happened")
        );
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  close(data) {
    this.dialogRef.close(data);
  }
}

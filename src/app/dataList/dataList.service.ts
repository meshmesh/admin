import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataListService {

  APIEndpoint = environment.ApiUrl;
constructor(private http: HttpClient) { }

getData() : Observable<any[]>{
 return this.http.get<any[]>(this.APIEndpoint + '/api/AdminLookups/search?Approved=false');
}

approval(param){
  return this.http.get(this.APIEndpoint + '/api/AdminLookups/Approve?id='+ param);
}

}

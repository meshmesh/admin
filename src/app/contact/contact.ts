import { Guid } from "guid-typescript";

export class Contact {
    id: string;

    ContactName: string;

    CompanyName: string;

    Email: string;

    PhoneNumber: string;

    Address: string;

    Source: string;

    Comment: string;

    Country: string;

    WebSite: string;

    country: any;
}

import { Component, OnInit, Inject } from '@angular/core';
import { Guid } from 'guid-typescript';
import { S3 } from 'aws-sdk/clients/all';
import { VideosPageService } from '../videosPage.service';
import { VideosForm } from '../VideosForm';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { _lockupService } from 'src/app/_services/_lockup.service';
import { ToastService } from 'src/app/_services/toast.service';

@Component({
  selector: 'app-addVideo',
  templateUrl: './addVideo.component.html',
  styleUrls: ['./addVideo.component.scss']
})
export class AddVideoComponent implements OnInit {
  localImages: File[] = [];
  imagePreview = [];
  Type: any;
  Title: any;
  Description: any;
  Link: any;
  Sort: any;
  Season: any;
  disabled = false;
  videoForm: VideosForm;
  Published: boolean;
  duration: number;
  Createdby : number;
  userlist : any;


  constructor(
    private serv: VideosPageService,
    private toast: ToastService,
    private lookupser : _lockupService,
    public dialogRef: MatDialogRef<AddVideoComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
  ) { }

  ngOnInit() {
    this.videoForm = new VideosForm();

    this.lookupser.getLockUps(31).subscribe(res => {
      this.userlist = res;
    });
    if (this.dialogData.id) {
      this.Type = this.dialogData.type;
      this.Title = this.dialogData.title;
      this.Description = this.dialogData.description;
      this.Link = this.dialogData.youtube;
      this.imagePreview[0] = this.dialogData.thumbnail;
      this.Sort = this.dialogData.sort;
      this.Published = this.dialogData.published;
      this.duration = this.dialogData.duration;
      this.Season = this.dialogData.season;
      
      this.Createdby =  this.dialogData.createdby
    }
  }


  uploadVideoImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'company/avatar/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);

      }
    });
  }



  handleBaseImageChange(e) {
    this.localImages[0] = e.target.files[0];
    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        this.imagePreview.pop();
        this.imagePreview.push(reader.result);
      };
    }
  }

  handleImageError(i) {
    this.imagePreview.splice(i, 1);
  }

  hideBaseImage() {
    this.imagePreview = [];
    this.localImages = [];
  }

  handleBaseImgError() {
    this.imagePreview.pop();
  }


  async submit() {
    if(this.Title && this.Type && this.Link && this.Description && this.localImages){
    if (this.dialogData.id) {

      if (this.localImages.length > 0) {


        const _thisProductImage = this;
        
       await this.uploadVideoImage(this.localImages[0], function (data) {
          _thisProductImage.videoForm.Id = _thisProductImage.dialogData.id
          _thisProductImage.videoForm.Type = _thisProductImage.Type
          _thisProductImage.videoForm.Season = _thisProductImage.Season
          _thisProductImage.videoForm.Title = _thisProductImage.Title
          _thisProductImage.videoForm.Sort = _thisProductImage.Sort
          _thisProductImage.videoForm.Youtube = _thisProductImage.Link
          _thisProductImage.videoForm.Description = _thisProductImage.Description
          _thisProductImage.videoForm.Published = _thisProductImage.Published;
          _thisProductImage.videoForm.duration = _thisProductImage.duration;
          _thisProductImage.videoForm.Thumbnail = data;
          _thisProductImage.videoForm.Createdby = _thisProductImage.Createdby;
          _thisProductImage.addUpdateFunction(_thisProductImage.videoForm);
        });

      }

      else {

        this.videoForm.Type = this.Type
        this.videoForm.Season = this.Season
        this.videoForm.Id = this.dialogData.id
        this.videoForm.Title = this.Title
        this.videoForm.Sort = this.Sort
        this.videoForm.Youtube = this.Link
        this.videoForm.Published = this.Published;
        this.videoForm.Description = this.Description;
        this.videoForm.duration = this.duration;
        this.videoForm.Thumbnail = this.dialogData.thumbnail;
        this.videoForm.Createdby = this.Createdby;
        this.addUpdateFunction(this.videoForm);
      }
    }
    else {

      this.videoForm.Type = this.Type
      this.videoForm.Title = this.Title
      this.videoForm.Sort = this.Sort
      this.videoForm.Season = this.Season
      this.videoForm.Youtube = this.Link
      this.videoForm.Published = this.Published;
      this.videoForm.duration = this.duration;
      this.videoForm.Description = this.Description
      this.videoForm.Createdby = this.Createdby;

      if (this.localImages.length > 0) {
        const _thisProductImage = this;
        this.uploadVideoImage(this.localImages[0], function (data) {
          _thisProductImage.videoForm.Thumbnail = data;

          _thisProductImage.addUpdateFunction(_thisProductImage.videoForm);
        });
      } else {
        this.addUpdateFunction(this.videoForm);
      }
    }
  }

  }

  addUpdateFunction(form) {
    this.disabled = true;
    const _thisTimer = this;
    this.serv.addVideo(this.videoForm).subscribe(x => {
      console.log(x)

      setTimeout(function () {
        _thisTimer.toast.success('Has been added successfuly');
        _thisTimer.close('success');
      }, 1500);
    })
   console.log(this.videoForm)
  }

  close(data): void {
    this.dialogRef.close(data);
  }

}

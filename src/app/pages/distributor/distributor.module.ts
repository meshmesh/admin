import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DistributorResolver } from "./distributor.resolver";
import { DataTableModule } from "angular2-datatable";
import { QuillEditorModule } from "ngx-quill-editor";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxCarouselModule } from "ngx-carousel";
import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { TagInputModule } from "ngx-chips";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { ClipboardModule } from "ngx-clipboard";
import { MatTooltipModule, MatDialogModule } from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "src/app/shared/shared.module";
import { DistributorViewRoutingModule } from "./distributor-routing.module";
import { InterestModule } from "../interest/interest.module";
import { FilterModule } from "src/app/filter/filter.module";
import { AddUpdateUserModule } from "../users/addUpdate-user/addUpdate-user.module";
import { DistributorViewComponent } from "./distributor.component";
import { DistributorListComponent } from "./components/distributor-list/distributor-list.component";
import { MergeDistributorComponent } from "./components/merge-distributor/merge-distributor.component";
import { AddCompanyModalComponent } from "./components/add-company-modal/add-company-modal.component";
import { AddUpdateUserComponent } from "../users/addUpdate-user/addUpdate-user.component";
import { AddUpdateComponent } from "../interest/addUpdate/addUpdate.component";


@NgModule({
  imports: [
    CommonModule,
    DistributorViewRoutingModule,
    SharedModule,
    DataTableModule,
    QuillEditorModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCarouselModule,
    TagInputModule,
    NgxMatSelectSearchModule,
    ClipboardModule,
    InterestModule,
    FilterModule,
    HttpClientModule,
    MatTooltipModule,
    AddUpdateUserModule,
    MatDialogModule,
    NgxMaterialTimepickerModule.forRoot(),
  ],
  exports: [NgxCarouselModule,     
    DistributorViewComponent,
    DistributorListComponent,
    MergeDistributorComponent,
    AddCompanyModalComponent,],

  declarations: [
    DistributorViewComponent,
    DistributorListComponent,
    MergeDistributorComponent,
    AddCompanyModalComponent,
  ],
  entryComponents: [
    AddUpdateUserComponent,
    AddCompanyModalComponent,
    DistributorListComponent,
    MergeDistributorComponent,
    AddUpdateComponent,
  ],
  providers: [DistributorResolver],
})
export class DistributorModule {}

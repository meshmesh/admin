import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-seo',
  templateUrl: './seo.component.html',
  styleUrls: ['./seo.component.scss']
})
export class SeoComponent implements OnInit, OnChanges {
  @Output() fromSeoToAddEdit = new EventEmitter();
  @Input() seoData;
  generalSeo = [];
  specialSeo = [];
  outputSeo: any;
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {
    this.generalSeo = this.seoData.a;
    if (this.seoData.b) {
      this.specialSeo = JSON.parse(this.seoData.b);
      for (let i = 0; i < this.generalSeo.length; i++) {
        for (let j = 0; j < this.specialSeo.length; j++) {
          if (this.generalSeo[i].id === this.specialSeo[j].id) {
            this.generalSeo[i].metaDescription = this.specialSeo[j].metaDescription;
            this.generalSeo[i].metaKeyWord = this.specialSeo[j].metaKeyWord;
            this.generalSeo[i].metaTitle = this.specialSeo[j].metaTitle;
          }
        }
      }
    }

  }


  handleSeo() {
    this.fromSeoToAddEdit.emit(this.generalSeo);
  }

}


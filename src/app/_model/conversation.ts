export interface Conversation {
  id: number;
  users: any;
  messages: Message[];
}

export interface User {
  id: number;
  full_name: string;
  image: string;
  token: string;
}

export interface Message {
  id: number;
  sender: number;
  received_date: [{ id: number; date: Date }];
  text: string;
  flag?: 'received' | 'sent';
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FollowupRoutingModule } from './followup-routing.module';
import { FollowupComponent } from './followup.component';
import { SharedModule } from '../shared/shared.module';
import { FollowUpService } from '../_services/followup.service';
import { UserInfoDialogComponent } from './user-info-dialog/user-info-dialog.component';
import { ActionsLogModalComponent } from './actions-log-modal/actions-log-modal.component';
import { FilterModule } from '../filter/filter.module';

const components = [FollowupComponent];
const modals = [UserInfoDialogComponent, ActionsLogModalComponent];
@NgModule({
  declarations: [...components, ...modals],
  providers: [FollowUpService],
  imports: [CommonModule, FollowupRoutingModule, SharedModule, FilterModule],
  entryComponents: [...modals]
})
export class FollowupModule {}

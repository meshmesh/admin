import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog, MatPaginator } from '@angular/material';
import { InquiryService, } from './inquiry.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NumOfSuggSciNameComponent } from './components/helper-popup/NumOfSuggSciName.component';
import { InquiryUnit, InquirySearchParams, UpdateInquiry, InquiryFilterOptions, InquiryResponse  } from "./inquiry.model";

import { ManufacturerDetailComponent } from '../manufacturer/manufacturer-detail/manufacturer-detail.component';
import { ManufacturerService } from '../manufacturer/services/manufacturer.service';
import { DistributorService } from '../distributor/services/distributor.service';
import { DistributorDetailsComponent } from '../distributor/distributor-details/distributor-details.component';

@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss'],
})
export class InquiryComponent implements OnInit, OnDestroy {

  @ViewChild('table', { read: MatPaginator }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataFromFilters: any[] = [];
  filteredString: string= '';
  displayedColumns: string[] = [
    'Actions',
    'distributorName',
    'manufacturerName',
    'manufacturerWebsite',
    'Country',
    'source',
    'messages',
    'LastStage',
    'LastStageDate',
    'NextStage',
    'NextStageDate',
    'inquirySource',
    'ManufacturerScientificNameCount',
    'ManufacturerSpecialtyCount',
    'ManufacturerMedicalLineCount',
    'ManufacturerUser'
  ];

  filterOptions = InquiryFilterOptions;
  searchParams: InquirySearchParams = new InquirySearchParams();

  onDestroy$: ReplaySubject<void> = new ReplaySubject();
  inquirySource: MatTableDataSource<InquiryUnit>;

  // flags
  currentPage: number = 1;
  pageNumber: number = 1;
  pageIndex: number = 1;
  pageSize: number = 10;
  pageType: string = 'Inquiry';
  length: number;
  isLoading: boolean;

  constructor(
    private dialog: MatDialog, 
    private distributorService: DistributorService,
    private inquiryService: InquiryService, 
    private manuService: ManufacturerService) { }

  ngOnInit() {
    this.getInquiries();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.dialog.closeAll();
  }

  getInquiries() {
    this.isLoading = true;
    this.inquiryService.getAll(this.searchParams, this.filteredString)
    .pipe(takeUntil(this.onDestroy$))
    .subscribe((res) => {
      
      // set length for paginator

      this.renderInquiriesData(res);
    });
  }

  renderInquiriesData(res: InquiryResponse) {
    if(res && res.data) {
      this.length = res.data.count;
      this.inquirySource = new MatTableDataSource<InquiryUnit>(res.data.items);
    } else {
      this.length = 0;
      this.inquirySource = new MatTableDataSource<InquiryUnit>([]);
    }
    this.isLoading = false;
  }

  pageChange(event) {
    this.pageSize = event.pageSize;
    this.searchParams.PageSize = event.pageSize;
    this.searchParams.PageNumber = event.pageIndex + 1;
    this.getInquiries();
  }

  actionTabChange(event) {
    if(this.paginator) this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;
    switch (event.nextId) {
      case '1':
        this.pageType = 'Inquiry';
        this.searchParams.Status = 1;
        this.displayedColumns = [
          'Actions',
          'distributorName',
          'manufacturerName',
          'manufacturerWebsite',
          'Country',
          'source',
          'messages',
          'LastStage',
          'LastStageDate',
          'NextStage',
          'NextStageDate',
          'inquirySource',
          'ManufacturerScientificNameCount',
          'ManufacturerSpecialtyCount',
          'ManufacturerMedicalLineCount',
          'ManufacturerUser'
        ];
        this.getInquiries();
        break;
      case '2':
        this.pageType = 'Email Needed';
        this.searchParams.Status = 2;
        this.displayedColumns = [
          'Actions',
          'distributorName',
          'manufacturerName',
          'manufacturerWebsite',
          'Country',
          'source',
          'messages',
          'reason',
          'statusDate',
          'inquirySource',
          'ManufacturerScientificNameCount',
          'ManufacturerSpecialtyCount',
          'ManufacturerMedicalLineCount',
          'ManufacturerUser'
        ];
        this.getInquiries();
        break;
      case '3':
        this.pageType = 'Stopped';
        this.searchParams.Status = 3;
        this.displayedColumns = [
          'distributorName',
          'manufacturerName',
          'manufacturerWebsite',
          'Country',
          'source',
          'messages',
          'lastActionText',
          'statusDate',
          'reason',
          'NextStageDate',
          'inquirySource',
          'ManufacturerScientificNameCount',
          'ManufacturerSpecialtyCount',
          'ManufacturerMedicalLineCount',
          'ManufacturerUser'
        ];
        this.getInquiries();
        break;
    }
  }

  openDialog(row: InquiryUnit, type: number) {
    this.dialog.open(NumOfSuggSciNameComponent, {
      width: '65%',
      // height: '70%',
      data: { 
        row: row, 
        type: type 
      }
    });
  }


  updateInquiryStatus(element: InquiryUnit, status: number) {
    let data: UpdateInquiry = {
      status: status,
      id: element.id
    }
    this.inquiryService.editInquiries(data)
    .pipe(takeUntil(this.onDestroy$))
    .subscribe((res: any) => {
      console.log(res);
      // should be called on success only
      if(res.success) {
        this.deleteRowDataTable(element)
      }
    });
  }

  private deleteRowDataTable(element: InquiryUnit) {
    const itemIndex = this.inquirySource.data.findIndex(obj => obj.id === element.id);
    this.inquirySource.data.splice(itemIndex, 1);
    this.inquirySource.paginator = this.paginator;
  }



  handleDataFromFilters(event) {
    // if(this.paginator) this.paginator.firstPage(); 
    if(this.paginator) this.paginator.pageIndex = 0;
    this.searchParams.PageNumber = 1;


    if (event === 0) {
      this.filteredString = "";
      this.getInquiries();
      return;
    }

    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join('');
      }
    });

    this.filteredString = '';
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });

    this.getInquiries();
  }


  displayManufacturer(row) {
    this.manuService.getManufacturerById(row.manufacturerId).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      const dialogRef = this.dialog.open(ManufacturerDetailComponent, {
        data: res.data,
        width: '85%',
        height: '80%'
      });
    });
  }


  displayDistributor(element) {
    this.distributorService.get(element.distributorId).pipe(takeUntil(this.onDestroy$)).subscribe((res:any) => {
      this.dialog.open(DistributorDetailsComponent, {
        data: res.data[0],
        width: '85%',
      height:'80%',
      });
    });
  }

}

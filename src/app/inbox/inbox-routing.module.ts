import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InboxComponent } from './inbox.component';
import { ConversationsResolver } from './conversations.resolver';
import { MessagesPanelComponent } from './messages-panel/messages-panel.component';
const routes: Routes = [
  {
    path: '',
    component: InboxComponent,
    data: {
      title: 'inbox',
      icon: 'icon-layout-cta-right',
      caption: 'my landing',
      status: true
    },
    resolve: {
      conversations: ConversationsResolver
    },
     children: [{
       path: ':id',
       component: MessagesPanelComponent,
     }] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[ConversationsResolver]
})
export class InboxRoutingModule { }

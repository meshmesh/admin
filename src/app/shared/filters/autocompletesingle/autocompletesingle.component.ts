import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AutoCompleteService } from './autocompletesingle.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-autocompletesingle',
  templateUrl: './autocompletesingle.component.html',
  styleUrls: ['./autocompletesingle.component.scss'],
  providers: [AutoCompleteService]
})
export class AutocompletesingleComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  @Input() mainData;
  @Output() dataFromAutoCompSingle = new EventEmitter();
  countryList: any[] = [];
  selectedcountry: any[] = [];
  constructor(private autoCompleteService: AutoCompleteService) { }

  ngOnInit() {
    //this.handleTextChanging('a');
    switch (this.mainData.subTitle) {
      case 'AnualSales':
        this.autoCompleteService.getAnnualSales()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'currentStageM':
        this.autoCompleteService.getManufactureStage()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'satisfaction':
        this.autoCompleteService.getSatisfaction()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'currentStageD':
        this.autoCompleteService.getDistributerStage()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'supervisorD':
        this.autoCompleteService.getDistributerSupervisor()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
        case 'supervisorM':
        this.autoCompleteService.getManufacturerSupervisor()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              for (let i = 0; i < response.length; i++) {
                this.countryList.push({
                  display: response[i].name,
                  value: response[i].value
                });
              }
            },
            (error) => console.log(error)
          );
        break;
    }
  }

  handleTextChanging() {

  }

  handleRemove() {
    this.mainData.value = this.selectedcountry;
    this.dataFromAutoCompSingle.emit(this.mainData);
  }

  handleAdding() {
    this.mainData.value = this.selectedcountry;
    this.dataFromAutoCompSingle.emit(this.mainData);
  }

}

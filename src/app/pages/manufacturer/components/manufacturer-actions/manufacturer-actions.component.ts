import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { ManufacturerService } from '../../services/manufacturer.service';
import { SelectionModel } from '@angular/cdk/collections';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-manufacturer-actions',
  templateUrl: './manufacturer-actions.component.html',
  styleUrls: ['./manufacturer-actions.component.scss']
})
export class ManufacturerActionsComponent implements OnInit, OnDestroy {

  dataSourceCountries: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumnsCountry: Array<string> = ['id', 'name'];

  onDestroy$ = new ReplaySubject<void>();
  pageprodIndex: number = 1;
  pageprodSize: number = 10;
  pagescieIndex: number = 1;
  pagesciSize: number = 10;
  List: any;
  ListProd: any;
  displayedColumns: string[] = ['id', 'Name', 'speciality', 'medline']
  displayedProdColumns: string[] = ['id', 'Title']
  dataSource: MatTableDataSource<any>;
  selection: SelectionModel<any>
  dataSourceProd: MatTableDataSource<any>;
  selectionProd: SelectionModel<any>;
  scilength: number = 0;
  prodlength: number = 0;
  popupType: number;
  title: string;
  comment: string;

  constructor(
    public dialogRef: MatDialogRef<ManufacturerActionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private serv: ManufacturerService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {

    this.popupType = this.data.type;


    switch (this.data.type) { 
      
      case 1:
        this.title = this.data.row.name;
        this.serv.getScientificNameListId(this.pagescieIndex, this.pagesciSize, this.data.row.prospectedCompanyId).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
          this.renderData(res.data);
        });
        this.serv.getProdcut(this.pageprodIndex, this.pageprodSize, this.data.row.companyId).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
          this.renderDataProd(res.data);
        });
      break;

      case 2:
        this.title = `Comments of ${this.data.row.name}`;
        this.comment = this.data.row.comment;
        break;  

      case 3:
        this.title = `View All Targeted Countries`;
        this.renderDataCountries(this.data.row.targetedCountries)
        break; 

    }

  }

  renderDataCountries(data) {
    this.dataSourceCountries = new MatTableDataSource<any>(data);
    this.dataSourceCountries.paginator = this.paginator;
    this.dataSourceCountries.sort = this.sort;
  }


  scipageChange(event) {
    this.pagescieIndex = event.pageIndex + 1;
    this.pagesciSize = event.pageSize;

    this.serv.getScientificNameListId(this.pagescieIndex, this.pagesciSize, this.data.row.prospectedCompanyId).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderData(res.data);
    });
  }

  prodpageChange(event) {
    this.pageprodIndex = event.pageIndex + 1;
    this.pageprodSize = event.pageSize;

    this.serv.getProdcut(this.pageprodIndex, this.pageprodSize, this.data.row.companyId).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      this.renderDataProd(res.data);
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  close(data): void {
    this.dialogRef.close(data);
  }

  renderDataProd(data) {
    this.prodlength = data.count;
    this.dataSourceProd = new MatTableDataSource<any>(data.items);
    this.selectionProd = new SelectionModel<any>(true, []);
  }

  renderData(data) {
    this.scilength = data.count;
    this.dataSource = new MatTableDataSource<any>(data.items);
    this.selection = new SelectionModel<any>(true, []);
  }

}
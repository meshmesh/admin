import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-seo-productpage',
  templateUrl: './seo-product.component.html',
  styleUrls: ['./seo-product.component.scss']
})
export class SeoProductComponent implements OnInit, OnChanges {
  @Input() seoData;
  generalSeoData = [];
  specialSeoData = [];
  outputSeo: any;
  @Output() dataFromSeo = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {
    this.generalSeoData = this.seoData.a;
    this.specialSeoData = this.seoData.b;
    for (let i = 0; i < this.generalSeoData.length; i++) {
      for (let j = 0; j < this.specialSeoData.length; j++) {
        if (this.generalSeoData[i].id === this.specialSeoData[j].id) {
          this.generalSeoData[i].metaDescription = this.specialSeoData[j].metaDescription;
          this.generalSeoData[i].metaKeyWord = this.specialSeoData[j].metaKeyWord;
          this.generalSeoData[i].metaTitle = this.specialSeoData[j].metaTitle;
        }
      }
    }
  }

  handleSeo() {
    this.dataFromSeo.emit(this.generalSeoData);
  }
}

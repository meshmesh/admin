import {
  Component,
  OnInit,
  Inject,
  OnDestroy,
  AfterViewInit,
} from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
import { ReplaySubject, Observable, of } from "rxjs";
import { takeUntil, startWith, debounceTime, switchMap, catchError, map } from "rxjs/operators";
import { BusinessService } from "../business.service";
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastService } from "src/app/_services/toast.service";

@Component({
  selector: "app-create-business-opp",
  templateUrl: "./create-business-opp.component.html",
  styleUrls: ["./create-business-opp.component.scss"],
})
export class CreateBusinessOppComponent implements OnInit, OnDestroy {


  agreementForm: FormGroup;

  manufacturerCtrl: FormControl = new FormControl();
  distributorCtrl: FormControl = new FormControl();


  manufacturers$: Observable<any[]>;
  distributors$: Observable<any[]>;


  Manufacturer: any;
  Distributor: any;

  isLoading: boolean = false;
  onDestroy$ = new ReplaySubject<void>();

  constructor(
    private fb: FormBuilder, 
    private toast: ToastService, 
    public dialogRef: MatDialogRef<CreateBusinessOppComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: BusinessService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnInit() {
    this.createForm();
    this.doOnChanges();
  }


  get dateCtrl() {
    return this.agreementForm.get('date');
  }

  get contractValueCtrl() {
    return this.agreementForm.get('contractValue');
  }

  createForm() {
    this.agreementForm = this.fb.group({
      manufacturerID: new FormControl('', Validators.required),
      distributorID: new FormControl('', Validators.required),
    })
  }

  doOnChanges() {

    this.manufacturers$ = this.manufacturerCtrl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once

      // switchMap(value => this.service.getAllManu(value))
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return this._filterManufacturer(value);
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );

    this.distributors$ = this.distributorCtrl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once

      // switchMap(value => this.service.getAllManu(value))
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return this._filterDistributor(value);
        } else {
          // if no value is present, return null
          return of(null);
        }
      })
    );


  }


  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }
  displayFnDistributor(distributor?: any): string | undefined {
    return distributor ? distributor.name : undefined;
  }


  _filterManufacturer(value: any): Observable<any> {
    if (value) {
      return this.service.getAllManu(typeof value === 'string' ? value.toLowerCase() : value.name).pipe(
        // map the item property of the github results as our return object
        // catch errors
        map(item => item.filter(manu => manu.manufacturerId !== 0)),
        catchError(_ => {
          return of(null);
        })
      );
    } else {
      return of(null);
    }

  }

  // ? Right way ?
  _filterDistributor(value: any): Observable<any[]> {
    if (typeof value === 'string') {
      return this.service.getAllDistributors(value.toLowerCase()).pipe(
        // map the item property of the github results as our return object
        // catch errors
        map(item => item.filter(dist => dist.id !== 0)),
        catchError(_ => {
          return of(null);
        })
      );
    } else {
      return of(null);
    }
  }


  onSubmit() {

    this.agreementForm.controls.manufacturerID.patchValue(this.manufacturerCtrl.value ? this.manufacturerCtrl.value.manufacturerId : null);
    this.agreementForm.controls.distributorID.patchValue(this.distributorCtrl.value ? this.distributorCtrl.value.id : null);


    if(!this.agreementForm.valid) {
      this.toast.error("Please check all required fields");
      return;
    }

    let data = {
      distributorId: this.agreementForm.value.distributorID,
      manufactureId: this.agreementForm.value.manufacturerID,
    };
    this.service
      .sendBusinessOpp(data)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        (res: any) => {
          if (res.success) {
            this.dialogRef.close(true);
            this.toast.success("Created Successfully!");
          } else this.toast.error("something wrong happened");
        },
        (err) => this.toast.error("something wrong happened")
      );
  }

  onCancel() {
    this.dialogRef.close();
  }
  
  close(data) {
    this.dialogRef.close(data);
  }
}

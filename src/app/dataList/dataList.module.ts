import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataListComponent } from './dataList.component';
import { SharedModule } from '../shared/shared.module';
import { MatTableModule, MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatDialogModule } from '@angular/material';
import { DataListRoutingModule } from './dataList-routing.module';
@NgModule({
  imports: [
    CommonModule,
    DataListRoutingModule,
    MatCardModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,  
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule

  ],
  declarations: [DataListComponent]
})
export class DataListModule { }

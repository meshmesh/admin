import { MaterialFormSharedModule } from './../../shared/material-form-shared.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrawlerComponent, LinkDialog } from './crawler.component';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import { CrawlerRoutingModule } from './crawler-routing.module';
import { MatAutocompleteModule, MatSlideToggleModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCheckboxModule, MatDialogModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { TagInputModule } from 'ngx-chips';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialFormSharedModule,
    NgxMatDrpModule,
    CrawlerRoutingModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    // MatSortModule,
    MatCheckboxModule,
    MatDialogModule,
    TagInputModule,
  ],
  declarations: [CrawlerComponent,LinkDialog],
  entryComponents:[LinkDialog]
})
export class CrawlerModule { }

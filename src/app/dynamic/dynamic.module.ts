import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicComponent } from './dynamic.component';
import { DynamicRoutingModule } from './dynamic-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatAutocompleteModule, MatExpansionModule } from '@angular/material';
import { AutoCompleteService } from '../shared/filters/autocomplete-multi/autoComlete.service';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { QuillEditorModule } from 'ngx-quill-editor';
import { DynamictableComponent } from './dynamictable/dynamictable/dynamictable.component';

@NgModule({
  imports: [
    CommonModule,
    DynamicRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    MatExpansionModule,
    QuillEditorModule
  ],
  declarations: [DynamicComponent,DynamictableComponent],
  providers: [AutoCompleteService]
})
export class DynamicModule { }

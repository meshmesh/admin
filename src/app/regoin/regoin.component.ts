import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { DestributorinregoinComponent } from './_modal/destributorinregoin/destributorinregoin.component';
import { RegionserviceService } from './regionservice.service';
import { SelectionModel } from '@angular/cdk/collections';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-regoin',
  templateUrl: './regoin.component.html',
  styleUrls: ['./regoin.component.scss']
})
export class RegoinComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  dataSource: MatTableDataSource<any>;
  selection : SelectionModel<any>
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  columns: any[];
  column: any;
  List:any;
  pageIndex = 1;
  filteredString : any;
  displayedColumns: string[] = ['Id', 'Name', 'Manufacturer', 'Distributor']
  filterOptions = [
    {
      index: 1,
      type: 'multi-select',
      output: 'id',
      apiurl: '/api/AdminRegion/Search',
      order: 2,
      value: {},
      selected: [],
      placeholder: 'Region',
      chipsList: [],
      list: [],
      searchQuery: '',
      querystring: 'RegionId'
    },];
  constructor(public dialog: MatDialog,public service : RegionserviceService) { }

  ngOnInit() {
    this.service.getall(this.pageIndex).pipe(takeUntil(this.onDestroy$)).subscribe((res : any) =>{
      this.dataSource = res;
      this.renderData(res);
      console.log(this.dataSource);
    })
  }

  handleDataFromFilters(event) {
    this.filterOptions.forEach(element => {
      if (element.type === 'multi-select') {
        element.searchQuery = element.selected.join("");
      }

    });
    this.filteredString = "";
    this.filterOptions.forEach(element => {
      this.filteredString += element.searchQuery;
    });
    this.service.searchfilter(this.pageIndex,  this.filteredString).pipe(takeUntil(this.onDestroy$)).subscribe(res => {
    this.renderData(res);
      
    });

  }
  opendialog(row) {

console.log(row);
     const dialogRef = this.dialog.open(DestributorinregoinComponent, {
      width: '1250px',
            data: { data : row.id  }
            
    });

    dialogRef.afterClosed().pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      console.log('The dialog was closed');

    }); 
  }




  renderData(data) {


    this.dataSource = new MatTableDataSource<any>(data);
    console.log(this.dataSource);
    this.dataSource.sort = this.sort;
    this.selection = new SelectionModel<any>(true, []);
    this.dataSource.sortingDataAccessor = (sortData, sortHeaderId) => {
      if (!sortData[sortHeaderId]) {
        return this.sort.direction === 'asc' ? '3' : '1';
      }

      // tslint:disable-next-line:max-line-length
      return /^\d+$/.test(sortData[sortHeaderId]) ? Number('2' + sortData[sortHeaderId]) : '2' + sortData[sortHeaderId].toString().toLocaleLowerCase();

    };
  }



}

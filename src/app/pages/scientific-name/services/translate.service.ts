import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  APIEndpoint = environment.ApiUrl;

constructor(private http: HttpClient) { }


getLangugaesId(MajorCode){
  return this.http.get<any>(this.APIEndpoint + '/api/AdminLookups/get?MajorCode=' + MajorCode);
 }
}

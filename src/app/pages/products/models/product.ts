export class ProductSciNames {
  display: string;
  doYouMean: string;
  iconImg: string;
  id: number;
  medicalLine: string;
  medicalLineId: number;
  name: string;
  nameDisplay: string;
  scientificName: string;
  speciality: string;
  specialityIconImg: string;
  specialityId: number;
  userId: number;
  value: number;
}

export class Product {
  LImage:any;
  id: number;
  manufacturer: any = null;
  speciality: any;
  productImage: any = '';
  productImages: any[] = [];
  catalogImage: any = '';
  catalogName: string;
  catalogChossen: number;
  buyer: any = [];
  sellingPoints: any = [];
  scientificNames: ProductSciNames;
  productTitle = '';
  productSubTitle = '';
  image = '';
  subImage: string[] = [];
  published: boolean;
  madeIn: any;
  description = '';
  aumetComment = '';
  ecommerce: any;
  productRangeId: number;
  // slug: string;
}

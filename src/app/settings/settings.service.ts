import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  APIEndpoint = environment.ApiUrl;
constructor(private http: HttpClient) { }

getAdmin() : Observable<any[]>{
 return this.http.get<any[]>(this.APIEndpoint + '/api/AdminSettings/GetAll');
}

saveEdit(param){
  return this.http.post(this.APIEndpoint + '/api/AdminSettings/AddUpdate', param);
}


getAdminbyID(param) : Observable<any[]>{
  return this.http.get<any[]>(this.APIEndpoint + '/api/AdminSettings/Get/' + param);
 }

}

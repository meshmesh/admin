import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { MatChipInputEvent, MAT_DIALOG_DATA, MatDialogRef, MatAutocompleteSelectedEvent } from '@angular/material';
import * as S3 from 'aws-sdk/clients/s3';
import { Observable, of, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Guid } from 'guid-typescript';

import { AddScientificNameService } from '../services/add-scientific-name.service';
import { ScientificNameModel, AddScientificNameModel, UpdateScientificNameModel } from '../model/add-scientific-name.model';
import { ScientificNameService } from '../services/scientific-name.service';
import { ToastService } from 'src/app/_services/toast.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-scientific-name',
  templateUrl: './add-scientific-name.component.html',
  styleUrls: ['./add-scientific-name.component.scss']
})
export class AddScientificNameComponent implements OnInit, OnDestroy {

  onDestroy$ = new ReplaySubject<void>();
  updateScientificNameform: UpdateScientificNameModel;
  AddScientificNameform: AddScientificNameModel;
  scientificnameform: ScientificNameModel;
  showmore = false;
  smbutton: any = 'Show';
  categoryList: any[] = [];
  FilterdcategoryList: Observable<any>;
  FilterScientificClass: Observable<any>;
  scientificclassList: any[] = [];
  attribute: string[] = [];
  scientificNameList: any[] = [];
  medicalLines: any[];
  medicalLine = 0;
  speciality: number;

  specialitieslist: any[] = [];
  specialitiesFilter: Observable<any>;
  manufacturerFilter: Observable<any>;
  ntSelected = [];
  scientificNames: any[] = [];
  filteredscientificNames: Observable<any>;
  filteredBroaderTerms: Observable<any>;
  medicallineslist: any[] = [];
  medicallinefilter: Observable<any>;
  SelectedNarrowTerms: any[] = [];
  SelectedManufacturer: any[] = [];
  SelectedSpeciality: any[] = [];
  SelectedRelatedTerms: any[] = [];
  SelectedBroaderTerms: any[] = [];
  removable = true;
  NoneDescriptor: any[] = [];
  NoneDescript: any[] = [];
  file: File;
  imagePreview: any;
  selectedCategory: any[] = [];
  ScientificClassList: any[] = [];
  SelectedScientificClass: any[] = [];
  totalCount = 1;
  totalTask = 0;
  attributeList: any = [];
  Manufacturer: any = [];
  selectedattribute: any[] = [];
  Filterattribute: Observable<any>;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  @ViewChild('ManuInput') ManuInput: ElementRef<HTMLInputElement>;
  @ViewChild('broadInput') broadInput: ElementRef<HTMLInputElement>;
  @ViewChild('relatedInput') relatedInput: ElementRef<HTMLInputElement>;


  submitLoader: boolean;
  basic_title: string;

  constructor(
    private scientificNameService: AddScientificNameService,
    private service: ScientificNameService,
    private toast: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddScientificNameComponent>
  ) { }


  ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  ngOnInit() {
    this.basic_title = "Add";
    this.scientificnameform = new ScientificNameModel();

    this.getMedicalLine();
    this.getScientificNameSuggestion();

    if (this.data && this.data.row && this.data.row.id) {
      let recordData = this.data.row;
      this.scientificnameform.Id = this.data.row.id;
      this.basic_title = "Update";
      this.scientificnameform.Name = recordData.name;
      this.SelectedNarrowTerms = recordData.nt;
      this.SelectedBroaderTerms = recordData.bt;
      this.SelectedRelatedTerms = recordData.rt;
      this.scientificnameform.MedicalLineId = recordData.medicalLine;
      this.imagePreview = recordData.imagePath;
      this.NoneDescriptor = recordData.alterNative ? recordData.alterNative : [];
      this.SelectedSpeciality = recordData.specialities;

      this.scientificNameService.getManufacturersuggestions('a').pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
        let manufacturerlist = respone.data;
        this.manufacturerFilter = of(manufacturerlist);
      });

      this.service.getManuInScientificname(recordData.id).pipe(takeUntil(this.onDestroy$)).subscribe((resp: any) => {
        this.SelectedManufacturer = resp.data;
      });

      this.scientificNameService.getSpecalities(recordData.medicalLine.id).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
        this.specialitieslist = respone.data;
        this.specialitiesFilter = of(this.specialitieslist);
      });

    }
  }


  getMedicalLine() {
    this.scientificNameService.getMedicalLines2().pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.medicallineslist = respone.data;
      this.medicallinefilter = of(this.medicallineslist);
    });
  }

  getScientificNameSuggestion() {
    this.scientificNameService
      .getScientificNameSuggestion('&key=')
      .pipe(takeUntil(this.onDestroy$)).subscribe(
        (response: any) => {
          this.scientificNames = response.data;
          this.filteredscientificNames = of(this.scientificNames);
          this.filteredBroaderTerms = of(this.scientificNames);
        },
        error => console.log(error)
      );
  }

  removeTag(i) {
    const temp = this.NoneDescriptor[i];
    this.NoneDescript.push(temp);
    this.NoneDescriptor.splice(i, 1);
  }

  removeattribute(i) {
    const temp = this.selectedattribute[i];
    if (temp.id !== undefined) {
      this.attributeList.push(temp);
    }
    this.selectedattribute.splice(i, 1);
  }

  removeManufacturer(i) {
    const temp = this.SelectedManufacturer[i];
    if (temp.id !== undefined) {
      this.Manufacturer.push(temp);
    }
    this.scientificNameService.removeManufacturer(temp.CompanyId,this.scientificnameform.Id).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
    });
    
    this.SelectedManufacturer.splice(i, 1);
  }


  removeSpeciality(i) {
    const temp = this.SelectedSpeciality[i];
    if (temp.id !== undefined) {
      this.specialitieslist.push(temp);
    }
    this.SelectedSpeciality.splice(i, 1);
  }

  displayFnSpeciality(Speciality?: any): string | undefined {
    return Speciality ? Speciality.name : undefined;
  }

  displayMedicalLine(medLine?: any): string | undefined {
    return medLine ? medLine.name : undefined;
  }

  displayFnManufacturer(manufacturer?: any): string | undefined {
    return manufacturer ? manufacturer.name : undefined;
  }



  _filterSpeciality(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.specialitieslist.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0));
  }

  _filterMedicalLine(value: any): Observable<any[]> {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.Name;
    } else {
      filterValue = value.toLowerCase();
    }
    return of(this.medicallineslist.filter(item => item.Name.toLowerCase().indexOf(filterValue) === 0));
  }

  _filterborderscientificNames(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = '&key=' + value.name;
    } else {
      filterValue = '&key=' + value.toLowerCase();
    }

    this.scientificNameService.getScientificNameSuggestion(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      // this.specialitieslist = respone.data;
      this.filteredBroaderTerms = of(respone.data);
    });
    // return data;
  }

  _filterscientificNames(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = '&key=' + value.name;
    } else {
      filterValue = '&key=' + value.toLowerCase();
    }

    this.scientificNameService.getScientificNameSuggestion(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.filteredscientificNames = of(respone.data);
    });
  }

  _filterManufacturer(value: any): any {
    let filterValue = '';
    if (value.id !== undefined) {
      filterValue = value.name;
    } else {
      filterValue = value.toLowerCase();
    }

    this.scientificNameService.getManufacturersuggestions(filterValue).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.manufacturerFilter = of(respone.data);
    });
  }

  bindspecialities(e: MatAutocompleteSelectedEvent) {
    this.SelectedSpeciality = [];
    this.scientificNameService.getSpecalities(e.option.value.id).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {
      this.specialitieslist = respone.data;
      this.specialitiesFilter = of(this.specialitieslist);
    });
  }

  AddSelectedSpeciality(event: MatAutocompleteSelectedEvent) {
    const index = this.SelectedSpeciality.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      this.SelectedSpeciality.push(event.option.value);
      if(this.ManuInput) this.ManuInput.nativeElement.value = '';
    }
  }

  AddSelectedNarrowTerms(event: MatAutocompleteSelectedEvent) {
    const index = this.SelectedNarrowTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    const index2 = this.SelectedBroaderTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);

    if (this.scientificnameform.Id && this.scientificnameform.Id != event.option.value.id) {
      if (index === -1 && index2 === -1) {
        this.SelectedNarrowTerms.push(event.option.value);
        this.searchInput.nativeElement.value = '';
      }

    } else if (index === -1 && index2 === -1) {
      this.SelectedNarrowTerms.push(event.option.value);
      this.searchInput.nativeElement.value = '';
    }
  }


  AddSelectedManufacturer(event: MatAutocompleteSelectedEvent) {
    const index = this.SelectedManufacturer.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      let data = {
        "companyId": event.option.value.id,
        "scientificNameId": this.scientificnameform.Id
      }
      this.scientificNameService.addManufacturer(data).pipe(takeUntil(this.onDestroy$)).subscribe(respone => {

      });

      this.SelectedManufacturer.push(event.option.value);
      this.ManuInput.nativeElement.value = '';
    }
  }

  AddSelectedRelatedNames(event: MatAutocompleteSelectedEvent) {
    const index = this.SelectedRelatedTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);
    if (index === -1) {
      this.SelectedRelatedTerms.push(event.option.value);
      this.relatedInput.nativeElement.value = '';
    }
  }

  AddSelectedBroaderNames(event: MatAutocompleteSelectedEvent) {
    const index = this.SelectedBroaderTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);

    const index2 = this.SelectedNarrowTerms.map(function (x) {
      return x.id;
    }).indexOf(event.option.value.id);

    if (this.scientificnameform.Id && this.scientificnameform.Id != event.option.value.id) {
      if (index === -1 && index2 === -1) {
        this.SelectedBroaderTerms.push(event.option.value);
        this.broadInput.nativeElement.value = '';
      }
    }
    else if (index === -1 && index2 === -1) {
      this.SelectedBroaderTerms.push(event.option.value);
      this.broadInput.nativeElement.value = '';
    }
  }

  removeNarrowTerms(i) {
    this.SelectedNarrowTerms.splice(i, 1);
  }

  removeRelatedTerms(i) {
    this.SelectedRelatedTerms.splice(i, 1);
  }

  removeBroaderTerms(i) {
    const temp = this.SelectedBroaderTerms[i];
    if (temp.id !== undefined) {
      this.attributeList.push(temp);
    }
    this.SelectedBroaderTerms.splice(i, 1);
    this.SelectedBroaderTerms.splice(i, 1);
  }

  moreoptions() {
    this.showmore = !this.showmore;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.showmore) {
      this.smbutton = 'Hide';
    } else {
      this.smbutton = 'Show';
    }
  }

  addNoneDescriptor(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.NoneDescriptor.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  getFileExtension(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  }

  hideImage() {
    this.imagePreview = '';
    this.file = null;
  }

  iconHandler(event) {
    this.file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
    }
  }

  uploadImage(image, imageLocation) {
    const paramsProduct = {
      Bucket: 'aumet-data',
      Key: 'icons/scientificname/' + Guid.create() + '.' + this.getFileExtension(image.name),
      Body: image
    };
    const _this2 = this;
    _this2.handleUpload(paramsProduct, function (resultc) {
      imageLocation(resultc);
    });
  }

  async handleUpload(params, images) {
    const bucket = new S3({
      accessKeyId: 'AKIAQQRZF2VNB3KYOT4O',
      secretAccessKey: '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',
      region: 'us-west-2'
    });

    await bucket.upload(params, function (err, data) {
      if (err) {
        images(false);
      } else {
        images(data.Location);
      }
    });
  }

  submit(form: NgForm) {
    debugger
    if (form.invalid) {
      return;
    }

    // check if SelectedSpeciality is empty
    if(!this.SelectedSpeciality.length) {
      this.toast.error("Please select at least one speciality.");
      return;
    }
    

    if (this.scientificnameform.Id > 0) {
      this.updateScientificNameform = new UpdateScientificNameModel();
      this.updateScientificNameform.Id = this.scientificnameform.Id;
      this.updateScientificNameform.Name = this.scientificnameform.Name;
      this.updateScientificNameform.IsApproved = true;
      this.updateScientificNameform.MedicalLineId = this.scientificnameform.MedicalLineId.id;

      this.SelectedNarrowTerms.forEach(element => {
        this.updateScientificNameform.NTIds.push(element.id);
      });
      this.SelectedBroaderTerms.forEach(element => {
        this.updateScientificNameform.BTIds.push(element.id);
      });
      this.SelectedRelatedTerms.forEach(element => {
        this.updateScientificNameform.RTIds.push(element.id);
      });
      this.updateScientificNameform.CompanyIds = this.SelectedManufacturer.map(manu => manu.id);

      this.NoneDescriptor.forEach(element => {
        if (element.value === 0) {
          this.updateScientificNameform.AlterNative.push(element.id);
        } else {
          this.updateScientificNameform.AlterNative.push(element);
        }
      });

      this.SelectedSpeciality.forEach(element => {
        this.updateScientificNameform.SpecialityIds.push(element.id);
      });

      if (!this.file && !this.imagePreview) {
        this.updateScientificNameform.ImagePath = 'https://aumet.me/assets/images/scientific.png';
        this.imagePreview = 'https://aumet.me/assets/images/scientific.png';
        
      }

      if (this.file) {
        const _thisfile = this;
        this.uploadImage(this.file, function (result) {
          _thisfile.updateScientificNameform.ImagePath = result;
          _thisfile.totalTask++;
          if (_thisfile.totalTask === _thisfile.totalCount) {
            _thisfile.addUpdateFunction(_thisfile.updateScientificNameform);
          }
        });
      } else if (this.imagePreview) {
        this.updateScientificNameform.ImagePath = this.imagePreview;
        this.addUpdateFunction(this.updateScientificNameform);
      }
    } else {
      this.AddScientificNameform = new AddScientificNameModel();
      this.AddScientificNameform.name = this.scientificnameform.Name;
      this.AddScientificNameform.IsApproved = true;
      this.AddScientificNameform.medicalLineId = this.scientificnameform.MedicalLineId.id;

      this.SelectedNarrowTerms.forEach(element => {
        this.AddScientificNameform.ntIds.push(element.id);
      });
      this.SelectedBroaderTerms.forEach(element => {
        this.AddScientificNameform.btIds.push(element.id);
      });
      this.SelectedRelatedTerms.forEach(element => {
        this.AddScientificNameform.rtIds.push(element.id);
      });
      this.AddScientificNameform.CompanyIds = this.SelectedManufacturer.map(manu => manu.id);

      this.NoneDescriptor.forEach(element => {
        if (element.value === 0) {
          this.AddScientificNameform.alternatives.push(element.id);
        } else {
          this.AddScientificNameform.alternatives.push(element);
        }
      });

      this.SelectedSpeciality.forEach(element => {
        this.AddScientificNameform.specialityIds.push(element.id);
      });

      if (!this.file && !this.imagePreview) {
        this.AddScientificNameform.imagePath = 'https://aumet.me/assets/images/scientific.png';
        this.imagePreview = 'https://aumet.me/assets/images/scientific.png';
      }

      if (this.file) {
        const _thisfile = this;
        this.uploadImage(this.file, function (result) {
          _thisfile.AddScientificNameform.imagePath = result;
          _thisfile.totalTask++;
          if (_thisfile.totalTask === _thisfile.totalCount) {
            _thisfile.addUpdateFunction(_thisfile.AddScientificNameform);
          }
        });
      } else if (this.imagePreview) {
        this.AddScientificNameform.imagePath = this.imagePreview;
        this.addUpdateFunction(this.AddScientificNameform);
      }
    }
  }

  addUpdateFunction(form) {
    this.submitLoader = true;
    this.scientificNameService.addUpdate(form).pipe(takeUntil(this.onDestroy$)).subscribe((res: any) => {
      if (res.success) {
        this.submitLoader = false;
        this.dialogRef.close(true);
        this.toast.success("Created Successfully!");
      } else {
        this.submitLoader = false;
        this.toast.error("Something wrong happened! Please try again");
      }
    }, err => {
      this.submitLoader = false;
      this.toast.error("Something wrong happened! Please try again");
    });
  }


  onCancel(): void {
    this.dialogRef.close(null);
  }

  close(data) {
    this.dialogRef.close(data);
  }

}

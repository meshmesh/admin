import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { SortingService } from './seortingService';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.scss'],
  providers: [SortingService]
})
export class SortingComponent implements OnInit , OnDestroy {
  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  onDestroy$ = new ReplaySubject<void>();
  @Input() mainData;
  @Output() dataFromSort = new EventEmitter;
  sortValues = [];
  sortType = '2';
  toggel = true;
  constructor(private sortingService: SortingService) { }

  ngOnInit() {
    this.mainData.value[0] = this.sortType;
    this.mainData.value[1] = 1;

    // console.log('sort>>>', this.mainData);
    switch (this.mainData.comp) {
      case 'Article':
        this.sortingService.getArticleLookup()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'Scientific':
        this.sortingService.getScientificNameLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;
      case 'Product':
        this.sortingService.getProductLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;

      case 'Catalog':
        this.sortingService.getProductLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;

      case 'Manufactorer':
        this.sortingService.getProductLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;

      case 'Manufactorer':
        this.sortingService.getProductLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;

      case 'Distributer':
        this.sortingService.getProductLookups()
        .pipe(takeUntil(this.onDestroy$)).subscribe(
            (response: any) => {
              console.log('lookup 16>>>', response);
              for (let i = 0; i < response.length; i++) {
                this.sortValues.push({
                  value: response[i].value,
                  name: response[i].name
                });
              }
            },
            (error) => console.log(error)
          );
        break;



    }

  }

  handleToggel() {
    if (this.sortType === '1') {
      this.sortType = '2';

    } else {
      this.sortType = '1';
    }
    this.mainData.value[0] = this.sortType;
    this.dataFromSort.emit(this.mainData);
  }


  selectHandler(e) {
    console.log(e.target.value);
    this.mainData.value[1] = e.target.value;
    this.dataFromSort.emit(this.mainData);
  }


}
